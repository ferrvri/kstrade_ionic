import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InitPage } from '../pages/init/init';
import { Network } from '../../node_modules/@ionic-native/network';
import { LocalNotifications } from '../../node_modules/@ionic-native/local-notifications';
import { ConnectionCheckerProvider } from '../providers/connection-checker/connection-checker';
import { HTTP } from '@ionic-native/http';

@Component({
  templateUrl: 'app.html'
})
export class MyApp{
  rootPage:any = InitPage;
  loginInterval: any = null;

  myIds : any = [];
  myMessages: any = [];

  constructor(
    public _localNotifications: LocalNotifications,
    public _alert: AlertController,
    public platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    _http: HTTP
  ) {
    splashScreen.hide();
    _http.setDataSerializer('json');
    platform.ready().then(() => {
      statusBar.styleDefault();  
    });
  }
}


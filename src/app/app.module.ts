import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { Camera } from '@ionic-native/camera';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyApp } from './app.component';
import { LoadingPage } from '../pages/loading/loading';
import { InitPage } from '../pages/init/init';
import { FormsModule } from '@angular/forms';
import { NewJobModalPage } from '../pages/new-job-modal/new-job-modal';
import { ViewPhotoPage } from '../pages/view-photo/view-photo';
import { Vibration } from '@ionic-native/vibration';
import { UpdatePage } from '../pages/update/update';
import { BrowserTab } from '@ionic-native/browser-tab';
import { Geolocation } from '@ionic-native/geolocation';
import { PesquisaModalPage } from '../pages/pesquisa-modal/pesquisa-modal';
import { UtilsProvider } from '../providers/utils/utils';
import { Network } from '../../node_modules/@ionic-native/network';
import { DatePicker } from '@ionic-native/date-picker';
import { Device } from '@ionic-native/device';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { QuizModalPage } from '../pages/quiz-modal/quiz-modal';
import { MarketShareChartPage } from '../pages/market-share-chart/market-share-chart';
import { MainPage } from '../pages/main/main';
import { MainPageModule } from '../pages/main/main.module';
import { RotaProvider } from '../providers/rota/rota';
import { HttpClientModule } from '@angular/common/http';
import { TelefoneMask } from 'telefone-mask-ng2';
import { ViewNewCompletePage } from '../pages/view-new-complete/view-new-complete';

import { IonicImageViewerModule } from 'ionic-img-viewer';
import { Keyboard } from '@ionic-native/keyboard'
import { HTTP } from '@ionic-native/http';

@NgModule({
  declarations: [
    MyApp,
    InitPage,
    LoadingPage,
    NewJobModalPage,
    // LoginPage,
    ViewPhotoPage,
    UpdatePage,
    PesquisaModalPage,
    QuizModalPage,
    MarketShareChartPage,
    TelefoneMask,
    ViewNewCompletePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {preloadModules: true}),
    HttpModule,
    FormsModule,
    // ChartsModule,
    BrowserAnimationsModule,
    MainPageModule,
    HttpClientModule,
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    InitPage,
    LoadingPage,
    NewJobModalPage,
    // LoginPage,
    ViewPhotoPage,
    UpdatePage,
    PesquisaModalPage,
    QuizModalPage,
    MarketShareChartPage,
    MainPage,
    ViewNewCompletePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    Vibration,
    BrowserTab,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UtilsProvider,
    Network,
    DatePicker,
    Device,
    LocalNotifications,
    RotaProvider,
    Keyboard,
    HTTP
  ]
})
export class AppModule {}

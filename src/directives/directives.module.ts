import { NgModule } from '@angular/core';
import { OnLongPressDirective } from './on-long-press/on-long-press';
@NgModule({
	declarations: [OnLongPressDirective],
	imports: [],
	exports: [OnLongPressDirective]
})
export class DirectivesModule {}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { Geolocation } from '@ionic-native/geolocation';

/*
  Generated class for the RotaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RotaProvider {

  private _enabled: boolean = false;

  public get enabled(): boolean {
    return this._enabled;
  }

  public set enabled(value: boolean) {
    this._enabled = value;
  }

  localStorageData;
  interval = undefined;

  constructor(public http: HttpClient, private _geolocation: Geolocation) {
    if (localStorage.getItem('session')){
      this.localStorageData = JSON.parse(localStorage.getItem('session'));
    }
  }

  enableRota(object){
    this.start(object);
  }

  disableRota(object){
    if (this.enabled == true && this.interval !== undefined){
      clearInterval(this.interval);
      this.interval = undefined;
      this.enabled = false;
    }
  }

  start(object){
    if (this.enabled == false && this.interval === undefined){
      this.enabled = true;
      console.log('started');
      this.enabled = true;
      this.http.post(
        ' http://eccotrade.com.br/sistema/assets/eccotrade_php/rota/insertRota.php',
        {
          type: 'first',
          promotor_id: object.pro_id,
          rede_id: object.rede_id,
          filial_id: object.rfil_id,
          aux_data_inicio: object.data,
          aux_hora_inicio: object.hora
        }
      ).subscribe( (response: any) => {
        if (response.status == '0x104'){
          console.log('starting on', response);
          this.interval = setInterval( () => {
            console.log('ok interval rota');
            this._geolocation.getCurrentPosition().then( (data) => {
              this.http.post( 
                ' http://eccotrade.com.br/sistema/assets/eccotrade_php/rota/insertRota.php', 
                {
                  type: 'second',
                  long: data.coords.longitude,
                  lat: data.coords.latitude,
                  rota_id: response.id
                } 
              ).subscribe( (response2: any) => {
                if (response2.status == '0xEC'){
                  clearInterval(this.interval);
                  this.interval = undefined
                  this.enabled = false;
                }else if (response2.status == '0x102'){
                  Swal({
                    title: 'Problema com rota',
                    text: 'Erro ao salvar rota.',
                    type: 'error'
                  });
                }
              });
            });
          // }, 5000);
          }, 1000 * 60 * 1);
        // }else{
        //   Swal({
        //     title: 'Problema com rota',
        //     text: 'Erro ao salvar rota.',
        //     type: 'error'
        //   });
        }
      });
    }else{
      Swal({
        title: 'Rota não iniciada',
        text: 'A rota não pôde ser iniciada..',
        type: 'warning',
        showCloseButton: true
      });
    }
  }

}

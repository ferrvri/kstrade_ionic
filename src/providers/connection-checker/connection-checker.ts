import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';

/*
  Generated class for the ConnectionCheckerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConnectionCheckerProvider {

  protected connection: Connection =  new Connection();

  public mStatus: string = '';

  constructor(public network: Network) {
    network.onConnect().subscribe( () => {
      this.connection.status = 'connected';
      if (this.network.type === 'wifi') {
        this.connection.type = 'wifi';
      }

      this.setMStatus('online');
    });

    network.onDisconnect().subscribe( () => {
      this.connection.status = 'disconnected';
      this.setMStatus('offline');
    });
  }

  public setMStatus(s: string){
    this.mStatus = s;
  }

  public getMStatus(){
    return this.mStatus;
  }
}

export class Connection {

  private _status: string = 'disconnected';

  public get status(): string {
    return this._status;
  }
  public set status(value: string) {
    this._status = value;
  }

  private _type: string = '3G';

  public get type(): string {
    return this._type;
  }
  public set type(value: string) {
    this._type = value;
  }

}

export class RequestResultPattern{

  private _status: string = '';

  public get status(): string {
    return this._status;
  }
  public set status(value: string) {
    this._status = value;
  }

  private _result: any = [];

  public get result(): any {
    return this._result;
  }
  public set result(value: any) {
    this._result = value;
  }

}

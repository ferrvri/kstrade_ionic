import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MarketShareChartPage } from './market-share-chart';

@NgModule({
  declarations: [
    MarketShareChartPage,
  ],
  imports: [
    IonicPageModule.forChild(MarketShareChartPage),
  ],
})
export class MarketShareChartPageModule {}

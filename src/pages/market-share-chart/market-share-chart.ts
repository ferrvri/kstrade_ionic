import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { newPageOptions } from '../../interfaces/newpageoptions';
import { Http } from '@angular/http';

import {Chart} from 'chart.js';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the MarketShareChartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-market-share-chart',
  templateUrl: 'market-share-chart.html',
})
export class MarketShareChartPage implements OnInit, newPageOptions {

  @ViewChild("chart") chartC: ElementRef;

  private chart: Chart;
  
  data = [];

  resultOf: any;

  doneChart = false;

  chartOpts = {
    type: "doughnut",
    data: {
      labels: ['', 'Outros produtos'],
      datasets: [
        {
          data: [],
          backgroundColor: [
            "#8c1515",
            "rgba(27, 55, 44, 0.25)"
          ],
          borderWidth: 1
        }
      ]
    },
    options: {
      circumference: 4 * Math.PI
    }
  }

  grafico_select: any = 0;

  resultData = []

  empty = true

  constructor(
    public platform: Platform, 
    private _alert: AlertController, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad MarketShareChartPage');
  }

  ngOnInit() {
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectMarketShare.php',
      {
        red_id: this.navParams.get('red_id'),
        rfil_id: this.navParams.get('rfil_id'),
        cliente: this.navParams.get('cli_id'),
        pro_id: this.navParams.get('pro_id'),
        pro_nome: this.navParams.get('pro_nome')
      },
      {}
    ).then((response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104' && response.data.result.length > 0) {
        this.empty = false;
        this.chartOpts.data.labels[0] = this.navParams.get('pro_nome');
        this.chartOpts.data.datasets[0].data[0] = parseInt(response.data.result[0].soma)
        this.data[0] = response.data.result[0].porcentagem;

        this.chart = new Chart(this.chartC.nativeElement, this.chartOpts);
      }
    });
  }

  goBack() {
    this.navCtrl.pop();
  }

  selectGrafico() {
    let a = this._alert.create();
    let ar = [
      {
        Nome: 'Em pizza',
        value: 'pie'
      },
      {
        Nome: 'Em rede',
        value: 'polarArea'
      },
      {
        Nome: 'Em circulo',
        value: 'doughnut'
      }
    ];

    ar.forEach((el, index) => {
      a.addInput({ type: 'radio', label: el.Nome, value: el.value + ':' + index, id: 'graf-' + index });
    });
    a.addButton('Cancel');
    a.addButton({
      text: 'OK',
      handler: (data) => {
        this.grafico_select = ar[data.split(':')[1]];
        // this.chartType = data.split(':')[0];

      }
    });
    a.present();
  }
}

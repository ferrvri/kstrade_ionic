import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, AlertController, ModalController, Platform, PopoverController } from 'ionic-angular';
import { LoadingPage } from '../loading/loading';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { ConfirmPage } from '../confirm/confirm';
import { EsqueceuSenhaPage } from '../esqueceu-senha/esqueceu-senha';
import swal from 'sweetalert2';
import { EmailValidator } from '@angular/forms';
import { LoginOptsPage } from './opts/login-opts/login-opts';
import { HTTP } from '@ionic-native/http';

@IonicPage({ name: 'login' })
@Component({
  selector: 'page-login',
  templateUrl: './login.html'
})
export class LoginPage {

  part = '';
  email: string = '';
  senha: string = '';
  nome: string = '';
  telefone: string = '';

  constructor(
    public popoverCtrl: PopoverController,
    public platform: Platform,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private _http: HTTP,
    private _alert: AlertController) {
  }

  goBack() {
    this.navCtrl.pop();
  }

  presentPopover() {
    const popover = this.popoverCtrl.create(LoginOptsPage);
    popover.present();
  }

  cadastrar() {
    if (this.email.length < 1 || this.email === undefined || this.senha.length < 1 || this.senha === undefined) {
      let a = this._alert.create({
        title: 'Preencha o campo!',
        subTitle: 'Preencha o campo e-mail para acessar o sistema!',
        buttons: ['Ok']
      });
      a.present();
    } else {
      if (localStorage.getItem('isMD') && localStorage.getItem('isMD') == '1') {
        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/app/updateNetStatus.php',
          {
            status: 'connected',
            email: JSON.parse(localStorage.getItem('creds')).email,
            senha: JSON.parse(localStorage.getItem('creds')).senha
          },
          {}
        ).then((response) => {
          response.data = JSON.parse(response.data)
          if (response.data.status == '0x104') {
            localStorage.removeItem('isMD')
            this.navCtrl.push(LoadingPage, { page: 'main' }, { animate: false });
          }
        });
      }
      this._http.setDataSerializer('json');
      this._http.post(
        // 'http://kstrade.com.br/kstrade_php/assets/sistema/assets/eccotrade_php/app/login.php',
        // 'https://kstrade.com.br/sistema/assets/kstrade_php/app/login.php',
        'https://kstrade.com.br/sistema/assets/kstrade_php/app/login.php',
        {
          email: this.email,
          senha: this.senha
        },
        {
          "Content-Type": "application/json"
        }
      ).then((response) => {
        response.data = JSON.parse(response.data)
        if (response.data.length < 1) {
          let a = this._alert.create({
            title: 'E-mail Inválido',
            subTitle: 'O E-mail inserido nao está cadastrado no sistema',
            buttons: ['Ok']
          });
          a.present();
        } else if (response.data[0] !== undefined) {
          if (response.data[0].content[0].PRO_Status == 'Inativo') {
            let a = this._alert.create({
              title: 'Login inativo',
              subTitle: 'Seu login foi inativado, por favor consulte-nos!',
              buttons: ['Ok']
            });
            a.present();
          } else {
            localStorage.setItem('session', JSON.stringify(response.data));
            this.navCtrl.push(LoadingPage, { page: 'main' }, { animate: false });

            localStorage.setItem('creds', JSON.stringify({ email: this.email, senha: this.senha }));
          }
        } else if (response.data.status == '0x104' && response.data.result.user.Usr_Nivel == 'Vendedor') {
          localStorage.setItem('session', JSON.stringify(response.data.result));
          this.navCtrl.push(LoadingPage, { page: 'main' }, { animate: false });
        } else if (response.data.status == '0x104' && response.data.result.user.Usr_Nivel == 'Representante') {
          localStorage.setItem('session', JSON.stringify(response.data.result));
          this.navCtrl.push(LoadingPage, { page: 'main' }, { animate: false });
        } else if (response.data.status == '0x104' && response.data.result.user.Usr_Nivel == 'Externo') {
          localStorage.setItem('session', JSON.stringify(response.data.result));
          this.navCtrl.push(LoadingPage, { page: 'main' }, { animate: false });
        } else if (response.data.status == '0x101') {
          let a = this._alert.create({
            title: 'E-mail ou senha incorretos',
            subTitle: 'E-mail ou senha inseridos estão incorretos',
            buttons: ['Ok']
          });
          a.present();
        }
      });
    }
  }

  forgot() {
    let m = this.modalCtrl.create(EsqueceuSenhaPage);
    m.present()
  }

  startWithGuest(nome, email: string, telefone) {
    if (<HTMLInputElement>document.getElementById('telefone') != null) {
      telefone = (<HTMLInputElement>document.getElementById('telefone')).value;
    } else {
      telefone = '';
    }

    if (this.platform.is('android') && nome.length < 1 && email.length < 1) {
      swal({
        title: 'Campos vazios',
        text: 'Preencha todos os campos para continuar!',
        type: 'warning'
      });
    } else if (
      this.platform.is('android') &&
      !email.split('@') && ((email.split('@')[0] === undefined && email.split('@')[1] === undefined))
    ) {
      swal({
        title: 'Preencher corretamente',
        text: 'Preencha um e-mail e/ou telefone válido!',
        type: 'warning'
      });
    } else if (
      this.platform.is('android') &&
      email.split('@') && ((email.split('@')[0].length < 3 || email.split('@')[1].length < 3))
      || (email.split('@')[1].indexOf('.com') == -1)
    ) {
      swal({
        title: 'Preencher corretamente',
        text: 'Preencha um e-mail e/ou telefone válido!',
        type: 'warning'
      });
    } else {
      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/insertContato.php',
        {
          nome: nome,
          telefoneFixo: telefone || '',
          telefoneMovel: '',
          email: email,
          obs: 'CONTATO CADASTRADO PELO APLICATIVO KSTRADE' + (
            this.platform.is('ios') ? ' DE UM DISPOSITIVO iOS' : ' DE UM DISPOSITIVO ANDROID'
          )
        },
        {}
      ).then((response) => {
        response.data = JSON.parse(response.data)
        if (response.data.status == '0x104') {
          localStorage.setItem('session', JSON.stringify({
            visitante: {
              Usr_Nome: nome,
              Usr_Email: email,
              Usr_Telefone: telefone,
              Usr_Nivel: 'Visitante'
            }
          }));
          this.navCtrl.push(LoadingPage, { page: 'main' }, { animate: false });
        } else {
          swal({
            title: 'Erro ao salvar',
            type: 'error'
          });
        }
      });
    }
  }
}

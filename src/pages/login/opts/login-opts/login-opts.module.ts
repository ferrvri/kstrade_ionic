import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginOptsPage } from './login-opts';

@NgModule({
  declarations: [
    LoginOptsPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginOptsPage),
  ],
})
export class LoginOptsPageModule {}

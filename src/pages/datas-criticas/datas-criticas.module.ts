import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DatasCriticasPage } from './datas-criticas';

@NgModule({
  declarations: [
    DatasCriticasPage,
  ],
  imports: [
    IonicPageModule.forChild(DatasCriticasPage),
  ],
})
export class DatasCriticasPageModule {}

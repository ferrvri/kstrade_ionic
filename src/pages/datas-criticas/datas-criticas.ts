import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import { CriticModalPage } from '../datas-criticas-modal/critic-modal';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the DatasCriticasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
*/

@IonicPage({name: 'criticDate'})
@Component({
  selector: 'page-datas-criticas',
  templateUrl: 'datas-criticas.html',
})
export class DatasCriticasPage implements OnInit{

  lojas = [];
  doneLoading: boolean = false;

  constructor(
    public platform: Platform, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP, 
    private _modal: ModalController ) {
  }

  ngOnInit(){
    if (localStorage.getItem('session')){
      JSON.parse(localStorage.getItem('session')).forEach(element => {
        if (element.content.length > 0){
          element.content.forEach(e => {
            this._http.post(
              'https://kstrade.com.br/sistema/assets/kstrade_php/app/promotorOfRedeFilial.php',
              {
                rede: e.RED_ID,
                filial: e.RFIL_ID,
                nome: JSON.parse(localStorage.getItem('session'))[0].content[0].PRO_Nome
              },
              {}
            ).then((response) => {
              response.data = JSON.parse(response.data)
              if (response.data.status == '0x104'){
                this._http.post(
                  'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectRotina.php',
                  {
                    rede: e.RED_ID,
                    filial: e.RFIL_ID,
                    promotor: response.data.result[0].Aux_Promotor_ID
                  },
                  {}
                ).then( (response2) => {
                  response2.data = JSON.parse(response2.data)
                  if (response2.data.status == '0x104'){
                    let  o  = {
                      RFIL_ID: e.RFIL_ID,
                      RFIL_Nome: e.RFIL_Nome,
                      RED_ID: e.RED_ID,
                      RED_Nome: e.RED_Nome,​
                      RFIL_Bairro: e.RFIL_Bairro,            ​
                      RFIL_CEP: e.RFIL_CEP,            ​
                      RFIL_Cidade: e.RFIL_Cidade,            ​​
                      RFIL_Endereco: e.RFIL_Endereco,            ​
                      RFIL_Estado: e.RFIL_Estado,            ​​
                      RFIL_Numero: e.RFIL_Numero,            ​
                      RFIL_Telefone: e.RFIL_Telefone,
                      // RFIL_Numero_Visitas: parseInt(e.RFIL_Numero_Visitas),            ​
                      RFIL_Numero_Visitas: 0,
                      cliente: [],
                      disabled: true
                    }

                    response2.data.result.forEach(element => {
                      let days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                      let dayName = days[new Date().getDay() ];
                      let elem = element.aux_semana.split(';');
                      elem.forEach(element2 => {
                        if (element2 == dayName){
                          o.cliente.push(element);
                          o.disabled = false;
                        }
                      });
                    });
                    this.lojas.push(o);
                  }
                });
                setTimeout( () => {
                  this.doneLoading = true;
                }, 1500);
              }
            });
          });
        }      
      });
    }
  }

  goBack(){
    this.navCtrl.pop();
  }

  openCritic(c){
    let m = this._modal.create(CriticModalPage, {
      cliente: c.cliente,
      rede_nome: c.RED_Nome,
      rede_id: c.RED_ID,
      rfil_nome: c.RFIL_Nome,
      rfil_id: c.RFIL_ID
    });
    m.present();
  }

}

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { ImageViewerController } from 'ionic-img-viewer';
import { Http } from '@angular/http';
import swal from 'sweetalert2';
import { Platform } from 'ionic-angular/platform/platform';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the ViewPhotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-photo',
  templateUrl: 'view-photo.html',
})
export class ViewPhotoPage implements OnInit {

  pageParams: any;
  errorOccured: boolean = false;

  fotos = [];
  comentarios = [];

  constructor(
    public platform: Platform,  
    private _alert: AlertController, 
    private _http: HTTP, 
    private _image: ImageViewerController, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController) {
  }

  ngOnInit(){
    this.pageParams = this.viewCtrl.getNavParams().data;
  
    this.fotos = this.viewCtrl.getNavParams().get('fotos');

    this.getComentarios()
  }

  getComentarios(){
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectComentarios.php',
      {
        new_id: this.pageParams.id
      },
      {}
    ).then( (response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104'){
        this.comentarios = response.data.result;
      }
    })
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

  callError(e, imgElem: HTMLImageElement){
    this.errorOccured = true;
    imgElem.style.display = 'none';
  }

  presentImage(image){
    this._image.create(image).present();
  }

  abrirOptsComentarios(){
    let alert = this._alert.create();
    alert.setTitle('Escolha um comentario:');

    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/selectComentariosDefinidos.php',
      {
        all: true
      },
      {}
    ).then( (response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104'){
    
        response.data.result.forEach( (e) => {
          alert.addInput({
            type: 'radio',
            label: e.comdef_title,
            value:  e.comdef_title
          });
        })
    
        alert.addButton('Fechar');
        alert.addButton({
          text: 'Comentar!',
          handler: data => {
            let o = {
              new_id: this.pageParams.id,
              comentario_ref_nome: '',
              comentario_desc: data
            }
    
            if (JSON.parse(localStorage.getItem('session')).visitante != null){
              o.comentario_ref_nome = JSON.parse(localStorage.getItem('session')).visitante.Usr_Nome
            }else if (JSON.parse(localStorage.getItem('session'))[0] !== undefined &&
                      JSON.parse(localStorage.getItem('session'))[0].PRO_ID !== undefined){
              o.comentario_ref_nome = JSON.parse(localStorage.getItem('session'))[0].content[0].PRO_Nome;
            }else if (JSON.parse(localStorage.getItem('session')).user != null){
              o.comentario_ref_nome = JSON.parse(localStorage.getItem('session')).user.Usr_Nome;
            }
    
            this._http.post(
              'https://kstrade.com.br/sistema/assets/kstrade_php/app/insertComentario.php',
              o,
              {}
            ).then( (response) => {
              response.data = JSON.parse(response.data)
              if (response.data.status == '0x104'){
                this.getComentarios();
              }
            })
          }
        });
        alert.present();
      }
    });
  }

}

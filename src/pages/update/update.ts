import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import { BrowserTab } from '@ionic-native/browser-tab';

/**
 * Generated class for the UpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update',
  templateUrl: 'update.html',
})
export class UpdatePage implements OnInit{

  resumeToUpdate: boolean = false;

  constructor(public platform: Platform, public _platform: Platform, public navCtrl: NavController, public navParams: NavParams) {
    this._platform.registerBackButtonAction(() => {
      //console.log('u cant return');
      return;
    },1);
  }

  ngOnInit(){

    document.addEventListener('backbutton', () => {
      //console.log('u cant return');
      return;
    }, false);
  }

  goBack(){
    this.navCtrl.pop();
  }

}

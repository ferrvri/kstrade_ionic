import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, LoadingController, LoadingCmp, ModalController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Http } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { PesquisaModalPage } from '../pesquisa-modal/pesquisa-modal';
import { Device } from '@ionic-native/device';
import { Vibration } from '@ionic-native/vibration';
import { RotaProvider } from '../../providers/rota/rota';
import { Platform } from 'ionic-angular/platform/platform';
import { HTTP } from '@ionic-native/http';
// import { BackgroundMode } from '@ionic-native/background-mode';

/**
 * Generated class for the NewJobModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-job-modal',
  templateUrl: 'new-job-modal.html',
})
export class NewJobModalPage implements OnInit {

  pageParams: any;

  camData: any = [
    'iVBORw0KGgoAAAANSUhEUgAAAMoAAACbCAIAAACs1ZOtAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABySURBVHhe7cEBDQAAAMKg909tDjcgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgB81b5QAAUGdPzoAAAAASUVORK5CYII=',
    'iVBORw0KGgoAAAANSUhEUgAAAMoAAACbCAIAAACs1ZOtAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABySURBVHhe7cEBDQAAAMKg909tDjcgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgB81b5QAAUGdPzoAAAAASUVORK5CYII=',
    // 'iVBORw0KGgoAAAANSUhEUgAAAgsAAAEACAIAAACyEqLHAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAGcSURBVHhe7cEBDQAAAMKg909tDwcEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMChGiJaAAEcsWQ3AAAAAElFTkSuQmCC',
    // 'iVBORw0KGgoAAAANSUhEUgAAAbIAAAEaCAIAAAD7XkXIAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAF7SURBVHhe7cExAQAAAMKg9U9tB28gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIBDDZuhAAHF4jNmAAAAAElFTkSuQmCC',
    // 'iVBORw0KGgoAAAANSUhEUgAAAWUAAAFmCAIAAAA6cxxCAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAGLSURBVHhe7cExAQAAAMKg9U9tDB8gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgrAbbawAB6IPe1AAAAABJRU5ErkJggg=='
  ];

  obs: string = '';

  cliente: string = 'Selecione o cliente';
  clienteIndex: number = 0;
  clientes = [];
  clienteOK: boolean = false;

  nameOfFile: string = '';
  promotorFromRedeFilial: number = 0;

  myLat = null;
  myLong = null;

  constructor(
    public platform: Platform,
    private _rota: RotaProvider,
    private _vibration: Vibration,
    private _device: Device,
    public modalCtrl: ModalController,
    private _geolocation: Geolocation,
    public loadingCtrl: LoadingController,
    private _http: HTTP,
    public _alert: AlertController,
    public camera: Camera,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) { }

  ngOnInit() {
    this.pageParams = this.viewCtrl.getNavParams().data;

    this._geolocation.getCurrentPosition().then((data) => {
      this.myLat = data.coords.latitude;
      this.myLong = data.coords.longitude;
    }).catch((e) => {
      let a = this._alert.create({
        title: 'Ative a localização',
        subTitle: 'Você deve ativar a localização! errorCode:' + JSON.stringify(e),
        buttons: ['Ok']
      });
      a.present();
      this.viewCtrl.dismiss({});
    })

    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/app/promotorOfRedeFilial.php',
      {
        rede: this.pageParams.RED_ID,
        filial: this.pageParams.RFIL_ID,
        nome: JSON.parse(localStorage.getItem('session'))[0].content[0].PRO_Nome
      },
      {}
    ).then((response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104') {
        this.promotorFromRedeFilial = response.data.result[0].Aux_Promotor_ID;
      }

      this.pageParams.cliente.forEach(element => {
        this.clientes.push(element)
      });
    }, () => {
      this.promotorFromRedeFilial = this.pageParams.PRO_ID

      this.pageParams.cliente.forEach(element => {
        this.clientes.push(element)
      });
    });
  }

  getInitAndFimSemana() {
    let init = '';
    let fim = '';
    switch (new Date().getDay()) {
      case 0: {
        init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate()).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 6).toISOString().substring(0, 10);
        break;
      }
      case 1: {
        init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 1).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 5).toISOString().substring(0, 10);
        break;
      }
      case 2: {
        init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 2).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 4).toISOString().substring(0, 10);
        break;
      }
      case 3: {
        init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 3).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 3).toISOString().substring(0, 10);
        break;
      }
      case 4: {
        init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 4).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 2).toISOString().substring(0, 10);
        break;
      }
      case 5: {
        init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 5).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 1).toISOString().substring(0, 10);
        break;
      }
      case 6: {
        init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 6).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate()).toISOString().substring(0, 10);
        break;
      }
    }
    return {
      init: init,
      end: fim
    }
  }

  get_orientation(src) {
    let img = new Image();
    img.src = src;
    var width = img.width;
    var height = img.height;
    var height_plus = height + (height / 2);

    if ((width >= height) || height_plus <= width) {
      return "landscape";
    } else {
      return "portrait";
    }
  }

  dismiss() {
    this.viewCtrl.dismiss({});
  }

  getDataUrl(img) {
    var canvas = document.createElement('canvas')
    var ctx = canvas.getContext('2d')

    canvas.width = img.width
    canvas.height = img.height
    ctx.drawImage(img, 0, 0)

    return canvas.toDataURL()
  }

  getData(path, cb) {
    var xmlHTTP = new XMLHttpRequest();
    xmlHTTP.open('GET', path, true);
    xmlHTTP.responseType = 'arraybuffer';
    xmlHTTP.onload = function (e) {
      var arr = new Uint8Array(this.response);
      var raw = String.fromCharCode.apply(null, arr);
      var b64 = btoa(raw);
      cb(b64);
    };
    xmlHTTP.send();
  }

  takePicture() {
    if (this.camData.length >= 3) {
      let a = this._alert.create({
        title: 'Fotos suficientes!',
        subTitle: 'Só é permitido 3 fotos por job!',
        buttons: ['OK']
      });
      a.present();
    } else {
      this.camera.getPicture({
        quality: 50,
        destinationType: this.platform.is('android') ? this.camera.DestinationType.FILE_URI : this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        targetHeight: 720,
        targetWidth: 1280,
        correctOrientation: true
      }).then((imageData) => {
        this.platform.is('android') ? (
          this.getData(imageData, (b64) => {
            this.camData.push(b64);
          })
        )
          :
          (
            this.camData.push(imageData)
          )
      });
    }
  }

  removePhoto(index) {
    this.camData.splice(index, 1);
  }

  doneJob() {
    let loading = this.loadingCtrl.create({
      content: 'Enviando...',
      enableBackdropDismiss: true
    });

    if (this.promotorFromRedeFilial == 0) {
      this.ngOnInit();
    }
    if (this.camData.length < 1) {
      let a = this._alert.create({
        title: 'Tire uma foto!',
        subTitle: 'É necessário tirar uma foto para completar o job',
        buttons: ['Ok']
      });
      a.present();
    } else if ((<HTMLTextAreaElement>document.getElementById('txtJob')).value.length < 1) {
      let a = this._alert.create({
        title: 'Preencha o campo!',
        subTitle: 'É necessário fazer uma observação do job',
        buttons: ['Ok']
      });
      a.present();
    } else {
      if (this.platform.is('android')) {
        this.nameOfFile = Math.ceil(Math.random() * 1000) + this.pageParams.RED_Nome + new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0, 10) + '.jpg'
      } else {
        this.nameOfFile = Math.ceil(Math.random() * 1000) + this.pageParams.RED_Nome + '-' + this.pageParams.RFIL_Nome + '-iOS.jpg'
      }

      let nameOfFile = this.nameOfFile.trim();

      loading.present();

      let hora = (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes());
      let data_inicial = new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-01').toISOString().substring(0, 10);

      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/app/insertJob.php',
        {
          title: nameOfFile,
          anexo: this.camData,
          // anexo: this.toUploadImage,
          cliente: this.clienteIndex,
          rede: this.pageParams.RED_ID,
          promotor: this.promotorFromRedeFilial,
          filial: this.pageParams.RFIL_ID,
          status: 'Realizado',
          ano: new Date().getFullYear(),
          mes: (new Date().getMonth() + 1),
          hora: hora,
          obs: this.obs,
          foto: nameOfFile,
          lat: this.myLat,
          lon: this.myLong,
          modelo: this._device.model.toString().trim() || null,
          plataforma: this._device.platform.toString().trim() || null,
          versao: this._device.version.toString().trim() || null,
          manufact: this._device.manufacturer.toString().trim() || null
        },
        {}
      ).then((response) => {
        console.log(response)
        response.data = JSON.parse(response.data)
        if (response.data.status == '0x104') {

          if (this.pageParams.RFIL_Nova_Entrada == false && this.pageParams.RFIL_Numero_Visitas == 1) {
            this._http.post(
              'https://kstrade.com.br/sistema/assets/kstrade_php/app/insertSaida.php',
              {
                pro_id: this.pageParams.PRO_ID,
                rede_id: this.pageParams.RED_ID,
                rfil_id: this.pageParams.RFIL_ID,
                hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                lat_saida: this.myLat,
                long_saida: this.myLong
              },
              {}
            ).then((response2) => {
              response2.data = JSON.parse(response2.data)
              if (response.data.status == '0x104') {
                this._rota.enableRota({
                  pro_id: this.pageParams.PRO_ID,
                  rede_id: this.pageParams.RED_ID,
                  rfil_id: this.pageParams.RFIL_ID,
                  data: new Date().toISOString().split('T')[0],
                  hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                  lat_saida: this.myLat,
                  long_saida: this.myLong
                });
                this._vibration.vibrate(250);
              }
            }, (err) => {
              if (!localStorage.getItem('temp_saidas')) {
                let temp = [];
                temp.push({
                  pro_id: this.pageParams.PRO_ID,
                  rede_id: this.pageParams.RED_ID,
                  rfil_id: this.pageParams.RFIL_ID,
                  data: new Date().toISOString().split('T')[0],
                  hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                  lat_saida: this.myLat,
                  long_saida: this.myLong
                });
                localStorage.setItem('temp_saidas', JSON.stringify(temp))
              } else {
                let temp = JSON.parse(localStorage.getItem('temp_saidas'))
                temp.push({
                  pro_id: this.pageParams.PRO_ID,
                  rede_id: this.pageParams.RED_ID,
                  rfil_id: this.pageParams.RFIL_ID,
                  data: new Date().toISOString().split('T')[0],
                  hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                  lat_saida: this.myLat,
                  long_saida: this.myLong
                });
                localStorage.setItem('temp_saidas', JSON.stringify(temp))
              }
            });
          }

          this._http.post(
            'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectProdutos.php',
            {
              cliente: this.clienteIndex
            },
            {}
          ).then((response) => {
            response.data = JSON.parse(response.data)
            if (response.data.status == '0x104') {
              if (response.data.result[0].pro_dias_semana.length > 0 && response.data.result[0].pro_dias_semana.split(';').length > 0) {
                let days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                let dayName = days[new Date().getDay()];

                let d = response.data.result[0].pro_dias_semana || '';
                let diassemana = d.split(';') || [];

                let doneExp1 = {};
                try {
                  diassemana.forEach(element => {
                    if (element == dayName) {
                      throw doneExp1;
                    }
                  });
                } catch (doneExp1) {
                  let modal = this.modalCtrl.create(PesquisaModalPage,
                    {
                      cliente: this.clienteIndex,
                      rede: this.pageParams.RED_ID,
                      promotor: this.promotorFromRedeFilial,
                      filial: this.pageParams.RFIL_ID,
                      data: new Date().toISOString().split('T')[0],
                      hora: hora,
                      produtos: response.data.result
                    }
                  );
                  setTimeout(() => {
                    modal.present();
                  }, 150);

                  modal.onDidDismiss((data) => {
                    if (data.done == true) {
                      let a = this._alert.create({
                        title: 'Enviado com sucesso!',
                        subTitle: 'O job foi enviado com sucesso!',
                        buttons: ['Ok']
                      });
                      a.present();
                    } else if (data.done == false && data.error == 'pesquisaError') {
                      let a = this._alert.create({
                        title: 'Erro na pesquisa',
                        subTitle: 'Informação não enviada por favor contate a central.',
                        buttons: ['Ok']
                      });
                      a.present();
                    }
                  });
                }
              } else {
                if (response.data.result[0].pro_numero_vezes > 0 && response.data.result[0].pro_numero_vezes !== null) {
                  this._http.post(
                    'https://kstrade.com.br/sistema/assets/kstrade_php/selectCountPesquisa.php',
                    {
                      cliente: this.clienteIndex,
                      rede: this.pageParams.RED_ID,
                      promotor: this.promotorFromRedeFilial,
                      filial: this.pageParams.RFIL_ID,
                      dataone: this.getInitAndFimSemana().init,
                      datatwo: this.getInitAndFimSemana().end
                    },
                    {}
                  ).then((response2) => {
                    response2.data = JSON.parse(response2.data)
                    if (response2.data.status == '0x104') {
                      if (response.data.result[0].pro_numero_vezes > response2.data.result[0].numero_pesquisas) {
                        let modal = this.modalCtrl.create(PesquisaModalPage,
                          {
                            cliente: this.clienteIndex,
                            rede: this.pageParams.RED_ID,
                            promotor: this.promotorFromRedeFilial,
                            filial: this.pageParams.RFIL_ID,
                            data: new Date().toISOString().split('T')[0],
                            hora: hora,
                            produtos: response.data.result
                          }
                        );
                        setTimeout(() => {
                          modal.present();
                        }, 150);

                        modal.onDidDismiss((data) => {
                          if (data.done == true) {
                            let a = this._alert.create({
                              title: 'Enviado com sucesso!',
                              subTitle: 'O job foi enviado com sucesso!',
                              buttons: ['Ok']
                            });
                            a.present();
                          } else if (data.done == false && data.error == 'pesquisaError') {
                            let a = this._alert.create({
                              title: 'Erro',
                              subTitle: 'O job não pode ser enviado!',
                              buttons: ['Ok']
                            });
                            a.present();
                          }
                        });
                      }
                    }
                  });
                } else {
                  let a = this._alert.create({
                    title: 'Enviado com sucesso!',
                    subTitle: 'O job foi enviado com sucesso!',
                    buttons: ['Ok']
                  });

                  a.present();
                }
              }
            } else {
              let a = this._alert.create({
                title: 'Enviado com sucesso!',
                subTitle: 'O job foi enviado com sucesso!',
                buttons: ['Ok']
              });
              a.present();
            }
            loading.dismiss();
            this.viewCtrl.dismiss({ job: 'done' });
          });
        } else if (response.data.status == '0x105') {
          loading.dismiss();
          let a = this._alert.create({
            title: 'Job duplicado!',
            subTitle: 'Você já inseriu esse job hoje!',
            buttons: ['Ok']
          });
          a.present();
        } else if (response.data.status == '0x101') {
          loading.dismiss();
          let a = this._alert.create({
            title: 'Erro',
            subTitle: 'Erro de inserção no banco de dados',
            buttons: ['Ok']
          });
          a.present();
        } else if (response.data.status == '0x102') {
          loading.dismiss();
          let a = this._alert.create({
            title: 'Já existe!',
            subTitle: 'O arquivo já existe no sistema',
            buttons: ['Ok']
          });
          a.present();
        } else if (response.data.status == '0x103') {
          loading.dismiss();
          let a = this._alert.create({
            title: 'Error 103',
            subTitle: 'Ocorreu um erro ao salvar o arquivo',
            buttons: ['Ok']
          });
          a.present();
        } else if (response.data.status == '0x110') {
          loading.dismiss();
          let a = this._alert.create({
            title: 'Erro de job',
            subTitle: 'Esse cliente não pertence a loja selecionada.',
            buttons: ['Ok']
          });
          a.present();
        }
      }, (err) => {
        loading.dismiss();
        this._alert.create({
          title: 'Job salvo!',
          message: 'O Job foi salvo com sucesso! você não está conectado.'
        }).present();
        if (!localStorage.getItem('temp_jobs')) {
          let temp = [];
          temp.push({
            title: nameOfFile,
            anexo: this.camData,
            // anexo: this.toUploadImage,
            cliente: this.clienteIndex,
            rede: this.pageParams.RED_ID,
            promotor: this.promotorFromRedeFilial,
            filial: this.pageParams.RFIL_ID,
            status: 'Realizado',
            data: new Date().toISOString().split('T')[0],
            hora: hora,
            obs: this.obs,
            foto: nameOfFile,
            lat: this.myLat,
            lon: this.myLong,
            modelo: this._device.model.toString().trim() || null,
            plataforma: this._device.platform.toString().trim() || null,
            versao: this._device.version.toString().trim() || null,
            manufact: this._device.manufacturer.toString().trim() || null
          });
          localStorage.setItem('temp_jobs', JSON.stringify(temp))
        } else {
          let temp = JSON.parse(localStorage.getItem('temp_jobs'));
          temp.push({
            title: nameOfFile,
            anexo: this.camData,
            // anexo: this.toUploadImage,
            cliente: this.clienteIndex,
            rede: this.pageParams.RED_ID,
            promotor: this.promotorFromRedeFilial,
            filial: this.pageParams.RFIL_ID,
            status: 'Realizado',
            data: new Date().toISOString().split('T')[0],
            hora: hora,
            obs: this.obs,
            foto: nameOfFile,
            lat: this.myLat,
            lon: this.myLong,
            modelo: this._device.model.toString().trim() || null,
            plataforma: this._device.platform.toString().trim() || null,
            versao: this._device.version.toString().trim() || null,
            manufact: this._device.manufacturer.toString().trim() || null
          });
          localStorage.setItem('temp_jobs', JSON.stringify(temp));
        }
      });
    }
  }

  setCliente(c) {
    if (c.aux_semana) {
      let days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
      let dayName = days[new Date().getDay()];
      let elem = c.aux_semana.split(';');
      for (let i = 0; i < elem.length; i++) {
        if (elem[i].trim() == dayName) {
          this.clienteOK = false;
          this.cliente = c.Cli_Nome;
          this.clienteIndex = c.Cli_ID;
          break;
        } else if (i == elem.length - 1) {
          let a = this._alert.create({
            title: 'Fora de rotina',
            subTitle: 'Sua rotina determina que não deve ter job deste cliente hoje!',
            buttons: ['Ok']
          });
          a.present();
        }
      }
    }
    this.clienteOK = false;
  }

  removePicture() {
    this.camData = [];
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewJobModalPage } from './new-job-modal';

@NgModule({
  declarations: [
    NewJobModalPage,
  ],
  imports: [
    IonicPageModule.forChild(NewJobModalPage),
  ],
})
export class NewJobModalPageModule {}

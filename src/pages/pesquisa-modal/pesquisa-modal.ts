import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import { UtilsProvider } from '../../providers/utils/utils';
import { DatePicker } from '../../../node_modules/@ionic-native/date-picker';
import { HTTP } from '@ionic-native/http';

declare var $: any;

/**
 * Generated class for the PesquisaModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pesquisa-modal',
  templateUrl: 'pesquisa-modal.html',
})
export class PesquisaModalPage implements OnInit {


  conjunto_datas = [];

  pageParams: any;

  cartoes = [];

  constructor(
    public platform: Platform,
    public _platform: Platform,
    public _datePicker: DatePicker,
    public _alert: AlertController,
    private utils: UtilsProvider,
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private _http: HTTP) {
    this._platform.registerBackButtonAction(() => {
      //console.log('u cant return');
      return;
    }, 1);
  }

  ngOnInit() {
    document.addEventListener('backbutton', () => {
      //console.log('u cant return');
      return;
    }, false);

    this.pageParams = this.viewCtrl.getNavParams().data;

    this.pageParams.produtos.forEach(element => {
      element.rupturaTotal = false;
      element.prateleira = false;
      element.estoque = false;
      element.data_vencimento = [
        {
          id: 1,
          value: ''
        }
      ];
      element.quantidade = [
        {
          id: 1,
          value: ''
        }
      ];
      element.data_vencimento_estoque = '';
      element.preco = '0.00';
      element.n_frentes = '0';
      element.n_frentes_concorrente = '0';
      element.n_max_frentes = '0';
      this.cartoes.push(element);
    });
  }

  addData(index) {
    this.cartoes[index].data_vencimento.push({
      id: this.cartoes.length + 1,
      value: ''
    });
  }

  removeData(index, indexx) {
    this.cartoes[index].data_vencimento.splice(indexx, 1);
  }

  addQuant(index) {
    this.cartoes[index].quantidade.push({
      id: this.cartoes.length + 1,
      value: ''
    });
  }

  removeQuant(index, indexx) {
    this.cartoes[index].quantidade.splice(indexx, 1);
  }

  applyDateMask(event: KeyboardEvent, c, index?, indexx?) {
    if (index !== undefined && indexx !== undefined) {
      let value = c;
      if (value.length == 2) {
        if (event.keyCode != 8) {
          value += '/'
        }
      } else if (value.length == 5) {
        if (event.keyCode != 8) {
          value += '/'
        }
      } else if (value.length == 10) {
        if (event.keyCode != 8) {
          event.preventDefault();
        }
      }
      this.cartoes[index].data_vencimento[indexx].value = value;
    } else if (index !== undefined && (indexx === undefined || indexx == '')) {
      let value = c;
      if (value.length == 2) {
        if (event.keyCode != 8) {
          value += '/'
        }
      } else if (value.length == 5) {
        if (event.keyCode != 8) {
          value += '/'
        }
      } else if (value.length == 10) {
        if (event.keyCode != 8) {
          event.preventDefault();
        }
      }
      this.cartoes[index].data_vencimento_estoque = value;
    }
    //  value.substring(0,2) + '/' + value.substring(2,4) + '/' + value.substring(4,8)
  }

  applyMoneyMask(c) {
    c.preco = this.utils.detectAmount(c.preco);
  }


  ///// NAO APAGAR COMENTARIOS DO METODO, POSSIVEL IMPLEMENTAÇÃO FUTURA
  ///// PEDIDO QUE FOSSE REMOVIDO TAIS FUNCOES, MAS COM POSSIBILIDADE DE RETORNO DAS MESMAS   --- 13/10/2019
  donePesquisa() {
    let exp = {};
    try {
      this.cartoes.forEach((c) => {
        let quantidade = '';
        let data = '';
        
        if (c.ruptura == false) {
          let havCallExp = false;
          if (
            c.n_frentes.trim().length < 1 ||
            c.n_max_frentes.trim().length < 1 ||
            c.preco.trim().length < 1 ||
            (c.estoque == false && c.prateleira == false && c.rupturaTotal == false)
          ) {
            havCallExp = true

          } else if (
            (c.n_frentes.trim().length < 1 ||
              c.n_max_frentes.trim().length < 1 ||
              c.preco.trim().length < 1)

            && c.prateleira == true
          ) {
            havCallExp = true
          }

          c.data_vencimento.forEach(element => {
            if (element.value.trim().length < 1) {
              havCallExp = true
            }
          });

          c.quantidade.forEach(element => {
            if (element.value.trim().length < 1) {
              havCallExp = true
            }
          });

          if (havCallExp) {
            throw exp;
          }
        }

        c.data_vencimento.forEach(element => {
          data += element.value + ';';
        });

        c.quantidade.forEach(element => {
          quantidade += element.value + ';';
        });

        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/app/insertPesquisa.php',
          {
            pro_id: c.pro_id,
            data_vencimento: data,
            data_vencimento_estoque: c.data_vencimento_estoque,
            preco: c.preco,
            nfrentes: c.n_frentes,
            nfrentes_concorrente: c.n_frentes_concorrente,
            nmaxfrentes: c.n_max_frentes,
            quantidade: quantidade,
            ruptura: c.rupturaTotal == true ? '1' : '0',
            estoque: c.estoque == true ? '1' : '0',
            prateleira: c.prateleira == true ? '1' : '0',
            rede: this.pageParams.rede,
            cliente: this.pageParams.cliente,
            promotor: this.pageParams.promotor,
            filial: this.pageParams.filial,
            data: this.pageParams.data,
            hora: this.pageParams.hora
          },
          {}
        ).then((response) => {
          console.log(response)
          response.data = JSON.parse(response.data)
          if (response.data.status == '0x104') {
            this.viewCtrl.dismiss({ done: true });
          } else if (response.data.status == '0x101') {
            this.viewCtrl.dismiss({ done: false, error: 'pesquisaError' });
          }
        });
      });
    } catch (exp) {
      let a = this._alert.create({
        title: 'Preencha os campos',
        subTitle: 'É necessário preencher toda a pesquisa!',
        buttons: ['Ok']
      });
      a.present();
    }
  }

  showDate(index, iindex) {
    this._datePicker.show({
      allowFutureDates: true,
      date: new Date(),
      mode: 'date',
      androidTheme: this._datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
    }).then((date) => {
      if (this.cartoes[index].data_vencimento.length == 1) {
        this.cartoes[index].data_vencimento[iindex] =
        {
          id: this.cartoes[index].data_vencimento.length + 1,
          value:
            date.toISOString().substring(0, 10).split('-')[2] + '/'
            + date.toISOString().substring(0, 10).split('-')[1] + '/' +
            date.toISOString().substring(0, 10).split('-')[0]
        };
      } else {
        if (this.cartoes[index].data_vencimento.length <= 3) {
          this.cartoes[index].data_vencimento[iindex] =
          {
            id: this.cartoes[index].data_vencimento.length + 1,
            value:
              date.toISOString().substring(0, 10).split('-')[2] + '/'
              + date.toISOString().substring(0, 10).split('-')[1] + '/' +
              date.toISOString().substring(0, 10).split('-')[0]
          }
        }
      }
    }, (err) => {
      let a = this._alert.create({
        title: 'Erro!',
        subTitle: 'Selecione a data corretamente!',
        buttons: ['OK']
      });
      a.present();
    });
  }

  clearData(index, iindex) {
    this.cartoes[index].data_vencimento[iindex].value = '';
  }
}

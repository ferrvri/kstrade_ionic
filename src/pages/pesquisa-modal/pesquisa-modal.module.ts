import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PesquisaModalPage } from './pesquisa-modal';

@NgModule({
  declarations: [
    PesquisaModalPage,
  ],
  imports: [
    IonicPageModule.forChild(PesquisaModalPage),
  ],
})
export class PesquisaModalPageModule {}

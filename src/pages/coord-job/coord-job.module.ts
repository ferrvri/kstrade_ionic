import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CoordJobPage } from './coord-job';

@NgModule({
  declarations: [
    CoordJobPage,
  ],
  imports: [
    IonicPageModule.forChild(CoordJobPage),
  ],
})
export class CoordJobPageModule {}

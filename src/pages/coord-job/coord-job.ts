import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { Http } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the CoordJobPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-coord-job',
  templateUrl: 'coord-job.html',
})
export class CoordJobPage {

  obs = '';
  myLat;
  myLong;

  constructor(
    private _geolocation: Geolocation,
    private _alert: AlertController, 
    private _viewCtrl: ViewController, 
    private _http: HTTP, 
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CoordJobPage');
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this._geolocation.getCurrentPosition().then( (data) => {
      this.myLat = data.coords.latitude;
      this.myLong = data.coords.longitude;
    }).catch( (e) => {
      //console.log(e);
      let a = this._alert.create( {
        title: 'Ative a localização',
        subTitle: 'Você deve ativar a localização!',
        buttons: ['Ok']
      } );
      a.present();
      this._viewCtrl.dismiss({});
    })
  }

  dismiss(){
    this._viewCtrl.dismiss();
  }

  doneJob(){
    let data = new Date( new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0,10);
    let hora = (new Date().getHours().valueOf() < 10 ? '0'+new Date().getHours():new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0'+new Date().getMinutes():new Date().getMinutes());
    
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/app/insertJobCoord.php',
      {
        usr_id: JSON.parse(localStorage.getItem('session')).user.Usr_ID,
        rede: this.navParams.get('rede_id'),
        filial: this.navParams.get('rfil_id'),
        status: 'Feedback',
        data: data,
        hora: hora,
        obs: this.obs,
        lat: this.myLat,
        lon: this.myLong
      },
      {}
    ).then( (response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104'){
        this._alert.create({
          subTitle: 'Feedback eviado com sucesso'
        }).present();
        this._viewCtrl.dismiss();
      }
    });
  }

}
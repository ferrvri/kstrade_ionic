import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { BrowserTab } from '@ionic-native/browser-tab';
import { Network } from '@ionic-native/network';

import { LocalNotifications } from '../../../node_modules/@ionic-native/local-notifications';
import { Platform } from 'ionic-angular/platform/platform';
import { LoadingPage } from '../loading/loading';

import { HTTP } from '@ionic-native/http';

@IonicPage()
@Component({
  selector: 'page-init',
  templateUrl: 'init.html',
})
export class InitPage implements OnInit {

  connected: boolean = false;
  problemLabel: boolean = false;

  version: string = '';

  constructor(
    public platform: Platform,
    public _localNotifications: LocalNotifications,
    // public _background: BackgroundMode, 
    public _network: Network,
    public browserTab: BrowserTab,
    public _alert: AlertController,
    public navParams: NavParams,
    private _http: HTTP,
    public navCtrl: NavController) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.problemLabel = true;
    }, 10000);

    this._http.get(
      'https://kstrade.com.br/sistema/assets/kstrade_php/checkversion.php',
      {},
      {}
    ).then((response) => {
      response.data = JSON.parse(response.data)

      if (response.status == 200) {
        localStorage.removeItem('netinfo');
        localStorage.setItem('netinfo', JSON.stringify({ connected: true }));

        if (response.data.version) {
          if (localStorage.getItem('version')) {
            this.version = JSON.parse(localStorage.getItem('version')).version[0];
            if (parseFloat(this.version) < parseFloat(response.data.version[0]) || parseFloat(response.data.version[0]) > parseFloat(this.version)) {
              document.addEventListener('backbutton', () => {
                let a = this._alert.create(
                  {
                    subTitle: 'Atualize!',
                    message: 'Você deve atualizar o aplicativo!',
                    buttons: ['OK']
                  }
                );
                a.present();
                return;
              }, false);
              document.addEventListener('pause', () => {
                localStorage.setItem('version', JSON.stringify(response.data));
              }, false);

              this.browserTab.openUrl('http://kstrade.com.br/sistema');
            } else {
              this.redirToMain();
            }
          } else {
            localStorage.setItem('version', JSON.stringify(response.data));
            this.redirToMain();
          }
        }
      } else if (response.status == 400 || response.status == 404 || response.status == 504) {
        this.connected = false;
        let a = this._alert.create({
          title: 'Sem internet',
          subTitle: 'Você não está conectado à internet, por favor verifique sua conectividade',
          buttons: ['Ok']
        });
        a.present();
      }
    }, (err) => {
      this.redirToMain()
    });
  }

  redirToMain() {
    setTimeout(() => {
      if (!localStorage.getItem('session')) {
        localStorage.setItem('session', JSON.stringify({
          visitante: {
            Usr_Nome: 'Temporario',
            Usr_Email: '',
            Usr_Telefone: '',
            Usr_Nivel: 'Visitante'
          }
        }));
      }
      this.navCtrl.push(LoadingPage, { page: 'main' }, { animate: false });
      this.connected = true;
    }, 1000);
  }

}

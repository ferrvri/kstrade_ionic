import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InitPage } from './init';
import { HTTP } from '@ionic-native/http';

@NgModule({
  declarations: [
    InitPage,
  ],
  imports: [
    IonicPageModule.forChild(InitPage),
    // HTTP
  ]
})
export class InitPageModule {}

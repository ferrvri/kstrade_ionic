import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { MainPage } from '../main/main';
import { Http } from '@angular/http';
import { ViewPhotoPage } from '../view-photo/view-photo';
import { Vibration } from '@ionic-native/vibration';
import { JobInfosPage } from '../area-cliente/job-infos/job-infos';
import { Platform } from 'ionic-angular/platform/platform';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the JobsDoDiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({name: 'jobsDoDia'})
@Component({
  selector: 'page-jobs-do-dia',
  templateUrl: 'jobs-do-dia.html',
})
export class JobsDoDiaPage implements OnInit {

  table = [];

  constructor(
    public platform: Platform,
    public _alert: AlertController,
    public _vibrate: Vibration,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP, 
    public modalCtrl: ModalController) {
  }

  ngOnInit(){
    let mes = new Date().getMonth()+1;
    let cl = JSON.parse(localStorage.getItem('session'));
    cl.forEach(element => {
      element.content.forEach(element2 => {
        let o = {
          rede_nome: '',
          filial_nome: '',
          content: []
        };
        
        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectJobsOf.php',
          {
            myid: element.PRO_ID,
            rede_id: element2.RED_ID,
            filial_id: element2.RFIL_ID,
            dataone: 
            new Date().getFullYear() + '-' +
            (mes.valueOf() < 10 ? '0'+mes.toString(): mes.toString()) + '-' +
            new Date().getDate()
            // dataone: '2018-04-10'
          },
          {}
        ).then( (response) => {
          response.data = JSON.parse(response.data)
          if (response.data.status == '0x104'){
            o.rede_nome = element2.RED_Nome;
            o.filial_nome = element2.RFIL_Nome;
            response.data.result.forEach(element3 => {
              o.content.push(element3);
            });
            this.table.push(o);
          }else if (response.data.status == '0x101'){
            o.rede_nome = element2.RED_Nome;
            o.filial_nome = element2.RFIL_Nome;
            o.content = [];
            this.table.push(o);
          }
        });
      });
    });
  }

  goBack(){
    this.navCtrl.pop();
  }

  seePhoto(cel){
    let modal = this.modalCtrl.create(JobInfosPage, {
      type: 'photo',
      job_id: cel.JOB_ID,
      job_foto: cel.JOB_Foto,
      obs: cel.JOB_Obs
    });
    modal.present();
  }

  isLong: boolean = false;
  clickTime: number = 0;
  upTime: number = 0;
  handler:any;

  longPress(event: MouseEvent, elem){
    //console.log(event);
    this._vibrate.vibrate(500);
    let alert = this._alert.create({
      title: 'Excluir?',
      subTitle: 'Deseja realmente excluir esse job?',
      buttons: [
        {
          text: 'Excluir!',
          handler: data => {
            //console.log(elem)
            let hora = (new Date().getHours().toString().length == 1 ? '0'+new Date().getHours().toString():new Date().getHours().toString());
            let minuto = (new Date().getMinutes().toString().length == 1 ? '0'+new Date().getMinutes().toString():new Date().getMinutes().toString());
            if (elem.JOB_Hora_Job.split(':')[0] == hora ){
              if (parseInt(minuto) - parseInt(elem.JOB_Hora_Job.split(':')[1]) <= 5 ){
                this._http.post(
                  'https://kstrade.com.br/sistema/assets/kstrade_php/app/removeJob.php',
                  {
                    job_id: elem.JOB_ID
                  },
                  {}
                ).then( (response) => {
                  response.data = JSON.parse(response.data)
                  if (response.data.status == '0x104'){
                    let msg = `O promotor ${JSON.parse(localStorage.getItem('session'))[0].content[0].PRO_Nome} apagou o seguinte job: 
                      ${elem.Cli_Nome} | Foto: ${'https://kstrade.com.br/sistema/assets/kstrade_php/uploads/'+elem.JOB_Foto}
                    `;
                    this._http.post(
                      'https://kstrade.com.br/sistema/assets/kstrade_php/sendMensagem.php',
                      {
                        deid: 5,
                        paraid: 2,
                        mensagem: msg,
                        status: 'Nao Lido',
                        dataenvio: new Date().getUTCDate().toString() + '/' + (parseInt(new Date().getUTCMonth().toString().length == 1 ? '0'+new Date().getUTCMonth().toString(): new Date().getUTCMonth().toString()) + 1) + '/' + new Date().getUTCFullYear().toString()
                      },
                      {}
                    ).then( (response) => {
                      response.data = JSON.parse(response.data)
                      if (response.data.status == '0x104'){
                        let alert = this._alert.create({
                          title: 'Job excluido',
                          subTitle: 'Job excluido com sucesso!',
                          buttons: ['Ok']
                        });
                        alert.present();
                        this.table = []
                        this.ngOnInit();
                      }else if (response.data.status == '0x101'){
                        let alert = this._alert.create({
                          title: 'Erro',
                          subTitle: 'Erro ao excluir o Job.',
                          buttons: ['Ok']
                        });
                        alert.present();
                      }
                    });
                  }
                });
              }else{
                let alert = this._alert.create({
                  title: 'Erro 0x102!',
                  subTitle: 'Já passou o tempo determinado para excluir!',
                  buttons: ['Ok']
                });
                alert.present();
              }
            }else{
              let alert = this._alert.create({
                title: 'Erro 0x101!',
                subTitle: 'Já passou o tempo determinado para excluir!',
                buttons: ['Ok']
              });
              alert.present();
            }
          }
        }
      ]
    });
    alert.present();
  }

  exechandler(event: MouseEvent){
    clearInterval(this.handler);
  }
}


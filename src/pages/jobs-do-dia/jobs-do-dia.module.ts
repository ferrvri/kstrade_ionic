import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JobsDoDiaPage } from './jobs-do-dia';

@NgModule({
  declarations: [
    JobsDoDiaPage,
  ],
  imports: [
    IonicPageModule.forChild(JobsDoDiaPage),
  ],
})
export class JobsDoDiaPageModule {}

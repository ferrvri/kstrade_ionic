import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LojasVisitantePage } from './lojas-visitante';

@NgModule({
  declarations: [
    LojasVisitantePage,
  ],
  imports: [
    IonicPageModule.forChild(LojasVisitantePage),
  ],
})
export class LojasVisitantePageModule {}

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import swal from 'sweetalert2';
import { text } from '@angular/core/src/render3/instructions';
import { Platform } from 'ionic-angular/platform/platform';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the LojasVisitantePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({name: 'LojasVisitante'})
@Component({
  selector: 'page-lojas-visitante',
  templateUrl: 'lojas-visitante.html',
})
export class LojasVisitantePage implements OnInit {

  redes = [];
  resultsFilterRedes = [];
  doneLoading = false;

  already_filtered = false;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    private _http: HTTP) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LojasVisitantePage');
  }

  ngOnInit(){
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectRedesFilialVisita.php',
      {
        all: true
      },
      {}
    ).then( (response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104'){
        response.data.result.forEach(element => {
          element.show = false
          if (element.content.length > 0){
            this.redes.push(element);
          }
        });
        this.resultsFilterRedes = this.redes;
      }
      this.doneLoading = true;
    });
  }

  goBack(){
    this.navCtrl.pop();
  }


  goBackFilter(){
    this.already_filtered = false;
    this.resultsFilterRedes = this.redes;
  }

  filter(name){
    this.already_filtered = true;
    var regexp = new RegExp((".*" + name + ".*"), "i");
    this.resultsFilterRedes = this.resultsFilterRedes.filter((e) => {
      return regexp.test(e.RED_Nome);
    });
  }

  solicitarAtendimento(rede){
    swal({
      title:'Inscreva-se',
      html: `
        <span style="width: 100%; font-weight: bold">Seja um FREELANCER KSTRADE</span>
        <span style="width: 100%;">Informe seu nome, whatsapp e quantidade de visitas e de dias de atendimento que entraremos em contato com vc!</span>
      `,
      type: "question",
      showCancelButton: true,
      cancelButtonText: 'Não',
      showConfirmButton: true,
      confirmButtonText: 'Sim',
      reverseButtons: true,
      input: "textarea",
      inputPlaceholder: 'Detalhes/Motivos da solicitação',
    }).then( (data) => {
      if (data.value !== undefined && data.value.length > 0){
        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/app/insertAtendimento.php',
          {
            rede_id: rede.RED_ID,
            filial_id: rede.RFIL_ID,
            detail: `
                    <b>Visitante que solicitou:</b><br>
                    ${JSON.parse(localStorage.getItem('session')).visitante.Usr_Nome}<br>
                    <b>Email:</b><br>
                    ${JSON.parse(localStorage.getItem('session')).visitante.Usr_Email}<br>
                    <b>Telefone:</b><br>
                    ${JSON.parse(localStorage.getItem('session')).visitante.Usr_Telefone}<br>
                    <b>Detalhes:</b>
                    ${data.value}
                    `,
            texto: data.value
          },
          {}
        ).then( (response) => {
          response.data = JSON.parse(response.data)
          if (response.data.status == '0x104'){
            swal( {
              title: 'Sucesso!',
              text: 'A sua solicitação foi enviada a nossa equipe, já estamos em analíse. Obrigado!',
              type: 'success'
            });
          }else{
            swal( {
              title: 'Erro!',
              text: 'Sua solicitação não foi enviada a nossa equipe',
              type: 'error'
            });
          }
        })
      }else if (data.value !== undefined && data.value.length < 1){
        swal({
          type: 'error',
          title: 'Preencha o campo!'
        })
      }
    });
  }
}

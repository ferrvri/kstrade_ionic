import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import { DatePicker } from '@ionic-native/date-picker';
import { timestamp } from 'rxjs/operators';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the CriticModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({ name: 'clientePesquisa' })
@Component({
  selector: 'page-critic-modal',
  templateUrl: 'critic-modal.html',
})
export class CriticModalPage implements OnInit {

  pesquisas = [];

  data_inicio = {
    value: '',
    showValue: 'Data inicial',
    selected: false
  };

  data_fim = {
    showValue: 'Data final',
    value: '',
    selected: false
  }

  constructor(
    public platform: Platform, 
    private _alert: AlertController, 
    private _datePicker: DatePicker, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP) {
  }

  daysBetween(date1, date2) {
    let one_day = 1000 * 60 * 60 * 24;
    let date1_ms = new Date(date1).getTime();
    let date2_ms = new Date(date2).getTime();
    let difference_ms = date2_ms - date1_ms;

    date2 = date2.split('-')[0] + '-' + date2.split('-')[2] + '-' + date2.split('-')[1];

    return Math.round(difference_ms / one_day);
  }

  ngOnInit() {
    if (this.navParams.get('cliente')) {
      this.pesquisas = [];
      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/selectPesquisas_2.php',
        {
          clientes: this.navParams.get('cliente'),
          dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
          datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
        },
        {}
      ).then((response) => {
        response.data = JSON.parse(response.data)
        if (response.data.status == '0x104') {
          this.pesquisas = response.data.result
        }
      });
    } else if (this.navParams.get('forVendedor') == true) {
      this.pesquisas = [];

      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/selectPesquisas_2.php',
        {
          cliente: JSON.parse(localStorage.getItem('session')).redes[0].aux_cliente_id,
          dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
          datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
        },
        {}
      ).then((response) => {
        response.data = JSON.parse(response.data)
        if (response.data.status == '0x104') {
          this.pesquisas = response.data.result
        }
      });
    } else if (this.navParams.get('forExterno') == true) {
      this.pesquisas = [];

      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/selectPesquisas_2.php',
        {
          dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
          datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
        },
        {}
      ).then((response) => {
        response.data = JSON.parse(response.data)
        if (response.data.status == '0x104') {
          this.pesquisas = response.data.result
        }
      });
    }
  }


  showDate(obj) {
    this._datePicker.show({
      allowFutureDates: true,
      date: new Date(),
      mode: 'date',
      androidTheme: this._datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
    }).then((date) => {
      obj.selected = true;
      obj.value = date;
      obj.showValue = date.toISOString().split('T')[0].split('-')[2] + '/' +
        date.toISOString().split('T')[0].split('-')[1] + '/' +
        date.toISOString().split('T')[0].split('-')[0]
    }, (err) => {
      obj.selected = false;
    });
  }

  goBack() {
    this.navCtrl.pop();
  }

  selectPesquisas(init, end) {
    if (this.data_inicio.selected == false || this.data_fim.selected == false) {
      let a = this._alert.create({
        title: 'Selecione uma data!',
        subTitle: 'Para utilizar o filtro, deve-se escolher a Data inicial e Data final'
      });
      a.present();
    } else {
      if (this.navParams.get('cliente')) {
        this.pesquisas = [];
        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/selectPesquisas_2.php',
          {
            clientes: this.navParams.get('cliente'),
            dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
            datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
          },
          {}
        ).then((response) => {
          response.data = JSON.parse(response.data)
          if (response.data.status == '0x104') {
            this.pesquisas = response.data.result
          }
        });
      } else if (this.navParams.get('forVendedor') == true) {
        this.pesquisas = [];

        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/selectPesquisas_2.php',
          {
            cliente: JSON.parse(localStorage.getItem('session')).redes[0].aux_cliente_id,
            dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
            datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
          },
          {}
        ).then((response) => {
          response.data = JSON.parse(response.data)
          if (response.data.status == '0x104') {
            this.pesquisas = response.data.result
          }
        });
      }
    }
  }
}

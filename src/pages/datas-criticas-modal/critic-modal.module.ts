import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CriticModalPage } from './critic-modal';

@NgModule({
  declarations: [
    CriticModalPage,
  ],
  imports: [
    IonicPageModule.forChild(CriticModalPage),
  ],
})
export class CriticModalPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EntradaSaidaJobPage } from './entrada-saida-job';

@NgModule({
  declarations: [
    EntradaSaidaJobPage,
  ],
  imports: [
    IonicPageModule.forChild(EntradaSaidaJobPage),
  ],
})
export class EntradaSaidaJobPageModule {}

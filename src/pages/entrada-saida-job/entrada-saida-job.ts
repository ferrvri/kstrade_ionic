import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ViewController } from 'ionic-angular';
import Swal from 'sweetalert2';
import { Http } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { Vibration } from '@ionic-native/vibration';
import { RotaProvider } from '../../providers/rota/rota';
import { Platform } from 'ionic-angular/platform/platform';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the EntradaSaidaJobPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-entrada-saida-job',
  templateUrl: 'entrada-saida-job.html',
})
export class EntradaSaidaJobPage implements OnInit{

  mylat = 0;
  mylong = 0;
  makeIt = false;

  constructor(
    public platform: Platform, 
    private _rota: RotaProvider, 
    public viewCtrl: ViewController, 
    private loadingCtrl: LoadingController, 
    private _vibration: Vibration, 
    private _alert: AlertController, 
    private _geolocation: Geolocation, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad EntradaSaidaJobPage');
  }

  ngOnInit(){
    this._geolocation.getCurrentPosition().then( (data) => {
      this.mylat = data.coords.latitude;
      this.mylong = data.coords.longitude;
    }).catch( (e) => {
      //console.log(e);
      let a = this._alert.create( {
        title: 'Ative a localização',
        subTitle: 'Você deve ativar a localização!',
        buttons: ['Ok']
      } );
      a.present();
    })
  }

  checkIn(kind){
    this.makeIt = true;
    switch(kind){
        case 'entrada' : {
          let s = setTimeout(() => {
            loading.dismiss();
            Swal({
              title: 'Conexão ruim!',
              text: 'A resposta demorou muito a chegar!',
              type: 'error',
              showCloseButton: true,
              confirmButtonText: 'OK'
            });
            this._vibration.vibrate(250);
            this._vibration.vibrate(250);
          }, 30000);

          let loading = this.loadingCtrl.create({
            content: 'Enviando...'
          });
          loading.present();

          this._http.post(
            'https://kstrade.com.br/sistema/assets/kstrade_php/app/insertEntrada.php',
            {
              pro_id: this.navParams.get('content').PRO_ID,
              rede_id: this.navParams.get('content').RED_ID,
              rfil_id: this.navParams.get('content').RFIL_ID,
              hora: (new Date().getHours().valueOf() < 10 ? '0'+new Date().getHours():new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0'+new Date().getMinutes():new Date().getMinutes()),
              lat_entrada: this.mylat,
              long_entrada: this.mylong
            },
            {}
          ).then( (response) => {
            response.data = JSON.parse(response.data)
            if (response.data.status == '0x104'){
              clearTimeout(s);
              loading.dismiss();
              Swal({
                title: 'Entrada registrada!',
                text: 'Sua entrada foi registrada com sucesso!',
                type: 'success',
                showCloseButton: true,
                confirmButtonText: 'OK'
              });
              this._vibration.vibrate(250);
              this._rota.disableRota({
                pro_id: this.navParams.get('content').PRO_ID,
                rede_id: this.navParams.get('content').RED_ID,
                rfil_id: this.navParams.get('content').RFIL_ID,
                data: new Date().toISOString().split('T')[0],
                hora: (new Date().getHours().valueOf() < 10 ? '0'+new Date().getHours():new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0'+new Date().getMinutes():new Date().getMinutes()),
                lat_entrada: this.mylat,
                long_entrada: this.mylong
              });
              this.viewCtrl.dismiss({done: true});
              this.makeIt = false;
            }
          }, (err) => {
            clearTimeout(s);
            loading.dismiss();
            Swal({
              title: 'Entrada salva!',
              text: 'Sua entrada foi salva com sucesso!',
              type: 'success',
              showCloseButton: true,
              confirmButtonText: 'OK'
            });

            if (!localStorage.getItem('temp_entradas')){
              let temp = [];
              temp.push({
                pro_id: this.navParams.get('content').PRO_ID,
                rede_id: this.navParams.get('content').RED_ID,
                rfil_id: this.navParams.get('content').RFIL_ID,
                data: new Date().toISOString().split('T')[0],
                hora: (new Date().getHours().valueOf() < 10 ? '0'+new Date().getHours():new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0'+new Date().getMinutes():new Date().getMinutes()),
                lat_entrada: this.mylat,
                long_entrada: this.mylong
              })
              localStorage.setItem('temp_entradas', JSON.stringify(temp));
            }else{
              let temp = JSON.parse(localStorage.getItem('temp_entradas'));
              temp.push({
                pro_id: this.navParams.get('content').PRO_ID,
                rede_id: this.navParams.get('content').RED_ID,
                rfil_id: this.navParams.get('content').RFIL_ID,
                data: new Date().toISOString().split('T')[0],
                hora: (new Date().getHours().valueOf() < 10 ? '0'+new Date().getHours():new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0'+new Date().getMinutes():new Date().getMinutes()),
                lat_entrada: this.mylat,
                long_entrada: this.mylong
              })
              localStorage.setItem('temp_entradas', JSON.stringify(temp));
            }
          });
          this.viewCtrl.dismiss({done: true});
          break;
        }
        case 'saida': {
          let s = setTimeout(() => {
            loading.dismiss();
            Swal({
              title: 'Conexão ruim!',
              text: 'A resposta demorou muito a chegar!',
              type: 'error',
              showCloseButton: true,
              confirmButtonText: 'OK'
            });
            
            this._vibration.vibrate(250);
            this._vibration.vibrate(250);
          }, 30000);

          let loading = this.loadingCtrl.create({
            content: 'Enviando...'
          });
          loading.present();

          this._http.post(
            'https://kstrade.com.br/sistema/assets/kstrade_php/app/insertSaida.php',
            {
              pro_id: this.navParams.get('content').PRO_ID,
              rede_id: this.navParams.get('content').RED_ID,
              rfil_id: this.navParams.get('content').RFIL_ID,
              hora: (new Date().getHours().valueOf() < 10 ? '0'+new Date().getHours():new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0'+new Date().getMinutes():new Date().getMinutes()),
              lat_saida: this.mylat,
              long_saida: this.mylong
            },
            {}
          ).then( (response) => {
            response.data = JSON.parse(response.data)
            if (response.data.status == '0x104'){
              clearTimeout(s);
              loading.dismiss();
              Swal({
                title: 'Saída registrada!',
                text: 'Sua saída foi registrada com sucesso!',
                type: 'success',
                showCloseButton: true,
                confirmButtonText: 'OK'
              });
              this._vibration.vibrate(250);
              this._rota.enableRota({
                pro_id: this.navParams.get('content').PRO_ID,
                rede_id: this.navParams.get('content').RED_ID,
                rfil_id: this.navParams.get('content').RFIL_ID,
                data: new Date().toISOString().split('T')[0],
                hora: (new Date().getHours().valueOf() < 10 ? '0'+new Date().getHours():new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0'+new Date().getMinutes():new Date().getMinutes()),
                lat_saida: this.mylat,
                long_saida: this.mylong
              });
              this.viewCtrl.dismiss({done: true});
              this.makeIt = false;
            }
          }, (err) => {
            clearTimeout(s);
            loading.dismiss();
            Swal({
              title: 'Saída salva!',
              text: 'Sua saída foi salva com sucesso!',
              type: 'success',
              showCloseButton: true,
              confirmButtonText: 'OK'
            });

            if (!localStorage.getItem('temp_saidas')){
              let temp = [];
              temp.push({
                pro_id: this.navParams.get('content').PRO_ID,
              rede_id: this.navParams.get('content').RED_ID,
              rfil_id: this.navParams.get('content').RFIL_ID,
              data: new Date().toISOString().split('T')[0],
              hora: (new Date().getHours().valueOf() < 10 ? '0'+new Date().getHours():new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0'+new Date().getMinutes():new Date().getMinutes()),
              lat_saida: this.mylat,
              long_saida: this.mylong
              })
              localStorage.setItem('temp_saidas', JSON.stringify(temp));
            }else{
              let temp = JSON.parse(localStorage.getItem('temp_saidas'));
              temp.push({
                pro_id: this.navParams.get('content').PRO_ID,
              rede_id: this.navParams.get('content').RED_ID,
              rfil_id: this.navParams.get('content').RFIL_ID,
              data: new Date().toISOString().split('T')[0],
              hora: (new Date().getHours().valueOf() < 10 ? '0'+new Date().getHours():new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0'+new Date().getMinutes():new Date().getMinutes()),
              lat_saida: this.mylat,
              long_saida: this.mylong
              })
              localStorage.setItem('temp_saidas', JSON.stringify(temp));
            }
          });

          this.viewCtrl.dismiss({done: true});
          break;
        }
    }
    
  }

}

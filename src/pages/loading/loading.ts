import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-loading',
  templateUrl: 'loading.html',
})
export class LoadingPage implements OnInit {

  constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit(){
    if (this.navParams.get('page')){
      setTimeout( () => {
        // this.navCtrl.pop();
        setTimeout( () => {
          this.navCtrl.setRoot(this.navParams.get('page'));
        }, 100);
        // window.location.href = '/#/'+this.navParams.get('page');
      }, 1500);
    }
  }
  

}

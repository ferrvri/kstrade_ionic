import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewNewCompletePage } from './view-new-complete';

@NgModule({
  declarations: [
    ViewNewCompletePage,
  ],
  imports: [
    IonicPageModule.forChild(ViewNewCompletePage),
  ],
})
export class ViewNewCompletePageModule {}

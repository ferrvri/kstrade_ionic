import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { ViewPhotoPage } from '../view-photo/view-photo';
import { Http } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';
import { Platform } from 'ionic-angular/platform/platform';
import { of } from 'rxjs/observable/of';
import { HTTP } from '@ionic-native/http';

declare var escape: any;
/**
 * Generated class for the ViewNewCompletePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-new-complete',
  templateUrl: 'view-new-complete.html',
})
export class ViewNewCompletePage implements OnInit{

  noticia = {grupos: []};
  newGroupName = '';

  constructor(
    public platform: Platform,  
    private _sanitization: DomSanitizer, 
    private _http: HTTP, 
    private viewCtrl: ViewController, 
    private _modal: ModalController, 
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  decode_utf8(s) {
    return decodeURIComponent(escape(s));
  }

  ngOnInit(): void {
    this.newGroupName = this.navParams.get('group_name')
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php',
      {
        forCliente: true,
        group_id: this.navParams.get('group_id')
      },
      {}
    ).then((response3) => {
      response3.data = JSON.parse(response3.data)
      if (response3.data.status == '0x104') {
        response3.data.result.forEach((element, index) => {
          element.app_title = this.decode_utf8(element.app_title);
          element.app_text = this.decode_utf8(element.app_text);
          element.activated = false;
          element.image = element.app_image;
          element.app_image = this._sanitization.bypassSecurityTrustStyle(`url(https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/${element.app_image}) center / cover`);       


          console.log(element);
          this.noticia.grupos.push(element);
        });
      }
    });
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

  openNewImage(title, text, cardImage, id, obj?) {
    console.log(obj);
    let o = {
      type: 'photo',
      JOB_Title: title,
      JOB_Obs: text,
      fotos: [],
      id: id
    }

    o.fotos.push('news/' + cardImage);
    obj.app_fotos_extras.forEach(el => {
      o.fotos.push('news/' + el.new_foto)
    });

    let modal = this._modal.create(ViewPhotoPage, o);
    modal.present();
  }

}

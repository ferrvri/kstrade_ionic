import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import { MainPage } from '../main/main';
import { LoadingPage } from '../loading/loading';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the ConfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm',
  templateUrl: 'confirm.html',
})
export class ConfirmPage implements OnInit {

  codigo: string = '';

  constructor(
    public platform: Platform, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP, 
    public _alert: AlertController) {
  }

  ngOnInit(){
    
  }


  confirm(){
    if (this.codigo.length < 1 || this.codigo === undefined){
      let a = this._alert.create( {
        title: 'Preencha o campo!',
        subTitle: 'Preencha todos os campos para confimar',
        buttons: ['Ok']
      } );
      a.present();
    }else{
      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/app/confirm.php',
        {
          pro_nome: JSON.parse(localStorage.getItem('session'))[0].content[0].PRO_Nome
        },
        {}
      ).then( (response) => {
        response.data = JSON.parse(response.data)
        if (response.data.status == '0x104'){
          localStorage.removeItem('wantconfirm');
          this.navCtrl.push(LoadingPage, {page: 'main'});
        }
      });
    }
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClienteLojasPage } from './cliente-lojas';

@NgModule({
  declarations: [
    ClienteLojasPage,
  ],
  imports: [
    IonicPageModule.forChild(ClienteLojasPage),
  ],
})
export class ClienteLojasPageModule {}

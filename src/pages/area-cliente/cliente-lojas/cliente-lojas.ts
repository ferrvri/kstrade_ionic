import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import { ClienteFiliaisPage } from '../cliente-filiais/cliente-filiais';


/**
 * Generated class for the ClienteLojasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({ name: 'clienteLojas' })
@Component({
  selector: 'page-cliente-lojas',
  templateUrl: 'cliente-lojas.html',
})
export class ClienteLojasPage implements OnInit {

  lojas: any = [];
  doneLoading: boolean = false;
  emptyResult: boolean = false;

  constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams, private _alert: AlertController, private _http: Http, private _modal: ModalController) {

  }

  ngOnInit() {
    if (!this.navParams.get('redes')) {
      if (localStorage.getItem('session')) {
        JSON.parse(localStorage.getItem('session')).redes.forEach(e1 => {
          this.lojas.push({
            nome: e1.RED_Nome,
            id: e1.RED_ID
          });
        });
        setTimeout(() => {
          this.lojas.sort(function (a, b) {
            if (a.RED_Nome < b.RED_Nome) { return -1; }
            if (a.RED_Nome > b.RED_Nome) { return 1; }
            return 0;
          });
        }, 2000);
      }
    } else {
      this.navParams.get('redes').forEach(e1 => {
        this.lojas.push({
          nome: e1.RED_Nome,
          id: e1.RED_ID
        });
      });

      setTimeout(() => {
        this.lojas.sort(function (a, b) {
          if (a.RED_Nome < b.RED_Nome) { return -1; }
          if (a.RED_Nome > b.RED_Nome) { return 1; }
          return 0;
        });
      }, 2000);
    }

  }

  goBack() {
    this.navCtrl.pop();
  }

  openLoja(loja) {
    let modal = this._modal.create(ClienteFiliaisPage, {
      RED_ID: loja.id,
      RED_Nome: loja.nome
    });
    modal.present();
  }
}

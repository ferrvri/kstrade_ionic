import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientePromotoresPage } from './cliente-promotores';

@NgModule({
  declarations: [
    ClientePromotoresPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientePromotoresPage),
  ],
})
export class ClientePromotoresPageModule {}

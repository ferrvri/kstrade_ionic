import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Keyboard, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import { ClienteLojasPage } from '../cliente-lojas/cliente-lojas';
import { JobsClientePromotorPage } from '../jobs-cliente-promotor/jobs-cliente-promotor';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the ClientePromotoresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({name: 'clientePromotores'})
@Component({
  selector: 'page-cliente-promotores',
  templateUrl: 'cliente-promotores.html',
})
export class ClientePromotoresPage implements OnInit {

  doneLoading = false;

  promos = []
  resultsPromo = []

  constructor(
    public platform: Platform, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP, 
    private _modal: ModalController ) {
  }

  ngOnInit(){
    let o = {
      all: true,
      redes: [],
      clientes: []
    }
    if (JSON.parse(localStorage.getItem('session')).user !== undefined && JSON.parse(localStorage.getItem('session')).clientes){
      delete o.redes;
      o.clientes = JSON.parse(localStorage.getItem('session')).clientes;
    }else{
      delete o.clientes;
      o.redes = JSON.parse(localStorage.getItem('session')).redes;
    }
    
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectPromotoresCliente.php',
      o,
      {}
    ).then( (response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104'){
        response.data.result.forEach(element2 => {
          this.promos.push({PRO_Nome: element2});
        });
    
        this.resultsPromo = this.promos;
        setTimeout( () => {
          this.resultsPromo.sort(function(a, b){
            if(a.PRO_Nome < b.PRO_Nome) { return -1; }
            if(a.PRO_Nome > b.PRO_Nome) { return 1; }
            return 0;
          });
          this.doneLoading = true;
        }, 3000);
      }
    });
  }

  goBack(){
    this.navCtrl.pop();
  }

  openRedesFromPromotor(promo){
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectPromotoresCliente.php',
      {
        nomes: promo.PRO_Nome,
        fromnome: true
      },
      {}
    ).then( (response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104'){
        setTimeout( () => {
          let modal = this._modal.create(JobsClientePromotorPage, {entrada: true, content: response.data.result, PRO_Nome: promo.PRO_Nome});
          modal.present();
        }, 500);
      }
    });
  }

  searchPromotor(string){
    var regexp = new RegExp((".*"+string.split("").join('.*')+ ".*"), "i");
    this.resultsPromo = this.promos.filter( (e) => {
      return regexp.test(e.PRO_Nome);
    });
  }

  restorePro(ev: KeyboardEvent, string){
    if (ev.which == 8){
      if (string.length <= 2 || string.length == 0){
        this.promos = [];
        this.ngOnInit();
      }
    }
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JobInfosPage } from './job-infos';

@NgModule({
  declarations: [
    JobInfosPage,
  ],
  imports: [
    IonicPageModule.forChild(JobInfosPage),
  ],
})
export class JobInfosPageModule {}

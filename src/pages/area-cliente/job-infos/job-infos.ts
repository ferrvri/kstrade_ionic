import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { DatePicker } from '@ionic-native/date-picker';
import { Platform } from 'ionic-angular/platform/platform';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the JobInfosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var window: any;

@IonicPage()
@Component({
  selector: 'page-job-infos',
  templateUrl: 'job-infos.html',
})
export class JobInfosPage implements OnInit{

  // map: GoogleMap;
  photos = [];

  constructor(
    public platform: Platform,  
    private _datePicker: DatePicker, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP) {
  }

  ngOnInit(){
    if (this.navParams.get('type') == 'photo'){
      //console.log(this.navParams.get('obs'));
      this.photos.push(this.navParams.get('job_foto'));
      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/selectFotosExtras.php',
        {
          job_id: this.navParams.get('job_id')
        },
        {}
      ).then( (response) => {
        response.data = JSON.parse(response.data)
        if (response.data.status == '0x104'){
          response.data.result.forEach(element => {
            this.photos.push(element.JFOT_Foto);
          });
        }
      });
    }else if (this.navParams.get('type') == 'local'){
      setTimeout( () => {
        this.loadMap(this.navParams.get('lat'), this.navParams.get('long'));
      }, 500);
    }
  }

  loadMap(lat, lon) {
    if (lat != 'nada' && lon != 'nada'){
      var myLatlng = new window.google.maps.LatLng(lat,lon);
      
      var map = new window.google.maps.Map(document.getElementById('map_canvas'), {
          center: myLatlng,
          zoom: 17
      });
  
      var marker = new window.google.maps.Marker({
          position: myLatlng,
          map: map
      });
    }
  }

  goBack(){
    this.navCtrl.pop();
  }
}

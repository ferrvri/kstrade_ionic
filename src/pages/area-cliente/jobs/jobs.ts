import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { JobInfosPage } from '../job-infos/job-infos';
import { DatePicker } from '@ionic-native/date-picker';
import { Platform } from 'ionic-angular/platform/platform';
import { CoordJobPage } from '../../coord-job/coord-job';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the JobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-jobs',
  templateUrl: 'jobs.html',
})
export class JobsPage implements OnInit {

  jobs: any = [];

  
  data_inicio = {
    value: '2018-02-01',
    showValue: 'Data inicial',
    selected: false
  };

  data_fim = {
    showValue: 'Data final',
    value: '2018-10-01',
    selected: false
  }

  userData;

  constructor(
    public platform: Platform,  
    private _alert: AlertController, 
    private _datePicker: DatePicker, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP, 
    private _modal: ModalController) {
  }

  ngOnInit(){
    let type = typeof this.navParams.get('cli_id');
    this.userData = JSON.parse(localStorage.getItem('session'));
    console.log(this.userData)
    if ( type === 'object' ){
      //console.log('is obj', this.navParams.get('cli_id'));
      this.navParams.get('cli_id').forEach(element => {
        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/cliente/selectJobs.php',
          {
            id:  JSON.parse(localStorage.getItem('session')).clientes !== undefined ?
            element.aux_cliente_id:
            element.Cli_ID,
            rede_id: this.navParams.get('red_id'),
            rfil_id: this.navParams.get('rfil_id'),
            dataone: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 7).toISOString().substring(0,10),
            datatwo: new Date(new Date().getFullYear(),new Date().getMonth(), new Date().getDate()).toISOString().substring(0,10)
          },
          {}
        ).then( (response) => {
          response.data = JSON.parse(response.data)
          if(response.data.status == '0x104'){
            response.data.result.forEach( (e) => {
              this.jobs.push(e);
            });
          }
        });
      });
    } else {
      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/cliente/selectJobs.php',
        {
          id: this.navParams.get('cli_id'),
          rede_id: this.navParams.get('red_id'),
          rfil_id: this.navParams.get('rfil_id'),
          dataone: new Date(new Date().getFullYear(), new Date().getMonth() , 1).toISOString().substring(0,10),
          datatwo: new Date(new Date().getFullYear(),new Date().getMonth() +1, 0).toISOString().substring(0,10)
        },
        {}
      ).then( (response) => {
        response.data = JSON.parse(response.data)
        if(response.data.status == '0x104'){
          response.data.result.forEach( (e) => {
            this.jobs.push(e);
          });
        }
      });
    }
  }

  goBack(){
    this.navCtrl.pop();
  }

  openInfos(type, j){
    switch(type){
      case 'photo':{
        //console.log('job', j);
        let a = this._modal.create(JobInfosPage, {
          type: 'photo',
          job_id: j.JOB_ID,
          job_foto: j.JOB_Foto,
          obs: j.JOB_Obs
        });
        a.present();
      }
      break;
      case 'local':{
        let a = this._modal.create(JobInfosPage, {
          type: 'local',
          long: j.JOB_Longitude !== null ? j.JOB_Longitude:'nada', 
          lat: j.JOB_Latitude !== null ? j.JOB_Latitude:'nada'
        });
        a.present();
      }
      break;
      case 'infos':{
        let a = this._modal.create(JobInfosPage, {
          type: 'infos',
          modelo: j.JOB_PRO_Modelo,
          plataforma: j.JOB_PRO_Plataforma,
          versao: j.JOB_PRO_Versao,
          manufact: j.JOB_PRO_Manufact,
          obs: j.JOB_Obs
        });
        a.present();
      }
      break;
    }
  }


  showDate(obj){
    this._datePicker.show({
      allowFutureDates: true,
      date: new Date(),
      mode: 'date',
      androidTheme: this._datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
    }).then( (date) => {
      obj.selected = true;
      obj.value = date.toISOString().split('T')[0];
      obj.showValue = date.toISOString().split('T')[0].split('-')[2] +'/'+
      date.toISOString().split('T')[0].split('-')[1] +'/'+
      date.toISOString().split('T')[0].split('-')[0] 
    }, (err) => {
      obj.selected = false;
    });
  }

  searchJobs(init, end){
    this.jobs = [];
    if (init.length < 1 || end.length < 1){
      let a = this._alert.create({
        title: 'Selecione uma data!',
        subTitle: 'Para utilizar o filtro, deve-se escolher a Data inicial e Data final'
      });
      a.present();
    }else{
      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/cliente/selectJobs.php',
        {
          id: this.navParams.get('cli_id'),
          rede_id: this.navParams.get('red_id'),
          rfil_id: this.navParams.get('rfil_id'),
          dataone: init,
          datatwo: end
        },
        {}
      ).then( (response) => {
        response.data = JSON.parse(response.data)
        if(response.data.status == '0x104'){
          response.data.result.forEach( (e) => {
            this.jobs.push(e);
          });
        }
      });
    }
  }


  insertFeedback(){
    this._modal.create(CoordJobPage, {
      Usr_ID: this.userData.user.Usr_ID,
      rede_id: this.navParams.get('red_id'),
      rfil_id: this.navParams.get('rfil_id'),
      rede_nome: this.navParams.get('red_nome'),
      rfil_nome: this.navParams.get('rfil_nome')
    }).present()
  }

}

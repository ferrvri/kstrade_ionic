import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClienteFiliaisPage } from './cliente-filiais';

@NgModule({
  declarations: [
    ClienteFiliaisPage,
  ],
  imports: [
    IonicPageModule.forChild(ClienteFiliaisPage),
  ],
})
export class ClienteFiliaisPageModule {}

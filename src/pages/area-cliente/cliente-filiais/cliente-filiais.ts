import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Http } from '@angular/http';
import { JobsPage } from '../jobs/jobs';
import { Platform } from 'ionic-angular/platform/platform';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the ClienteFiliaisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cliente-filiais',
  templateUrl: 'cliente-filiais.html',
})
export class ClienteFiliaisPage implements OnInit{

  red_id: number = 0;
  red_nome: string = '';

  filiais: any = [];

  doneloading = false;

  constructor(
    public platform: Platform,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP, 
    private _modal: ModalController) {
  }

  ngOnInit(){
    this.red_id = this.navParams.get('RED_ID');
    this.red_nome = this.navParams.get('RED_Nome');

    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/cliente/selectFilial.php',
      {
        id: this.red_id
      },
      {}
    ).then( (response2) => {
      response2.data = JSON.parse(response2.data)
      if (response2.data.status == '0x104'){
        response2.data.result.forEach(element => {
          this.filiais.push(element)
        });
        setTimeout( () => {
          this.doneloading = true;
        }, 1500);
      }
    }); 
  }

  goBack(){
    this.navCtrl.pop();
  }

  seeJobs(job){
    let cli_id;
    if (JSON.parse(localStorage.getItem('session')).clientes !== undefined && JSON.parse(localStorage.getItem('session')).clientes.length > 0){
      cli_id = JSON.parse(localStorage.getItem('session')).clientes[0].aux_cliente_id;
    }else{
      cli_id = JSON.parse(localStorage.getItem('session')).redes[0].aux_cliente_id;
    }
    let mo = this._modal.create(JobsPage, {
      red_id: this.red_id,
      rfil_id: job.RFIL_ID,
      rfil_nome: job.RFIL_Nome,
      red_nome: job.RED_Nome,
      cli_id:  cli_id
    });
    mo.present();
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JobsClientePromotorPage } from './jobs-cliente-promotor';

@NgModule({
  declarations: [
    JobsClientePromotorPage,
  ],
  imports: [
    IonicPageModule.forChild(JobsClientePromotorPage),
  ],
})
export class JobsClientePromotorPageModule {}

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Http } from '@angular/http';
import { ViewPhotoPage } from '../../view-photo/view-photo';
import { JobInfosPage } from '../job-infos/job-infos';
import { Platform } from 'ionic-angular/platform/platform';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the JobsClientePromotorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-jobs-cliente-promotor',
  templateUrl: 'jobs-cliente-promotor.html',
})
export class JobsClientePromotorPage implements OnInit{

  table = [];
  entradas = [];

  showJob = true;
  showEntrada = false;

  constructor(
    public platform: Platform, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP, 
    private _modal: ModalController ) {
  }

  ngOnInit(){
    let mes = new Date().getMonth()+1;

    this.navParams.get("content").forEach(element => {
      element.forEach(elm => {
        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectJobsOf.php',
          {
            myid: elm.Aux_Promotor_ID,
            rede_id: elm.Aux_Rede_ID,
            filial_id: elm.Aux_Filial_ID,
            dataone: 
            new Date().getFullYear() + '-' +
            (mes.valueOf() < 10 ? '0'+mes.toString(): mes.toString()) + '-' +
            new Date().getDate()
            // dataone: '2018-04-10'
          },
          {}
        ).then( (response) => {
          response.data = JSON.parse(response.data)
          if (response.data.status == '0x104'){
            let o = {
              rede_nome: '',
              filial_nome: '',
              content: []
            }

            if (response.data.result.length> 0){
              o.rede_nome = response.data.result[0].RED_Nome || '';
              o.filial_nome = response.data.result[0].RFIL_Nome || '';
              o.content = response.data.result;
            }

            this.table.push(o);
          }
        });
      });
    });

    if (this.navParams.get('entrada') == true){
      this.navParams.get("content").forEach(element => {
        element.forEach(elm => {
          this._http.post(
            'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectEntradaOf.php',
            {
              pro_id: elm.Aux_Promotor_ID,
              rede_id: elm.Aux_Rede_ID,
              rfil_id: elm.Aux_Filial_ID,
              dataone:
              new Date().getFullYear() + '-' +
              (mes.valueOf() < 10 ? '0'+mes.toString(): mes.toString()) + '-' +
              new Date().getDate()
            },
            {}
          ).then( (response) => {
            response.data = JSON.parse(response.data)
            if (response.data.status == '0x104'){
              let o = {
                rede_nome: '',
                filial_nome: '',
                content: []
              }
  
              //console.log(response);
              if (response.data.result.length > 0){
                o.rede_nome = response.data.result[0].RED_Nome || '';
                o.filial_nome = response.data.result[0].RFIL_Nome || '';
                o.content = response.data.result;
              }
  
              this.entradas.push(o);
              //console.log(this.entradas);
            }
          });
        });
      });
    }
  }

  goBack(){
    this.navCtrl.pop();
  }

  seePhoto(type, j){
    switch(type){
      case 'photo':{
        let a = this._modal.create(JobInfosPage, {
          type: 'photo',
          job_id: j.JOB_ID,
          job_foto: j.JOB_Foto,
          obs: j.JOB_Obs
        });
        a.present();
      }
      break;
      case 'local':{
        let a = this._modal.create(JobInfosPage, {
          type: 'local',
          long: j.JOB_Longitude !== null ? j.JOB_Longitude:'nada', 
          lat: j.JOB_Latitude !== null ? j.JOB_Latitude:'nada'
        });
        a.present();
      }
      break;
      case 'infos':{
        let a = this._modal.create(JobInfosPage, {
          type: 'infos',
          modelo: j.JOB_PRO_Modelo,
          plataforma: j.JOB_PRO_Plataforma,
          versao: j.JOB_PRO_Versao,
          manufact: j.JOB_PRO_Manufact,
          obs: j.JOB_Obs
        });
        a.present();
      }
      break;
    }
  }

  setOption(op){
    switch(op){
      case 'job': {
        this.showJob = !this.showJob;
        this.showEntrada = false;
        break;
      }

      case 'entrada': {
        this.showEntrada = !this.showEntrada;
        this.showJob = false;
        break;
      }
    }
  }

}

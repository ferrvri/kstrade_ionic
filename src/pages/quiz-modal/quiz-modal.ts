import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import { PlatformLocation } from '@angular/common';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the QuizModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quiz-modal',
  templateUrl: 'quiz-modal.html',
})
export class QuizModalPage implements OnInit {

  questionTime:string = '';
  questionTitle:string = '';
  options: any = [];

  alreadyAnsw = false;

  constructor(
    public platform: Platform,  
    private _platform: Platform, 
    private _alert: AlertController, 
    private _view: ViewController, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP ) {
    this._platform.registerBackButtonAction(() => {
      //console.log('u cant return');
      return;
    },1);
  }

  ngOnInit(){
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php',
      {
        unique: true,
        id: this.navParams.get('per_id')
      },
      {}
    ).then( (response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104'){
        this.questionTime = response.data.result[0].PER_Tempo;
        this.questionTitle = response.data.result[0].PER_Title;
        
        response.data.result[0].PER_Text.split(';').forEach(element => {
          this.options.push(element);
        });

        if (this._view.readReady){
          this.tick(parseInt(this.questionTime));
        }
      }
    });
  }

  tick(time){
    if (time > 0){
      setTimeout( () => {
        this.questionTime = ''+ (time-1);
        this.tick(time-1);
      }, 1000)
    }else if ((time == 0 || time < 0 ) && this.alreadyAnsw == false){
      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/insertQuizResposta.php',
        {
          timed: true,
          per_id: this.navParams.get('per_id'),
          usr_id: JSON.parse(localStorage.getItem('session'))[0].PRO_ID
        },
        {}
      ).then( (response) => {
        response.data = JSON.parse(response.data)
        if (response.data.status == '0x104'){
          let a = this._alert.create({
            title: 'KSTrade Merchandising',
            subTitle: 'Você não respondeu a questão a tempo!'
          });
          a.present();
          a.onDidDismiss( () => {
            this._view.dismiss();
          });
        }
      });
    }
  }

  setOption(op){
    let c = this._alert.create({
      title: 'KSTrade Merchandising',
      subTitle: 'Confirmar?',
      buttons: [{
        text: 'OK',
        handler: () => {
          let id = JSON.parse(localStorage.getItem('session'))[0] !== undefined && JSON.parse(localStorage.getItem('session'))[0].PRO_ID != undefined ? JSON.parse(localStorage.getItem('session'))[0].PRO_ID:JSON.parse(localStorage.getItem('session')).user.Usr_ID;
          this.alreadyAnsw = true;
          this._http.post(
            'https://kstrade.com.br/sistema/assets/kstrade_php/insertQuizResposta.php',
            {
              per_id: this.navParams.get('per_id'),
              usr_id: id,
              option: op,
              time: this.questionTime
            },
            {}
          ).then( (response) => {
            response.data = JSON.parse(response.data)
            if (response.data.status == '0x104'){
              let a = this._alert.create({
                title: 'KSTrade Merchandising',
                subTitle: 'Enviado!'
              });
              a.present();
              a.onDidDismiss(() => {
                this._view.dismiss();
              });
            }else{
              let a = this._alert.create({
                title: 'KSTrade Merchandising',
                subTitle: 'Problemas ao enviar sua resposta, por favor contate-nos'
              });
              a.present();
              a.onDidDismiss(() => {
                this._view.dismiss();
              });
            }
          });
        }
      },{
        text: 'Cancelar',
        handler: () => {
          this.alreadyAnsw = false;
        }
      }]
    });
    c.present();
  }
}

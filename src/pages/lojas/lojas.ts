import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, App, ViewController, Platform } from 'ionic-angular';
import { MainPage } from '../main/main';
import { Http } from '@angular/http';
import { NewJobModalPage } from '../new-job-modal/new-job-modal';
import { LoginPage } from '../login/login';
import { MyApp } from '../../app/app.component';
import { EntradaSaidaJobPage } from '../entrada-saida-job/entrada-saida-job';
import { HTTP } from '@ionic-native/http';

@IonicPage({ name: 'lojas' })
@Component({
  selector: 'page-lojas',
  templateUrl: 'lojas.html',
})
export class LojasPage implements OnInit {

  menu: Array<Object> = [
    { title: 'Inicio', icon: 'home', media: 'main' },
    { title: 'Lojas', icon: 'meeting_room', media: 'lojas' },
    { title: 'Tarefas', icon: 'ballot', media: 'home' },
    { title: 'Jobs realizados', icon: 'group_work', media: 'home' },
    { title: 'Sair', icon: 'exit_to_app', action: 'logout' }
  ];

  redes: any = [];

  totalJobs: number = 0;

  doneLoading: boolean = false;

  constructor(
    public platform: Platform,
    public _alert: AlertController,
    public viewCtrl: ViewController,
    public app: App, public navCtrl: NavController,
    public navParams: NavParams,
    private _http: HTTP,
    public modalCtrl: ModalController) {
  }

  ngOnInit() {
    if (localStorage.getItem('session')) {
      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectRotina2_.php',
        {
          content: JSON.parse(localStorage.getItem('session'))[0].content
        },
        {}
      ).then((response) => {
        console.log(response)
        response.data = JSON.parse(response.data)
        if (response.data.status == '0x104') {
          this.redes = response.data.result;
        }
      }, () => {
        let userData = JSON.parse(localStorage.getItem('session'))[0];
        userData.content.forEach((e, index) => {
          let o = {
            RFIL_ID: e.RFIL_ID,
            RFIL_Nome: e.RFIL_Nome,
            RED_ID: e.RED_ID,
            RED_Nome: e.RED_Nome,
            RFIL_Bairro: e.RFIL_Bairro,
            RFIL_CEP: e.RFIL_CEP,
            RFIL_Cidade: e.RFIL_Cidade,
            RFIL_Endereco: e.RFIL_Endereco,
            RFIL_Estado: e.RFIL_Estado,
            RFIL_Numero: e.RFIL_Numero,
            RFIL_Telefone: e.RFIL_Telefone,
            RFIL_Numero_Visitas: 0,
            RFIL_Nova_Entrada: true,
            RFIL_Nova_Saida: false,
            RFIL_All_Done: false,
            cliente: [],
            visible: true,
            PRO_Nome: JSON.parse(localStorage.getItem('session'))[0].content[0].PRO_Nome,
            PRO_ID: e.Aux_Promotor_ID
          }

          userData.clientes.forEach(element => {
            if (element.aux_rede_id == e.RED_ID && element.aux_rede_filial == e.RFIL_ID) {
              let days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
              let dayName = days[new Date().getDay()];
              let elem = element.aux_semana.split(';');
              elem.forEach(element2 => {
                if (element2 == dayName) {
                  o.cliente.push(element);
                  o.RFIL_Numero_Visitas++;
                }
              });
            }
          });

          this.redes.push(o);

          if (index == (userData.content.length - 1)) {
            this.doneLoading = true
          }
        });
      });
    }
  }

  logOut() {
    if (localStorage.getItem('session')) {
      localStorage.removeItem('session');
      this.viewCtrl.dismiss();
      this.app.getRootNav().push(LoginPage);
    }
  }

  goBack() {
    this.navCtrl.pop();
  }

  newJob(rede) {
    if (rede.RFIL_Nova_Entrada == true && rede.RFIL_Nova_Saida == false && rede.RFIL_Numero_Visitas > 0) {
      let modal = this.modalCtrl.create(EntradaSaidaJobPage, { kind: 'entrada', content: rede });
      modal.present();
      modal.onDidDismiss((data) => {
        if (data.done && data.done == true) {
          this.redes = [];
          this.doneLoading = false;
          this.ngOnInit();
        }
      });
    } else if (rede.RFIL_Nova_Saida == true && rede.RFIL_Nova_Entrada == false && rede.RFIL_Numero_Visitas < 1) {
      let modal = this.modalCtrl.create(EntradaSaidaJobPage, { kind: 'saida', content: rede });
      modal.present();
      modal.onDidDismiss(() => {
        this.redes = [];
        this.doneLoading = false;
        this.ngOnInit();
      });
    } else if (rede.RFIL_Numero_Visitas > 0 && rede.cliente.length > 0 && rede.RFIL_Nova_Entrada == false && rede.RFIL_Nova_Saida == false) {
      rede.menuRow = false;
      let modal = this.modalCtrl.create(NewJobModalPage, rede);
      setTimeout(() => {
        modal.present();
      }, 150);
      modal.onDidDismiss((data) => {
        if (data.job) {
          if (data.job == 'done') {
            this.doneLoading = false;
            this.redes = [];
            this.ngOnInit();
          }
        }
      });
    } else {
      return
    }
  }

  newJobOFF(rede) {
    rede.offline = true
    let modal = this.modalCtrl.create(NewJobModalPage, rede);
    setTimeout(() => {
      modal.present();
    }, 150);
  }

}

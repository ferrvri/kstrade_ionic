import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the EsqueceuSenhaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-esqueceu-senha',
  templateUrl: 'esqueceu-senha.html',
})
export class EsqueceuSenhaPage {

  email: string = '';

  constructor(
    public platform: Platform, 
    public viewCtrl: ViewController, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private _http: HTTP, 
    public alertCtrl: AlertController) {
  }

  recuperar(){
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/app/forgotSenha.php',
      {
        email: this.email
      },
      {}
    ).then( (response) => {
      if (response.status == 200){
        let a = this.alertCtrl.create( {
          title: 'Sucesso!',
          subTitle: 'Um e-mail foi enviado a você verifique a caixa de entrada',
          buttons: ['Ok']
        } );
        a.present();
        this.viewCtrl.dismiss();
      }else{
        let a = this.alertCtrl.create( {
          title: 'Erro',
          subTitle: 'Ocorreu um erro ao enviar o e-mail, por favor tente novamente!',
          buttons: ['Ok']
        } );
        a.present();
      }
    });
  }


}

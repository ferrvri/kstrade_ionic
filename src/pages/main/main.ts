import { Component, OnInit, Injectable } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ViewController, AlertController, LoadingController, ModalController, Platform } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Http } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';
import { QuizModalPage } from '../quiz-modal/quiz-modal';
import { ViewPhotoPage } from '../view-photo/view-photo';
import md5 from 'blueimp-md5';

// import { AdMobFreeBannerConfig, AdMobFree } from '@ionic-native/admob-free';

import { Md5 } from 'ts-md5/dist/md5';
import { ViewNewCompletePage } from '../view-new-complete/view-new-complete';

import { Keyboard } from '@ionic-native/keyboard'
import { GreetingsPage } from '../greetings/greetings';
import { HTTP } from '@ionic-native/http';

declare function escape(s: string);

// const bannerConfig: AdMobFreeBannerConfig = {
//   id: 'ca-app-pub-6624972784895874/3496159241',
//   isTesting: false,
//   autoShow: true
// };

@Injectable()
@IonicPage({
  name: 'main'
})
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage implements OnInit {

  md5 = new Md5();
  myIds = [];
  myLastTime: number = 0;

  cards = [];

  firstRun = true;
  status: any = null;

  noticia = {
    grupos: []
  }

  menu: Array<Object> = [
    { title: 'Inicio', icon: 'home', media: 'main' },
    { title: 'Lojas', icon: 'meeting_room', media: 'lojas' },
    { title: 'Jobs do dia', icon: 'playlist_add_check', media: 'jobsDoDia' },
    { title: 'Datas críticas', icon: 'warning', media: 'criticDate' },
    // {title: 'Jobs realizados', icon: 'group_work', media: 'home'},
    { title: 'Market Share', icon: 'add_shopping_cart', media: 'marketShare', param: { forPromotor: true } },
    { title: 'Sair', icon: 'exit_to_app', action: 'logout' }
  ];

  constructor(public platform: Platform,
    private _platform: Platform,
    private _alert: AlertController,
    private _sanitization: DomSanitizer,
    private _http: HTTP,
    public navCtrl: NavController,
    public navParams: NavParams,
    public app: App,
    public viewCtrl: ViewController,
    public _modal: ModalController,
    public loadingCtrl: LoadingController,
    private _keyboard: Keyboard,
    // private _ad: AdMobFree
  ) {

    this._platform.registerBackButtonAction(() => {
      //console.log('u cant return');
      return;
    }, 1);
  }

  ngOnInit() {
    if (!localStorage.getItem('session')) {
      this.navCtrl.setRoot(LoginPage);
    } else {
      // this._ad.banner.config(bannerConfig);

      // this._ad.banner.prepare()
      //   .then(() => {
      //     this._ad.banner.show();
      //   })
      //   .catch(e =>
      //     console.log(e)
      //   );

      // this._keyboard.onKeyboardShow().subscribe((data) => {
      //   this._ad.banner.hide();
      // })

      // this._keyboard.onKeyboardHide().subscribe((data) => {
      //   this._ad.banner.show()
      // })

      // this._keyboard.onKeyboardWillShow().subscribe((data) => {
      //   this._ad.banner.hide();
      // })

      // this._keyboard.onKeyboardWillHide().subscribe((data) => {
      //   this._ad.banner.show()
      // })

      if (JSON.parse(localStorage.getItem('session')).visitante !== undefined) {
        this._modal.create(GreetingsPage, {}).present();
        this.menu = [
          { title: 'Inicio', icon: 'home', media: 'main' },
          { title: 'Lojas', icon: 'group', media: 'LojasVisitante' },
          { title: 'Entrar', icon: 'work', media: 'login' }
        ];

        let r = []
        let final_hash = '';
        let temp_pattern = '';

        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php',
          {
            is_guest: true
          },
          {}
        ).then((response3) => {
          response3.data = JSON.parse(response3.data)
          if (response3.data.status == '0x104') {
            this.firstRun = false;
            if (this.noticia.grupos.length < response3.data.result.length) {
              response3.data.result.forEach((element, index) => {
                temp_pattern += element.content.length + '?' + response3.data.result.length
                element.activated = false;

                element.content.forEach(ele => {
                  ele.app_title = this.decode_utf8(ele.app_title);
                  ele.app_text = this.decode_utf8(ele.app_text);
                  ele.image = ele.app_image;
                  ele.app_image = this._sanitization.bypassSecurityTrustStyle(`url(https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/${ele.app_image}) center / cover`);
                });
                if (index == (response3.data.result.length - 1)) {
                  element.activated = true;
                }
                r.push(element);
                this.menu.push({ customized: true, title: element.group_nome, icon: '', action: '', news: true, group_id: element.group_id });
                if (index == (response3.data.result.length - 1)) {
                  if (JSON.parse(localStorage.getItem('session')).visitante.Usr_Nome != 'Temporario') {
                    this.menu.push({ title: 'Sair', icon: 'exit_to_app', action: 'logout' });
                  }
                }
              });
              final_hash = md5(temp_pattern);
              if (!localStorage.getItem('visit_new_hash')) {
                localStorage.setItem('visit_new_hash', final_hash);
              } else if (localStorage.getItem('visit_new_hash') != final_hash) {
                localStorage.setItem('visit_new_hash', final_hash);
              }
            }
          }
        });
      } else if (JSON.parse(localStorage.getItem('session')).user !== undefined) {

        if (JSON.parse(localStorage.getItem('session')).user.Usr_Nivel == 'Vendedor') {
          // this._ad.banner.hide();
          this.menu = [
            { title: 'Inicio', icon: 'home', media: 'main' },
            { title: 'Lojas', icon: 'meeting_room', media: 'clienteLojas' },
            { title: 'Pesquisas', icon: 'bookmark', media: 'clientePesquisa', param: { forVendedor: true } },
            { title: 'Market Share', icon: 'add_shopping_cart', media: 'marketShare', param: { forVendedor: true } },
            { title: 'Sair', icon: 'exit_to_app', action: 'logout' }
          ];

          this._http.post(
            'https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php',
            {
              tipo: 2
            },
            {}
          ).then((response) => {
            response.data = JSON.parse(response.data)
            if (response.data.status == '0x104') {
              this._http.post(
                'https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php',
                {
                  already: true,
                  per_id: response.data.result[0].PER_ID,
                  usr_id: JSON.parse(localStorage.getItem('session')).user.Usr_ID
                },
                {}
              ).then((response2) => {
                if (response2.data.status != '0x102') {
                  let days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                  let dayName = days[new Date().getDay()];

                  response.data.result.forEach(element => {
                    let elem = element.PER_Dias_Semana.split(';');
                    elem.forEach(element2 => {
                      if (element2 == dayName) {
                        let arr = [];
                        arr.push(element.PER_ID);
                        let modal = this._modal.create(QuizModalPage, {
                          per_id: element.PER_ID,
                          pergunta: element.PER_Title,
                          opcoes: element.PER_Text
                        });
                        modal.present();
                      }
                    });
                  });
                }
              });
            }
          });
        } else if (JSON.parse(localStorage.getItem('session')).user.Usr_Nivel == 'Externo') {
          // this._ad.banner.hide();
          this.menu = [
            { title: 'Inicio', icon: 'home', media: 'main' },
            { title: 'Lojas', icon: 'meeting_room', media: 'clienteLojas' },
            { title: 'Promotores', icon: 'group', media: 'clientePromotores' },
            { title: 'Pesquisas', icon: 'bookmark', media: 'clientePesquisa', param: { forExterno: true } },
            { title: 'Market Share', icon: 'add_shopping_cart', media: 'marketShare' },
            { title: 'Sair', icon: 'exit_to_app', action: 'logout' }
          ];

          this._http.post(
            'https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php',
            {
              tipo: 1
            },
            {}
          ).then((response) => {
            response.data = JSON.parse(response.data)
            if (response.data.status == '0x104') {
              this._http.post(
                'https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php',
                {
                  already: true,
                  per_id: response.data.result[0].PER_ID,
                  usr_id: JSON.parse(localStorage.getItem('session')).user.Usr_ID
                },
                {}
              ).then((response2) => {
                if (response2.data.status != '0x102') {
                  let days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                  let dayName = days[new Date().getDay()];

                  response.data.result.forEach(element => {
                    let elem = element.PER_Dias_Semana.split(';');
                    elem.forEach(element2 => {
                      if (element2 == dayName) {
                        let arr = [];
                        arr.push(element.PER_ID);
                        let modal = this._modal.create(QuizModalPage, {
                          per_id: element.PER_ID,
                          pergunta: element.PER_Title,
                          opcoes: element.PER_Text
                        });
                        modal.present();
                      }
                    });
                  });
                }
              });
            }
          });
        } else if (JSON.parse(localStorage.getItem('session')).user.Usr_Nivel == 'Representante') {
          this.menu = [
            { title: 'Inicio', icon: 'home', media: 'main' },
            { title: 'Lojas', icon: 'meeting_room', media: 'clienteLojas', param: { redes: JSON.parse(localStorage.getItem('session')).metas } },
            { title: 'Pedidos', icon: 'check_box', media: 'pedidos' },
            { title: 'Metas', icon: 'insert_chart_outlined', media: 'metas' },
            { title: 'Sair', icon: 'exit_to_app', action: 'logout' }
          ];

          this._http.post(
            'https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php',
            {
              tipo: 1
            },
            {}
          ).then((response) => {
            response.data = JSON.parse(response.data)
            if (response.data.status == '0x104') {
              this._http.post(
                'https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php',
                {
                  already: true,
                  per_id: response.data.result[0].PER_ID,
                  usr_id: JSON.parse(localStorage.getItem('session')).user.Usr_ID
                },
                {}
              ).then((response2) => {
                if (response2.data.status != '0x102') {
                  let days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                  let dayName = days[new Date().getDay()];

                  response.data.result.forEach(element => {
                    let elem = element.PER_Dias_Semana.split(';');
                    elem.forEach(element2 => {
                      if (element2 == dayName) {
                        let arr = [];
                        arr.push(element.PER_ID);
                        let modal = this._modal.create(QuizModalPage, {
                          per_id: element.PER_ID,
                          pergunta: element.PER_Title,
                          opcoes: element.PER_Text
                        });
                        modal.present();
                      }
                    });
                  });
                }
              });
            }
          });
        }
      } else {
        // this.loginCheck()

        let lasId = 0;
        JSON.parse(localStorage.getItem('session')).forEach((element) => {
          element.content.forEach((element) => {
            if (element.PRO_ID && lasId != element.PRO_ID) {
              this.myIds.push(element.PRO_ID);
              lasId = element.PRO_ID;
            }
          });
        });

        let myIds = []
        JSON.parse(localStorage.getItem('session')).forEach(element => {
          myIds.push(element.PRO_ID);
        });

        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php',
          {
            tipo: 0
          },
          {}
        ).then((response) => {
          response.data = JSON.parse(response.data)
          if (response.data.status == '0x104') {
            this._http.post(
              'https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php',
              {
                already: true,
                per_id: response.data.result[0].PER_ID,
                pro_id: myIds
              },
              {}
            ).then((response2) => {
              if (response2.data.status != '0x102') {
                let days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                let dayName = days[new Date().getDay()];

                response.data.result.forEach(element => {
                  let elem = element.PER_Dias_Semana.split(';');
                  elem.forEach(element2 => {
                    if (element2 == dayName) {
                      let arr = [];
                      arr.push(element.PER_ID);
                      let modal = this._modal.create(QuizModalPage, {
                        per_id: element.PER_ID,
                        pergunta: element.PER_Title,
                        opcoes: element.PER_Text
                      });
                      modal.present();
                    }
                  });
                });
              }
            });
          }
        });
      }

      this.noticia.grupos = [];
      // Noticias
      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php',
        {},
        {}
      ).then((response) => {
        console.log('ok resp')
        response.data = JSON.parse(response.data)
        console.log('response news', response)
        if (response.data.status == '0x104') {
          response.data.result.forEach(element => {
            element.image = element.app_image;
            element.app_title = this.decode_utf8(element.app_title);
            element.app_text = this.decode_utf8(element.app_text);
            element.app_image = this._sanitization.bypassSecurityTrustStyle(`url('https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/${element.app_image}') center / cover`);
            this.cards.push(element);
          });
        }

        if (JSON.parse(localStorage.getItem('session'))[0] !== undefined &&
          JSON.parse(localStorage.getItem('session'))[0].PRO_ID !== undefined) {

          this._http.post(
            'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectClientePromotor.php',
            {
              pro_id: JSON.parse(localStorage.getItem('session'))[0].PRO_ID
            },
            {}
          ).then((response2) => {
            response2.data = JSON.parse(response2.data)
            if (response2.data.status == '0x104') {
              //console.log(response2.data);
              response2.data.result.forEach(element => {
                this._http.post(
                  'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php',
                  {
                    cliente: element.aux_cliente_id
                  },
                  {}
                ).then((response3) => {
                  response3.data = JSON.parse(response3.data)
                  if (response3.data.status == '0x104') {
                    response3.data.result.forEach((element, index) => {
                      element.activated = false;
                      element.content.forEach(ele => {
                        ele.app_title = this.decode_utf8(ele.app_title);
                        ele.app_text = this.decode_utf8(ele.app_text);
                        ele.image = ele.app_image;
                        ele.app_image = this._sanitization.bypassSecurityTrustStyle(`url(https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/${ele.app_image}) center / cover`);
                      });
                      if (index == (response3.data.result.length - 1)) {
                        element.activated = true;
                      }
                      this.noticia.grupos.push(element);
                    });
                    //console.log('1', this.noticia.grupos);
                  }
                });
              });
            }
          });
        } else if (JSON.parse(localStorage.getItem('session')).user !== undefined) {
          if (JSON.parse(localStorage.getItem('session')).user.Usr_Nivel == 'Externo' || JSON.parse(localStorage.getItem('session')).user.Usr_Nivel == 'Representante') {
            JSON.parse(localStorage.getItem('session')).clientes.forEach(element => {
              this._http.post(
                'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php',
                {
                  cliente: element.Cli_ID
                },
                {}
              ).then((response2) => {
                response2.data = JSON.parse(response2.data)
                if (response2.data.status == '0x104') {
                  response2.data.result.forEach((element, index) => {
                    element.activated = false;
                    element.content.forEach(ele => {
                      ele.app_title = this.decode_utf8(ele.app_title);
                      ele.app_text = this.decode_utf8(ele.app_text);
                      ele.image = ele.app_image;
                      ele.app_image = this._sanitization.bypassSecurityTrustStyle(`url(https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/${ele.app_image}) center / cover`);
                    });

                    if (index == (response2.data.result.length - 1)) {
                      element.activated = true;
                    }

                    this.noticia.grupos.push(element);
                  });
                  //console.log('2', this.noticia.grupos);
                }
              });
            });
          } else {
            this._http.post(
              'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php',
              {
                cliente: JSON.parse(localStorage.getItem('session')).redes[0].aux_cliente_id
              },
              {}
            ).then((response2) => {
              response2.data = JSON.parse(response2.data)
              if (response2.data.status == '0x104') {
                response2.data.result.forEach((element, index) => {
                  element.activated = false;
                  element.content.forEach(ele => {
                    ele.app_title = this.decode_utf8(ele.app_title);
                    ele.app_text = this.decode_utf8(ele.app_text);
                    ele.image = ele.app_image;
                    ele.app_image = this._sanitization.bypassSecurityTrustStyle(`url(https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/${ele.app_image}) center / cover`);
                  });

                  if (index == (response2.data.result.length - 1)) {
                    element.activated = true;
                  }

                  this.noticia.grupos.push(element);
                });
                //console.log('2', this.noticia.grupos);
              }
            });
          }
        }
      });
    }

    let temp_jobs = JSON.parse(localStorage.getItem('temp_jobs')) || [];
    let temp_entradas = JSON.parse(localStorage.getItem('temp_entradas')) || [];
    let temp_saidas = JSON.parse(localStorage.getItem('temp_saidas')) || [];

    if ((temp_jobs.length > 0 || temp_entradas.length > 0 || temp_saidas.length > 0) && localStorage.getItem('session')) {
      let a = this._alert.create({
        title: 'Sincronizar informaçãoes?',
        message: 'Você possui ' + (temp_entradas.length + temp_saidas.length + temp_jobs.length) + ' itens para sincronizar, deseja iniciar?',
        buttons: [
          {
            text: 'Não',
            handler: (data) => {
              a.dismiss();
            }
          },
          {
            text: 'Sim',
            handler: (data) => {
              let max = (temp_entradas.length + temp_saidas.length + temp_jobs.length);
              let mine = 0;
              let load = this.loadingCtrl.create({
                content: 'Sincronizando... ' + (temp_entradas.length + temp_saidas.length + temp_jobs.length) + ' itens restantes'
              })
              load.present();

              if (temp_jobs instanceof Array) {
                temp_jobs.forEach((jobs_element, jobs_index) => {
                  this._http.post(
                    'https://kstrade.com.br/sistema/assets/kstrade_php/app/insertJob.php',
                    jobs_element,
                    {}
                  ).then((response) => {
                    response.data = JSON.parse(response.data)
                    if (response.data.status == '0x104') {
                      mine++;
                      temp_jobs.splice(jobs_index, 1);
                      if (mine == max) {
                        load.dismiss();
                        this._alert.create({
                          title: 'Sincronizado com sucesso!',
                          message: 'Todos o itens foram sincronizados com sucesso!'
                        }).present()

                        localStorage.setItem('temp_jobs', JSON.stringify(temp_jobs))
                      }
                    } else if (response.data.status == '0x105') {
                      let a = this._alert.create({
                        title: 'Job duplicado!',
                        subTitle: 'Você já inseriu esse job hoje!',
                        buttons: ['Ok']
                      });
                      a.present();
                    } else if (response.data.status == '0x101') {
                      let a = this._alert.create({
                        title: 'Erro',
                        subTitle: 'Erro de inserção no banco de dados',
                        buttons: ['Ok']
                      });
                      a.present();
                    } else if (response.data.status == '0x102') {
                      let a = this._alert.create({
                        title: 'Já existe!',
                        subTitle: 'O arquivo já existe no sistema',
                        buttons: ['Ok']
                      });
                      a.present();
                    } else if (response.data.status == '0x103') {
                      let a = this._alert.create({
                        title: 'Error 103',
                        subTitle: 'Ocorreu um erro ao salvar o arquivo',
                        buttons: ['Ok']
                      });
                      a.present();
                    } else if (response.data.status == '0x110') {
                      let a = this._alert.create({
                        title: 'Erro de job',
                        subTitle: 'Esse cliente não pertence a loja selecionada.',
                        buttons: ['Ok']
                      });
                      a.present();
                    }
                  });
                });

                if (temp_entradas instanceof Array) {
                  temp_entradas.forEach((entrada_el, entrada_index) => {
                    this._http.post(
                      'https://kstrade.com.br/sistema/assets/kstrade_php/app/insertEntrada.php',
                      entrada_el,
                      {}
                    ).then((response) => {
                      response.data = JSON.parse(response.data)
                      if (response.data.status == '0x104') {
                        temp_entradas.splice(entrada_index, 1)
                        mine++;
                        // alert('entradas worked ' + mine + ' max: ' + max)

                        if (mine == max) {
                          load.dismiss();
                          this._alert.create({
                            title: 'Sincronizado com sucesso!',
                            message: 'Todos o itens foram sincronizados com sucesso!'
                          }).present()

                          localStorage.setItem('temp_entradas', JSON.stringify(temp_entradas))
                        }
                      }
                    });
                  });
                }

                if (temp_saidas instanceof Array) {
                  temp_saidas.forEach((saida_element, saida_index) => {
                    this._http.post(
                      'https://kstrade.com.br/sistema/assets/kstrade_php/app/insertSaida.php',
                      saida_element,
                      {}
                    ).then((response) => {
                      response.data = JSON.parse(response.data)
                      if (response.data.status == '0x104') {
                        temp_saidas.splice(saida_index, 1);
                        mine++;
                        // alert('saidas worked ' + mine + ' max: ' + max)

                        if (mine == max) {
                          load.dismiss();
                          this._alert.create({
                            title: 'Sincronizado com sucesso!',
                            message: 'Todos o itens foram sincronizados com sucesso!'
                          }).present()
                          localStorage.setItem('temp_saidas', JSON.stringify(temp_saidas))
                        }
                      }
                    });
                  });
                }
              }
            }
          }]
      });
      a.present();
    }
  }

  decode_utf8(s) {
    return decodeURIComponent(escape(s));
  }

  upgradeAccordion(child, index) {
    if (child.activated == true) {
      document.getElementById('accordion_' + index).className =
        document.getElementById('accordion_' + index).className.replace('resizeDown', '');
      document.getElementById('accordion_' + index).className += 'resizeUp';
    } else {
      document.getElementById('accordion_' + index).className =
        document.getElementById('accordion_' + index).className.replace('resizeUp', '');
      document.getElementById('accordion_' + index).className += 'resizeDown';
    }

    child.activated = !child.activated;
  }

  OpenDrawer() {
    document.getElementById('drawerMain').className += ' is-visible';
    document.getElementById('obfuscatorMain').className += ' is-visible';
  }

  closeDrawer() {
    document.getElementById('drawerMain').className =
      document.getElementById('drawerMain').className.replace(' is-visible', '');

    document.getElementsByClassName('mdl-layout__obfuscator is-visible')[0].className =
      document.getElementsByClassName('mdl-layout__obfuscator is-visible')[0].className.replace(' is-visible', '');
  }

  openFromMenu(m) {
    if (m.action) {
      switch (m.action) {
        case 'logout':
          this.logOut();
          break
      }
    } else {
      this.closeDrawer();
      if (m.title != 'Inicio') {
        setTimeout(() => {
          if (m.param) {
            this.navCtrl.push(m.media, m.param, { animate: false });
          } else {
            this.navCtrl.push(m.media, {}, { animate: false });
          }
        }, 100);
      }
    }
  }

  logOut() {
    localStorage.removeItem('session');
    localStorage.removeItem('creds');
    localStorage.setItem('session', JSON.stringify({
      visitante: {
        Usr_Nome: 'Temporario',
        Usr_Email: '',
        Usr_Telefone: '',
        Usr_Nivel: 'Visitante'
      }
    }));
    this.navCtrl.setRoot(MainPage);
  }

  doRefresh(event) {
    this.noticia.grupos = [];
    this.cards = [];
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php',
      {},
      {}
    ).then((response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104') {
        if (this.cards.length < response.data.result.length) {
          response.data.result.forEach(element => {
            element.image = element.app_image;
            element.app_image = this._sanitization.bypassSecurityTrustStyle(`url('https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/${element.app_image}') center / cover`);
            this.cards.push(element);
          });
        }
      }

      if (JSON.parse(localStorage.getItem('session'))[0] !== undefined &&
        JSON.parse(localStorage.getItem('session'))[0].PRO_ID !== undefined) {

        JSON.parse(localStorage.getItem('session')).forEach(element => {
          this._http.post(
            'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectClientePromotor.php',
            {
              pro_id: element.PRO_ID
            },
            {}
          ).then((response2) => {
            response2.data = JSON.parse(response2.data)
            if (response2.data.status == '0x104') {
              response2.data.result.forEach(element => {
                this._http.post(
                  'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php',
                  {
                    cliente: element.aux_cliente_id
                  },
                  {}
                ).then((response3) => {
                  response3.data = JSON.parse(response3.data)
                  if (response3.data.status == '0x104') {
                    response3.data.result.forEach((element, index) => {
                      element.activated = false;
                      this.noticia.grupos.push(element);
                      if (index == (response3.data.result.length - 1)) {
                        element.activated = true;
                      }
                    });
                  }
                });
              });
            }
          });
        });
      } else if (JSON.parse(localStorage.getItem('session')).user !== undefined) {
        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php',
          {
            cliente: JSON.parse(localStorage.getItem('session')).redes[0].aux_cliente_id
          },
          {}
        ).then((response2) => {
          response2.data = JSON.parse(response2.data)
          if (response2.data.status == '0x104') {
            response2.data.result.forEach((element, index) => {
              element.activated = false;
              this.noticia.grupos.push(element);
              if (index == (response2.data.result.length - 1)) {
                element.activated = true;
              }
            });
          }
        });
      } else if (JSON.parse(localStorage.getItem('session')).visitante !== undefined) {
        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php',
          {
            is_guest: true
          },
          {}
        ).then((response3) => {
          response3.data = JSON.parse(response3.data)
          if (response3.data.status == '0x104') {

            response3.data.result.forEach((element, index) => {
              element.activated = false;
              element.content.forEach(ele => {
                ele.image = ele.app_image;
                ele.app_image = this._sanitization.bypassSecurityTrustStyle(`url(https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/${ele.app_image}) center / cover`);
              });

              if (index == (response3.data.result.length - 1)) {
                element.activated = true;
              }
              this.noticia.grupos.push(element);
            });
          }
        });
      }

      event.complete();
    });
  }


  openNewImage(c, id) {
    //console.log(c);

    let fotos = [];

    fotos.push('news/' + c.image)

    if (c.app_fotos_extras.length > 0) {
      c.app_fotos_extras.forEach(element => {
        fotos.push('news/' + element.new_foto)
      });
    }

    let modal = this._modal.create(ViewPhotoPage, {
      type: 'photo',
      fotos: fotos,
      id: id
    }, {
      enableBackdropDismiss: true,
      showBackdrop: true,
      cssClass: 'view-photo-modal'
    });
    modal.present();
  }


  openNews(obj) {
    //console.log(obj)
    let modal = this._modal.create(ViewNewCompletePage, {
      group_id: obj.group_id,
      group_name: obj.group_name
    });
    modal.present();
  }
}

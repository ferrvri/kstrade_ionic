import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainPage } from './main';
import { GreetingsPage } from '../greetings/greetings';

@NgModule({
  declarations: [
    MainPage,
    GreetingsPage
  ],
  imports: [
    IonicPageModule.forChild(MainPage),
  ],
  entryComponents: [
    GreetingsPage
  ]
})
export class MainPageModule {}

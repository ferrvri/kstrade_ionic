import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MarketSharePage } from './market-share';

@NgModule({
  declarations: [
    MarketSharePage,
  ],
  imports: [
    IonicPageModule.forChild(MarketSharePage),
  ],
})
export class MarketSharePageModule {}

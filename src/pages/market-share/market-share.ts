import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { Http } from '@angular/http';
import { newPageOptions } from '../../interfaces/newpageoptions';
import { DatePicker } from '@ionic-native/date-picker';
import { MarketShareChartPage } from '../market-share-chart/market-share-chart';
import { Platform } from 'ionic-angular/platform/platform';
import { HTTP } from '@ionic-native/http';

@IonicPage({
  name: 'marketShare'
})
@Component({
  selector: 'page-market-share',
  templateUrl: 'market-share.html',
})
export class MarketSharePage implements OnInit, newPageOptions {

  produtos = [];
  clientes = [];
  redes = [];
  filiais = [];
  concorrentes = [];

  data_inicio = {
    value: '2019-05-01',
    // value: '',
    showValue: 'Data inicial',
    // selected: false
    selected: true
  }

  data_fim = {
    value: '2019-06-01',
    // value: '',
    showValue: 'Data final',
    // selected: false
    selected: true
  }

  produto_select: any = 0;
  cliente_select: any = 0;
  rede_select: any = 0;
  filial_select: any = 0;
  concorrente_select: any = 0;

  constructor(
    public platform: Platform, 
    private _modal: ModalController, 
    private _alert: AlertController, 
    private _datePicker: DatePicker, 
    private _http: HTTP, 
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {

  }

  ngOnInit() {
    if (this.navParams.get('forPromotor') && this.navParams.get('forPromotor') == true) {
      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectClientePromotor.php',
        {
          pro_id: JSON.parse(localStorage.getItem('session'))[0].PRO_ID
        },
        {}
      ).then((response) => {
        response.data = JSON.parse(response.data)
        if (response.data.status == '0x104') {
          response.data.result.forEach(element => {
            this.clientes.push(element);
          });

          JSON.parse(localStorage.getItem('session')).forEach(element => {
            element.content.forEach(element2 => {
              this.redes.push({
                RED_ID: element2.RED_ID,
                RED_Nome: element2.RED_Nome
              });
            });
          });
        }
      });
    } else if (this.navParams.get('forVendedor') && this.navParams.get('forVendedor') == true) {
      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/selectClientes.php',
        {
          unique: true,
          id: JSON.parse(localStorage.getItem('session')).redes[0].aux_cliente_id
        },
        {}
      ).then((response) => {
        if (response.data.status == '0x104') {
          this.redes = JSON.parse(localStorage.getItem('session')).redes;
          response.data.result.forEach(element => {
            this.clientes.push(element);
          });
        }
      });
    } else {
      this.clientes = JSON.parse(localStorage.getItem('session')).clientes;
      this.redes = JSON.parse(localStorage.getItem('session')).redes;
    }
  }

  goBack() {
    this.navCtrl.pop();
  }

  showDate(obj) {
    this._datePicker.show({
      allowFutureDates: true,
      date: new Date(),
      mode: 'date',
      androidTheme: this._datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
    }).then((date) => {
      obj.selected = true;
      obj.value = date;
      obj.showValue = date.toISOString().split('T')[0].split('-')[2] + '/' +
        date.toISOString().split('T')[0].split('-')[1] + '/' +
        date.toISOString().split('T')[0].split('-')[0]
    }, (err) => {
      obj.selected = false;
    });
  }

  selectCliente() {
    let a = this._alert.create();
    this.clientes.forEach((el, index) => {
      if (this.navParams.get('forVendedor') == true) {
        a.addInput({ type: 'radio', label: el.Cli_Nome, value: el.Cli_ID, id: 'cli-' + index });
      } else {
        a.addInput({ type: 'radio', label: el.Cli_Nome, value: el.aux_cliente_id, id: 'cli-' + index });
      }
    });
    a.addButton('Cancel');
    a.addButton({
      text: 'OK',
      handler: (data) => {
        let o = { cliente: 0 }

        if (this.navParams.get('forVendedor') == true) {
          this.cliente_select = this.clientes.filter((e) => {
            return e.Cli_ID == data
          })[0];

          o.cliente = this.cliente_select.Cli_ID
        } else {
          this.cliente_select = this.clientes.filter((e) => {
            return e.aux_cliente_id == data
          })[0];

          o.cliente = this.cliente_select.aux_cliente_id
        }

        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectProdutos.php',
          o,
          {}
        ).then((response) => {
          response.data = JSON.parse(response.data)
          if (response.data.status == '0x104') {
            this.produtos = [];
            response.data.result.forEach(element => {
              this.produtos.push(element);
            });
          } else {
            let a = this._alert.create({
              title: 'Sem produtos!',
              subTitle: 'Este cliente não possui produtos cadastrados.'
            });
            a.present();
          }
        });
      }
    });
    a.present();
  }

  selectConcorrentes() {
    let a = this._alert.create();
    let reg = new RegExp((".*" + 'concorrente'.split("").join(".*") + ".*"), "i");
    this.produtos.forEach((el, index) => {
      if (reg.test(el.pro_nome)) {
        a.addInput({ type: 'radio', label: el.pro_nome, value: el.pro_id, id: 'pro-' + index });
      } else {
        a.addInput({ type: 'radio', label: el.pro_nome, value: el.pro_id, id: 'pro-' + index });
      }
    });
    a.addButton('Cancel');
    a.addButton({
      text: 'OK',
      handler: (data) => {
        this.produtos.forEach((el) => {
          if (el.pro_id === data) {
            this.concorrente_select = el;
          }
        });
      }
    });
    a.present();
  }

  selectProdutos() {
    let a = this._alert.create();
    this.produtos.forEach((el, index) => {
      a.addInput({ type: 'radio', label: el.pro_nome, value: el.pro_id, id: 'pro-' + index });
    });
    a.addButton('Cancel');
    a.addButton({
      text: 'OK',
      handler: (data) => {
        this.produtos.forEach((el) => {
          if (el.pro_id === data) {
            this.produto_select = el;
          }
        });
      }
    });
    a.present();
  }

  selectRede() {
    let a = this._alert.create();
    this.redes.forEach((el, index) => {
      a.addInput({ type: 'radio', label: el.RED_Nome, value: el.RED_ID, id: 'RED-' + index });
    });
    a.addButton('Cancel');
    a.addButton({
      text: 'OK',
      handler: (data) => {
        this.redes.forEach((el) => {
          if (el.RED_ID === data) {
            this.rede_select = el;
            this._http.post(
              'https://kstrade.com.br/sistema/assets/kstrade_php/cliente/selectFilial.php',
              {
                id: data
              },
              {}
            ).then((response2) => {
              response2.data = JSON.parse(response2.data)
              if (response2.data.status == '0x104') {
                this.filiais = [];
                response2.data.result.forEach(element => {
                  this.filiais.push(element)
                });
              }
            });
          }
        });
      }
    });
    a.present();
  }

  selectFilial() {
    this.filial_select = [];
    let a = this._alert.create();
    this.filiais.forEach((el, index) => {
      a.addInput({ type: 'radio', label: el.RFIL_Nome, value: el.RFIL_ID, id: 'RFIL-' + index });
    });
    a.addButton('Cancel');
    a.addButton({
      text: 'OK',
      handler: (data) => {
        this.filiais.forEach((el) => {
          if (el.RFIL_ID === data) {
            this.filial_select = el;
          }
        });
      }
    });
    a.present();
  }

  next() {
    //console.log(this.filial_select);
    let m = this._modal.create(MarketShareChartPage, {
      red_id: this.rede_select.RED_ID,
      red_nome: this.rede_select.RED_Nome,
      rfil_id: this.filial_select.RFIL_ID,
      rfil_nome: this.filial_select.RFIL_Nome,
      cli_id: this.navParams.get('forVendedor') == true ? this.cliente_select.Cli_ID : this.cliente_select.aux_cliente_id,
      cli_nome: this.cliente_select.Cli_Nome,
      pro_id: this.produto_select.pro_id,
      pro_nome: this.produto_select.pro_nome
    });
    m.present();
  }
}

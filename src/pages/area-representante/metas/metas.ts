import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController } from 'ionic-angular';

import {Chart} from 'chart.js';
import { HTTP } from '@ionic-native/http';

@IonicPage({ name: 'metas' })
@Component({
  selector: 'page-metas',
  templateUrl: 'metas.html',
})
export class MetasPage implements OnInit {

  @ViewChild("chart") chartC: ElementRef;

  private chart: Chart;
  
  userData: any;

  result = [];
  redes = []
  clientes = []

  redeOK = false;
  clienteOK = false;

  clienteIndex = '';
  redeIndex = '';

  rede = 'Redes';
  cliente = 'Clientes';

  chartOpts = {
    type: "doughnut",
    data: {
      labels: ["Meta já atingida", "Valor restante"],
      datasets: [
        {
          data: [],
          backgroundColor: [
            "#8c1515",
            "rgba(27, 55, 44, 0.25)"
          ],
          borderWidth: 1
        }
      ]
    },
    options: {
      circumference: 4 * Math.PI
    }
  }

  constructor(
    private _alert: AlertController, 
    private _http: HTTP, 
    public platform: Platform, 
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MetasPage');
  }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('session'));
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectRedesRepresentante.php',
      {
        usuario_id: this.userData.user.Usr_ID
      },
      {}
    ).then((response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104') {
        this.redes = response.data.result;

        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectClientesRepresentante.php',
          {
            usuario_id: this.userData.user.Usr_ID
          },
          {}
        ).then((response2) => {
          response2.data = JSON.parse(response2.data)
          if (response2.data.status == '0x104') {
            this.clientes = response2.data.result;
          }
        });
      }
    })
  }

  goBack() {
    this.navCtrl.pop()
  }

  setRede(rede) {
    this.redeOK = !this.redeOK;
    this.redeIndex = rede.RED_ID
    this.rede = rede.RED_Nome
  }

  setCliente(cliente) {
    this.clienteOK = !this.clienteOK;
    this.clienteIndex = cliente.Cli_ID
    this.cliente = cliente.Cli_Nome
  }

  search() {
    if (this.redeIndex.length < 1) {
      this._alert.create({
        title: 'Selecione a rede'
      }).present();
    } else if (this.clienteIndex.length < 1) {
      this._alert.create({
        title: 'Selecione o cliente'
      }).present();
    } else {
      this._http.post(
        'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectMeta.php',
        {
          cliente: this.clienteIndex,
          rede: this.redeIndex,
          usuario: this.userData.user.Usr_ID,
          dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 0).toISOString().substring(0, 10),
          datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
        },
        {}
      ).then((response) => {
        response.data = JSON.parse(response.data)
        if (response.data.status == '0x104') {
          this.result.push(response.data.meta);
          this.result.push(response.data.valores);

          this.chartOpts.data.datasets[0].data[0] = response.data.valores;
          this.chartOpts.data.datasets[0].data[1] = response.data.meta;

          console.log(this.chartOpts);
          this.chart = new Chart(this.chartC.nativeElement, this.chartOpts);
        }
      })
    }
  }
}

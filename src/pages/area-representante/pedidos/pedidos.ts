import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { Platform } from 'ionic-angular/platform/platform';
import { Http } from '@angular/http';
import { UtilsProvider } from '../../../providers/utils/utils';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the PedidosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({name: 'pedidos'})
@Component({
  selector: 'page-pedidos',
  templateUrl: 'pedidos.html',
})
export class PedidosPage implements OnInit{

  valor = '';
  userData: any;

  redes = [];
  redeOK = false;
  rede = 'Redes';
  redeIndex = '';

  clientes = [];
  clienteOK = false;
  cliente = 'Clientes';
  clienteIndex = '';

  obs = '';

  constructor(
    private _viewCtrl: ViewController, 
    private _alert: AlertController, 
    private utils: UtilsProvider, 
    private _http: HTTP, 
    public platform: Platform, 
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PedidosPage');
  }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('session'));
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectRedesRepresentante.php',
      {
        usuario_id: this.userData.user.Usr_ID
      },
      {}
    ).then( (response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104'){
        this.redes = response.data.result;

        this._http.post(
          'https://kstrade.com.br/sistema/assets/kstrade_php/app/selectClientesRepresentante.php',
          {
            usuario_id: this.userData.user.Usr_ID
          },
          {}
        ).then( (response2) => {
          response2.data = JSON.parse(response2.data)
          if (response2.data.status == '0x104'){
            this.clientes = response2.data.result;
          }
        });
      }
    })
  }

  goBack(){
    this.navCtrl.pop();
  }

  setRede(rede){
    this.rede = rede.RED_Nome
    this.redeIndex = rede.RED_ID
    this.redeOK = !this.redeOK;
  }

  setCliente(cliente){
    this.cliente = cliente.Cli_Nome;
    this.clienteIndex = cliente.Cli_ID;
    this.clienteOK = !this.clienteOK;
  }

  insertPedido(){
    this._http.post(
      'https://kstrade.com.br/sistema/assets/kstrade_php/insertPedido.php',
      {
        rede: this.redeIndex,
        cliente: this.clienteIndex,
        usuario: this.userData.user.Usr_ID,
        valor: this.valor.replace('.', '').replace(',', '.')
      },
      {}
    ).then( (response) => {
      response.data = JSON.parse(response.data)
      if (response.data.status == '0x104'){
        let a = this._alert.create({
          subTitle: 'Seu pedido foi enviado!',
          message: 'A equipe entrará em contato em breve.'
        });
        a.present();
        a.onDidDismiss( () => {
          this._viewCtrl.dismiss()
        });
      }
    });
  }

  applyMoneyMask(){
    this.valor = this.utils.detectAmount(this.valor);
  }
}

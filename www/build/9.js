webpackJsonp([9],{

/***/ 590:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntradaSaidaJobPageModule", function() { return EntradaSaidaJobPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__entrada_saida_job__ = __webpack_require__(613);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EntradaSaidaJobPageModule = /** @class */ (function () {
    function EntradaSaidaJobPageModule() {
    }
    EntradaSaidaJobPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__entrada_saida_job__["a" /* EntradaSaidaJobPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__entrada_saida_job__["a" /* EntradaSaidaJobPage */]),
            ],
        })
    ], EntradaSaidaJobPageModule);
    return EntradaSaidaJobPageModule;
}());

//# sourceMappingURL=entrada-saida-job.module.js.map

/***/ }),

/***/ 613:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntradaSaidaJobPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_sweetalert2__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_vibration__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_rota_rota__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the EntradaSaidaJobPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EntradaSaidaJobPage = /** @class */ (function () {
    function EntradaSaidaJobPage(platform, _rota, viewCtrl, loadingCtrl, _vibration, _alert, _geolocation, navCtrl, navParams, _http) {
        this.platform = platform;
        this._rota = _rota;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        this._vibration = _vibration;
        this._alert = _alert;
        this._geolocation = _geolocation;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this.mylat = 0;
        this.mylong = 0;
        this.makeIt = false;
    }
    EntradaSaidaJobPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad EntradaSaidaJobPage');
    };
    EntradaSaidaJobPage.prototype.ngOnInit = function () {
        var _this = this;
        this._geolocation.getCurrentPosition().then(function (data) {
            _this.mylat = data.coords.latitude;
            _this.mylong = data.coords.longitude;
        }).catch(function (e) {
            //console.log(e);
            var a = _this._alert.create({
                title: 'Ative a localização',
                subTitle: 'Você deve ativar a localização!',
                buttons: ['Ok']
            });
            a.present();
        });
    };
    EntradaSaidaJobPage.prototype.checkIn = function (kind) {
        var _this = this;
        this.makeIt = true;
        switch (kind) {
            case 'entrada': {
                var s_1 = setTimeout(function () {
                    loading_1.dismiss();
                    __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                        title: 'Conexão ruim!',
                        text: 'A resposta demorou muito a chegar!',
                        type: 'error',
                        showCloseButton: true,
                        confirmButtonText: 'OK'
                    });
                    _this._vibration.vibrate(250);
                    _this._vibration.vibrate(250);
                }, 30000);
                var loading_1 = this.loadingCtrl.create({
                    content: 'Enviando...'
                });
                loading_1.present();
                this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertEntrada.php', {
                    pro_id: this.navParams.get('content').PRO_ID,
                    rede_id: this.navParams.get('content').RED_ID,
                    rfil_id: this.navParams.get('content').RFIL_ID,
                    data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                    hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                    lat_entrada: this.mylat,
                    long_entrada: this.mylong
                }, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        clearTimeout(s_1);
                        loading_1.dismiss();
                        __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                            title: 'Entrada registrada!',
                            text: 'Sua entrada foi registrada com sucesso!',
                            type: 'success',
                            showCloseButton: true,
                            confirmButtonText: 'OK'
                        });
                        _this._vibration.vibrate(250);
                        _this._rota.disableRota({
                            pro_id: _this.navParams.get('content').PRO_ID,
                            rede_id: _this.navParams.get('content').RED_ID,
                            rfil_id: _this.navParams.get('content').RFIL_ID,
                            data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                            hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                            lat_entrada: _this.mylat,
                            long_entrada: _this.mylong
                        });
                        _this.viewCtrl.dismiss({ done: true });
                        _this.makeIt = false;
                    }
                }, function (err) {
                    clearTimeout(s_1);
                    loading_1.dismiss();
                    __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                        title: 'Entrada salva!',
                        text: 'Sua entrada foi salva com sucesso!',
                        type: 'success',
                        showCloseButton: true,
                        confirmButtonText: 'OK'
                    });
                    if (!localStorage.getItem('temp_entradas')) {
                        var temp = [];
                        temp.push({
                            pro_id: _this.navParams.get('content').PRO_ID,
                            rede_id: _this.navParams.get('content').RED_ID,
                            rfil_id: _this.navParams.get('content').RFIL_ID,
                            data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                            hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                            lat_entrada: _this.mylat,
                            long_entrada: _this.mylong
                        });
                        localStorage.setItem('temp_entradas', JSON.stringify(temp));
                    }
                    else {
                        var temp = JSON.parse(localStorage.getItem('temp_entradas'));
                        temp.push({
                            pro_id: _this.navParams.get('content').PRO_ID,
                            rede_id: _this.navParams.get('content').RED_ID,
                            rfil_id: _this.navParams.get('content').RFIL_ID,
                            data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                            hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                            lat_entrada: _this.mylat,
                            long_entrada: _this.mylong
                        });
                        localStorage.setItem('temp_entradas', JSON.stringify(temp));
                    }
                });
                this.viewCtrl.dismiss({ done: true });
                break;
            }
            case 'saida': {
                var s_2 = setTimeout(function () {
                    loading_2.dismiss();
                    __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                        title: 'Conexão ruim!',
                        text: 'A resposta demorou muito a chegar!',
                        type: 'error',
                        showCloseButton: true,
                        confirmButtonText: 'OK'
                    });
                    _this._vibration.vibrate(250);
                    _this._vibration.vibrate(250);
                }, 30000);
                var loading_2 = this.loadingCtrl.create({
                    content: 'Enviando...'
                });
                loading_2.present();
                this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertSaida.php', {
                    pro_id: this.navParams.get('content').PRO_ID,
                    rede_id: this.navParams.get('content').RED_ID,
                    rfil_id: this.navParams.get('content').RFIL_ID,
                    data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                    hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                    lat_saida: this.mylat,
                    long_saida: this.mylong
                }, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        clearTimeout(s_2);
                        loading_2.dismiss();
                        __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                            title: 'Saída registrada!',
                            text: 'Sua saída foi registrada com sucesso!',
                            type: 'success',
                            showCloseButton: true,
                            confirmButtonText: 'OK'
                        });
                        _this._vibration.vibrate(250);
                        _this._rota.enableRota({
                            pro_id: _this.navParams.get('content').PRO_ID,
                            rede_id: _this.navParams.get('content').RED_ID,
                            rfil_id: _this.navParams.get('content').RFIL_ID,
                            data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                            hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                            lat_saida: _this.mylat,
                            long_saida: _this.mylong
                        });
                        _this.viewCtrl.dismiss({ done: true });
                        _this.makeIt = false;
                    }
                }, function (err) {
                    clearTimeout(s_2);
                    loading_2.dismiss();
                    __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                        title: 'Saída salva!',
                        text: 'Sua saída foi salva com sucesso!',
                        type: 'success',
                        showCloseButton: true,
                        confirmButtonText: 'OK'
                    });
                    if (!localStorage.getItem('temp_saidas')) {
                        var temp = [];
                        temp.push({
                            pro_id: _this.navParams.get('content').PRO_ID,
                            rede_id: _this.navParams.get('content').RED_ID,
                            rfil_id: _this.navParams.get('content').RFIL_ID,
                            data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                            hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                            lat_saida: _this.mylat,
                            long_saida: _this.mylong
                        });
                        localStorage.setItem('temp_saidas', JSON.stringify(temp));
                    }
                    else {
                        var temp = JSON.parse(localStorage.getItem('temp_saidas'));
                        temp.push({
                            pro_id: _this.navParams.get('content').PRO_ID,
                            rede_id: _this.navParams.get('content').RED_ID,
                            rfil_id: _this.navParams.get('content').RFIL_ID,
                            data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                            hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                            lat_saida: _this.mylat,
                            long_saida: _this.mylong
                        });
                        localStorage.setItem('temp_saidas', JSON.stringify(temp));
                    }
                });
                this.viewCtrl.dismiss({ done: true });
                break;
            }
        }
    };
    EntradaSaidaJobPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-entrada-saida-job',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\entrada-saida-job\entrada-saida-job.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content mdl-color--white" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <span style="padding: 10px; display: table; margin: 15px auto; font-size: 32px; font-weight: bold;">Realizar {{this.navParams.get(\'kind\') == \'entrada\' ? \'Check-In\': \'Check-Out\'}}</span>\n\n      <span style="text-align: center; display: table; margin: 5px auto; font-size: 12px; width: 95%" > Olá <b>{{this.navParams.get(\'content\').PRO_Nome}}</b> realize o {{this.navParams.get(\'kind\') == \'entrada\' ? \'Check-In\': \'Check-Out\'}} na loja para garantirmos ainda mais a efetividade dos trabalhos!</span>\n\n      <div >\n\n        <div style="display: table; margin: auto; text-align: center; width: 85%;" >\n\n          <span style="display: table; margin: auto;text-align: center; font-size: 12px; font-weight: bold;"> Quais informações são adquiridas: </span>\n\n          <span style="display: table; margin: auto;text-align: center; font-size: 10px"> Localização GPS, Horario, Loja, Email </span>\n\n          <span style="display: table; margin: auto;text-align: center; font-size: 12px; font-weight: bold;"> É realmente necessario? </span>\n\n          <span style="display: table; margin: auto;text-align: center; font-size: 10px"> Essas informações são de extrema importância, elas garantem sua segurança e nossa funcionalidade, sempre adquirindo um feedback muito transparente do que realmente ocorre em nosso sistema. </span>\n\n        </div>\n\n        \n\n        <button [disabled]="this.makeIt" class=" mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab mdl-color--red-500" (click)="checkIn(this.navParams.get(\'kind\'))" style="display: table; margin: 20px auto; width: 120px; height: 120px;">\n\n          <i class="material-icons mdl-color-text--white">add_location</i>\n\n        </button>\n\n        <span style="display: table; margin: auto; font-size: 10px; position:relative; bottom: 15px; padding: 5px; width: 85px; border-radius: 4px; background: rgba(0,0,0,0.8); color: #f5f7fa">Relizar {{this.navParams.get(\'kind\') == \'entrada\' ? \'Check-In\': \'Check-Out\'}}</span>\n\n      </div>\n\n    </div>\n\n  </main>\n\n  <div style="z-index: 5; position: relative; background: #8c1515; width: 100vw;" [ngStyle]="{\'bottom\': (this.platform.is(\'ios\') ? \'35px\':\'0px\')}">\n\n    <div style="display: table; margin: auto;">\n\n      <button (click)="this.viewCtrl.dismiss({done: false})" class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-color--white">\n\n        <i class="material-icons">arrow_back</i>\n\n      </button>\n\n    </div>\n\n  </div>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\entrada-saida-job\entrada-saida-job.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__providers_rota_rota__["a" /* RotaProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_vibration__["a" /* Vibration */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_http__["a" /* HTTP */]])
    ], EntradaSaidaJobPage);
    return EntradaSaidaJobPage;
}());

//# sourceMappingURL=entrada-saida-job.js.map

/***/ })

});
//# sourceMappingURL=9.js.map
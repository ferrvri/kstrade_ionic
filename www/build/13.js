webpackJsonp([13],{

/***/ 585:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LojasVisitantePageModule", function() { return LojasVisitantePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lojas_visitante__ = __webpack_require__(618);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LojasVisitantePageModule = /** @class */ (function () {
    function LojasVisitantePageModule() {
    }
    LojasVisitantePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__lojas_visitante__["a" /* LojasVisitantePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__lojas_visitante__["a" /* LojasVisitantePage */]),
            ],
        })
    ], LojasVisitantePageModule);
    return LojasVisitantePageModule;
}());

//# sourceMappingURL=lojas-visitante.module.js.map

/***/ }),

/***/ 618:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LojasVisitantePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_sweetalert2__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the LojasVisitantePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LojasVisitantePage = /** @class */ (function () {
    function LojasVisitantePage(platform, navCtrl, navParams, _http) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this.redes = [];
        this.resultsFilterRedes = [];
        this.doneLoading = false;
        this.already_filtered = false;
    }
    LojasVisitantePage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad LojasVisitantePage');
    };
    LojasVisitantePage.prototype.ngOnInit = function () {
        var _this = this;
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectRedesFilialVisita.php', {
            all: true
        }, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            if (response.data.status == '0x104') {
                response.data.result.forEach(function (element) {
                    element.show = false;
                    if (element.content.length > 0) {
                        _this.redes.push(element);
                    }
                });
                _this.resultsFilterRedes = _this.redes;
            }
            _this.doneLoading = true;
        });
    };
    LojasVisitantePage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    LojasVisitantePage.prototype.goBackFilter = function () {
        this.already_filtered = false;
        this.resultsFilterRedes = this.redes;
    };
    LojasVisitantePage.prototype.filter = function (name) {
        this.already_filtered = true;
        var regexp = new RegExp((".*" + name + ".*"), "i");
        this.resultsFilterRedes = this.resultsFilterRedes.filter(function (e) {
            return regexp.test(e.RED_Nome);
        });
    };
    LojasVisitantePage.prototype.solicitarAtendimento = function (rede) {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
            title: 'Inscreva-se',
            html: "\n        <span style=\"width: 100%; font-weight: bold\">Seja um FREELANCER KSTRADE</span>\n        <span style=\"width: 100%;\">Informe seu nome, whatsapp e quantidade de visitas e de dias de atendimento que entraremos em contato com vc!</span>\n      ",
            type: "question",
            showCancelButton: true,
            cancelButtonText: 'Não',
            showConfirmButton: true,
            confirmButtonText: 'Sim',
            reverseButtons: true,
            input: "textarea",
            inputPlaceholder: 'Detalhes/Motivos da solicitação',
        }).then(function (data) {
            if (data.value !== undefined && data.value.length > 0) {
                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertAtendimento.php', {
                    rede_id: rede.RED_ID,
                    filial_id: rede.RFIL_ID,
                    detail: "\n                    <b>Visitante que solicitou:</b><br>\n                    " + JSON.parse(localStorage.getItem('session')).visitante.Usr_Nome + "<br>\n                    <b>Email:</b><br>\n                    " + JSON.parse(localStorage.getItem('session')).visitante.Usr_Email + "<br>\n                    <b>Telefone:</b><br>\n                    " + JSON.parse(localStorage.getItem('session')).visitante.Usr_Telefone + "<br>\n                    <b>Detalhes:</b>\n                    " + data.value + "\n                    ",
                    texto: data.value
                }, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                            title: 'Sucesso!',
                            text: 'A sua solicitação foi enviada a nossa equipe, já estamos em analíse. Obrigado!',
                            type: 'success'
                        });
                    }
                    else {
                        __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                            title: 'Erro!',
                            text: 'Sua solicitação não foi enviada a nossa equipe',
                            type: 'error'
                        });
                    }
                });
            }
            else if (data.value !== undefined && data.value.length < 1) {
                __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                    type: 'error',
                    title: 'Preencha o campo!'
                });
            }
        });
    };
    LojasVisitantePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-lojas-visitante',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\area-visitante\lojas-visitante\lojas-visitante.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;"\n\n      class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n      <i class="material-icons">arrow_back</i>\n\n    </button>\n\n    <img src="./assets/images/logoecco.png"\n\n      style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content mdl-color--white" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <!-- <div *ngIf="this.navParams.get(\'rede_nome\') !== undefined" style="background: #8c1515; height: 60px;">\n\n        <span class="char"\n\n          style="position: relative; top: 20px; left: 25px; color: #f5f7fa; font-weight: bold;"\n\n        >{{r.RED_Nome}}</span>\n\n      </div> -->\n\n\n\n      <div style="display: table; margin: auto; width: 100%;">\n\n        <span style="display: table; margin: 4px auto">Filtrar</span>\n\n        <ion-item style="width: 90%; display: table; margin: auto">\n\n          <div class="mdl-textfield mdl-js-textfield">\n\n            <input class="mdl-textfield__input" placeholder="Pesquise pelo nome da rede" #redeInput/>\n\n          </div>\n\n        </ion-item>\n\n\n\n        <button class="mdl-button mdl-js-button mdl-color--red-500 mdl-color-text--white"\n\n          (click)="filter(redeInput.value)"\n\n          style="margin: auto;display: table; width: 100%;">\n\n          <span>Filtrar</span>\n\n        </button>\n\n\n\n        <button *ngIf="already_filtered" style="padding-top: 4px;" class="mdl-button mdl-js-button mdl-color--red-500 mdl-color-text--white"\n\n          (click)="goBackFilter()"\n\n          style="margin: auto;display: table; width: 100%;">\n\n          <span>Voltar</span>\n\n        </button>\n\n      </div>\n\n\n\n      <div style="text-align: center; width: 100%; border-bottom: 1px solid #940000">\n\n        <span style="font-weight: bold; font-size: 12px">Seja um FREELANCER KSTRADE</span>\n\n        <br>\n\n        <span style="font-size: 10px">É muito simples, basta tocar sobre uma loja</span>\n\n      </div>\n\n\n\n      <div *ngIf="!doneLoading" style="display: table; margin: 2% auto;">\n\n        <div class="rotate" style="border-radius: 200px; width: 40px; height: 40px; border: 2px solid #940000; border-style: dashed solid dashed solid;">\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngFor="let r of resultsFilterRedes" style="text-align: left; border-top: 1px solid #212121; margin-top: 10px; cursor: pointer">\n\n        <div style="width: 100%; min-height: 45px" (click)="r.show = !r.show">\n\n            <span\n\n              style="font-size: 16px; cursor: pointer; position: relative; font-weight: bold; padding-left: 5px;">\n\n              {{r.RED_Nome}}\n\n              <i class="material-icons" style="margin-left: 10px;">{{ r.show == true? \'keyboard_arrow_up\':\'keyboard_arrow_down\'}}</i>\n\n            </span>\n\n        </div>\n\n\n\n        <div *ngIf="r.show == true">\n\n          <table class="ion-diagnostic-table"\n\n            style="font-size: 13px; margin-bottom: 20px; width: 100%;">\n\n            <thead>\n\n              <tr style="border-bottom: 2px solid #282828;">\n\n                <td></td>\n\n                <td></td>\n\n              </tr>\n\n            </thead>\n\n            <tbody>\n\n              <tr (click)="solicitarAtendimento({RED_Nome: r.RED_Nome, RED_ID: r.RED_ID, RFIL_ID: rc.RFIL_ID, RFIL_Nome: rc.RFIL_Nome})" style="cursor: pointer; border-bottom: 1px solid #dcdcdc" *ngFor="let rc of r.content; let rI = index" >\n\n                <td>\n\n                  <div style="width: 32px; height: 32px;">\n\n                  </div>\n\n                </td>\n\n                <td>{{rc.RFIL_Nome}}</td>\n\n              </tr>\n\n            </tbody>\n\n          </table>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\area-visitante\lojas-visitante\lojas-visitante.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__["a" /* HTTP */]])
    ], LojasVisitantePage);
    return LojasVisitantePage;
}());

//# sourceMappingURL=lojas-visitante.js.map

/***/ })

});
//# sourceMappingURL=13.js.map
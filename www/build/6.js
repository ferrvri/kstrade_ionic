webpackJsonp([6],{

/***/ 594:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobsDoDiaPageModule", function() { return JobsDoDiaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__jobs_do_dia__ = __webpack_require__(621);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var JobsDoDiaPageModule = /** @class */ (function () {
    function JobsDoDiaPageModule() {
    }
    JobsDoDiaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__jobs_do_dia__["a" /* JobsDoDiaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__jobs_do_dia__["a" /* JobsDoDiaPage */]),
            ],
        })
    ], JobsDoDiaPageModule);
    return JobsDoDiaPageModule;
}());

//# sourceMappingURL=jobs-do-dia.module.js.map

/***/ }),

/***/ 607:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobInfosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JobInfosPage = /** @class */ (function () {
    function JobInfosPage(platform, _datePicker, navCtrl, navParams, _http) {
        this.platform = platform;
        this._datePicker = _datePicker;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        // map: GoogleMap;
        this.photos = [];
    }
    JobInfosPage.prototype.ngOnInit = function () {
        var _this = this;
        if (this.navParams.get('type') == 'photo') {
            //console.log(this.navParams.get('obs'));
            this.photos.push(this.navParams.get('job_foto'));
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectFotosExtras.php', {
                job_id: this.navParams.get('job_id')
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    response.data.result.forEach(function (element) {
                        _this.photos.push(element.JFOT_Foto);
                    });
                }
            });
        }
        else if (this.navParams.get('type') == 'local') {
            setTimeout(function () {
                _this.loadMap(_this.navParams.get('lat'), _this.navParams.get('long'));
            }, 500);
        }
    };
    JobInfosPage.prototype.loadMap = function (lat, lon) {
        if (lat != 'nada' && lon != 'nada') {
            var myLatlng = new window.google.maps.LatLng(lat, lon);
            var map = new window.google.maps.Map(document.getElementById('map_canvas'), {
                center: myLatlng,
                zoom: 17
            });
            var marker = new window.google.maps.Marker({
                position: myLatlng,
                map: map
            });
        }
    };
    JobInfosPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    JobInfosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-job-infos',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\job-infos\job-infos.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #f5f7fa; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">\n\n  <!-- <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n      <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n        <i class="material-icons">arrow_back</i>\n\n      </button>\n\n      <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n      <div class="mdl-layout-spacer"></div>\n\n    </header> -->\n\n  <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: rgba(0,0,0,0.4);" (click)="this.navParams.get(\'type\') != \'local\' ? this.navCtrl.pop() : return;">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <div *ngIf="this.navParams.get(\'type\') == \'photo\'">\n\n        <div style="background: #f5f7fa; border-radius: 2px;width: 95%; display: table; margin: 10% auto">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 42px; color: #000;">info_outline</i>\n\n          <div style="width: 100%; height: 1px; border-top: 1px solid #000; display: table; margin: 5px auto;"></div>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Observações</span>\n\n          <span style="display: table; margin: auto; font-size: 16px; padding: 15px;">{{this.navParams.get(\'obs\')}}</span>\n\n        </div>\n\n        <ion-slides zoom="true" style="margin: auto; display: table; margin-top: 5%">\n\n          <ion-slide *ngFor="let p of photos">\n\n            <div class="swiper-zoom-container">\n\n              <img style="width: 95%; margin: auto; display: table;" [src]="\'https://kstrade.com.br/sistema/assets/kstrade_php/uploads/\'+p" />\n\n            </div>\n\n          </ion-slide>\n\n        </ion-slides>\n\n        <div style="display: table; margin: auto; width: 100%; border-bottom: 1px solid #dcdcdc">\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Arraste para os\n\n            lados para visualizar as demais fotos</span>\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Toque fora da\n\n            foto para fechar</span>\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngIf="this.navParams.get(\'type\') == \'local\'">\n\n        <div style="position: absolute; top: 0; width: 100%; height: 100%; z-index: 5" (click)="this.navCtrl.pop()">\n\n        </div>\n\n        <div id="map_canvas" style="margin: 15% auto; width: 300px; height: 360px;" *ngIf="this.navParams.get(\'lat\') != \'nada\' && this.navParams.get(\'lon\') != \'nada\'"></div>\n\n        <div style="background: #f5f7fa; border-radius: 2px;width: 95%; display: table; margin: 20% auto" *ngIf="this.navParams.get(\'lat\') == \'nada\' || this.navParams.get(\'lon\') == \'nada\'">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 42px; color: #000;">location_off</i>\n\n          <div style="width: 80%; height: 1px; border-top: 1px solid #000; display: table; margin: 5px auto;"></div>\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #000; font-weight: bold">Este promotor não\n\n            divulgou sua localização</span>\n\n        </div>\n\n        <div style="display: table; margin: 10px auto; width: 100%; border-bottom: 1px solid #dcdcdc">\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Toque fora do\n\n            mapa para fechar</span>\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngIf="this.navParams.get(\'type\') == \'infos\'">\n\n        <div style="background: #f5f7fa; border-radius: 2px;width: 95%; display: table; margin: 20% auto">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 42px; color: #000;">info_outline</i>\n\n          <div style="width: 100%; height: 1px; border-top: 1px solid #000; display: table; margin: 5px auto;"></div>\n\n          <span style="display: table; margin: auto; margin-top: 30px; margin-bottom: 10px; font-size: 18px; font-weight: bold;">Informações\n\n            adquiridas do aparelho:</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 20px; font-size: 12px;">Para evitarmos fraudes,\n\n            adquirimos informações do aparelho.</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Modelo do\n\n            aparelho</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 15px; font-size: 16px;">{{this.navParams.get(\'modelo\')}}</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Sistema\n\n            operacional e versão</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 15px; font-size: 18px;">{{this.navParams.get(\'plataforma\')}}\n\n            - {{this.navParams.get(\'versao\')}}</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Fabricante</span>\n\n          <span style="display: table; margin: auto; font-size: 18px;">{{this.navParams.get(\'manufact\')}}</span>\n\n        </div>\n\n        <div style="display: table; margin: auto; width: 100%; border-bottom: 1px solid #dcdcdc">\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Toque fora da\n\n            foto para fechar</span>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\job-infos\job-infos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__["a" /* DatePicker */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__["a" /* HTTP */]])
    ], JobInfosPage);
    return JobInfosPage;
}());

//# sourceMappingURL=job-infos.js.map

/***/ }),

/***/ 621:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobsDoDiaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_vibration__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__area_cliente_job_infos_job_infos__ = __webpack_require__(607);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the JobsDoDiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var JobsDoDiaPage = /** @class */ (function () {
    function JobsDoDiaPage(platform, _alert, _vibrate, navCtrl, navParams, _http, modalCtrl) {
        this.platform = platform;
        this._alert = _alert;
        this._vibrate = _vibrate;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this.modalCtrl = modalCtrl;
        this.table = [];
        this.isLong = false;
        this.clickTime = 0;
        this.upTime = 0;
    }
    JobsDoDiaPage.prototype.ngOnInit = function () {
        var _this = this;
        var mes = new Date().getMonth() + 1;
        var cl = JSON.parse(localStorage.getItem('session'));
        cl.forEach(function (element) {
            element.content.forEach(function (element2) {
                var o = {
                    rede_nome: '',
                    filial_nome: '',
                    content: []
                };
                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectJobsOf.php', {
                    myid: element.PRO_ID,
                    rede_id: element2.RED_ID,
                    filial_id: element2.RFIL_ID,
                    dataone: new Date().getFullYear() + '-' +
                        (mes.valueOf() < 10 ? '0' + mes.toString() : mes.toString()) + '-' +
                        new Date().getDate()
                    // dataone: '2018-04-10'
                }, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        o.rede_nome = element2.RED_Nome;
                        o.filial_nome = element2.RFIL_Nome;
                        response.data.result.forEach(function (element3) {
                            o.content.push(element3);
                        });
                        _this.table.push(o);
                    }
                    else if (response.data.status == '0x101') {
                        o.rede_nome = element2.RED_Nome;
                        o.filial_nome = element2.RFIL_Nome;
                        o.content = [];
                        _this.table.push(o);
                    }
                });
            });
        });
    };
    JobsDoDiaPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    JobsDoDiaPage.prototype.seePhoto = function (cel) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__area_cliente_job_infos_job_infos__["a" /* JobInfosPage */], {
            type: 'photo',
            job_id: cel.JOB_ID,
            job_foto: cel.JOB_Foto,
            obs: cel.JOB_Obs
        });
        modal.present();
    };
    JobsDoDiaPage.prototype.longPress = function (event, elem) {
        var _this = this;
        //console.log(event);
        this._vibrate.vibrate(500);
        var alert = this._alert.create({
            title: 'Excluir?',
            subTitle: 'Deseja realmente excluir esse job?',
            buttons: [
                {
                    text: 'Excluir!',
                    handler: function (data) {
                        //console.log(elem)
                        var hora = (new Date().getHours().toString().length == 1 ? '0' + new Date().getHours().toString() : new Date().getHours().toString());
                        var minuto = (new Date().getMinutes().toString().length == 1 ? '0' + new Date().getMinutes().toString() : new Date().getMinutes().toString());
                        if (elem.JOB_Hora_Job.split(':')[0] == hora) {
                            if (parseInt(minuto) - parseInt(elem.JOB_Hora_Job.split(':')[1]) <= 5) {
                                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/removeJob.php', {
                                    job_id: elem.JOB_ID
                                }, {}).then(function (response) {
                                    response.data = JSON.parse(response.data);
                                    if (response.data.status == '0x104') {
                                        var msg = "O promotor " + JSON.parse(localStorage.getItem('session'))[0].content[0].PRO_Nome + " apagou o seguinte job: \n                      " + elem.Cli_Nome + " | Foto: " + ('https://kstrade.com.br/sistema/assets/kstrade_php/uploads/' + elem.JOB_Foto) + "\n                    ";
                                        _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/sendMensagem.php', {
                                            deid: 5,
                                            paraid: 2,
                                            mensagem: msg,
                                            status: 'Nao Lido',
                                            dataenvio: new Date().getUTCDate().toString() + '/' + (parseInt(new Date().getUTCMonth().toString().length == 1 ? '0' + new Date().getUTCMonth().toString() : new Date().getUTCMonth().toString()) + 1) + '/' + new Date().getUTCFullYear().toString()
                                        }, {}).then(function (response) {
                                            response.data = JSON.parse(response.data);
                                            if (response.data.status == '0x104') {
                                                var alert_1 = _this._alert.create({
                                                    title: 'Job excluido',
                                                    subTitle: 'Job excluido com sucesso!',
                                                    buttons: ['Ok']
                                                });
                                                alert_1.present();
                                                _this.table = [];
                                                _this.ngOnInit();
                                            }
                                            else if (response.data.status == '0x101') {
                                                var alert_2 = _this._alert.create({
                                                    title: 'Erro',
                                                    subTitle: 'Erro ao excluir o Job.',
                                                    buttons: ['Ok']
                                                });
                                                alert_2.present();
                                            }
                                        });
                                    }
                                });
                            }
                            else {
                                var alert_3 = _this._alert.create({
                                    title: 'Erro 0x102!',
                                    subTitle: 'Já passou o tempo determinado para excluir!',
                                    buttons: ['Ok']
                                });
                                alert_3.present();
                            }
                        }
                        else {
                            var alert_4 = _this._alert.create({
                                title: 'Erro 0x101!',
                                subTitle: 'Já passou o tempo determinado para excluir!',
                                buttons: ['Ok']
                            });
                            alert_4.present();
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    JobsDoDiaPage.prototype.exechandler = function (event) {
        clearInterval(this.handler);
    };
    JobsDoDiaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-jobs-do-dia',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\jobs-do-dia\jobs-do-dia.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" [ngStyle]="{\'height\': (this.platform.is(\'ios\') ? \'95vh\':undefined)}">\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n      <i class="material-icons">arrow_back</i>\n\n    </button>\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <div style="background: #8c1515; height: 60px;">\n\n        <span class="char" style="position: relative; top: 20px; left: 25px; color: #f5f7fa; font-weight: bold;">Jobs realizados hoje</span>\n\n      </div>\n\n      <div style="display: table; margin: 5px auto; border-bottom: 1px solid #dcdcdc;" *ngFor="let t of table; let i = index">\n\n        <div *ngIf="t.content.length > 0; else jobsElse">\n\n          <div style="width: 100%; margin: 10px;">\n\n            <span class="descChar" style="font-size: 14px; font-weight: bold">{{t.rede_nome}} - {{t.filial_nome}}</span>\n\n          </div>\n\n          <table class="mdl-data-table mdl-js-data-table" style="white-space: unset">\n\n            <thead>\n\n              <tr>\n\n                <th class="mdl-data-table__cell--non-numeric">Cliente</th>\n\n                <th>Data / Hora</th>\n\n                <th>Status</th>\n\n                <th>Foto</th>\n\n              </tr>\n\n            </thead>\n\n            <tbody> <!-- (mousedown)="longPress($event, tc)" (mouseup)="exechandler($event, tc)" -->\n\n              <tr *ngFor="let tc of t.content; let i = index" (press)="longPress($event, tc)">\n\n                <td style="padding: 0; padding-left: 2px; text-align: center;">{{tc.Cli_Nome}}</td>\n\n                <td>{{tc.JOB_Data_Job2}} {{tc.JOB_Hora_Job}}</td>\n\n                <td>{{tc.JOB_Status}}</td>\n\n                <td>\n\n                  <button (click)="seePhoto(tc)" class="mdl-button mdl-js-button mdl-button--icon">\n\n                    <i class="material-icons mdl-color-text--red-600">remove_red_eye</i>\n\n                  </button>\n\n                </td>\n\n              </tr>\n\n            </tbody>\n\n          </table>      \n\n        </div>\n\n        <ng-template #jobsElse>\n\n          <div style="width: 100%">\n\n            <div style="width: 100%; margin: 10px;">\n\n              <span class="descChar" style="font-size: 14px; font-weight: bold">{{t.rede_nome}} - {{t.filial_nome}}</span>\n\n            </div>\n\n            <div style="display: table; margin: auto;">\n\n              <i class="material-icons" style="display: table; margin: auto; font-size: 32px;">mood_bad</i>\n\n              <span style="display: table; margin: auto; font-size: 12px;">Nenhum job realizado até agora!</span>\n\n            </div>\n\n          </div>\n\n        </ng-template>\n\n      </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\jobs-do-dia\jobs-do-dia.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_vibration__["a" /* Vibration */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */]])
    ], JobsDoDiaPage);
    return JobsDoDiaPage;
}());

//# sourceMappingURL=jobs-do-dia.js.map

/***/ })

});
//# sourceMappingURL=6.js.map
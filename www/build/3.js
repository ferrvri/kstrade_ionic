webpackJsonp([3],{

/***/ 579:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientePromotoresPageModule", function() { return ClientePromotoresPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cliente_promotores__ = __webpack_require__(615);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ClientePromotoresPageModule = /** @class */ (function () {
    function ClientePromotoresPageModule() {
    }
    ClientePromotoresPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__cliente_promotores__["a" /* ClientePromotoresPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cliente_promotores__["a" /* ClientePromotoresPage */]),
            ],
        })
    ], ClientePromotoresPageModule);
    return ClientePromotoresPageModule;
}());

//# sourceMappingURL=cliente-promotores.module.js.map

/***/ }),

/***/ 607:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobInfosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JobInfosPage = /** @class */ (function () {
    function JobInfosPage(platform, _datePicker, navCtrl, navParams, _http) {
        this.platform = platform;
        this._datePicker = _datePicker;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        // map: GoogleMap;
        this.photos = [];
    }
    JobInfosPage.prototype.ngOnInit = function () {
        var _this = this;
        if (this.navParams.get('type') == 'photo') {
            //console.log(this.navParams.get('obs'));
            this.photos.push(this.navParams.get('job_foto'));
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectFotosExtras.php', {
                job_id: this.navParams.get('job_id')
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    response.data.result.forEach(function (element) {
                        _this.photos.push(element.JFOT_Foto);
                    });
                }
            });
        }
        else if (this.navParams.get('type') == 'local') {
            setTimeout(function () {
                _this.loadMap(_this.navParams.get('lat'), _this.navParams.get('long'));
            }, 500);
        }
    };
    JobInfosPage.prototype.loadMap = function (lat, lon) {
        if (lat != 'nada' && lon != 'nada') {
            var myLatlng = new window.google.maps.LatLng(lat, lon);
            var map = new window.google.maps.Map(document.getElementById('map_canvas'), {
                center: myLatlng,
                zoom: 17
            });
            var marker = new window.google.maps.Marker({
                position: myLatlng,
                map: map
            });
        }
    };
    JobInfosPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    JobInfosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-job-infos',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\job-infos\job-infos.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #f5f7fa; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">\n\n  <!-- <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n      <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n        <i class="material-icons">arrow_back</i>\n\n      </button>\n\n      <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n      <div class="mdl-layout-spacer"></div>\n\n    </header> -->\n\n  <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: rgba(0,0,0,0.4);" (click)="this.navParams.get(\'type\') != \'local\' ? this.navCtrl.pop() : return;">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <div *ngIf="this.navParams.get(\'type\') == \'photo\'">\n\n        <div style="background: #f5f7fa; border-radius: 2px;width: 95%; display: table; margin: 10% auto">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 42px; color: #000;">info_outline</i>\n\n          <div style="width: 100%; height: 1px; border-top: 1px solid #000; display: table; margin: 5px auto;"></div>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Observações</span>\n\n          <span style="display: table; margin: auto; font-size: 16px; padding: 15px;">{{this.navParams.get(\'obs\')}}</span>\n\n        </div>\n\n        <ion-slides zoom="true" style="margin: auto; display: table; margin-top: 5%">\n\n          <ion-slide *ngFor="let p of photos">\n\n            <div class="swiper-zoom-container">\n\n              <img style="width: 95%; margin: auto; display: table;" [src]="\'https://kstrade.com.br/sistema/assets/kstrade_php/uploads/\'+p" />\n\n            </div>\n\n          </ion-slide>\n\n        </ion-slides>\n\n        <div style="display: table; margin: auto; width: 100%; border-bottom: 1px solid #dcdcdc">\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Arraste para os\n\n            lados para visualizar as demais fotos</span>\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Toque fora da\n\n            foto para fechar</span>\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngIf="this.navParams.get(\'type\') == \'local\'">\n\n        <div style="position: absolute; top: 0; width: 100%; height: 100%; z-index: 5" (click)="this.navCtrl.pop()">\n\n        </div>\n\n        <div id="map_canvas" style="margin: 15% auto; width: 300px; height: 360px;" *ngIf="this.navParams.get(\'lat\') != \'nada\' && this.navParams.get(\'lon\') != \'nada\'"></div>\n\n        <div style="background: #f5f7fa; border-radius: 2px;width: 95%; display: table; margin: 20% auto" *ngIf="this.navParams.get(\'lat\') == \'nada\' || this.navParams.get(\'lon\') == \'nada\'">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 42px; color: #000;">location_off</i>\n\n          <div style="width: 80%; height: 1px; border-top: 1px solid #000; display: table; margin: 5px auto;"></div>\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #000; font-weight: bold">Este promotor não\n\n            divulgou sua localização</span>\n\n        </div>\n\n        <div style="display: table; margin: 10px auto; width: 100%; border-bottom: 1px solid #dcdcdc">\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Toque fora do\n\n            mapa para fechar</span>\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngIf="this.navParams.get(\'type\') == \'infos\'">\n\n        <div style="background: #f5f7fa; border-radius: 2px;width: 95%; display: table; margin: 20% auto">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 42px; color: #000;">info_outline</i>\n\n          <div style="width: 100%; height: 1px; border-top: 1px solid #000; display: table; margin: 5px auto;"></div>\n\n          <span style="display: table; margin: auto; margin-top: 30px; margin-bottom: 10px; font-size: 18px; font-weight: bold;">Informações\n\n            adquiridas do aparelho:</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 20px; font-size: 12px;">Para evitarmos fraudes,\n\n            adquirimos informações do aparelho.</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Modelo do\n\n            aparelho</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 15px; font-size: 16px;">{{this.navParams.get(\'modelo\')}}</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Sistema\n\n            operacional e versão</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 15px; font-size: 18px;">{{this.navParams.get(\'plataforma\')}}\n\n            - {{this.navParams.get(\'versao\')}}</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Fabricante</span>\n\n          <span style="display: table; margin: auto; font-size: 18px;">{{this.navParams.get(\'manufact\')}}</span>\n\n        </div>\n\n        <div style="display: table; margin: auto; width: 100%; border-bottom: 1px solid #dcdcdc">\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Toque fora da\n\n            foto para fechar</span>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\job-infos\job-infos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__["a" /* DatePicker */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__["a" /* HTTP */]])
    ], JobInfosPage);
    return JobInfosPage;
}());

//# sourceMappingURL=job-infos.js.map

/***/ }),

/***/ 611:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobsClientePromotorPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__job_infos_job_infos__ = __webpack_require__(607);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the JobsClientePromotorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var JobsClientePromotorPage = /** @class */ (function () {
    function JobsClientePromotorPage(platform, navCtrl, navParams, _http, _modal) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this._modal = _modal;
        this.table = [];
        this.entradas = [];
        this.showJob = true;
        this.showEntrada = false;
    }
    JobsClientePromotorPage.prototype.ngOnInit = function () {
        var _this = this;
        var mes = new Date().getMonth() + 1;
        this.navParams.get("content").forEach(function (element) {
            element.forEach(function (elm) {
                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectJobsOf.php', {
                    myid: elm.Aux_Promotor_ID,
                    rede_id: elm.Aux_Rede_ID,
                    filial_id: elm.Aux_Filial_ID,
                    dataone: new Date().getFullYear() + '-' +
                        (mes.valueOf() < 10 ? '0' + mes.toString() : mes.toString()) + '-' +
                        new Date().getDate()
                    // dataone: '2018-04-10'
                }, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        var o = {
                            rede_nome: '',
                            filial_nome: '',
                            content: []
                        };
                        if (response.data.result.length > 0) {
                            o.rede_nome = response.data.result[0].RED_Nome || '';
                            o.filial_nome = response.data.result[0].RFIL_Nome || '';
                            o.content = response.data.result;
                        }
                        _this.table.push(o);
                    }
                });
            });
        });
        if (this.navParams.get('entrada') == true) {
            this.navParams.get("content").forEach(function (element) {
                element.forEach(function (elm) {
                    _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectEntradaOf.php', {
                        pro_id: elm.Aux_Promotor_ID,
                        rede_id: elm.Aux_Rede_ID,
                        rfil_id: elm.Aux_Filial_ID,
                        dataone: new Date().getFullYear() + '-' +
                            (mes.valueOf() < 10 ? '0' + mes.toString() : mes.toString()) + '-' +
                            new Date().getDate()
                    }, {}).then(function (response) {
                        response.data = JSON.parse(response.data);
                        if (response.data.status == '0x104') {
                            var o = {
                                rede_nome: '',
                                filial_nome: '',
                                content: []
                            };
                            //console.log(response);
                            if (response.data.result.length > 0) {
                                o.rede_nome = response.data.result[0].RED_Nome || '';
                                o.filial_nome = response.data.result[0].RFIL_Nome || '';
                                o.content = response.data.result;
                            }
                            _this.entradas.push(o);
                            //console.log(this.entradas);
                        }
                    });
                });
            });
        }
    };
    JobsClientePromotorPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    JobsClientePromotorPage.prototype.seePhoto = function (type, j) {
        switch (type) {
            case 'photo':
                {
                    var a = this._modal.create(__WEBPACK_IMPORTED_MODULE_2__job_infos_job_infos__["a" /* JobInfosPage */], {
                        type: 'photo',
                        job_id: j.JOB_ID,
                        job_foto: j.JOB_Foto,
                        obs: j.JOB_Obs
                    });
                    a.present();
                }
                break;
            case 'local':
                {
                    var a = this._modal.create(__WEBPACK_IMPORTED_MODULE_2__job_infos_job_infos__["a" /* JobInfosPage */], {
                        type: 'local',
                        long: j.JOB_Longitude !== null ? j.JOB_Longitude : 'nada',
                        lat: j.JOB_Latitude !== null ? j.JOB_Latitude : 'nada'
                    });
                    a.present();
                }
                break;
            case 'infos':
                {
                    var a = this._modal.create(__WEBPACK_IMPORTED_MODULE_2__job_infos_job_infos__["a" /* JobInfosPage */], {
                        type: 'infos',
                        modelo: j.JOB_PRO_Modelo,
                        plataforma: j.JOB_PRO_Plataforma,
                        versao: j.JOB_PRO_Versao,
                        manufact: j.JOB_PRO_Manufact,
                        obs: j.JOB_Obs
                    });
                    a.present();
                }
                break;
        }
    };
    JobsClientePromotorPage.prototype.setOption = function (op) {
        switch (op) {
            case 'job': {
                this.showJob = !this.showJob;
                this.showEntrada = false;
                break;
            }
            case 'entrada': {
                this.showEntrada = !this.showEntrada;
                this.showJob = false;
                break;
            }
        }
    };
    JobsClientePromotorPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-jobs-cliente-promotor',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\jobs-cliente-promotor\jobs-cliente-promotor.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header mdl-color--white">\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n      <i class="material-icons">arrow_back</i>\n\n    </button>\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content mdl-color--white" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content mdl-color--white" style="background: #f5f7fa;">\n\n      <div style="background: #8c1515; height: 60px;">\n\n        <span class="char" style="position: relative; top: 15px; left: 25px; color: #f5f7fa; font-weight: bold;">{{this.navParams.get("PRO_Nome")}}</span>\n\n      </div>\n\n\n\n      <div style="width: 100%;">\n\n        <div (click)="setOption(\'job\')" style="width: 48%; margin: auto; display: inline-table;" class="{{showJob == true ? \'sublin\': \'\'}}">\n\n          <span style="color: #8c1515; display: table; margin: 5px auto">Jobs</span>\n\n        </div>\n\n        <div (click)="setOption(\'entrada\')" style="width: 48%; margin: auto; display: inline-table;" class="{{showEntrada == true ? \'sublin\': \'\'}}">\n\n          <span style="color: #8c1515; display: table; margin: 5px auto">Entradas / Saidas</span>\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngIf="showJob && !showEntrada">\n\n        <div style="display: table; margin: 2px auto; border-bottom: 1px solid #dcdcdc; background: #f5f7fa;" *ngFor="let t of table; let i = index">\n\n          <div *ngIf="t.content.length > 0; else jobsElse">\n\n            <div style="width: 100%; margin: 5px;">\n\n              <span class="descChar" style="font-size: 14px; font-weight: bold">{{t.rede_nome}} - {{t.filial_nome}}</span>\n\n            </div>\n\n            <table class="mdl-data-table mdl-js-data-table" style="white-space: unset">\n\n              <thead>\n\n                <tr>\n\n                  <th class="mdl-data-table__cell--non-numeric">Cliente</th>\n\n                  <th>Data / Hora</th>\n\n                  <th>Status</th>\n\n                  <th>Foto / Info</th>\n\n                </tr>\n\n              </thead>\n\n              <tbody>\n\n                <!-- (mousedown)="longPress($event, tc)" (mouseup)="exechandler($event, tc)" -->\n\n                <tr *ngFor="let tc of t.content; let i = index" (press)="longPress($event, tc)">\n\n                  <td style="padding: 0; padding-left: 2px; text-align: center;">{{tc.Cli_Nome}}</td>\n\n                  <td>{{tc.JOB_Data_Job2}} {{tc.JOB_Hora_Job}}</td>\n\n                  <td>{{tc.JOB_Status}}</td>\n\n                  <td>\n\n                    <button (click)="seePhoto(\'photo\', tc)" class="mdl-button mdl-js-button mdl-button--icon">\n\n                      <i class="material-icons mdl-color-text--red-600">remove_red_eye</i>\n\n                    </button>\n\n                    <button (click)="seePhoto(\'infos\', tc)" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n                      <i class="material-icons" style="color: #8c1515">info_outline</i>\n\n                    </button>\n\n                  </td>\n\n                </tr>\n\n              </tbody>\n\n            </table>\n\n          </div>\n\n          <ng-template #jobsElse>\n\n            <div style="width: 100%">\n\n              <div style="width: 100%; margin: 10px;">\n\n                <span class="descChar" style="font-size: 14px; font-weight: bold">{{t.rede_nome}} - {{t.filial_nome}}</span>\n\n              </div>\n\n              <div style="display: table; margin: auto;">\n\n                <i class="material-icons" style="display: table; margin: auto; font-size: 32px;">mood_bad</i>\n\n                <span style="display: table; margin: auto; font-size: 12px;">Nenhum job realizado até agora!</span>\n\n              </div>\n\n            </div>\n\n          </ng-template>\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngIf="showEntrada && !showJob">\n\n        <div style="display: table; margin: 2px auto; border-bottom: 1px solid #dcdcdc; background: #f5f7fa;" *ngFor="let t of entradas; let i = index">\n\n          <div *ngIf="t.content.length > 0; else entradaelse">\n\n            <div style="width: 100%; margin: 5px;">\n\n              <span class="descChar" style="font-size: 14px; font-weight: bold">{{t.rede_nome}} - {{t.filial_nome}}</span>\n\n            </div>\n\n\n\n            <table class="mdl-data-table mdl-js-data-table" style="white-space: unset">\n\n              <thead>\n\n                <tr>\n\n                  <th><i class="material-icons">account_circle</i></th>\n\n                  <th>Tipo</th>\n\n                  <th>Data/Hora Entrada</th>\n\n                  <th>Data/Hora Saida</th>\n\n                  <!-- <th>Informações</th> -->\n\n                </tr>\n\n              </thead>\n\n              <tbody>\n\n                <tr *ngFor="let o of t.content; let i = index" style="margin-bottom: 5px; margin-top: 5px;">\n\n                  <td style="width: 42px;">\n\n                    <div style="width: 32px; height: 32px; border-radius: 250px; position: relative;"\n\n                      [style.backgroundColor]="o.type == \'saida\' ? \'#f4e242\':\'#940000\'">\n\n                      <span class="material-icons" style="font-weight: bold; display: table; margin: auto; position: absolute; top: 25%; left: 25%;"\n\n                        [style.color]="o.type == \'saida\' ? \'#000\':\'#f5f7fa\'">transfer_within_a_station</span>\n\n                    </div>\n\n                  </td>\n\n\n\n                  <td>\n\n                    <span>\n\n                      {{ o.type == \'saida\' ? \'Saida\': \'Entrada\' }}\n\n                    </span>\n\n                  </td>\n\n\n\n                  <td>\n\n                    <span>\n\n                      {{ (o.aux_data_entrada + \' / \' + o.aux_hora_entrada ) }}\n\n                    </span>\n\n                  </td>\n\n\n\n                  <td>\n\n                    <span>\n\n                      {{(o.aux_data_saida || \'\') + \' / \' + (o.aux_hora_saida || \'\')}}\n\n                    </span>\n\n                  </td>\n\n                </tr>\n\n              </tbody>\n\n            </table>\n\n          </div>\n\n          <ng-template #entradaelse>\n\n            <div style="width: 100%">\n\n              <div style="width: 100%; margin: 10px;">\n\n                <span class="descChar" style="font-size: 14px; font-weight: bold">{{t.rede_nome}} - {{t.filial_nome}}</span>\n\n              </div>\n\n              <div style="display: table; margin: auto;">\n\n                <i class="material-icons" style="display: table; margin: auto; font-size: 32px;">mood_bad</i>\n\n                <span style="display: table; margin: auto; font-size: 12px;">Nenhuma entrada/saida realizada até agora!</span>\n\n              </div>\n\n            </div>\n\n          </ng-template>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\jobs-cliente-promotor\jobs-cliente-promotor.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */]])
    ], JobsClientePromotorPage);
    return JobsClientePromotorPage;
}());

//# sourceMappingURL=jobs-cliente-promotor.js.map

/***/ }),

/***/ 615:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClientePromotoresPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__jobs_cliente_promotor_jobs_cliente_promotor__ = __webpack_require__(611);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ClientePromotoresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ClientePromotoresPage = /** @class */ (function () {
    function ClientePromotoresPage(platform, navCtrl, navParams, _http, _modal) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this._modal = _modal;
        this.doneLoading = false;
        this.promos = [];
        this.resultsPromo = [];
    }
    ClientePromotoresPage.prototype.ngOnInit = function () {
        var _this = this;
        var o = {
            all: true,
            redes: [],
            clientes: []
        };
        if (JSON.parse(localStorage.getItem('session')).user !== undefined && JSON.parse(localStorage.getItem('session')).clientes) {
            delete o.redes;
            o.clientes = JSON.parse(localStorage.getItem('session')).clientes;
        }
        else {
            delete o.clientes;
            o.redes = JSON.parse(localStorage.getItem('session')).redes;
        }
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectPromotoresCliente.php', o, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            if (response.data.status == '0x104') {
                response.data.result.forEach(function (element2) {
                    _this.promos.push({ PRO_Nome: element2 });
                });
                _this.resultsPromo = _this.promos;
                setTimeout(function () {
                    _this.resultsPromo.sort(function (a, b) {
                        if (a.PRO_Nome < b.PRO_Nome) {
                            return -1;
                        }
                        if (a.PRO_Nome > b.PRO_Nome) {
                            return 1;
                        }
                        return 0;
                    });
                    _this.doneLoading = true;
                }, 3000);
            }
        });
    };
    ClientePromotoresPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    ClientePromotoresPage.prototype.openRedesFromPromotor = function (promo) {
        var _this = this;
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectPromotoresCliente.php', {
            nomes: promo.PRO_Nome,
            fromnome: true
        }, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            if (response.data.status == '0x104') {
                setTimeout(function () {
                    var modal = _this._modal.create(__WEBPACK_IMPORTED_MODULE_2__jobs_cliente_promotor_jobs_cliente_promotor__["a" /* JobsClientePromotorPage */], { entrada: true, content: response.data.result, PRO_Nome: promo.PRO_Nome });
                    modal.present();
                }, 500);
            }
        });
    };
    ClientePromotoresPage.prototype.searchPromotor = function (string) {
        var regexp = new RegExp((".*" + string.split("").join('.*') + ".*"), "i");
        this.resultsPromo = this.promos.filter(function (e) {
            return regexp.test(e.PRO_Nome);
        });
    };
    ClientePromotoresPage.prototype.restorePro = function (ev, string) {
        if (ev.which == 8) {
            if (string.length <= 2 || string.length == 0) {
                this.promos = [];
                this.ngOnInit();
            }
        }
    };
    ClientePromotoresPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cliente-promotores',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\cliente-promotores\cliente-promotores.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n      <i class="material-icons">arrow_back</i>\n\n    </button>\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" >\n\n      <div *ngIf="!doneLoading" style="display: table; margin: 2% auto;">\n\n        <div class="rotate" style="border-radius: 200px; width: 40px; height: 40px; border: 2px solid #940000; border-style: dashed solid dashed solid;">\n\n        </div>\n\n      </div>\n\n\n\n\n\n      \n\n      <div *ngIf="doneLoading"> \n\n        <div style="width: 100%; margin-bottom: 20px; position: relative;">\n\n          <input type="text" class="mdl-textfield__input" placeholder="Pesquisar promotor" style="width: 85%; display: table; margin: 15px auto;" #inputPro/>\n\n          <button (click)="searchPromotor(inputPro.value)" (keydown)="restorePro($event)" class="mdl-button mdl-js-button mdl-button--icon mdl-color--red-500 mdl-color-text--white" style="position: absolute; right: 10px; top: 0">\n\n            <i class="material-icons">search</i>\n\n          </button>\n\n        </div>\n\n\n\n        <div style="display: table; margin: auto;" *ngIf="this.resultsPromo.length < 1">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 32px;">info</i>\n\n          <span style="display: table; margin: auto; font-size: 12px;">Nenhum resultado encontrado!</span>\n\n        </div>\n\n\n\n        <div *ngFor="let p of resultsPromo; let i = index">\n\n            <div id="loja-{{i}}" class="mdl-card mdl-shadow--2dp mdl-js-button mdl-js-ripple-effect" style="position: relative; display: table; margin: 10px auto;" (click)="openRedesFromPromotor(p)">\n\n              <div class="mdl-card__title" style="position: relative; top: 10px; color: #8c1515;">\n\n                  <i class="material-icons" style="color: #8c1515; font-size: 30px; margin: 5px; position: relative; top: 3px">account_circle</i>\n\n                  <h2 class="mdl-card__title-text descChar" >{{p.PRO_Nome}}</h2>\n\n              </div>\n\n            </div>\n\n          </div>\n\n      </div>\n\n    </div>  \n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\cliente-promotores\cliente-promotores.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */]])
    ], ClientePromotoresPage);
    return ClientePromotoresPage;
}());

//# sourceMappingURL=cliente-promotores.js.map

/***/ })

});
//# sourceMappingURL=3.js.map
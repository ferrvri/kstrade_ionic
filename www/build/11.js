webpackJsonp([11],{

/***/ 587:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoordJobPageModule", function() { return CoordJobPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__coord_job__ = __webpack_require__(608);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CoordJobPageModule = /** @class */ (function () {
    function CoordJobPageModule() {
    }
    CoordJobPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__coord_job__["a" /* CoordJobPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__coord_job__["a" /* CoordJobPage */]),
            ],
        })
    ], CoordJobPageModule);
    return CoordJobPageModule;
}());

//# sourceMappingURL=coord-job.module.js.map

/***/ }),

/***/ 608:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoordJobPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_view_controller__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CoordJobPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CoordJobPage = /** @class */ (function () {
    function CoordJobPage(_geolocation, _alert, _viewCtrl, _http, navCtrl, navParams) {
        this._geolocation = _geolocation;
        this._alert = _alert;
        this._viewCtrl = _viewCtrl;
        this._http = _http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.obs = '';
    }
    CoordJobPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CoordJobPage');
    };
    CoordJobPage.prototype.ngOnInit = function () {
        var _this = this;
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        this._geolocation.getCurrentPosition().then(function (data) {
            _this.myLat = data.coords.latitude;
            _this.myLong = data.coords.longitude;
        }).catch(function (e) {
            //console.log(e);
            var a = _this._alert.create({
                title: 'Ative a localização',
                subTitle: 'Você deve ativar a localização!',
                buttons: ['Ok']
            });
            a.present();
            _this._viewCtrl.dismiss({});
        });
    };
    CoordJobPage.prototype.dismiss = function () {
        this._viewCtrl.dismiss();
    };
    CoordJobPage.prototype.doneJob = function () {
        var _this = this;
        var data = new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0, 10);
        var hora = (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes());
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertJobCoord.php', {
            usr_id: JSON.parse(localStorage.getItem('session')).user.Usr_ID,
            rede: this.navParams.get('rede_id'),
            filial: this.navParams.get('rfil_id'),
            status: 'Feedback',
            data: data,
            hora: hora,
            obs: this.obs,
            lat: this.myLat,
            lon: this.myLong
        }, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            if (response.data.status == '0x104') {
                _this._alert.create({
                    subTitle: 'Feedback eviado com sucesso'
                }).present();
                _this._viewCtrl.dismiss();
            }
        });
    };
    CoordJobPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-coord-job',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\coord-job\coord-job.html"*/'<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n    <div class="mdl-layout-spacer"></div>\n  </header>\n  <main class="mdl-layout__content mdl-color--white" style="height: calc(100vh - 56px); background: #f5f7fa">\n    <div class="page-content" style="background: #f5f7fa">\n      <div class="mdl-card" style="width: 100vw;">\n        <div class="mdl-card__title">\n        </div>\n        <div class="mdl-card__media mdl-color--white">\n          <span style="display: table; margin: auto">{{this.navParams.get(\'rede_nome\')}}</span>\n          <span style="display: table; margin: auto">{{this.navParams.get(\'rfil_nome\')}}</span>\n          <small style="text-align: center; display: table; margin: auto;">Deixe seu feedback para a equipe, o mesmo é muito importante para novos horizontes.</small>\n\n          <div style="width: 90%; margin: 30px auto; display: table;">\n            <span style="display: table; margin: auto;">Observações: </span>\n            <textarea id="txtJob" class="simpleInput" type="text" [(ngModel)]="this.obs" rows="5" cols="30"></textarea>\n          </div>\n        </div>\n      </div>\n    </div>\n  </main>\n  <div style="z-index: 5; position: relative; background: #8c1515; width: 100vw;">\n    <div style="display: table; margin: auto;">\n      <button (click)="dismiss()" class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-color--white">\n        <i class="material-icons">arrow_back</i>\n      </button>\n\n      <button (click)="doneJob()" class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-color--white">\n        <i class="material-icons">done</i>\n      </button>\n    </div>\n  </div>\n</div>\n'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\coord-job\coord-job.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_view_controller__["a" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], CoordJobPage);
    return CoordJobPage;
}());

//# sourceMappingURL=coord-job.js.map

/***/ })

});
//# sourceMappingURL=11.js.map
webpackJsonp([16],{

/***/ 580:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobInfosPageModule", function() { return JobInfosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__job_infos__ = __webpack_require__(607);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var JobInfosPageModule = /** @class */ (function () {
    function JobInfosPageModule() {
    }
    JobInfosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__job_infos__["a" /* JobInfosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__job_infos__["a" /* JobInfosPage */]),
            ],
        })
    ], JobInfosPageModule);
    return JobInfosPageModule;
}());

//# sourceMappingURL=job-infos.module.js.map

/***/ }),

/***/ 607:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobInfosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JobInfosPage = /** @class */ (function () {
    function JobInfosPage(platform, _datePicker, navCtrl, navParams, _http) {
        this.platform = platform;
        this._datePicker = _datePicker;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        // map: GoogleMap;
        this.photos = [];
    }
    JobInfosPage.prototype.ngOnInit = function () {
        var _this = this;
        if (this.navParams.get('type') == 'photo') {
            //console.log(this.navParams.get('obs'));
            this.photos.push(this.navParams.get('job_foto'));
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectFotosExtras.php', {
                job_id: this.navParams.get('job_id')
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    response.data.result.forEach(function (element) {
                        _this.photos.push(element.JFOT_Foto);
                    });
                }
            });
        }
        else if (this.navParams.get('type') == 'local') {
            setTimeout(function () {
                _this.loadMap(_this.navParams.get('lat'), _this.navParams.get('long'));
            }, 500);
        }
    };
    JobInfosPage.prototype.loadMap = function (lat, lon) {
        if (lat != 'nada' && lon != 'nada') {
            var myLatlng = new window.google.maps.LatLng(lat, lon);
            var map = new window.google.maps.Map(document.getElementById('map_canvas'), {
                center: myLatlng,
                zoom: 17
            });
            var marker = new window.google.maps.Marker({
                position: myLatlng,
                map: map
            });
        }
    };
    JobInfosPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    JobInfosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-job-infos',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\job-infos\job-infos.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #f5f7fa; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">\n\n  <!-- <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n      <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n        <i class="material-icons">arrow_back</i>\n\n      </button>\n\n      <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n      <div class="mdl-layout-spacer"></div>\n\n    </header> -->\n\n  <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: rgba(0,0,0,0.4);" (click)="this.navParams.get(\'type\') != \'local\' ? this.navCtrl.pop() : return;">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <div *ngIf="this.navParams.get(\'type\') == \'photo\'">\n\n        <div style="background: #f5f7fa; border-radius: 2px;width: 95%; display: table; margin: 10% auto">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 42px; color: #000;">info_outline</i>\n\n          <div style="width: 100%; height: 1px; border-top: 1px solid #000; display: table; margin: 5px auto;"></div>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Observações</span>\n\n          <span style="display: table; margin: auto; font-size: 16px; padding: 15px;">{{this.navParams.get(\'obs\')}}</span>\n\n        </div>\n\n        <ion-slides zoom="true" style="margin: auto; display: table; margin-top: 5%">\n\n          <ion-slide *ngFor="let p of photos">\n\n            <div class="swiper-zoom-container">\n\n              <img style="width: 95%; margin: auto; display: table;" [src]="\'https://kstrade.com.br/sistema/assets/kstrade_php/uploads/\'+p" />\n\n            </div>\n\n          </ion-slide>\n\n        </ion-slides>\n\n        <div style="display: table; margin: auto; width: 100%; border-bottom: 1px solid #dcdcdc">\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Arraste para os\n\n            lados para visualizar as demais fotos</span>\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Toque fora da\n\n            foto para fechar</span>\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngIf="this.navParams.get(\'type\') == \'local\'">\n\n        <div style="position: absolute; top: 0; width: 100%; height: 100%; z-index: 5" (click)="this.navCtrl.pop()">\n\n        </div>\n\n        <div id="map_canvas" style="margin: 15% auto; width: 300px; height: 360px;" *ngIf="this.navParams.get(\'lat\') != \'nada\' && this.navParams.get(\'lon\') != \'nada\'"></div>\n\n        <div style="background: #f5f7fa; border-radius: 2px;width: 95%; display: table; margin: 20% auto" *ngIf="this.navParams.get(\'lat\') == \'nada\' || this.navParams.get(\'lon\') == \'nada\'">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 42px; color: #000;">location_off</i>\n\n          <div style="width: 80%; height: 1px; border-top: 1px solid #000; display: table; margin: 5px auto;"></div>\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #000; font-weight: bold">Este promotor não\n\n            divulgou sua localização</span>\n\n        </div>\n\n        <div style="display: table; margin: 10px auto; width: 100%; border-bottom: 1px solid #dcdcdc">\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Toque fora do\n\n            mapa para fechar</span>\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngIf="this.navParams.get(\'type\') == \'infos\'">\n\n        <div style="background: #f5f7fa; border-radius: 2px;width: 95%; display: table; margin: 20% auto">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 42px; color: #000;">info_outline</i>\n\n          <div style="width: 100%; height: 1px; border-top: 1px solid #000; display: table; margin: 5px auto;"></div>\n\n          <span style="display: table; margin: auto; margin-top: 30px; margin-bottom: 10px; font-size: 18px; font-weight: bold;">Informações\n\n            adquiridas do aparelho:</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 20px; font-size: 12px;">Para evitarmos fraudes,\n\n            adquirimos informações do aparelho.</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Modelo do\n\n            aparelho</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 15px; font-size: 16px;">{{this.navParams.get(\'modelo\')}}</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Sistema\n\n            operacional e versão</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 15px; font-size: 18px;">{{this.navParams.get(\'plataforma\')}}\n\n            - {{this.navParams.get(\'versao\')}}</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Fabricante</span>\n\n          <span style="display: table; margin: auto; font-size: 18px;">{{this.navParams.get(\'manufact\')}}</span>\n\n        </div>\n\n        <div style="display: table; margin: auto; width: 100%; border-bottom: 1px solid #dcdcdc">\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Toque fora da\n\n            foto para fechar</span>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\job-infos\job-infos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__["a" /* DatePicker */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__["a" /* HTTP */]])
    ], JobInfosPage);
    return JobInfosPage;
}());

//# sourceMappingURL=job-infos.js.map

/***/ })

});
//# sourceMappingURL=16.js.map
webpackJsonp([14],{

/***/ 584:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PedidosPageModule", function() { return PedidosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pedidos__ = __webpack_require__(617);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PedidosPageModule = /** @class */ (function () {
    function PedidosPageModule() {
    }
    PedidosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__pedidos__["a" /* PedidosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pedidos__["a" /* PedidosPage */]),
            ],
        })
    ], PedidosPageModule);
    return PedidosPageModule;
}());

//# sourceMappingURL=pedidos.module.js.map

/***/ }),

/***/ 617:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PedidosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_connection_checker_connection_checker__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the PedidosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PedidosPage = /** @class */ (function () {
    function PedidosPage(_viewCtrl, _alert, utils, _http, _checker, platform, navCtrl, navParams) {
        this._viewCtrl = _viewCtrl;
        this._alert = _alert;
        this.utils = utils;
        this._http = _http;
        this._checker = _checker;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.valor = '';
        this.redes = [];
        this.redeOK = false;
        this.rede = 'Redes';
        this.redeIndex = '';
        this.clientes = [];
        this.clienteOK = false;
        this.cliente = 'Clientes';
        this.clienteIndex = '';
        this.obs = '';
    }
    PedidosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PedidosPage');
    };
    PedidosPage.prototype.ngOnInit = function () {
        var _this = this;
        this.userData = JSON.parse(localStorage.getItem('session'));
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectRedesRepresentante.php', {
            usuario_id: this.userData.user.Usr_ID
        }, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            if (response.data.status == '0x104') {
                _this.redes = response.data.result;
                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectClientesRepresentante.php', {
                    usuario_id: _this.userData.user.Usr_ID
                }, {}).then(function (response2) {
                    response2.data = JSON.parse(response2.data);
                    if (response2.data.status == '0x104') {
                        _this.clientes = response2.data.result;
                    }
                });
            }
        });
    };
    PedidosPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    PedidosPage.prototype.setRede = function (rede) {
        this.rede = rede.RED_Nome;
        this.redeIndex = rede.RED_ID;
        this.redeOK = !this.redeOK;
    };
    PedidosPage.prototype.setCliente = function (cliente) {
        this.cliente = cliente.Cli_Nome;
        this.clienteIndex = cliente.Cli_ID;
        this.clienteOK = !this.clienteOK;
    };
    PedidosPage.prototype.insertPedido = function () {
        var _this = this;
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/insertPedido.php', {
            rede: this.redeIndex,
            cliente: this.clienteIndex,
            usuario: this.userData.user.Usr_ID,
            valor: this.valor.replace('.', '').replace(',', '.'),
            data: new Date(Date.now()).toISOString().split("T")[0]
        }, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            if (response.data.status == '0x104') {
                var a = _this._alert.create({
                    subTitle: 'Seu pedido foi enviado!',
                    message: 'A equipe entrará em contato em breve.'
                });
                a.present();
                a.onDidDismiss(function () {
                    _this._viewCtrl.dismiss();
                });
            }
        });
    };
    PedidosPage.prototype.applyMoneyMask = function () {
        this.valor = this.utils.detectAmount(this.valor);
    };
    PedidosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pedidos',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\area-representante\pedidos\pedidos.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;"></div>\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n    <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;"\n      class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n      <i class="material-icons">arrow_back</i>\n    </button>\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n    <div class="mdl-layout-spacer"></div>\n  </header>\n\n  <div *ngIf="this._checker.getMStatus() == \'offline\'" style="text-align:center; width: 100%; background: #f44242">\n    <p style="font-size: 10px; margin: 2px; color: white">Sem internet</p>\n  </div>\n\n  <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: #f5f7fa">\n    <div class="page-content" style="background: #f5f7fa">\n      <span style="display: table; margin: auto; font-size: 14px; font-weight: 700">Novo pedido</span>\n\n      <span style="display: table; margin: auto; font-size: 12px;">Selecione a Rede</span>\n\n      <div class="mdl-textfield mdl-js-textfield comboBox">\n        <input style="cursor: pointer" readonly [(ngModel)]="this.rede" (click)="redeOK = !redeOK" type="text"\n          class="comboBox__input">\n\n        <button class="comboBox__button mdl-button mdl-js-button mdl-button--icon" (click)="redeOK = !redeOK">\n          <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>\n        </button>\n\n        <div class="comboBox__items" *ngIf="redeOK" (blur)="redeOK = false">\n          <button *ngFor="let c of redes; let i = index" (click)="setRede(c)"\n            class="comboBox__item mdl-button mdl-js-button" style="position: relative;">\n            <span style="position: absolute; left: 2px; top: 2px;">{{c.RED_Nome}}</span>\n          </button>\n        </div>\n      </div>\n\n\n      <span style="display: table; margin: auto; font-size: 12px;">Selecione o Cliente</span>\n\n      <div class="mdl-textfield mdl-js-textfield comboBox">\n        <input style="cursor: pointer" readonly [(ngModel)]="this.cliente" (click)="clienteOK = !clienteOK" type="text"\n          class="comboBox__input">\n\n        <button class="comboBox__button mdl-button mdl-js-button mdl-button--icon" (click)="clienteOK = !clienteOK">\n          <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>\n        </button>\n\n        <div class="comboBox__items" *ngIf="clienteOK" (blur)="clienteOK = false">\n          <button *ngFor="let c of clientes; let i = index" (click)="setCliente(c)"\n            class="comboBox__item mdl-button mdl-js-button" style="position: relative;">\n            <span style="position: absolute; left: 2px; top: 2px;">{{c.Cli_Nome}}</span>\n          </button>\n        </div>\n      </div>\n\n      <div style="width: 90%; margin: auto; display: table;">\n        <span style="display: table; margin: auto; font-size: 12px;">Observações: </span>\n        <textarea id="txtJob" class="simpleInput" style="border: 1px solid #dcdcdc; border-radius: 6px" type="text" [(ngModel)]="this.obs" rows="5" cols="30"></textarea>\n      </div>\n\n      <span style="display: table; margin: auto; font-size: 12px;">Valor do pedido</span>\n\n      <div class="mdl-textfield mdl-js-textfield comboBox">\n        <input [(ngModel)]="this.valor" type="text" class="comboBox__input" (keydown)="applyMoneyMask(this.valor)">\n      </div>\n\n\n      <button (click)="insertPedido()" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-color-text--white"\n        style="background: #8c1515; z-index: 999; margin: 10px; width: 85%; margin: auto; display: table;">\n        <span>Enviar pedido</span>\n      </button>\n\n    </div>\n  </main>\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\area-representante\pedidos\pedidos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_3__providers_connection_checker_connection_checker__["a" /* ConnectionCheckerProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], PedidosPage);
    return PedidosPage;
}());

//# sourceMappingURL=pedidos.js.map

/***/ })

});
//# sourceMappingURL=14.js.map
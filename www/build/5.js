webpackJsonp([5],{

/***/ 589:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatasCriticasPageModule", function() { return DatasCriticasPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__datas_criticas__ = __webpack_require__(620);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DatasCriticasPageModule = /** @class */ (function () {
    function DatasCriticasPageModule() {
    }
    DatasCriticasPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__datas_criticas__["a" /* DatasCriticasPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__datas_criticas__["a" /* DatasCriticasPage */]),
            ],
        })
    ], DatasCriticasPageModule);
    return DatasCriticasPageModule;
}());

//# sourceMappingURL=datas-criticas.module.js.map

/***/ }),

/***/ 612:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CriticModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CriticModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CriticModalPage = /** @class */ (function () {
    function CriticModalPage(platform, _alert, _datePicker, navCtrl, navParams, _http) {
        this.platform = platform;
        this._alert = _alert;
        this._datePicker = _datePicker;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this.pesquisas = [];
        this.data_inicio = {
            value: '',
            showValue: 'Data inicial',
            selected: false
        };
        this.data_fim = {
            showValue: 'Data final',
            value: '',
            selected: false
        };
    }
    CriticModalPage.prototype.daysBetween = function (date1, date2) {
        var one_day = 1000 * 60 * 60 * 24;
        var date1_ms = new Date(date1).getTime();
        var date2_ms = new Date(date2).getTime();
        var difference_ms = date2_ms - date1_ms;
        date2 = date2.split('-')[0] + '-' + date2.split('-')[2] + '-' + date2.split('-')[1];
        return Math.round(difference_ms / one_day);
    };
    CriticModalPage.prototype.ngOnInit = function () {
        var _this = this;
        if (this.navParams.get('cliente')) {
            this.pesquisas = [];
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectPesquisas_2.php', {
                clientes: this.navParams.get('cliente'),
                dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
                datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    _this.pesquisas = response.data.result;
                }
            });
        }
        else if (this.navParams.get('forVendedor') == true) {
            this.pesquisas = [];
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectPesquisas_2.php', {
                cliente: JSON.parse(localStorage.getItem('session')).redes[0].aux_cliente_id,
                dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
                datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    _this.pesquisas = response.data.result;
                }
            });
        }
        else if (this.navParams.get('forExterno') == true) {
            this.pesquisas = [];
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectPesquisas_2.php', {
                dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
                datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    _this.pesquisas = response.data.result;
                }
            });
        }
    };
    CriticModalPage.prototype.showDate = function (obj) {
        this._datePicker.show({
            allowFutureDates: true,
            date: new Date(),
            mode: 'date',
            androidTheme: this._datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
        }).then(function (date) {
            obj.selected = true;
            obj.value = date;
            obj.showValue = date.toISOString().split('T')[0].split('-')[2] + '/' +
                date.toISOString().split('T')[0].split('-')[1] + '/' +
                date.toISOString().split('T')[0].split('-')[0];
        }, function (err) {
            obj.selected = false;
        });
    };
    CriticModalPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    CriticModalPage.prototype.selectPesquisas = function (init, end) {
        var _this = this;
        if (this.data_inicio.selected == false || this.data_fim.selected == false) {
            var a = this._alert.create({
                title: 'Selecione uma data!',
                subTitle: 'Para utilizar o filtro, deve-se escolher a Data inicial e Data final'
            });
            a.present();
        }
        else {
            if (this.navParams.get('cliente')) {
                this.pesquisas = [];
                this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectPesquisas_2.php', {
                    clientes: this.navParams.get('cliente'),
                    dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
                    datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
                }, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        _this.pesquisas = response.data.result;
                    }
                });
            }
            else if (this.navParams.get('forVendedor') == true) {
                this.pesquisas = [];
                this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectPesquisas_2.php', {
                    cliente: JSON.parse(localStorage.getItem('session')).redes[0].aux_cliente_id,
                    dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
                    datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
                }, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        _this.pesquisas = response.data.result;
                    }
                });
            }
        }
    };
    CriticModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-critic-modal',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\datas-criticas-modal\critic-modal.html"*/'<!-- NAO APAGAR COMENTARIOS DO METODO, POSSIVEL IMPLEMENTAÇÃO FUTURA -->\n\n<!-- PEDIDO QUE FOSSE REMOVIDO TAIS FUNCOES, MAS COM POSSIBILIDADE DE RETORNO DAS MESMAS   --- 13/10/2019 -->\n\n\n\n\n\n<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n      <i class="material-icons">arrow_back</i>\n\n    </button>\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content mdl-color--white" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <div *ngIf="this.navParams.get(\'rede_nome\') !== undefined" style="background: #8c1515; height: 60px;">\n\n        <span class="char" style="position: relative; top: 20px; left: 25px; color: #f5f7fa; font-weight: bold;">{{this.navParams.get(\'rede_nome\')}} - {{this.navParams.get(\'rfil_nome\')}}</span>\n\n      </div>\n\n\n\n      <!-- <div style="display: table; margin: auto; width: 100%;">\n\n        <span style="display: table; margin: 4px auto">Filtrar</span>\n\n        <ion-item style="width: 90%; display: table; margin: auto">\n\n          <button class="mdl-button mdl-js-button mdl-js-ripple-effect" (click)="showDate(data_inicio)" style="width: 120px; text-transform: none;">\n\n            <span>{{data_inicio.showValue}}</span>\n\n          </button>\n\n          <div style="display: inline-table; width: 35px; height: 30px; background: #8c1515; color: #f5f7fa; border-radius: 2px;" >\n\n            Data\n\n          </div>\n\n          <button class="mdl-button mdl-js-button mdl-js-ripple-effect" (click)="showDate(data_fim)" style="width: 120px; text-transform: none;">\n\n            <span>{{data_fim.showValue}}</span>\n\n          </button>\n\n        </ion-item>\n\n        <button class="mdl-button mdl-js-button mdl-color--red-500 mdl-color-text--white" (click)="selectPesquisas(data_inicio.value, data_fim.value)" style="margin: auto;display: table; width: 100%;">\n\n          <span>Filtrar</span>\n\n        </button>\n\n      </div> -->\n\n      \n\n      <div *ngFor="let f of pesquisas" style="border-top: 1px solid #212121; margin-top: 10px; cursor: pointer">\n\n          <span  style="font-size: 16px; cursor: pointer; position: relative; top: 10px; font-weight: bold; display: table; margin: auto">\n\n            {{f.cliente_nome}} - {{f.dias_semana.replace(\';\', \' / \').replace(\';\', \' / \').replace(\';\', \' / \').replace(\';\', \' / \').replace(\';\', \' / \')}}\n\n          </span>\n\n\n\n          <div *ngFor="let fc of f.content; let fI = index">\n\n            <span style="font-size: 12px; cursor: pointer; position: relative; top: 10px; font-weight: bold;">{{fc.pro_nome}}</span>\n\n            <div style="width: 100%; border-top: 1px solid #940000; display: table;margin: 25px auto; border-bottom: 1px solid #fefefe;" >\n\n              \n\n              <div *ngIf="fc.pesquisas.length < 1" style="display: table; margin: 5px auto; width: 55%">\n\n                <i class="material-icons" style="font-size: 22px; display: table; margin: auto; margin-bottom: 10px ">warning</i>\n\n                <div style="width: 100%; height: 1px; background-color: #dcdcdc;"></div>\n\n                <span style="display: table; margin: auto;  margin-top: 10px; font-size: 10px">Nenhuma pesquisa encontrada!</span>\n\n              </div>\n\n\n\n              <table *ngIf="fc.pesquisas.length > 0" class="ion-diagnostic-table" style="font-size: 13px; margin-bottom: 20px; width: 100%;">\n\n                <thead>\n\n                  <tr>\n\n                    <td>Dt. de realização</td>\n\n                    <!-- <td>Dt. de vencimento</td> -->\n\n                    <!-- <td>Qntd. (Un.)</td> -->\n\n                    <!-- <td >Preço (R$)</td>\n\n                    <td>N. de Frentes</td> -->\n\n                    <td>Rupt. Total</td>\n\n                    <td>Prod. em Estoque</td>\n\n                    <td>Prod. em venda</td>\n\n                  </tr>\n\n                </thead>\n\n                <tbody>\n\n                  <tr style="cursor: pointer" *ngFor="let infos of fc.pesquisas; let pI = index" class="{{(infos.hasDif === true) ? \'dangerRow\':\'\'}}">\n\n                    <td>{{infos.pes_data2}}</td>\n\n                    <!-- \n\n                    <td>\n\n                      <span *ngIf="infos.pes_data_vencimento.split(\';\').length == 0">Vazio</span>\n\n                      <span *ngFor="let sd of infos.pes_data_vencimento.split(\';\'); let sindex = index">{{(sd == \'\' ? \'\': sd)}}<br /></span>\n\n                    </td>\n\n                    <td>\n\n                      <span *ngIf="infos.pes_quantidade.split(\';\').length < 1">Vazio</span>\n\n                      <span *ngFor="let si of infos.pes_quantidade.split(\';\'); let sindex = index">{{ (si == \'\' ? \'\': si)}} <br /></span>\n\n                    </td> -->\n\n                    <!-- <td>{{infos.pes_preco.replace(\'.\',\',\').replace(\',\', \'.\')}}</td>\n\n                    <td>{{infos.pes_numero_frentes || \'Vazio\'}}</td>\n\n                    -->\n\n                    <td>\n\n                      <span class="material-icons {{infos.pes_rupturatotal == \'1\'? \'rupturaTr\':\'normalTr\'}}">done_outline</span>\n\n                    </td>\n\n                    <td>\n\n                      <span class="material-icons {{infos.pes_estoque == \'1\'? \'dangerTr\':\'normalTr\'}}">done_outline</span>\n\n                    </td>\n\n                    <td>\n\n                      <span class="material-icons {{infos.pes_prateleira == \'1\'? \'dangerTr\':\'normalTr\'}}">done_outline</span>\n\n                    </td>\n\n                  </tr>\n\n                </tbody>\n\n              </table>\n\n            </div>\n\n          </div>\n\n        </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\datas-criticas-modal\critic-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__["a" /* DatePicker */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__["a" /* HTTP */]])
    ], CriticModalPage);
    return CriticModalPage;
}());

//# sourceMappingURL=critic-modal.js.map

/***/ }),

/***/ 620:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatasCriticasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__datas_criticas_modal_critic_modal__ = __webpack_require__(612);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the DatasCriticasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
*/
var DatasCriticasPage = /** @class */ (function () {
    function DatasCriticasPage(platform, navCtrl, navParams, _http, _modal) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this._modal = _modal;
        this.lojas = [];
        this.doneLoading = false;
    }
    DatasCriticasPage.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem('session')) {
            JSON.parse(localStorage.getItem('session')).forEach(function (element) {
                if (element.content.length > 0) {
                    element.content.forEach(function (e) {
                        _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/promotorOfRedeFilial.php', {
                            rede: e.RED_ID,
                            filial: e.RFIL_ID,
                            nome: JSON.parse(localStorage.getItem('session'))[0].content[0].PRO_Nome
                        }, {}).then(function (response) {
                            response.data = JSON.parse(response.data);
                            if (response.data.status == '0x104') {
                                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectRotina.php', {
                                    rede: e.RED_ID,
                                    filial: e.RFIL_ID,
                                    promotor: response.data.result[0].Aux_Promotor_ID
                                }, {}).then(function (response2) {
                                    response2.data = JSON.parse(response2.data);
                                    if (response2.data.status == '0x104') {
                                        var o_1 = {
                                            RFIL_ID: e.RFIL_ID,
                                            RFIL_Nome: e.RFIL_Nome,
                                            RED_ID: e.RED_ID,
                                            RED_Nome: e.RED_Nome,
                                            RFIL_Bairro: e.RFIL_Bairro,
                                            RFIL_CEP: e.RFIL_CEP,
                                            RFIL_Cidade: e.RFIL_Cidade,
                                            RFIL_Endereco: e.RFIL_Endereco,
                                            RFIL_Estado: e.RFIL_Estado,
                                            RFIL_Numero: e.RFIL_Numero,
                                            RFIL_Telefone: e.RFIL_Telefone,
                                            // RFIL_Numero_Visitas: parseInt(e.RFIL_Numero_Visitas),            ​
                                            RFIL_Numero_Visitas: 0,
                                            cliente: [],
                                            disabled: true
                                        };
                                        response2.data.result.forEach(function (element) {
                                            var days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                                            var dayName = days[new Date().getDay()];
                                            var elem = element.aux_semana.split(';');
                                            elem.forEach(function (element2) {
                                                if (element2 == dayName) {
                                                    o_1.cliente.push(element);
                                                    o_1.disabled = false;
                                                }
                                            });
                                        });
                                        _this.lojas.push(o_1);
                                    }
                                });
                                setTimeout(function () {
                                    _this.doneLoading = true;
                                }, 1500);
                            }
                        });
                    });
                }
            });
        }
    };
    DatasCriticasPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    DatasCriticasPage.prototype.openCritic = function (c) {
        var m = this._modal.create(__WEBPACK_IMPORTED_MODULE_2__datas_criticas_modal_critic_modal__["a" /* CriticModalPage */], {
            cliente: c.cliente,
            rede_nome: c.RED_Nome,
            rede_id: c.RED_ID,
            rfil_nome: c.RFIL_Nome,
            rfil_id: c.RFIL_ID
        });
        m.present();
    };
    DatasCriticasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-datas-criticas',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\datas-criticas\datas-criticas.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n      <i class="material-icons">arrow_back</i>\n\n    </button>\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" >\n\n      <div *ngIf="!doneLoading" style="display: table; margin: 2% auto;">\n\n        <div class="rotate" style="border-radius: 200px; width: 40px; height: 40px; border: 2px solid #940000; border-style: dashed solid dashed solid;">\n\n        </div>\n\n      </div>\n\n      <div *ngIf="doneLoading">\n\n        <span class="mdl-color-text--red-500" style="display: table; margin: 10px auto; font-size: 18px">Datas críticas</span>\n\n        <div *ngFor="let r of lojas; let i = index">\n\n          <div id="loja-{{i}}" class="mdl-card mdl-shadow--2dp mdl-js-button mdl-js-ripple-effect" style="position: relative; display: table; margin: 10px auto;" (click)="r.cliente.length > 0 && r.disabled == false ? openCritic(r): return;">\n\n            <div class="mdl-card__title" style="color: #8c1515;">\n\n              <h2 class="mdl-card__title-text descChar" style="position: relative; top: 15px">{{r.RED_Nome}} - {{r.RFIL_Nome}} - {{r.RFIL_Cidade}}</h2>\n\n            </div>\n\n            <div class="mdl-card__supporting-text">\n\n              <span>Endereço: {{r.RFIL_Endereco}}</span><br />\n\n              <span>Bairro : {{r.RFIL_Bairro}}</span><br />\n\n              <span>CEP: {{r.RFIL_CEP}}</span> ​<br />\n\n              <span> Estado: {{r.RFIL_Estado}}</span><br />\n\n              <span>Numero: {{r.RFIL_Numero}}</span>​<br />\n\n              <span>Telefone de Contato: {{r.RFIL_Telefone}}</span>\n\n            </div>\n\n            <div class="mdl-card__actions" style="border-top: 1px solid #dcdcdc; min-height: 0; max-height: 0; height: 60px;">\n\n              <span class="material-icons" style="float: right; margin: 10px; padding: 10px; background-color: rgba(122, 23, 23, .7); border-radius: 150px; color: #f5f7fa; font-weight: bold">{{r.disabled == true  || r.cliente.length < 1 ? \'done\': \'warning\' }}</span>\n\n            </div>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>  \n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\datas-criticas\datas-criticas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */]])
    ], DatasCriticasPage);
    return DatasCriticasPage;
}());

//# sourceMappingURL=datas-criticas.js.map

/***/ })

});
//# sourceMappingURL=5.js.map
webpackJsonp([30],{

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RotaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_sweetalert2__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the RotaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RotaProvider = /** @class */ (function () {
    function RotaProvider(http, _geolocation) {
        this.http = http;
        this._geolocation = _geolocation;
        this._enabled = false;
        this.interval = undefined;
        if (localStorage.getItem('session')) {
            this.localStorageData = JSON.parse(localStorage.getItem('session'));
        }
    }
    Object.defineProperty(RotaProvider.prototype, "enabled", {
        get: function () {
            return this._enabled;
        },
        set: function (value) {
            this._enabled = value;
        },
        enumerable: true,
        configurable: true
    });
    RotaProvider.prototype.enableRota = function (object) {
        this.start(object);
    };
    RotaProvider.prototype.disableRota = function (object) {
        if (this.enabled == true && this.interval !== undefined) {
            clearInterval(this.interval);
            this.interval = undefined;
            this.enabled = false;
        }
    };
    RotaProvider.prototype.start = function (object) {
        var _this = this;
        if (this.enabled == false && this.interval === undefined) {
            this.enabled = true;
            console.log('started');
            this.enabled = true;
            this.http.post(' http://eccotrade.com.br/sistema/assets/eccotrade_php/rota/insertRota.php', {
                type: 'first',
                promotor_id: object.pro_id,
                rede_id: object.rede_id,
                filial_id: object.rfil_id,
                aux_data_inicio: object.data,
                aux_hora_inicio: object.hora
            }).subscribe(function (response) {
                if (response.status == '0x104') {
                    console.log('starting on', response);
                    _this.interval = setInterval(function () {
                        console.log('ok interval rota');
                        _this._geolocation.getCurrentPosition().then(function (data) {
                            _this.http.post(' http://eccotrade.com.br/sistema/assets/eccotrade_php/rota/insertRota.php', {
                                type: 'second',
                                long: data.coords.longitude,
                                lat: data.coords.latitude,
                                rota_id: response.id
                            }).subscribe(function (response2) {
                                if (response2.status == '0xEC') {
                                    clearInterval(_this.interval);
                                    _this.interval = undefined;
                                    _this.enabled = false;
                                }
                                else if (response2.status == '0x102') {
                                    __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                                        title: 'Problema com rota',
                                        text: 'Erro ao salvar rota.',
                                        type: 'error'
                                    });
                                }
                            });
                        });
                        // }, 5000);
                    }, 1000 * 60 * 1);
                    // }else{
                    //   Swal({
                    //     title: 'Problema com rota',
                    //     text: 'Erro ao salvar rota.',
                    //     type: 'error'
                    //   });
                }
            });
        }
        else {
            __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                title: 'Rota não iniciada',
                text: 'A rota não pôde ser iniciada..',
                type: 'warning',
                showCloseButton: true
            });
        }
    };
    RotaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */]])
    ], RotaProvider);
    return RotaProvider;
}());

//# sourceMappingURL=rota.js.map

/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/*
  Generated class for the UtilsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var UtilsProvider = /** @class */ (function () {
    function UtilsProvider() {
    }
    UtilsProvider.prototype.detectAmount = function (v) {
        if (v) {
            this.n = v[v.length - 1];
            if (isNaN(this.n)) {
                v = v.substring(0, v.length - 1);
                return v;
            }
            v = this.fixAmount(v);
            return v;
        }
    };
    UtilsProvider.prototype.fixAmount = function (a) {
        var period = a.indexOf(".");
        if (period > -1) {
            a = a.substring(0, period) + a.substring(period + 1);
        }
        this.len = a.length;
        while (this.len < 3) {
            a = "0" + a;
            this.len = a.length;
        }
        a = a.substring(0, this.len - 2) + "." + a.substring(this.len - 2, this.len);
        while (a.length > 4 && (a[0] == '0')) {
            a = a.substring(1);
        }
        if (a[0] == ".") {
            a = "0" + a;
        }
        return (a);
    };
    UtilsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], UtilsProvider);
    return UtilsProvider;
}());

//# sourceMappingURL=utils.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GreetingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_view_controller__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the GreetingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GreetingsPage = /** @class */ (function () {
    function GreetingsPage(viewCtrl, navCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.showButton = false;
    }
    GreetingsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad GreetingsPage');
        setTimeout(function () {
            _this.showButton = true;
        }, 2000);
    };
    GreetingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-greetings',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\greetings\greetings.html"*/'<div style="position: relative; width: 100%; height: 100vh">\n  <div style="background: rgba(0,0,0,0.925); position: absolute; top: 0; width: 100%; height: 100vh">\n  </div>\n\n  <img src="./assets/images/logoecco.png" style="width: 70%; z-index: 10; position: absolute; left: 15%; top: 5%;"\n    class="element-animation">\n\n\n  <div style="width: 100%; position: absolute; z-index: 10; top: 25%; height: 100vh">\n    <div class="element-animation" style="width: 90%; margin: auto; display: table; text-align: center; color: #fff;">\n      <div style="width: 100%;">\n        <span style="font-weight: bold;" class="element-animation">\n          Seja muito bem vindo!\n        </span>\n      </div>\n\n      <br>\n      <br>\n      \n      <span style="width: 85%; font-size: 14px; margin-top: 50px">\n        Acompanhe todas as novidades! <br> Com os tablóides atualizados acompanhe tudo o que acontece no mundo do\n        Merchandising.\n      </span>\n      <br>\n      <span style="width: 85%; font-size: 14px; margin-top: 50px">\n        Aqui você encontrará com facilidade aquela promoção que tanto procura!\n      </span>\n\n      <button  (click)="viewCtrl.dismiss()" *ngIf="showButton" class="fade-in mdl-button mdl-js-button mdl-js-ripple-effect"\n        style="border: 1px solid #fff; width: 100%; margin: 10% auto; display: table;">\n        <span style="color: #fff">Okay</span>\n      </button>\n\n\n\n      <a href="https://technoxinformatica.com.br/">\n        <img src="./assets/images/logo_technox.png" style="width: 50%; z-index: 10; display: table; margin: 25% auto">\n      </a>\n    </div>\n  </div>\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\greetings\greetings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_view_controller__["a" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], GreetingsPage);
    return GreetingsPage;
}());

//# sourceMappingURL=greetings.js.map

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InitPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_browser_tab__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_network__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_local_notifications__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_connection_checker_connection_checker__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__loading_loading__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var InitPage = /** @class */ (function () {
    function InitPage(checker, platform, _connCheck, _localNotifications, 
        // public _background: BackgroundMode, 
        _network, browserTab, _alert, navParams, _http, navCtrl) {
        this.checker = checker;
        this.platform = platform;
        this._connCheck = _connCheck;
        this._localNotifications = _localNotifications;
        this._network = _network;
        this.browserTab = browserTab;
        this._alert = _alert;
        this.navParams = navParams;
        this._http = _http;
        this.navCtrl = navCtrl;
        this.connected = false;
        this.problemLabel = false;
        this.version = '';
    }
    InitPage.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.problemLabel = true;
        }, 10000);
        this._http.get('https://kstrade.com.br/sistema/assets/kstrade_php/checkversion.php', {}, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            _this.checker.setMStatus('online');
            if (response.status == 200) {
                localStorage.removeItem('netinfo');
                localStorage.setItem('netinfo', JSON.stringify({ connected: true }));
                if (response.data.version) {
                    if (localStorage.getItem('version')) {
                        _this.version = JSON.parse(localStorage.getItem('version')).version[0];
                        if (parseFloat(_this.version) < parseFloat(response.data.version[0]) || parseFloat(response.data.version[0]) > parseFloat(_this.version)) {
                            document.addEventListener('backbutton', function () {
                                var a = _this._alert.create({
                                    subTitle: 'Atualize!',
                                    message: 'Você deve atualizar o aplicativo!',
                                    buttons: ['OK']
                                });
                                a.present();
                                return;
                            }, false);
                            document.addEventListener('pause', function () {
                                localStorage.setItem('version', JSON.stringify(response.data));
                            }, false);
                            _this.browserTab.openUrl('http://kstrade.com.br/sistema');
                        }
                        else {
                            _this.redirToMain();
                        }
                    }
                    else {
                        localStorage.setItem('version', JSON.stringify(response.data));
                        _this.redirToMain();
                    }
                }
            }
            else if (response.status == 400 || response.status == 404 || response.status == 504) {
                _this.connected = false;
                var a = _this._alert.create({
                    title: 'Sem internet',
                    subTitle: 'Você não está conectado à internet, por favor verifique sua conectividade',
                    buttons: ['Ok']
                });
                a.present();
            }
        }, function (err) {
            if (localStorage.getItem('session')) {
                _this.checker.setMStatus('offline');
            }
            else {
                _this.checker.setMStatus('offline');
            }
            _this.redirToMain();
        });
    };
    InitPage.prototype.redirToMain = function () {
        var _this = this;
        setTimeout(function () {
            if (!localStorage.getItem('session')) {
                localStorage.setItem('session', JSON.stringify({
                    visitante: {
                        Usr_Nome: 'Temporario',
                        Usr_Email: '',
                        Usr_Telefone: '',
                        Usr_Nivel: 'Visitante'
                    }
                }));
            }
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__loading_loading__["a" /* LoadingPage */], { page: 'main' }, { animate: false });
            _this.connected = true;
        }, 1000);
    };
    InitPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-init',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\init\init.html"*/'<div *ngIf="!connected;" style="background: #f5f7fa; height: calc(100vh - 10px)">\n\n  <img src="./assets/images/logoecco.png" style="position: relative; left: 15px; display: table; margin: auto; width: 200px; top: 5%">\n\n  <img src="./assets/images/loading.gif" style="display: table; margin: 25% auto; width: 64px;"/>\n\n  <span *ngIf="problemLabel" style="font-size: 14px; position: relative;bottom: 65px;margin: auto;display: table;">Problemas com a Internet...</span>\n\n  <i *ngIf="problemLabel" class="material-icons routerAnim" style="display: table; margin: auto">router</i>\n\n  <span style="display: table; margin:  auto; font-size: 10px;">{{version}}</span>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\init\init.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__providers_connection_checker_connection_checker__["a" /* ConnectionCheckerProvider */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__providers_connection_checker_connection_checker__["a" /* ConnectionCheckerProvider */],
            __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_local_notifications__["a" /* LocalNotifications */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_browser_tab__["a" /* BrowserTab */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */]])
    ], InitPage);
    return InitPage;
}());

//# sourceMappingURL=init.js.map

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PesquisaModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_utils_utils__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_date_picker__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the PesquisaModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PesquisaModalPage = /** @class */ (function () {
    function PesquisaModalPage(platform, _platform, _datePicker, _alert, utils, viewCtrl, navCtrl, navParams, _http) {
        this.platform = platform;
        this._platform = _platform;
        this._datePicker = _datePicker;
        this._alert = _alert;
        this.utils = utils;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this.conjunto_datas = [];
        this.cartoes = [];
        this._platform.registerBackButtonAction(function () {
            //console.log('u cant return');
            return;
        }, 1);
    }
    PesquisaModalPage.prototype.ngOnInit = function () {
        var _this = this;
        document.addEventListener('backbutton', function () {
            //console.log('u cant return');
            return;
        }, false);
        this.pageParams = this.viewCtrl.getNavParams().data;
        this.pageParams.produtos.forEach(function (element) {
            element.rupturaTotal = false;
            element.prateleira = false;
            element.estoque = false;
            element.data_vencimento = [
                {
                    id: 1,
                    value: ''
                }
            ];
            element.quantidade = [
                {
                    id: 1,
                    value: ''
                }
            ];
            element.data_vencimento_estoque = '';
            element.preco = '0.00';
            element.n_frentes = '0';
            element.n_frentes_concorrente = '0';
            element.n_max_frentes = '0';
            _this.cartoes.push(element);
        });
    };
    PesquisaModalPage.prototype.addData = function (index) {
        this.cartoes[index].data_vencimento.push({
            id: this.cartoes.length + 1,
            value: ''
        });
    };
    PesquisaModalPage.prototype.removeData = function (index, indexx) {
        this.cartoes[index].data_vencimento.splice(indexx, 1);
    };
    PesquisaModalPage.prototype.addQuant = function (index) {
        this.cartoes[index].quantidade.push({
            id: this.cartoes.length + 1,
            value: ''
        });
    };
    PesquisaModalPage.prototype.removeQuant = function (index, indexx) {
        this.cartoes[index].quantidade.splice(indexx, 1);
    };
    PesquisaModalPage.prototype.applyDateMask = function (event, c, index, indexx) {
        if (index !== undefined && indexx !== undefined) {
            var value = c;
            if (value.length == 2) {
                if (event.keyCode != 8) {
                    value += '/';
                }
            }
            else if (value.length == 5) {
                if (event.keyCode != 8) {
                    value += '/';
                }
            }
            else if (value.length == 10) {
                if (event.keyCode != 8) {
                    event.preventDefault();
                }
            }
            this.cartoes[index].data_vencimento[indexx].value = value;
        }
        else if (index !== undefined && (indexx === undefined || indexx == '')) {
            var value = c;
            if (value.length == 2) {
                if (event.keyCode != 8) {
                    value += '/';
                }
            }
            else if (value.length == 5) {
                if (event.keyCode != 8) {
                    value += '/';
                }
            }
            else if (value.length == 10) {
                if (event.keyCode != 8) {
                    event.preventDefault();
                }
            }
            this.cartoes[index].data_vencimento_estoque = value;
        }
        //  value.substring(0,2) + '/' + value.substring(2,4) + '/' + value.substring(4,8)
    };
    PesquisaModalPage.prototype.applyMoneyMask = function (c) {
        c.preco = this.utils.detectAmount(c.preco);
    };
    ///// NAO APAGAR COMENTARIOS DO METODO, POSSIVEL IMPLEMENTAÇÃO FUTURA
    ///// PEDIDO QUE FOSSE REMOVIDO TAIS FUNCOES, MAS COM POSSIBILIDADE DE RETORNO DAS MESMAS   --- 13/10/2019
    PesquisaModalPage.prototype.donePesquisa = function () {
        var _this = this;
        var exp = {};
        try {
            this.cartoes.forEach(function (c) {
                var quantidade = '';
                var data = '';
                if (c.ruptura == false) {
                    var havCallExp_1 = false;
                    if (c.n_frentes.trim().length < 1 ||
                        c.n_max_frentes.trim().length < 1 ||
                        c.preco.trim().length < 1 ||
                        (c.estoque == false && c.prateleira == false && c.rupturaTotal == false)) {
                        havCallExp_1 = true;
                    }
                    else if ((c.n_frentes.trim().length < 1 ||
                        c.n_max_frentes.trim().length < 1 ||
                        c.preco.trim().length < 1)
                        && c.prateleira == true) {
                        havCallExp_1 = true;
                    }
                    c.data_vencimento.forEach(function (element) {
                        if (element.value.trim().length < 1) {
                            havCallExp_1 = true;
                        }
                    });
                    c.quantidade.forEach(function (element) {
                        if (element.value.trim().length < 1) {
                            havCallExp_1 = true;
                        }
                    });
                    if (havCallExp_1) {
                        throw exp;
                    }
                }
                c.data_vencimento.forEach(function (element) {
                    data += element.value + ';';
                });
                c.quantidade.forEach(function (element) {
                    quantidade += element.value + ';';
                });
                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertPesquisa.php', {
                    pro_id: c.pro_id,
                    data_vencimento: data,
                    data_vencimento_estoque: c.data_vencimento_estoque,
                    preco: c.preco,
                    nfrentes: c.n_frentes,
                    nfrentes_concorrente: c.n_frentes_concorrente,
                    nmaxfrentes: c.n_max_frentes,
                    quantidade: quantidade,
                    ruptura: c.rupturaTotal == true ? '1' : '0',
                    estoque: c.estoque == true ? '1' : '0',
                    prateleira: c.prateleira == true ? '1' : '0',
                    rede: _this.pageParams.rede,
                    cliente: _this.pageParams.cliente,
                    promotor: _this.pageParams.promotor,
                    filial: _this.pageParams.filial,
                    data: _this.pageParams.data,
                    hora: _this.pageParams.hora
                }, {}).then(function (response) {
                    console.log(response);
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        _this.viewCtrl.dismiss({ done: true });
                    }
                    else if (response.data.status == '0x101') {
                        _this.viewCtrl.dismiss({ done: false, error: 'pesquisaError' });
                    }
                });
            });
        }
        catch (exp) {
            var a = this._alert.create({
                title: 'Preencha os campos',
                subTitle: 'É necessário preencher toda a pesquisa!',
                buttons: ['Ok']
            });
            a.present();
        }
    };
    PesquisaModalPage.prototype.showDate = function (index, iindex) {
        var _this = this;
        this._datePicker.show({
            allowFutureDates: true,
            date: new Date(),
            mode: 'date',
            androidTheme: this._datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
        }).then(function (date) {
            if (_this.cartoes[index].data_vencimento.length == 1) {
                _this.cartoes[index].data_vencimento[iindex] =
                    {
                        id: _this.cartoes[index].data_vencimento.length + 1,
                        value: date.toISOString().substring(0, 10).split('-')[2] + '/'
                            + date.toISOString().substring(0, 10).split('-')[1] + '/' +
                            date.toISOString().substring(0, 10).split('-')[0]
                    };
            }
            else {
                if (_this.cartoes[index].data_vencimento.length <= 3) {
                    _this.cartoes[index].data_vencimento[iindex] =
                        {
                            id: _this.cartoes[index].data_vencimento.length + 1,
                            value: date.toISOString().substring(0, 10).split('-')[2] + '/'
                                + date.toISOString().substring(0, 10).split('-')[1] + '/' +
                                date.toISOString().substring(0, 10).split('-')[0]
                        };
                }
            }
        }, function (err) {
            var a = _this._alert.create({
                title: 'Erro!',
                subTitle: 'Selecione a data corretamente!',
                buttons: ['OK']
            });
            a.present();
        });
    };
    PesquisaModalPage.prototype.clearData = function (index, iindex) {
        this.cartoes[index].data_vencimento[iindex].value = '';
    };
    PesquisaModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pesquisa-modal',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\pesquisa-modal\pesquisa-modal.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content mdl-color--white" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <div class="mdl-card mdl-shadow--2dp" *ngFor="let c of cartoes; let i = index" style="margin: 5px auto; width: 90%; display: table;">\n\n        <div class="mdl-card__title">\n\n          <span>Cliente: {{c.Cli_Nome}} - Produto: {{c.pro_nome}}</span>\n\n        </div>\n\n        <div class="mdl-card__media mdl-color--white">\n\n          <ion-list>\n\n              <ion-item>\n\n                <ion-label>Produto em ruptura total</ion-label>\n\n                <ion-checkbox [(ngModel)]="c.rupturaTotal" (click)="c.prateleira = false; c.estoque = false;"></ion-checkbox>\n\n              </ion-item>\n\n            \n\n              <ion-item *ngIf="c.rupturaTotal == false">\n\n                <ion-label>Produto em área de venda</ion-label>\n\n                <ion-checkbox [(ngModel)]="c.prateleira" ></ion-checkbox>\n\n              </ion-item>\n\n\n\n              <ion-item *ngIf="c.rupturaTotal == false">\n\n                <ion-label>Produto em estoque</ion-label>\n\n                <ion-checkbox [(ngModel)]="c.estoque" ></ion-checkbox>\n\n              </ion-item>\n\n\n\n              <div style="position: relative;" *ngIf="c.rupturaTotal == false && c.prateleira == true">\n\n                <button *ngIf="c.data_vencimento.length <= 2" class="mdl-button mdl-js-button mdl-button--icon mdl-color--red-500 mdl-color-text--white" (click)="addData(i)" style="position: absolute; top: 2px; left: 2px; z-index: 10">\n\n                  <i class="material-icons">add</i>\n\n                </button>\n\n                <div style="width: 100%; display: table; margin: auto; position: relative"  *ngFor="let d of c.data_vencimento; let ii = index">\n\n                  <button class="mdl-button mdl-js-button mdl-button--icon mdl-color--red-500 mdl-color-text--white" (click)="clearData(i, ii)" style="position: absolute; right: 20px; top: 25px; z-index: 10">\n\n                    <i class="material-icons">delete_outline</i>\n\n                  </button>\n\n                  <button *ngIf="ii > 0" class="mdl-button mdl-js-button mdl-button--icon mdl-color--red-500 mdl-color-text--white" (click)="removeData(i, ii)" style="position: absolute; right: 60px; top: 25px; z-index: 10">\n\n                    <i class="material-icons">remove</i>\n\n                  </button>\n\n                  <ion-item style="width: 90%; display: table; margin: auto">\n\n                    <ion-label color="primary" floating>Data de vencimento</ion-label>\n\n                    <ion-input [(ngModel)]="d.value" (click)="showDate(i, ii)" ></ion-input>\n\n                  </ion-item>\n\n                </div>\n\n              </div>\n\n\n\n              <ion-item style="width: 90%; display: table; margin: auto">\n\n                <ion-label color="primary" floating>Data de vencimento (Estoque)</ion-label>\n\n                <ion-input [(ngModel)]="c.data_vencimento_estoque" (keydown)="applyDateMask($event, c.data_vencimento_estoque, i)" (keyup)="applyDateMask($event, c.data_vencimento_estoque, i)"></ion-input>\n\n              </ion-item>\n\n\n\n              <!-- <div style="position: relative;" *ngIf="c.rupturaTotal == false &&  (c.prateleira == true || c.estoque == true) ">\n\n                <button *ngIf="c.quantidade.length <= 2" class="mdl-button mdl-js-button mdl-button--icon mdl-color--red-500 mdl-color-text--white" (click)="addQuant(i)" style="position: absolute; top: 2px; left: 2px; z-index: 10">\n\n                  <i class="material-icons">add</i>\n\n                </button>\n\n                <div style="width: 100%; display: table; margin: auto; position: relative"  *ngFor="let d of c.quantidade; let ii = index">\n\n                  <button *ngIf="ii > 0" class="mdl-button mdl-js-button mdl-button--icon mdl-color--red-500 mdl-color-text--white" (click)="removeQuant(i, ii)" style="position: absolute; right: 20px; top: 25px; z-index: 10">\n\n                    <i class="material-icons">remove</i>\n\n                  </button>\n\n                  <ion-item style="width: 90%; display: table; margin: auto">\n\n                    <ion-label color="primary" floating>Quantidade</ion-label>\n\n                    <ion-input [(ngModel)]="d.value"></ion-input>\n\n                  </ion-item>\n\n                </div>\n\n              </div> -->\n\n\n\n              <ion-item style="width: 90%; display: table; margin: auto" *ngIf="c.rupturaTotal == false && c.prateleira == true">\n\n                <ion-label color="primary" floating>Preço</ion-label>\n\n                <ion-input [(ngModel)]="c.preco" (keydown)="applyMoneyMask(c)" (keyup)="applyMoneyMask(c)"></ion-input>\n\n              </ion-item>\n\n\n\n              \n\n              <ion-item style="width: 90%; display: table; margin: auto" *ngIf="c.rupturaTotal == false && c.prateleira == true">\n\n                <ion-label color="primary" floating>Numero de Frentes</ion-label>\n\n                <ion-input [(ngModel)]="c.n_frentes"></ion-input>\n\n              </ion-item>\n\n              \n\n              <ion-item style="width: 90%; display: table; margin: auto" *ngIf="c.rupturaTotal == false && c.prateleira == true">\n\n                <ion-label color="primary" floating>Total de Frentes da Categoria</ion-label>\n\n                <ion-input [(ngModel)]="c.n_max_frentes"></ion-input>\n\n              </ion-item>\n\n          </ion-list>\n\n          <ion-list>\n\n            <ion-list-header>Concorrente: {{c.pro_concorrente_nome}}</ion-list-header>\n\n\n\n            <ion-item style="width: 90%; display: table; margin: auto">\n\n              <ion-label color="primary" floating>Numero de Frentes</ion-label>\n\n              <ion-input [(ngModel)]="c.n_frentes_concorrente"></ion-input>\n\n            </ion-item>\n\n          </ion-list>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </main>\n\n  <div style="z-index: 5; position: relative; background: #8c1515; width: 100vw;">\n\n    <div style="display: table; margin: auto;">\n\n      <!-- <button (click)="dismiss()" style="float: left; margin-top: 10px;" class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-color--white">\n\n        <i class="material-icons">arrow_back</i>\n\n      </button> -->\n\n      <button (click)="donePesquisa()" style="float: right; margin-top: 10px; margin-bottom: 5px;" class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-color--white">\n\n        <i class="material-icons">done</i>\n\n      </button>\n\n    </div>\n\n  </div>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\pesquisa-modal\pesquisa-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_date_picker__["a" /* DatePicker */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__["a" /* HTTP */]])
    ], PesquisaModalPage);
    return PesquisaModalPage;
}());

//# sourceMappingURL=pesquisa-modal.js.map

/***/ }),

/***/ 144:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuizModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the QuizModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var QuizModalPage = /** @class */ (function () {
    function QuizModalPage(platform, _platform, _alert, _view, navCtrl, navParams, _http) {
        this.platform = platform;
        this._platform = _platform;
        this._alert = _alert;
        this._view = _view;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this.questionTime = '';
        this.questionTitle = '';
        this.options = [];
        this.alreadyAnsw = false;
        this._platform.registerBackButtonAction(function () {
            //console.log('u cant return');
            return;
        }, 1);
    }
    QuizModalPage.prototype.ngOnInit = function () {
        var _this = this;
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php', {
            unique: true,
            id: this.navParams.get('per_id')
        }, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            if (response.data.status == '0x104') {
                _this.questionTime = response.data.result[0].PER_Tempo;
                _this.questionTitle = response.data.result[0].PER_Title;
                response.data.result[0].PER_Text.split(';').forEach(function (element) {
                    _this.options.push(element);
                });
                if (_this._view.readReady) {
                    _this.tick(parseInt(_this.questionTime));
                }
            }
        });
    };
    QuizModalPage.prototype.tick = function (time) {
        var _this = this;
        if (time > 0) {
            setTimeout(function () {
                _this.questionTime = '' + (time - 1);
                _this.tick(time - 1);
            }, 1000);
        }
        else if ((time == 0 || time < 0) && this.alreadyAnsw == false) {
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/insertQuizResposta.php', {
                timed: true,
                per_id: this.navParams.get('per_id'),
                usr_id: JSON.parse(localStorage.getItem('session'))[0].PRO_ID
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    var a = _this._alert.create({
                        title: 'KSTrade Merchandising',
                        subTitle: 'Você não respondeu a questão a tempo!'
                    });
                    a.present();
                    a.onDidDismiss(function () {
                        _this._view.dismiss();
                    });
                }
            });
        }
    };
    QuizModalPage.prototype.setOption = function (op) {
        var _this = this;
        var c = this._alert.create({
            title: 'KSTrade Merchandising',
            subTitle: 'Confirmar?',
            buttons: [{
                    text: 'OK',
                    handler: function () {
                        var id = JSON.parse(localStorage.getItem('session'))[0] !== undefined && JSON.parse(localStorage.getItem('session'))[0].PRO_ID != undefined ? JSON.parse(localStorage.getItem('session'))[0].PRO_ID : JSON.parse(localStorage.getItem('session')).user.Usr_ID;
                        _this.alreadyAnsw = true;
                        _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/insertQuizResposta.php', {
                            per_id: _this.navParams.get('per_id'),
                            usr_id: id,
                            option: op,
                            time: _this.questionTime
                        }, {}).then(function (response) {
                            response.data = JSON.parse(response.data);
                            if (response.data.status == '0x104') {
                                var a = _this._alert.create({
                                    title: 'KSTrade Merchandising',
                                    subTitle: 'Enviado!'
                                });
                                a.present();
                                a.onDidDismiss(function () {
                                    _this._view.dismiss();
                                });
                            }
                            else {
                                var a = _this._alert.create({
                                    title: 'KSTrade Merchandising',
                                    subTitle: 'Problemas ao enviar sua resposta, por favor contate-nos'
                                });
                                a.present();
                                a.onDidDismiss(function () {
                                    _this._view.dismiss();
                                });
                            }
                        });
                    }
                }, {
                    text: 'Cancelar',
                    handler: function () {
                        _this.alreadyAnsw = false;
                    }
                }]
        });
        c.present();
    };
    QuizModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-quiz-modal',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\quiz-modal\quiz-modal.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content mdl-color--white" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" style="background: #f5f7fa; height: 100%">\n\n      <span style="display: table; margin: auto; font-weight: bold; font-size: 22px; position: relative; top: 5px">Quiz Eccotrade</span>      \n\n      <div style="width: 100%; display: table; margin: 15px auto;">\n\n        <span style="display: table; margin: 10px auto">{{questionTime}}</span>\n\n        <span style="display: table; margin: auto">{{questionTitle}}</span>\n\n        <div style="display: table; margin: 10px auto; width: 100%; border-top: 1px solid #8c1515;">\n\n          <button class="mdl-button mdl-js-button mdl-js-ripple-effect" *ngFor="let o of options;" style="width: 100%; margin: auto; display: table; height: 100%; border-bottom: 1px solid #dcdcdc;" (click)="setOption(o)">\n\n            {{o}}\n\n          </button>\n\n        </div>\n\n      </div>\n\n    </div>\n\n    </main>\n\n</div>\n\n'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\quiz-modal\quiz-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_http__["a" /* HTTP */]])
    ], QuizModalPage);
    return QuizModalPage;
}());

//# sourceMappingURL=quiz-modal.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewNewCompletePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__view_photo_view_photo__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ViewNewCompletePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewNewCompletePage = /** @class */ (function () {
    function ViewNewCompletePage(platform, _sanitization, _http, viewCtrl, _modal, navCtrl, navParams) {
        this.platform = platform;
        this._sanitization = _sanitization;
        this._http = _http;
        this.viewCtrl = viewCtrl;
        this._modal = _modal;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.noticia = { grupos: [] };
        this.newGroupName = '';
    }
    ViewNewCompletePage.prototype.decode_utf8 = function (s) {
        return decodeURIComponent(escape(s));
    };
    ViewNewCompletePage.prototype.ngOnInit = function () {
        var _this = this;
        this.newGroupName = this.navParams.get('group_name');
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php', {
            forCliente: true,
            group_id: this.navParams.get('group_id')
        }, {}).then(function (response3) {
            response3.data = JSON.parse(response3.data);
            if (response3.data.status == '0x104') {
                response3.data.result.forEach(function (element, index) {
                    element.app_title = _this.decode_utf8(element.app_title);
                    element.app_text = _this.decode_utf8(element.app_text);
                    element.activated = false;
                    element.image = element.app_image;
                    element.app_image = _this._sanitization.bypassSecurityTrustStyle("url(https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/" + element.app_image + ") center / cover");
                    console.log(element);
                    _this.noticia.grupos.push(element);
                });
            }
        });
    };
    ViewNewCompletePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ViewNewCompletePage.prototype.openNewImage = function (title, text, cardImage, id, obj) {
        console.log(obj);
        var o = {
            type: 'photo',
            JOB_Title: title,
            JOB_Obs: text,
            fotos: [],
            id: id
        };
        o.fotos.push('news/' + cardImage);
        obj.app_fotos_extras.forEach(function (el) {
            o.fotos.push('news/' + el.new_foto);
        });
        var modal = this._modal.create(__WEBPACK_IMPORTED_MODULE_2__view_photo_view_photo__["a" /* ViewPhotoPage */], o);
        modal.present();
    };
    ViewNewCompletePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-view-new-complete',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\view-new-complete\view-new-complete.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #f5f7fa; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">\n\n  <main class="mdl-layout__content mdl-color--white"\n\n    style="height: calc(100vh - 56px); background-color: #f5f7fa !important">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <div class="mdl-card" style="width: 100%; background-color: transparent">\n\n        <button (click)="dismiss()" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n          <i class="material-icons" style="color: red;">arrow_back</i>\n\n        </button>\n\n        <div class="mdl-card__media" style="margin-top: 55px; background-color: transparent">\n\n          <span style="display: table; margin: auto; font-weight: bold">{{newGroupName}}</span>\n\n          <div *ngFor="let c of noticia.grupos; let i = index">\n\n            <div *ngIf="c.app_type == \'image\'" class="mdl-card mdl-shadow--2dp"\n\n              style="display: table; margin: 10px auto;">\n\n              <div (click)="openNewImage(c.app_title, c.app_text, c.image, c.app_id, c)" [style.background]="c.app_image" class="mdl-card__title"\n\n                style="color: #f5f7fa; height: 176px;">\n\n                <span *ngIf="c.app_fotos_extras.length > 0" style="position: absolute; top: 5px; right: 5px;">\n\n                  <i class="material-icons" style="color: red">style</i>\n\n                </span>\n\n                <h2 class="mdl-card__title-text descChar"\n\n                  style="padding: 10px; position: relative; right: 15px; background: rgba(0,0,0,0.4); color: #f5f7fa; position: relative; top: 15px">\n\n                  {{c.app_title}}</h2>\n\n              </div>\n\n              <div class="mdl-card__supporting-text">\n\n                {{c.app_text}}\n\n              </div>\n\n              <div class="mdl-card__actions mdl-card--border">\n\n                <a (click)="openNewImage(c.app_title, c.app_text, c.image, c.app_id)" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect mdl-color-text--red-700" style="width: 100%; display: table; margin: auto">\n\n                  Comentar\n\n                </a>\n\n              </div>\n\n            </div>\n\n\n\n            <div *ngIf="c.app_type == \'simple\'" class="mdl-card mdl-shadow--2dp"\n\n              style="display: table; margin: 10px auto;">\n\n              <div class="mdl-card__title mdl-color--grey-200" style="color: #000;">\n\n                <h2 class="mdl-card__title-text descChar" style="position: relative; top: 5px; font-size: 18px">\n\n                  {{c.app_title}}</h2>\n\n              </div>\n\n              <div class="mdl-card__supporting-text">\n\n                <p>\n\n                  {{c.app_text}}\n\n                </p>\n\n\n\n              </div>\n\n            </div>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\view-new-complete\view-new-complete.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], ViewNewCompletePage);
    return ViewNewCompletePage;
}());

//# sourceMappingURL=view-new-complete.js.map

/***/ }),

/***/ 156:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 156;

/***/ }),

/***/ 199:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/area-cliente/cliente-filiais/cliente-filiais.module": [
		577,
		1
	],
	"../pages/area-cliente/cliente-lojas/cliente-lojas.module": [
		578,
		0
	],
	"../pages/area-cliente/cliente-promotores/cliente-promotores.module": [
		579,
		3
	],
	"../pages/area-cliente/job-infos/job-infos.module": [
		580,
		16
	],
	"../pages/area-cliente/jobs-cliente-promotor/jobs-cliente-promotor.module": [
		581,
		7
	],
	"../pages/area-cliente/jobs/jobs.module": [
		582,
		2
	],
	"../pages/area-representante/metas/metas.module": [
		583,
		15
	],
	"../pages/area-representante/pedidos/pedidos.module": [
		584,
		14
	],
	"../pages/area-visitante/lojas-visitante/lojas-visitante.module": [
		585,
		13
	],
	"../pages/confirm/confirm.module": [
		586,
		12
	],
	"../pages/coord-job/coord-job.module": [
		587,
		11
	],
	"../pages/datas-criticas-modal/critic-modal.module": [
		588,
		10
	],
	"../pages/datas-criticas/datas-criticas.module": [
		589,
		5
	],
	"../pages/entrada-saida-job/entrada-saida-job.module": [
		590,
		9
	],
	"../pages/esqueceu-senha/esqueceu-senha.module": [
		591,
		29
	],
	"../pages/greetings/greetings.module": [
		592,
		28
	],
	"../pages/init/init.module": [
		593,
		27
	],
	"../pages/jobs-do-dia/jobs-do-dia.module": [
		594,
		6
	],
	"../pages/loading/loading.module": [
		595,
		26
	],
	"../pages/login/login.module": [
		597,
		25
	],
	"../pages/login/opts/login-opts/login-opts.module": [
		596,
		24
	],
	"../pages/lojas/lojas.module": [
		598,
		4
	],
	"../pages/main/main.module": [
		334
	],
	"../pages/market-share-chart/market-share-chart.module": [
		599,
		23
	],
	"../pages/market-share/market-share.module": [
		600,
		8
	],
	"../pages/new-job-modal/new-job-modal.module": [
		601,
		22
	],
	"../pages/pesquisa-modal/pesquisa-modal.module": [
		602,
		21
	],
	"../pages/quiz-modal/quiz-modal.module": [
		603,
		20
	],
	"../pages/update/update.module": [
		604,
		19
	],
	"../pages/view-new-complete/view-new-complete.module": [
		605,
		18
	],
	"../pages/view-photo/view-photo.module": [
		606,
		17
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 199;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainPageModule", function() { return MainPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__main__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__greetings_greetings__ = __webpack_require__(141);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MainPageModule = /** @class */ (function () {
    function MainPageModule() {
    }
    MainPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__main__["a" /* MainPage */],
                __WEBPACK_IMPORTED_MODULE_3__greetings_greetings__["a" /* GreetingsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__main__["a" /* MainPage */]),
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__greetings_greetings__["a" /* GreetingsPage */]
            ]
        })
    ], MainPageModule);
    return MainPageModule;
}());

//# sourceMappingURL=main.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__quiz_modal_quiz_modal__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__view_photo_view_photo__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_blueimp_md5__ = __webpack_require__(553);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_blueimp_md5___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_blueimp_md5__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_admob_free__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ts_md5_dist_md5__ = __webpack_require__(554);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ts_md5_dist_md5___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ts_md5_dist_md5__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__view_new_complete_view_new_complete__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_keyboard__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_connection_checker_connection_checker__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__greetings_greetings__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var bannerConfig = {
    id: 'ca-app-pub-6624972784895874/3496159241',
    isTesting: false,
    autoShow: true
};
var MainPage = /** @class */ (function () {
    function MainPage(platform, _platform, _alert, _sanitization, _http, navCtrl, navParams, app, viewCtrl, _modal, loadingCtrl, _checker, _keyboard, _ad) {
        this.platform = platform;
        this._platform = _platform;
        this._alert = _alert;
        this._sanitization = _sanitization;
        this._http = _http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
        this.viewCtrl = viewCtrl;
        this._modal = _modal;
        this.loadingCtrl = loadingCtrl;
        this._checker = _checker;
        this._keyboard = _keyboard;
        this._ad = _ad;
        this.md5 = new __WEBPACK_IMPORTED_MODULE_8_ts_md5_dist_md5__["Md5"]();
        this.myIds = [];
        this.myLastTime = 0;
        this.cards = [];
        this.firstRun = true;
        this.status = null;
        this.noticia = {
            grupos: []
        };
        this.menu = [
            { title: 'Inicio', icon: 'home', media: 'main' },
            { title: 'Lojas', icon: 'meeting_room', media: 'lojas' },
            { title: 'Jobs do dia', icon: 'playlist_add_check', media: 'jobsDoDia' },
            { title: 'Datas críticas', icon: 'warning', media: 'criticDate' },
            // {title: 'Jobs realizados', icon: 'group_work', media: 'home'},
            { title: 'Market Share', icon: 'add_shopping_cart', media: 'marketShare', param: { forPromotor: true } },
            { title: 'Sair', icon: 'exit_to_app', action: 'logout' }
        ];
        this._platform.registerBackButtonAction(function () {
            //console.log('u cant return');
            return;
        }, 1);
    }
    MainPage_1 = MainPage;
    MainPage.prototype.ngOnInit = function () {
        var _this = this;
        //console.log(this._checker.getMStatus());
        if (!localStorage.getItem('session')) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
        }
        else {
            this._platform.ready().then(function () {
                _this._ad.banner.config(bannerConfig);
                _this._ad.banner.prepare()
                    .then(function () {
                    _this._ad.banner.show();
                })
                    .catch(function (e) {
                    return console.log(e);
                });
                _this._keyboard.onKeyboardShow().subscribe(function (data) {
                    _this._ad.banner.hide();
                });
                _this._keyboard.onKeyboardHide().subscribe(function (data) {
                    _this._ad.banner.show();
                });
                _this._keyboard.onKeyboardWillShow().subscribe(function (data) {
                    _this._ad.banner.hide();
                });
                _this._keyboard.onKeyboardWillHide().subscribe(function (data) {
                    _this._ad.banner.show();
                });
            });
            if (JSON.parse(localStorage.getItem('session')).visitante !== undefined) {
                this._modal.create(__WEBPACK_IMPORTED_MODULE_12__greetings_greetings__["a" /* GreetingsPage */], {}).present();
                this.menu = [
                    { title: 'Inicio', icon: 'home', media: 'main' },
                    { title: 'Lojas', icon: 'group', media: 'LojasVisitante' },
                    { title: 'Entrar', icon: 'work', media: 'login' }
                ];
                var r_1 = [];
                var final_hash_1 = '';
                var temp_pattern_1 = '';
                this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php', {
                    is_guest: true
                }, {}).then(function (response3) {
                    response3.data = JSON.parse(response3.data);
                    _this._checker.setMStatus('online');
                    if (response3.data.status == '0x104') {
                        _this.firstRun = false;
                        if (_this.noticia.grupos.length < response3.data.result.length) {
                            response3.data.result.forEach(function (element, index) {
                                temp_pattern_1 += element.content.length + '?' + response3.data.result.length;
                                element.activated = false;
                                element.content.forEach(function (ele) {
                                    ele.app_title = _this.decode_utf8(ele.app_title);
                                    ele.app_text = _this.decode_utf8(ele.app_text);
                                    ele.image = ele.app_image;
                                    ele.app_image = _this._sanitization.bypassSecurityTrustStyle("url(https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/" + ele.app_image + ") center / cover");
                                });
                                if (index == (response3.data.result.length - 1)) {
                                    element.activated = true;
                                }
                                r_1.push(element);
                                _this.menu.push({ customized: true, title: element.group_nome, icon: '', action: '', news: true, group_id: element.group_id });
                                if (index == (response3.data.result.length - 1)) {
                                    if (JSON.parse(localStorage.getItem('session')).visitante.Usr_Nome != 'Temporario') {
                                        _this.menu.push({ title: 'Sair', icon: 'exit_to_app', action: 'logout' });
                                    }
                                }
                            });
                            final_hash_1 = __WEBPACK_IMPORTED_MODULE_6_blueimp_md5___default()(temp_pattern_1);
                            if (!localStorage.getItem('visit_new_hash')) {
                                localStorage.setItem('visit_new_hash', final_hash_1);
                            }
                            else if (localStorage.getItem('visit_new_hash') != final_hash_1) {
                                localStorage.setItem('visit_new_hash', final_hash_1);
                            }
                        }
                    }
                }, function () {
                    _this._checker.setMStatus('offline');
                });
            }
            else if (JSON.parse(localStorage.getItem('session')).user !== undefined) {
                if (JSON.parse(localStorage.getItem('session')).user.Usr_Nivel == 'Vendedor') {
                    // this._ad.banner.hide();
                    this.menu = [
                        { title: 'Inicio', icon: 'home', media: 'main' },
                        { title: 'Lojas', icon: 'meeting_room', media: 'clienteLojas' },
                        { title: 'Pesquisas', icon: 'bookmark', media: 'clientePesquisa', param: { forVendedor: true } },
                        { title: 'Market Share', icon: 'add_shopping_cart', media: 'marketShare', param: { forVendedor: true } },
                        { title: 'Sair', icon: 'exit_to_app', action: 'logout' }
                    ];
                    this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php', {
                        tipo: 2
                    }, {}).then(function (response) {
                        response.data = JSON.parse(response.data);
                        if (response.data.status == '0x104') {
                            _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php', {
                                already: true,
                                per_id: response.data.result[0].PER_ID,
                                usr_id: JSON.parse(localStorage.getItem('session')).user.Usr_ID
                            }, {}).then(function (response2) {
                                if (response2.data.status != '0x102') {
                                    var days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                                    var dayName_1 = days[new Date().getDay()];
                                    response.data.result.forEach(function (element) {
                                        var elem = element.PER_Dias_Semana.split(';');
                                        elem.forEach(function (element2) {
                                            if (element2 == dayName_1) {
                                                var arr = [];
                                                arr.push(element.PER_ID);
                                                var modal = _this._modal.create(__WEBPACK_IMPORTED_MODULE_4__quiz_modal_quiz_modal__["a" /* QuizModalPage */], {
                                                    per_id: element.PER_ID,
                                                    pergunta: element.PER_Title,
                                                    opcoes: element.PER_Text
                                                });
                                                modal.present();
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    }, function () {
                        _this._checker.setMStatus('offline');
                    });
                }
                else if (JSON.parse(localStorage.getItem('session')).user.Usr_Nivel == 'Externo') {
                    // this._ad.banner.hide();
                    this.menu = [
                        { title: 'Inicio', icon: 'home', media: 'main' },
                        { title: 'Lojas', icon: 'meeting_room', media: 'clienteLojas' },
                        { title: 'Promotores', icon: 'group', media: 'clientePromotores' },
                        { title: 'Pesquisas', icon: 'bookmark', media: 'clientePesquisa', param: { forExterno: true } },
                        { title: 'Market Share', icon: 'add_shopping_cart', media: 'marketShare' },
                        { title: 'Sair', icon: 'exit_to_app', action: 'logout' }
                    ];
                    this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php', {
                        tipo: 1
                    }, {}).then(function (response) {
                        response.data = JSON.parse(response.data);
                        if (response.data.status == '0x104') {
                            _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php', {
                                already: true,
                                per_id: response.data.result[0].PER_ID,
                                usr_id: JSON.parse(localStorage.getItem('session')).user.Usr_ID
                            }, {}).then(function (response2) {
                                if (response2.data.status != '0x102') {
                                    var days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                                    var dayName_2 = days[new Date().getDay()];
                                    response.data.result.forEach(function (element) {
                                        var elem = element.PER_Dias_Semana.split(';');
                                        elem.forEach(function (element2) {
                                            if (element2 == dayName_2) {
                                                var arr = [];
                                                arr.push(element.PER_ID);
                                                var modal = _this._modal.create(__WEBPACK_IMPORTED_MODULE_4__quiz_modal_quiz_modal__["a" /* QuizModalPage */], {
                                                    per_id: element.PER_ID,
                                                    pergunta: element.PER_Title,
                                                    opcoes: element.PER_Text
                                                });
                                                modal.present();
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    }, function () {
                        _this._checker.setMStatus('offline');
                    });
                }
                else if (JSON.parse(localStorage.getItem('session')).user.Usr_Nivel == 'Representante') {
                    this.menu = [
                        { title: 'Inicio', icon: 'home', media: 'main' },
                        { title: 'Lojas', icon: 'meeting_room', media: 'clienteLojas', param: { redes: JSON.parse(localStorage.getItem('session')).metas } },
                        { title: 'Pedidos', icon: 'check_box', media: 'pedidos' },
                        { title: 'Metas', icon: 'insert_chart_outlined', media: 'metas' },
                        { title: 'Sair', icon: 'exit_to_app', action: 'logout' }
                    ];
                    this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php', {
                        tipo: 1
                    }, {}).then(function (response) {
                        response.data = JSON.parse(response.data);
                        if (response.data.status == '0x104') {
                            _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php', {
                                already: true,
                                per_id: response.data.result[0].PER_ID,
                                usr_id: JSON.parse(localStorage.getItem('session')).user.Usr_ID
                            }, {}).then(function (response2) {
                                if (response2.data.status != '0x102') {
                                    var days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                                    var dayName_3 = days[new Date().getDay()];
                                    response.data.result.forEach(function (element) {
                                        var elem = element.PER_Dias_Semana.split(';');
                                        elem.forEach(function (element2) {
                                            if (element2 == dayName_3) {
                                                var arr = [];
                                                arr.push(element.PER_ID);
                                                var modal = _this._modal.create(__WEBPACK_IMPORTED_MODULE_4__quiz_modal_quiz_modal__["a" /* QuizModalPage */], {
                                                    per_id: element.PER_ID,
                                                    pergunta: element.PER_Title,
                                                    opcoes: element.PER_Text
                                                });
                                                modal.present();
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    }, function () {
                        _this._checker.setMStatus('offline');
                    });
                }
            }
            else {
                // this.loginCheck()
                var lasId_1 = 0;
                JSON.parse(localStorage.getItem('session')).forEach(function (element) {
                    element.content.forEach(function (element) {
                        if (element.PRO_ID && lasId_1 != element.PRO_ID) {
                            _this.myIds.push(element.PRO_ID);
                            lasId_1 = element.PRO_ID;
                        }
                    });
                });
                var myIds_1 = [];
                JSON.parse(localStorage.getItem('session')).forEach(function (element) {
                    myIds_1.push(element.PRO_ID);
                });
                this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php', {
                    tipo: 0
                }, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectQuiz.php', {
                            already: true,
                            per_id: response.data.result[0].PER_ID,
                            pro_id: myIds_1
                        }, {}).then(function (response2) {
                            if (response2.data.status != '0x102') {
                                var days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                                var dayName_4 = days[new Date().getDay()];
                                response.data.result.forEach(function (element) {
                                    var elem = element.PER_Dias_Semana.split(';');
                                    elem.forEach(function (element2) {
                                        if (element2 == dayName_4) {
                                            var arr = [];
                                            arr.push(element.PER_ID);
                                            var modal = _this._modal.create(__WEBPACK_IMPORTED_MODULE_4__quiz_modal_quiz_modal__["a" /* QuizModalPage */], {
                                                per_id: element.PER_ID,
                                                pergunta: element.PER_Title,
                                                opcoes: element.PER_Text
                                            });
                                            modal.present();
                                        }
                                    });
                                });
                            }
                        });
                    }
                }, function () {
                    _this._checker.setMStatus('offline');
                });
            }
            this.noticia.grupos = [];
            // Noticias
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php', {}, {}).then(function (response) {
                console.log('ok resp');
                response.data = JSON.parse(response.data);
                console.log('response news', response);
                if (response.data.status == '0x104') {
                    response.data.result.forEach(function (element) {
                        element.image = element.app_image;
                        element.app_title = _this.decode_utf8(element.app_title);
                        element.app_text = _this.decode_utf8(element.app_text);
                        element.app_image = _this._sanitization.bypassSecurityTrustStyle("url('https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/" + element.app_image + "') center / cover");
                        _this.cards.push(element);
                    });
                }
                if (JSON.parse(localStorage.getItem('session'))[0] !== undefined &&
                    JSON.parse(localStorage.getItem('session'))[0].PRO_ID !== undefined) {
                    _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectClientePromotor.php', {
                        pro_id: JSON.parse(localStorage.getItem('session'))[0].PRO_ID
                    }, {}).then(function (response2) {
                        response2.data = JSON.parse(response2.data);
                        if (response2.data.status == '0x104') {
                            //console.log(response2.data);
                            response2.data.result.forEach(function (element) {
                                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php', {
                                    cliente: element.aux_cliente_id
                                }, {}).then(function (response3) {
                                    response3.data = JSON.parse(response3.data);
                                    if (response3.data.status == '0x104') {
                                        response3.data.result.forEach(function (element, index) {
                                            element.activated = false;
                                            element.content.forEach(function (ele) {
                                                ele.app_title = _this.decode_utf8(ele.app_title);
                                                ele.app_text = _this.decode_utf8(ele.app_text);
                                                ele.image = ele.app_image;
                                                ele.app_image = _this._sanitization.bypassSecurityTrustStyle("url(https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/" + ele.app_image + ") center / cover");
                                            });
                                            if (index == (response3.data.result.length - 1)) {
                                                element.activated = true;
                                            }
                                            _this.noticia.grupos.push(element);
                                        });
                                        //console.log('1', this.noticia.grupos);
                                    }
                                });
                            });
                        }
                    }, function () {
                        _this._checker.setMStatus('offline');
                    });
                }
                else if (JSON.parse(localStorage.getItem('session')).user !== undefined) {
                    if (JSON.parse(localStorage.getItem('session')).user.Usr_Nivel == 'Externo' || JSON.parse(localStorage.getItem('session')).user.Usr_Nivel == 'Representante') {
                        JSON.parse(localStorage.getItem('session')).clientes.forEach(function (element) {
                            _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php', {
                                cliente: element.Cli_ID
                            }, {}).then(function (response2) {
                                response2.data = JSON.parse(response2.data);
                                if (response2.data.status == '0x104') {
                                    response2.data.result.forEach(function (element, index) {
                                        element.activated = false;
                                        element.content.forEach(function (ele) {
                                            ele.app_title = _this.decode_utf8(ele.app_title);
                                            ele.app_text = _this.decode_utf8(ele.app_text);
                                            ele.image = ele.app_image;
                                            ele.app_image = _this._sanitization.bypassSecurityTrustStyle("url(https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/" + ele.app_image + ") center / cover");
                                        });
                                        if (index == (response2.data.result.length - 1)) {
                                            element.activated = true;
                                        }
                                        _this.noticia.grupos.push(element);
                                    });
                                    //console.log('2', this.noticia.grupos);
                                }
                            }, function () {
                                _this._checker.setMStatus('offline');
                            });
                        });
                    }
                    else {
                        _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php', {
                            cliente: JSON.parse(localStorage.getItem('session')).redes[0].aux_cliente_id
                        }, {}).then(function (response2) {
                            response2.data = JSON.parse(response2.data);
                            if (response2.data.status == '0x104') {
                                response2.data.result.forEach(function (element, index) {
                                    element.activated = false;
                                    element.content.forEach(function (ele) {
                                        ele.app_title = _this.decode_utf8(ele.app_title);
                                        ele.app_text = _this.decode_utf8(ele.app_text);
                                        ele.image = ele.app_image;
                                        ele.app_image = _this._sanitization.bypassSecurityTrustStyle("url(https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/" + ele.app_image + ") center / cover");
                                    });
                                    if (index == (response2.data.result.length - 1)) {
                                        element.activated = true;
                                    }
                                    _this.noticia.grupos.push(element);
                                });
                                //console.log('2', this.noticia.grupos);
                            }
                        }, function () {
                            _this._checker.setMStatus('offline');
                        });
                    }
                }
            }, function () {
                _this._checker.setMStatus('offline');
            });
        }
        var temp_jobs = JSON.parse(localStorage.getItem('temp_jobs')) || [];
        var temp_entradas = JSON.parse(localStorage.getItem('temp_entradas')) || [];
        var temp_saidas = JSON.parse(localStorage.getItem('temp_saidas')) || [];
        if ((temp_jobs.length > 0 || temp_entradas.length > 0 || temp_saidas.length > 0) && localStorage.getItem('session')) {
            var a_1 = this._alert.create({
                title: 'Sincronizar informaçãoes?',
                message: 'Você possui ' + (temp_entradas.length + temp_saidas.length + temp_jobs.length) + ' itens para sincronizar, deseja iniciar?',
                buttons: [
                    {
                        text: 'Não',
                        handler: function (data) {
                            a_1.dismiss();
                        }
                    },
                    {
                        text: 'Sim',
                        handler: function (data) {
                            var max = (temp_entradas.length + temp_saidas.length + temp_jobs.length);
                            var mine = 0;
                            var load = _this.loadingCtrl.create({
                                content: 'Sincronizando... ' + (temp_entradas.length + temp_saidas.length + temp_jobs.length) + ' itens restantes'
                            });
                            load.present();
                            if (temp_jobs instanceof Array) {
                                temp_jobs.forEach(function (jobs_element, jobs_index) {
                                    _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertJob.php', jobs_element, {}).then(function (response) {
                                        response.data = JSON.parse(response.data);
                                        if (response.data.status == '0x104') {
                                            mine++;
                                            temp_jobs.splice(jobs_index, 1);
                                            if (mine == max) {
                                                load.dismiss();
                                                _this._alert.create({
                                                    title: 'Sincronizado com sucesso!',
                                                    message: 'Todos o itens foram sincronizados com sucesso!'
                                                }).present();
                                                localStorage.setItem('temp_jobs', JSON.stringify(temp_jobs));
                                            }
                                        }
                                        else if (response.data.status == '0x105') {
                                            var a_2 = _this._alert.create({
                                                title: 'Job duplicado!',
                                                subTitle: 'Você já inseriu esse job hoje!',
                                                buttons: ['Ok']
                                            });
                                            a_2.present();
                                        }
                                        else if (response.data.status == '0x101') {
                                            var a_3 = _this._alert.create({
                                                title: 'Erro',
                                                subTitle: 'Erro de inserção no banco de dados',
                                                buttons: ['Ok']
                                            });
                                            a_3.present();
                                        }
                                        else if (response.data.status == '0x102') {
                                            var a_4 = _this._alert.create({
                                                title: 'Já existe!',
                                                subTitle: 'O arquivo já existe no sistema',
                                                buttons: ['Ok']
                                            });
                                            a_4.present();
                                        }
                                        else if (response.data.status == '0x103') {
                                            var a_5 = _this._alert.create({
                                                title: 'Error 103',
                                                subTitle: 'Ocorreu um erro ao salvar o arquivo',
                                                buttons: ['Ok']
                                            });
                                            a_5.present();
                                        }
                                        else if (response.data.status == '0x110') {
                                            var a_6 = _this._alert.create({
                                                title: 'Erro de job',
                                                subTitle: 'Esse cliente não pertence a loja selecionada.',
                                                buttons: ['Ok']
                                            });
                                            a_6.present();
                                        }
                                    });
                                });
                                if (temp_entradas instanceof Array) {
                                    temp_entradas.forEach(function (entrada_el, entrada_index) {
                                        _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertEntrada.php', entrada_el, {}).then(function (response) {
                                            response.data = JSON.parse(response.data);
                                            if (response.data.status == '0x104') {
                                                temp_entradas.splice(entrada_index, 1);
                                                mine++;
                                                // alert('entradas worked ' + mine + ' max: ' + max)
                                                if (mine == max) {
                                                    load.dismiss();
                                                    _this._alert.create({
                                                        title: 'Sincronizado com sucesso!',
                                                        message: 'Todos o itens foram sincronizados com sucesso!'
                                                    }).present();
                                                    localStorage.setItem('temp_entradas', JSON.stringify(temp_entradas));
                                                }
                                            }
                                        });
                                    });
                                }
                                if (temp_saidas instanceof Array) {
                                    temp_saidas.forEach(function (saida_element, saida_index) {
                                        _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertSaida.php', saida_element, {}).then(function (response) {
                                            response.data = JSON.parse(response.data);
                                            if (response.data.status == '0x104') {
                                                temp_saidas.splice(saida_index, 1);
                                                mine++;
                                                // alert('saidas worked ' + mine + ' max: ' + max)
                                                if (mine == max) {
                                                    load.dismiss();
                                                    _this._alert.create({
                                                        title: 'Sincronizado com sucesso!',
                                                        message: 'Todos o itens foram sincronizados com sucesso!'
                                                    }).present();
                                                    localStorage.setItem('temp_saidas', JSON.stringify(temp_saidas));
                                                }
                                            }
                                        });
                                    });
                                }
                            }
                        }
                    }
                ]
            });
            a_1.present();
        }
    };
    MainPage.prototype.decode_utf8 = function (s) {
        return decodeURIComponent(escape(s));
    };
    MainPage.prototype.upgradeAccordion = function (child, index) {
        if (child.activated == true) {
            document.getElementById('accordion_' + index).className =
                document.getElementById('accordion_' + index).className.replace('resizeDown', '');
            document.getElementById('accordion_' + index).className += 'resizeUp';
        }
        else {
            document.getElementById('accordion_' + index).className =
                document.getElementById('accordion_' + index).className.replace('resizeUp', '');
            document.getElementById('accordion_' + index).className += 'resizeDown';
        }
        child.activated = !child.activated;
    };
    MainPage.prototype.OpenDrawer = function () {
        document.getElementById('drawerMain').className += ' is-visible';
        document.getElementById('obfuscatorMain').className += ' is-visible';
    };
    MainPage.prototype.closeDrawer = function () {
        document.getElementById('drawerMain').className =
            document.getElementById('drawerMain').className.replace(' is-visible', '');
        document.getElementsByClassName('mdl-layout__obfuscator is-visible')[0].className =
            document.getElementsByClassName('mdl-layout__obfuscator is-visible')[0].className.replace(' is-visible', '');
    };
    MainPage.prototype.openFromMenu = function (m) {
        var _this = this;
        if (m.action) {
            switch (m.action) {
                case 'logout':
                    this.logOut();
                    break;
            }
        }
        else {
            this.closeDrawer();
            if (m.title != 'Inicio') {
                setTimeout(function () {
                    if (m.param) {
                        _this.navCtrl.push(m.media, m.param, { animate: false });
                    }
                    else {
                        _this.navCtrl.push(m.media, {}, { animate: false });
                    }
                }, 100);
            }
        }
    };
    MainPage.prototype.logOut = function () {
        localStorage.removeItem('session');
        localStorage.removeItem('creds');
        localStorage.setItem('session', JSON.stringify({
            visitante: {
                Usr_Nome: 'Temporario',
                Usr_Email: '',
                Usr_Telefone: '',
                Usr_Nivel: 'Visitante'
            }
        }));
        this.navCtrl.setRoot(MainPage_1);
    };
    MainPage.prototype.doRefresh = function (event) {
        var _this = this;
        this.noticia.grupos = [];
        this.cards = [];
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php', {}, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            if (response.data.status == '0x104') {
                if (_this.cards.length < response.data.result.length) {
                    response.data.result.forEach(function (element) {
                        element.image = element.app_image;
                        element.app_image = _this._sanitization.bypassSecurityTrustStyle("url('https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/" + element.app_image + "') center / cover");
                        _this.cards.push(element);
                    });
                }
            }
            if (JSON.parse(localStorage.getItem('session'))[0] !== undefined &&
                JSON.parse(localStorage.getItem('session'))[0].PRO_ID !== undefined) {
                JSON.parse(localStorage.getItem('session')).forEach(function (element) {
                    _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectClientePromotor.php', {
                        pro_id: element.PRO_ID
                    }, {}).then(function (response2) {
                        response2.data = JSON.parse(response2.data);
                        if (response2.data.status == '0x104') {
                            response2.data.result.forEach(function (element) {
                                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php', {
                                    cliente: element.aux_cliente_id
                                }, {}).then(function (response3) {
                                    response3.data = JSON.parse(response3.data);
                                    if (response3.data.status == '0x104') {
                                        response3.data.result.forEach(function (element, index) {
                                            element.activated = false;
                                            _this.noticia.grupos.push(element);
                                            if (index == (response3.data.result.length - 1)) {
                                                element.activated = true;
                                            }
                                        });
                                    }
                                });
                            });
                        }
                    });
                });
            }
            else if (JSON.parse(localStorage.getItem('session')).user !== undefined) {
                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php', {
                    cliente: JSON.parse(localStorage.getItem('session')).redes[0].aux_cliente_id
                }, {}).then(function (response2) {
                    response2.data = JSON.parse(response2.data);
                    if (response2.data.status == '0x104') {
                        response2.data.result.forEach(function (element, index) {
                            element.activated = false;
                            _this.noticia.grupos.push(element);
                            if (index == (response2.data.result.length - 1)) {
                                element.activated = true;
                            }
                        });
                    }
                });
            }
            else if (JSON.parse(localStorage.getItem('session')).visitante !== undefined) {
                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectNews.php', {
                    is_guest: true
                }, {}).then(function (response3) {
                    response3.data = JSON.parse(response3.data);
                    if (response3.data.status == '0x104') {
                        response3.data.result.forEach(function (element, index) {
                            element.activated = false;
                            element.content.forEach(function (ele) {
                                ele.image = ele.app_image;
                                ele.app_image = _this._sanitization.bypassSecurityTrustStyle("url(https://kstrade.com.br/sistema/assets/kstrade_php/uploads/news/" + ele.app_image + ") center / cover");
                            });
                            if (index == (response3.data.result.length - 1)) {
                                element.activated = true;
                            }
                            _this.noticia.grupos.push(element);
                        });
                    }
                });
            }
            event.complete();
        });
    };
    MainPage.prototype.openNewImage = function (c, id) {
        //console.log(c);
        var fotos = [];
        fotos.push('news/' + c.image);
        if (c.app_fotos_extras.length > 0) {
            c.app_fotos_extras.forEach(function (element) {
                fotos.push('news/' + element.new_foto);
            });
        }
        var modal = this._modal.create(__WEBPACK_IMPORTED_MODULE_5__view_photo_view_photo__["a" /* ViewPhotoPage */], {
            type: 'photo',
            fotos: fotos,
            id: id
        });
        modal.present();
    };
    MainPage.prototype.openNews = function (obj) {
        //console.log(obj)
        var modal = this._modal.create(__WEBPACK_IMPORTED_MODULE_9__view_new_complete_view_new_complete__["a" /* ViewNewCompletePage */], {
            group_id: obj.group_id,
            group_name: obj.group_name
        });
        modal.present();
    };
    MainPage = MainPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-main',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\main\main.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <button (click)="this.OpenDrawer()" style="position: absolute; left: 10px; top: 12px;" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n      <i class="material-icons">reorder</i>\n\n    </button>\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n    <span *ngIf="status == false" style="float: right; font-size: 12px; display: table; margin: auto">Desconectado</span>\n\n  </header>\n\n  <div class="mdl-layout__drawer" id="drawerMain">\n\n    <span class="mdl-layout-title char">Menu</span>\n\n    <nav class="mdl-navigation">\n\n      <a *ngFor="let m of this.menu; let i = index" class="mdl-navigation__link descChar" (click)="m.news == true ? this.openNews(m):this.openFromMenu(m)" style="position: relative;" [ngStyle]="{\'marginBottom\': (i == this.menu.length -1 ? \'45px\':\'0px\')}">\n\n        <i class="material-icons" *ngIf="m.customized === undefined">{{m.icon}}</i>\n\n        <span *ngIf="m.customized === undefined">{{m.title}}</span>\n\n\n\n        <span *ngIf="m.customized == true" style="border-radius: 100px; padding: 10px; background: #f44242; color: #000; width: 40px; height: 40px;position: absolute;text-align: center;left: 5px;">{{m.title.split(\'\')[0]}}</span>\n\n        <span *ngIf="m.customized == true" style="padding-left: 40px;position: relative;top: 10px;">{{m.title}}</span>\n\n      </a>\n\n    </nav>\n\n  </div>\n\n  <!-- <div *ngIf="this._checker.getMStatus() == \'offline\'" style="text-align:center; width: 100%; background: #f44242">\n\n    <p style="font-size: 10px; margin: 2px; color: white">Sem internet</p>\n\n  </div> -->\n\n  \n\n  <ion-content style="background: #f5f7fa">\n\n    <!-- <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: #f5f7fa"> -->\n\n      <!-- <ion-refresher (ionRefresh)="doRefresh($event)">\n\n        <ion-refresher-content\n\n          pullingIcon="arrow-dropdown"\n\n          pullingText="Solte para atualizar as noticias"\n\n          refreshingSpinner="circles"\n\n          refreshingText="Atualizando noticias...">\n\n        </ion-refresher-content>\n\n      </ion-refresher> -->\n\n\n\n      <div class="page-content" style="background: #f5f7fa">\n\n        <div *ngIf="noticia.grupos.length < 1 && noticia.grupos === undefined">\n\n          <span style="display: table; margin: auto; font-size: 22px; font-weight: bold;">Nenhuma notícia!</span>\n\n        </div>\n\n        <div *ngFor="let n of noticia.grupos; let i = index">\n\n          <div (click)="upgradeAccordion(n, i)" style="cursor: pointer;" class="accordion_title">\n\n            <h5 style="border-bottom: 1px solid #dcdcdc; margin: 5px;">\n\n              {{n.group_nome}}\n\n              <i class="material-icons" style="margin-left: 10px;">{{ n.activated == true? \'keyboard_arrow_up\':\'keyboard_arrow_down\'}}</i>\n\n            </h5>\n\n            \n\n          </div>\n\n          <div id="accordion_{{i}}" class="mdl-grid resizeUp" style="display: table;">\n\n            <div *ngIf="n.activated == true"> \n\n              <div *ngFor="let c of n.content; let i = index">\n\n                <div *ngIf="c.app_type == \'image\'" class="mdl-card mdl-shadow--2dp" style="display: table; margin: 10px auto;">\n\n                  <div (click)="openNewImage(c, c.app_id)" [style.background]="c.app_image" class="mdl-card__title" style="color: #f5f7fa; height: 176px;">\n\n                  </div>\n\n                  <div style="width: 100%; text-align: center; background: rgba(0,0,0,0.4)">\n\n                    <h2 class="mdl-card__title-text descChar" style="background: rgba(0,0,0,0.4); color: #f5f7fa;">{{c.app_title}}</h2>\n\n                  </div>\n\n                  <div class="mdl-card__supporting-text">\n\n                    {{c.app_text}}\n\n                  </div>\n\n                  <div class="mdl-card__actions mdl-card--border">\n\n                    <a (click)="openNewImage(c.image, c.app_id)" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect mdl-color-text--red-700" style="width: 100%; display: table; margin: auto">\n\n                      Comentar\n\n                    </a>\n\n                  </div>\n\n                </div>\n\n    \n\n                <div *ngIf="c.app_type == \'simple\'" class="mdl-card mdl-shadow--2dp" style="display: table; margin: 10px auto;">\n\n                  <div class="mdl-card__title mdl-color--grey-200" style="color: #000;">\n\n                    <h2 class="mdl-card__title-text descChar" style="position: relative; top: 5px; font-size: 18px">{{c.app_title}}</h2>\n\n                  </div>\n\n                  <div class="mdl-card__supporting-text">\n\n                    <p>\n\n                      {{c.app_text}}\n\n                    </p>\n\n        \n\n                  </div>\n\n                </div>\n\n              </div>\n\n            </div>\n\n          </div>\n\n        </div>\n\n\n\n        <div *ngIf="cards.length < 1" style="width: 100%;">\n\n          <i class="material-icons" style="display: table; margin: 5% auto; font-size: 60px">rss_feed</i>\n\n          <span style="position: relative; font-size: 14px; display: table; margin: auto">Nenhuma noticia</span>\n\n        </div>\n\n\n\n        <div *ngFor="let c of cards; let i = index">\n\n            <div *ngIf="c.app_type == \'image\'" class="mdl-card mdl-shadow--2dp" style="margin: 10px auto; position: relative; width: 85%">\n\n              \n\n              <span *ngIf="c.app_fotos_extras.length > 0" style="position: absolute; top: 5px; right: 5px;">\n\n                <i class="material-icons" style="color: red">style</i>\n\n              </span>\n\n              \n\n              <div (click)="openNewImage(c, c.app_id)" [style.background]="c.app_image" class="mdl-card__title" style="color: #f5f7fa; height: 176px;">\n\n              </div>\n\n\n\n              <div style="width: 100%; min-height: 45px; text-align: center; background: rgba(0,0,0,0.4)">\n\n                <h2 class="mdl-card__title-text descChar" style="color: #f5f7fa; display: table; margin: 2px auto">{{c.app_title}}</h2>\n\n              </div>\n\n              <div class="mdl-card__supporting-text">\n\n                {{c.app_text}}\n\n              </div>\n\n              <div class="mdl-card__actions mdl-card--border">\n\n                <a (click)="openNewImage(c, c.app_id)" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect mdl-color-text--red-700" style="width: 100%; display: table; margin: auto">\n\n                  Comentar\n\n                </a>\n\n              </div>\n\n            </div>\n\n\n\n            <div *ngIf="c.app_type == \'simple\'" class="mdl-card mdl-shadow--2dp" style="margin: 10px auto;">\n\n              <div class="mdl-card__title mdl-color--grey-200" style="color: #000;">\n\n                <h2 class="mdl-card__title-text descChar" style="position: relative; top: 5px; font-size: 18px">{{c.app_title}}</h2>\n\n              </div>\n\n              <div class="mdl-card__supporting-text">\n\n                <p>\n\n                  {{c.app_text}}\n\n                </p>\n\n    \n\n              </div>\n\n            </div>\n\n        </div>\n\n        <!-- <div class="mdl-card mdl-shadow--2dp" style="margin: 10px auto;">\n\n          <div class="mdl-card__title" style="color: #f5f7fa; height: 176px; background: url(\'./assets/images/card_bak.jpg\') center / cover;">\n\n            <h2 class="mdl-card__title-text descChar" style="position: relative; top: 15px">Bem vindo ao Eccotrade</h2>\n\n          </div>\n\n          <div class="mdl-card__supporting-text">\n\n            Esse é o sistema KSTrade Merchandising, aproveite todas as funcionalidades! :)\n\n          </div>\n\n        </div>\n\n\n\n        <div class="mdl-card mdl-shadow--2dp" style="margin: 10px auto;">\n\n          <div class="mdl-card__title" style="color: #f5f7fa; height: 176px; background: url(\'./assets/images/localizacao_eccotrade.png\') center / cover;">\n\n            <h2 class="mdl-card__title-text descChar" style="padding: 10px; position: relative; right: 15px; background: rgba(0,0,0,0.4); color: #f5f7fa; position: relative; top: 15px">Serviço de Localização</h2>\n\n          </div>\n\n          <div class="mdl-card__supporting-text">\n\n            Agora o sistema KSTrade Merchandising conta com o serviço de localização!<br>\n\n            Sempre trazendo mais efetividade aos jobs!\n\n          </div>\n\n        </div>\n\n\n\n        <div class="mdl-card mdl-shadow--2dp" style="margin: 10px auto;">\n\n          <div class="mdl-card__title mdl-color--grey-200" style="color: #000;">\n\n            <h2 class="mdl-card__title-text descChar" style="position: relative; top: 5px; font-size: 18px">Agora você pode excluir o Job!</h2>\n\n          </div>\n\n          <div class="mdl-card__supporting-text">\n\n            <p>\n\n              O uso é muito simples. <br>\n\n              Acesse o menu e em <b (click)="openFromMenu(this.menu[2])" style="font-size: 16px">"Jobs do dia"</b>, pressione e segure sob um job para usar!<br>\n\n              O tempo máximo de exclusão é de <b style="font-size: 16px">5 minutos</b>, após isso não é possível mais excluir o Job.\n\n            </p>\n\n\n\n          </div>\n\n        </div> -->\n\n      </div>\n\n    <!-- </main> -->\n\n  </ion-content>\n\n  <div class="mdl-layout__obfuscator" id="obfuscatorMain" (click)="closeDrawer()"></div>\n\n</div>  \n\n'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\main\main.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_11__providers_connection_checker_connection_checker__["a" /* ConnectionCheckerProvider */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_keyboard__["a" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_admob_free__["a" /* AdMobFree */]])
    ], MainPage);
    return MainPage;
    var MainPage_1;
}());

//# sourceMappingURL=main.js.map

/***/ }),

/***/ 400:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loading_loading__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__esqueceu_senha_esqueceu_senha__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert2__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__opts_login_opts_login_opts__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginPage = /** @class */ (function () {
    function LoginPage(popoverCtrl, platform, modalCtrl, navCtrl, navParams, _http, _alert) {
        this.popoverCtrl = popoverCtrl;
        this.platform = platform;
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this._alert = _alert;
        this.part = '';
        this.email = '';
        this.senha = '';
        this.nome = '';
        this.telefone = '';
    }
    LoginPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    LoginPage.prototype.presentPopover = function () {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_5__opts_login_opts_login_opts__["a" /* LoginOptsPage */]);
        popover.present();
    };
    LoginPage.prototype.cadastrar = function () {
        var _this = this;
        if (this.email.length < 1 || this.email === undefined || this.senha.length < 1 || this.senha === undefined) {
            var a = this._alert.create({
                title: 'Preencha o campo!',
                subTitle: 'Preencha o campo e-mail para acessar o sistema!',
                buttons: ['Ok']
            });
            a.present();
        }
        else {
            if (localStorage.getItem('isMD') && localStorage.getItem('isMD') == '1') {
                this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/updateNetStatus.php', {
                    status: 'connected',
                    email: JSON.parse(localStorage.getItem('creds')).email,
                    senha: JSON.parse(localStorage.getItem('creds')).senha
                }, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        localStorage.removeItem('isMD');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__loading_loading__["a" /* LoadingPage */], { page: 'main' }, { animate: false });
                    }
                });
            }
            this._http.setDataSerializer('json');
            this._http.post(
            // 'http://kstrade.com.br/kstrade_php/assets/sistema/assets/eccotrade_php/app/login.php',
            // 'https://kstrade.com.br/sistema/assets/kstrade_php/app/login.php',
            'https://kstrade.com.br/sistema/assets/kstrade_php/app/login.php', {
                email: this.email,
                senha: this.senha
            }, {
                "Content-Type": "application/json"
            }).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.length < 1) {
                    var a = _this._alert.create({
                        title: 'E-mail Inválido',
                        subTitle: 'O E-mail inserido nao está cadastrado no sistema',
                        buttons: ['Ok']
                    });
                    a.present();
                }
                else if (response.data[0] !== undefined) {
                    if (response.data[0].content[0].PRO_Status == 'Inativo') {
                        var a = _this._alert.create({
                            title: 'Login inativo',
                            subTitle: 'Seu login foi inativado, por favor consulte-nos!',
                            buttons: ['Ok']
                        });
                        a.present();
                    }
                    else {
                        localStorage.setItem('session', JSON.stringify(response.data));
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__loading_loading__["a" /* LoadingPage */], { page: 'main' }, { animate: false });
                        localStorage.setItem('creds', JSON.stringify({ email: _this.email, senha: _this.senha }));
                    }
                }
                else if (response.data.status == '0x104' && response.data.result.user.Usr_Nivel == 'Vendedor') {
                    localStorage.setItem('session', JSON.stringify(response.data.result));
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__loading_loading__["a" /* LoadingPage */], { page: 'main' }, { animate: false });
                }
                else if (response.data.status == '0x104' && response.data.result.user.Usr_Nivel == 'Representante') {
                    localStorage.setItem('session', JSON.stringify(response.data.result));
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__loading_loading__["a" /* LoadingPage */], { page: 'main' }, { animate: false });
                }
                else if (response.data.status == '0x104' && response.data.result.user.Usr_Nivel == 'Externo') {
                    localStorage.setItem('session', JSON.stringify(response.data.result));
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__loading_loading__["a" /* LoadingPage */], { page: 'main' }, { animate: false });
                }
                else if (response.data.status == '0x101') {
                    var a = _this._alert.create({
                        title: 'E-mail ou senha incorretos',
                        subTitle: 'E-mail ou senha inseridos estão incorretos',
                        buttons: ['Ok']
                    });
                    a.present();
                }
            });
        }
    };
    LoginPage.prototype.forgot = function () {
        var m = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__esqueceu_senha_esqueceu_senha__["a" /* EsqueceuSenhaPage */]);
        m.present();
    };
    LoginPage.prototype.startWithGuest = function (nome, email, telefone) {
        var _this = this;
        if (document.getElementById('telefone') != null) {
            telefone = document.getElementById('telefone').value;
        }
        else {
            telefone = '';
        }
        if (this.platform.is('android') && nome.length < 1 && email.length < 1) {
            __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default()({
                title: 'Campos vazios',
                text: 'Preencha todos os campos para continuar!',
                type: 'warning'
            });
        }
        else if (this.platform.is('android') &&
            !email.split('@') && ((email.split('@')[0] === undefined && email.split('@')[1] === undefined))) {
            __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default()({
                title: 'Preencher corretamente',
                text: 'Preencha um e-mail e/ou telefone válido!',
                type: 'warning'
            });
        }
        else if (this.platform.is('android') &&
            email.split('@') && ((email.split('@')[0].length < 3 || email.split('@')[1].length < 3))
            || (email.split('@')[1].indexOf('.com') == -1)) {
            __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default()({
                title: 'Preencher corretamente',
                text: 'Preencha um e-mail e/ou telefone válido!',
                type: 'warning'
            });
        }
        else {
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/insertContato.php', {
                nome: nome,
                telefoneFixo: telefone || '',
                telefoneMovel: '',
                email: email,
                obs: 'CONTATO CADASTRADO PELO APLICATIVO KSTRADE' + (this.platform.is('ios') ? ' DE UM DISPOSITIVO iOS' : ' DE UM DISPOSITIVO ANDROID')
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    localStorage.setItem('session', JSON.stringify({
                        visitante: {
                            Usr_Nome: nome,
                            Usr_Email: email,
                            Usr_Telefone: telefone,
                            Usr_Nivel: 'Visitante'
                        }
                    }));
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__loading_loading__["a" /* LoadingPage */], { page: 'main' }, { animate: false });
                }
                else {
                    __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default()({
                        title: 'Erro ao salvar',
                        type: 'error'
                    });
                }
            });
        }
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\login\login.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;"></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">\n\n  <div\n\n    style="height: calc(100vh); background: url(\'./assets/images/bgg.png\'); background-position: center; /*! opacity: 0.25; */background-size: 100% 67%; filter: blur(2px); -webkit-filter: blur(2px)">\n\n  </div>\n\n\n\n  <main class="mdl-layout__content"\n\n    style="height: calc(100vh - 56px); background: #f5f7fa; position: absolute; top: 0;width: 100%;height: 100%;background: transparent;">\n\n    <div class="page-content " style="z-index: 10">\n\n      <div *ngIf="this.part != \'guest\'" class="mdl-card mdl-shadow--2dp"\n\n        style="width: 85%; display: table; margin: 25% auto">\n\n        <button class="mdl-button mdl-button--icon" (click)="goBack()">\n\n          <i class="material-icons">keyboard_arrow_left</i>\n\n        </button>\n\n\n\n        <img src="./assets/images/logoecco.png" style="width: 25%; display: table; margin: 5px auto">\n\n        <div class="mdl-card__title">\n\n          <span class="descChar" style="font-size: 12px">Olá seja muito bem vindo!</span>\n\n        </div>\n\n        <div class="mdl-card__media mdl-color--white">\n\n          <ion-list inset>\n\n            <ion-item>\n\n              <ion-label>E-mail</ion-label>\n\n              <ion-input [(ngModel)]="this.email" class="simpleInput" type="text"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item>\n\n              <ion-label>Senha</ion-label>\n\n              <ion-input [(ngModel)]="this.senha" (keydown.enter)="cadastrar()" class="simpleInput" type="password">\n\n              </ion-input>\n\n            </ion-item>\n\n          </ion-list>\n\n          <button (click)="cadastrar()"\n\n            class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-color--red-600 mdl-color-text--white"\n\n            style="width: 90%; margin: auto; display: table;">\n\n            <span>Acessar</span>\n\n            <i class="material-icons mdl-color-text--red-900" style="float: right; margin-top: 5px">send</i>\n\n          </button>\n\n        </div>\n\n        <div class="mdl-card__actions">\n\n          <p style="padding-left: 10px; font-size: 10px; padding-right: 10px;">\n\n            Acompanhe todas as novidades do mundo do Merchandising que nós separamos para você!\n\n          </p>\n\n          <p style="padding-left: 10px; font-size: 10px; padding-right: 10px;">\n\n            Veja também tablóides de todas as redes.\n\n          </p>\n\n          <span class="descChar" style="font-size: 14px; padding: 12px; text-align: center; margin-top: 10px;">Conheça\n\n            agora\n\n            mesmo! <a (click)="cadastra" style="color: #d50000; font-weight: bold;"\n\n              (click)="this.part = \'guest\'">Cadastre-se</a></span>\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngIf="this.part == \'guest\'" class="mdl-card mdl-shadow--2dp"\n\n        style="width: 85%; display: table; margin: 25% auto">\n\n        <button (click)="this.part = \'\'" class="mdl-button mdl-js-button mdl-button--icon"\n\n          style="position: absolute; top: 5px; left: 15px">\n\n          <i class="material-icons">keyboard_arrow_left</i>\n\n        </button>\n\n        <img src="./assets/images/logoecco.png" style="width: 25%; display: table; margin: 5px auto">\n\n        <div class="mdl-card__title">\n\n          <span class="descChar" style="font-size: 12px">Cadastre-se já e comece a utilizar!</span>\n\n        </div>\n\n        <div class="mdl-card__media mdl-color--white">\n\n          <ion-list inset>\n\n            <ion-item>\n\n              <ion-label style="font-size: 14px">Nome</ion-label>\n\n              <ion-input [(ngModel)]="this.nome" class="simpleInput" type="text"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item>\n\n              <ion-label style="font-size: 14px">E-mail</ion-label>\n\n              <ion-input [(ngModel)]="this.email" class="simpleInput" type="text"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item>\n\n              <ion-label style="font-size: 14px">Telefone</ion-label>\n\n              <ion-input [(ngModel)]="this.telefone" class="simpleInput" type="text"></ion-input>\n\n            </ion-item>\n\n\n\n          </ion-list>\n\n          <button *ngIf="false" (click)="presentPopover()" class="mdl-button mdl-js-button mdl-js-ripple-effect"\n\n            style="width: 95%; margin: auto; display: table; border-bottom: 1px solid #e8e8e8; text-transform: none">\n\n            <span>Descreva sua atividade</span>\n\n            <i class="material-icons mdl-color-text--red-900"\n\n              style="float: right; margin-top: 5px">keyboard_down_arrow</i>\n\n          </button>\n\n          <div class="mdl-card__actions">\n\n            <button (click)="startWithGuest(this.nome, this.email, this.telefone)"\n\n              class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-color--red-600 mdl-color-text--white"\n\n              style="width: 90%; margin: auto; display: table;">\n\n              <span>Cadastrar</span>\n\n              <i class="material-icons mdl-color-text--red-900" style="float: right; margin-top: 5px">send</i>\n\n            </button>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewJobModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pesquisa_modal_pesquisa_modal__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_device__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_vibration__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_rota_rota__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_connection_checker_connection_checker__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











// import { BackgroundMode } from '@ionic-native/background-mode';
/**
 * Generated class for the NewJobModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NewJobModalPage = /** @class */ (function () {
    function NewJobModalPage(platform, _rota, _vibration, _device, modalCtrl, _geolocation, loadingCtrl, _http, _alert, camera, navCtrl, navParams, viewCtrl, _checker) {
        this.platform = platform;
        this._rota = _rota;
        this._vibration = _vibration;
        this._device = _device;
        this.modalCtrl = modalCtrl;
        this._geolocation = _geolocation;
        this.loadingCtrl = loadingCtrl;
        this._http = _http;
        this._alert = _alert;
        this.camera = camera;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this._checker = _checker;
        this.camData = [];
        this.obs = '';
        this.cliente = 'Selecione o cliente';
        this.clienteIndex = 0;
        this.clientes = [];
        this.clienteOK = false;
        this.nameOfFile = '';
        this.promotorFromRedeFilial = 0;
        this.myLat = null;
        this.myLong = null;
    }
    NewJobModalPage.prototype.ngOnInit = function () {
        // if (this._device.platform.toLowerCase() == 'android' && parseInt( this._device.version, 10 ) < 8){
        //   this._background.enable();
        // }
        var _this = this;
        this.pageParams = this.viewCtrl.getNavParams().data;
        this._geolocation.getCurrentPosition().then(function (data) {
            _this.myLat = data.coords.latitude;
            _this.myLong = data.coords.longitude;
        }).catch(function (e) {
            var a = _this._alert.create({
                title: 'Ative a localização',
                subTitle: 'Você deve ativar a localização! errorCode:' + JSON.stringify(e),
                buttons: ['Ok']
            });
            a.present();
            _this.viewCtrl.dismiss({});
        });
        if (this._checker.getMStatus() == 'offline') {
            this.promotorFromRedeFilial = this.pageParams.PRO_ID;
            this.pageParams.cliente.forEach(function (element) {
                _this.clientes.push(element);
            });
        }
        else {
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/promotorOfRedeFilial.php', {
                rede: this.pageParams.RED_ID,
                filial: this.pageParams.RFIL_ID,
                nome: JSON.parse(localStorage.getItem('session'))[0].content[0].PRO_Nome
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    _this.promotorFromRedeFilial = response.data.result[0].Aux_Promotor_ID;
                }
                _this.pageParams.cliente.forEach(function (element) {
                    _this.clientes.push(element);
                });
            }, function () {
                _this.promotorFromRedeFilial = _this.pageParams.PRO_ID;
                _this.pageParams.cliente.forEach(function (element) {
                    _this.clientes.push(element);
                });
            });
        }
    };
    NewJobModalPage.prototype.getInitAndFimSemana = function () {
        var init = '';
        var fim = '';
        switch (new Date().getDay()) {
            case 0: {
                init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate()).toISOString().substring(0, 10);
                fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 6).toISOString().substring(0, 10);
                break;
            }
            case 1: {
                init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 1).toISOString().substring(0, 10);
                fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 5).toISOString().substring(0, 10);
                break;
            }
            case 2: {
                init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 2).toISOString().substring(0, 10);
                fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 4).toISOString().substring(0, 10);
                break;
            }
            case 3: {
                init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 3).toISOString().substring(0, 10);
                fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 3).toISOString().substring(0, 10);
                break;
            }
            case 4: {
                init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 4).toISOString().substring(0, 10);
                fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 2).toISOString().substring(0, 10);
                break;
            }
            case 5: {
                init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 5).toISOString().substring(0, 10);
                fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 1).toISOString().substring(0, 10);
                break;
            }
            case 6: {
                init = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 6).toISOString().substring(0, 10);
                fim = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate()).toISOString().substring(0, 10);
                break;
            }
        }
        return {
            init: init,
            end: fim
        };
    };
    NewJobModalPage.prototype.get_orientation = function (src) {
        var img = new Image();
        img.src = src;
        var width = img.width;
        var height = img.height;
        // height = height + height // Double the height to get best
        // height = height + (height / 4) // Increase height by 50%
        var height_plus = height + (height / 2);
        if ((width >= height) || height_plus <= width) {
            return "landscape";
        }
        else {
            return "portrait";
        }
    };
    NewJobModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss({});
    };
    NewJobModalPage.prototype.getDataUrl = function (img) {
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0);
        return canvas.toDataURL();
    };
    NewJobModalPage.prototype.getData = function (path, cb) {
        var xmlHTTP = new XMLHttpRequest();
        xmlHTTP.open('GET', path, true);
        xmlHTTP.responseType = 'arraybuffer';
        xmlHTTP.onload = function (e) {
            var arr = new Uint8Array(this.response);
            var raw = String.fromCharCode.apply(null, arr);
            var b64 = btoa(raw);
            cb(b64);
        };
        xmlHTTP.send();
    };
    NewJobModalPage.prototype.takePicture = function () {
        var _this = this;
        if (this.camData.length >= 3) {
            var a = this._alert.create({
                title: 'Fotos suficientes!',
                subTitle: 'Só é permitido 3 fotos por job!',
                buttons: ['OK']
            });
            a.present();
        }
        else {
            this.camera.getPicture({
                quality: 50,
                destinationType: this.platform.is('android') ? this.camera.DestinationType.FILE_URI : this.camera.DestinationType.NATIVE_URI,
                encodingType: this.camera.EncodingType.JPEG,
                mediaType: this.camera.MediaType.PICTURE,
                targetHeight: 720,
                targetWidth: 1280,
                correctOrientation: true
            }).then(function (imageData) {
                _this.getData(imageData, function (b64) {
                    _this.camData.push(b64);
                });
            });
        }
    };
    NewJobModalPage.prototype.removePhoto = function (index) {
        this.camData.splice(index, 1);
    };
    NewJobModalPage.prototype.doneJob = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Enviando...',
            enableBackdropDismiss: true
        });
        if (this.promotorFromRedeFilial == 0) {
            this.ngOnInit();
        }
        if (this.camData.length < 1) {
            var a = this._alert.create({
                title: 'Tire uma foto!',
                subTitle: 'É necessário tirar uma foto para completar o job',
                buttons: ['Ok']
            });
            a.present();
        }
        else if (document.getElementById('txtJob').value.length < 1) {
            var a = this._alert.create({
                title: 'Preencha o campo!',
                subTitle: 'É necessário fazer uma observação do job',
                buttons: ['Ok']
            });
            a.present();
        }
        else {
            var exp_1 = {};
            try {
                if (!this.platform.is('ios')) {
                    this.camData.forEach(function (element) {
                        if (_this.get_orientation('data:image/png;base64,' + element) == 'portrait') {
                            throw exp_1;
                        }
                    });
                }
                this.nameOfFile = Math.ceil(Math.random() * 1000) + this.pageParams.RED_Nome + new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0, 10) + '.jpg';
                var nameOfFile_1 = this.nameOfFile.trim();
                loading.present();
                var data_1 = new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0, 10);
                var hora_1 = (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes());
                var data_inicial = new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-01').toISOString().substring(0, 10);
                this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertJob.php', {
                    title: nameOfFile_1,
                    anexo: this.camData,
                    // anexo: this.toUploadImage,
                    cliente: this.clienteIndex,
                    rede: this.pageParams.RED_ID,
                    promotor: this.promotorFromRedeFilial,
                    filial: this.pageParams.RFIL_ID,
                    status: 'Realizado',
                    data: data_1,
                    data_inicial: data_inicial,
                    ano: new Date().getFullYear(),
                    mes: (new Date().getMonth() + 1),
                    hora: hora_1,
                    obs: this.obs,
                    foto: nameOfFile_1,
                    lat: this.myLat,
                    lon: this.myLong,
                    modelo: this._device.model.toString().trim() || null,
                    plataforma: this._device.platform.toString().trim() || null,
                    versao: this._device.version.toString().trim() || null,
                    manufact: this._device.manufacturer.toString().trim() || null
                }, {}).then(function (response) {
                    console.log(response);
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        if (_this.pageParams.RFIL_Nova_Entrada == false && _this.pageParams.RFIL_Numero_Visitas == 1) {
                            _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertSaida.php', {
                                pro_id: _this.pageParams.PRO_ID,
                                rede_id: _this.pageParams.RED_ID,
                                rfil_id: _this.pageParams.RFIL_ID,
                                data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                                hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                                lat_saida: _this.myLat,
                                long_saida: _this.myLong
                            }, {}).then(function (response2) {
                                response2.data = JSON.parse(response2.data);
                                if (response.data.status == '0x104') {
                                    _this._rota.enableRota({
                                        pro_id: _this.pageParams.PRO_ID,
                                        rede_id: _this.pageParams.RED_ID,
                                        rfil_id: _this.pageParams.RFIL_ID,
                                        data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                                        hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                                        lat_saida: _this.myLat,
                                        long_saida: _this.myLong
                                    });
                                    _this._vibration.vibrate(250);
                                }
                            }, function (err) {
                                if (!localStorage.getItem('temp_saidas')) {
                                    var temp = [];
                                    temp.push({
                                        pro_id: _this.pageParams.PRO_ID,
                                        rede_id: _this.pageParams.RED_ID,
                                        rfil_id: _this.pageParams.RFIL_ID,
                                        data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                                        hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                                        lat_saida: _this.myLat,
                                        long_saida: _this.myLong
                                    });
                                    localStorage.setItem('temp_saidas', JSON.stringify(temp));
                                }
                                else {
                                    var temp = JSON.parse(localStorage.getItem('temp_saidas'));
                                    temp.push({
                                        pro_id: _this.pageParams.PRO_ID,
                                        rede_id: _this.pageParams.RED_ID,
                                        rfil_id: _this.pageParams.RFIL_ID,
                                        data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                                        hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                                        lat_saida: _this.myLat,
                                        long_saida: _this.myLong
                                    });
                                    localStorage.setItem('temp_saidas', JSON.stringify(temp));
                                }
                            });
                        }
                        _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectProdutos.php', {
                            cliente: _this.clienteIndex
                        }, {}).then(function (response) {
                            response.data = JSON.parse(response.data);
                            if (response.data.status == '0x104') {
                                if (response.data.result[0].pro_dias_semana.length > 0 && response.data.result[0].pro_dias_semana.split(';').length > 0) {
                                    var days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                                    var dayName_1 = days[new Date().getDay()];
                                    var d = response.data.result[0].pro_dias_semana || '';
                                    var diassemana = d.split(';') || [];
                                    var doneExp1_1 = {};
                                    try {
                                        diassemana.forEach(function (element) {
                                            if (element == dayName_1) {
                                                throw doneExp1_1;
                                            }
                                        });
                                    }
                                    catch (doneExp1) {
                                        var modal_1 = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pesquisa_modal_pesquisa_modal__["a" /* PesquisaModalPage */], {
                                            cliente: _this.clienteIndex,
                                            rede: _this.pageParams.RED_ID,
                                            promotor: _this.promotorFromRedeFilial,
                                            filial: _this.pageParams.RFIL_ID,
                                            data: data_1,
                                            hora: hora_1,
                                            produtos: response.data.result
                                        });
                                        setTimeout(function () {
                                            modal_1.present();
                                        }, 150);
                                        modal_1.onDidDismiss(function (data) {
                                            if (data.done == true) {
                                                var a = _this._alert.create({
                                                    title: 'Enviado com sucesso!',
                                                    subTitle: 'O job foi enviado com sucesso!',
                                                    buttons: ['Ok']
                                                });
                                                a.present();
                                            }
                                            else if (data.done == false && data.error == 'pesquisaError') {
                                                var a = _this._alert.create({
                                                    title: 'Erro na pesquisa',
                                                    subTitle: 'Informação não enviada por favor contate a central.',
                                                    buttons: ['Ok']
                                                });
                                                a.present();
                                            }
                                        });
                                    }
                                }
                                else {
                                    if (response.data.result[0].pro_numero_vezes > 0 && response.data.result[0].pro_numero_vezes !== null) {
                                        _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectCountPesquisa.php', {
                                            cliente: _this.clienteIndex,
                                            rede: _this.pageParams.RED_ID,
                                            promotor: _this.promotorFromRedeFilial,
                                            filial: _this.pageParams.RFIL_ID,
                                            dataone: _this.getInitAndFimSemana().init,
                                            datatwo: _this.getInitAndFimSemana().end
                                        }, {}).then(function (response2) {
                                            response2.data = JSON.parse(response2.data);
                                            if (response2.data.status == '0x104') {
                                                if (response.data.result[0].pro_numero_vezes > response2.data.result[0].numero_pesquisas) {
                                                    var modal_2 = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pesquisa_modal_pesquisa_modal__["a" /* PesquisaModalPage */], {
                                                        cliente: _this.clienteIndex,
                                                        rede: _this.pageParams.RED_ID,
                                                        promotor: _this.promotorFromRedeFilial,
                                                        filial: _this.pageParams.RFIL_ID,
                                                        data: data_1,
                                                        hora: hora_1,
                                                        produtos: response.data.result
                                                    });
                                                    setTimeout(function () {
                                                        modal_2.present();
                                                    }, 150);
                                                    modal_2.onDidDismiss(function (data) {
                                                        if (data.done == true) {
                                                            var a = _this._alert.create({
                                                                title: 'Enviado com sucesso!',
                                                                subTitle: 'O job foi enviado com sucesso!',
                                                                buttons: ['Ok']
                                                            });
                                                            a.present();
                                                        }
                                                        else if (data.done == false && data.error == 'pesquisaError') {
                                                            var a = _this._alert.create({
                                                                title: 'Erro',
                                                                subTitle: 'O job não pode ser enviado!',
                                                                buttons: ['Ok']
                                                            });
                                                            a.present();
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        var a = _this._alert.create({
                                            title: 'Enviado com sucesso!',
                                            subTitle: 'O job foi enviado com sucesso!',
                                            buttons: ['Ok']
                                        });
                                        a.present();
                                    }
                                }
                            }
                            else {
                                var a = _this._alert.create({
                                    title: 'Enviado com sucesso!',
                                    subTitle: 'O job foi enviado com sucesso!',
                                    buttons: ['Ok']
                                });
                                a.present();
                            }
                            loading.dismiss();
                            _this.viewCtrl.dismiss({ job: 'done' });
                        });
                    }
                    else if (response.data.status == '0x105') {
                        loading.dismiss();
                        var a = _this._alert.create({
                            title: 'Job duplicado!',
                            subTitle: 'Você já inseriu esse job hoje!',
                            buttons: ['Ok']
                        });
                        a.present();
                    }
                    else if (response.data.status == '0x101') {
                        loading.dismiss();
                        var a = _this._alert.create({
                            title: 'Erro',
                            subTitle: 'Erro de inserção no banco de dados',
                            buttons: ['Ok']
                        });
                        a.present();
                    }
                    else if (response.data.status == '0x102') {
                        loading.dismiss();
                        var a = _this._alert.create({
                            title: 'Já existe!',
                            subTitle: 'O arquivo já existe no sistema',
                            buttons: ['Ok']
                        });
                        a.present();
                    }
                    else if (response.data.status == '0x103') {
                        loading.dismiss();
                        var a = _this._alert.create({
                            title: 'Error 103',
                            subTitle: 'Ocorreu um erro ao salvar o arquivo',
                            buttons: ['Ok']
                        });
                        a.present();
                    }
                    else if (response.data.status == '0x110') {
                        loading.dismiss();
                        var a = _this._alert.create({
                            title: 'Erro de job',
                            subTitle: 'Esse cliente não pertence a loja selecionada.',
                            buttons: ['Ok']
                        });
                        a.present();
                    }
                }, function (err) {
                    loading.dismiss();
                    _this._alert.create({
                        title: 'Job salvo!',
                        message: 'O Job foi salvo com sucesso! você não está conectado.'
                    }).present();
                    if (!localStorage.getItem('temp_jobs')) {
                        var temp = [];
                        temp.push({
                            title: nameOfFile_1,
                            anexo: _this.camData,
                            // anexo: this.toUploadImage,
                            cliente: _this.clienteIndex,
                            rede: _this.pageParams.RED_ID,
                            promotor: _this.promotorFromRedeFilial,
                            filial: _this.pageParams.RFIL_ID,
                            status: 'Realizado',
                            data: data_1,
                            hora: hora_1,
                            obs: _this.obs,
                            foto: nameOfFile_1,
                            lat: _this.myLat,
                            lon: _this.myLong,
                            modelo: _this._device.model.toString().trim() || null,
                            plataforma: _this._device.platform.toString().trim() || null,
                            versao: _this._device.version.toString().trim() || null,
                            manufact: _this._device.manufacturer.toString().trim() || null
                        });
                        localStorage.setItem('temp_jobs', JSON.stringify(temp));
                    }
                    else {
                        var temp = JSON.parse(localStorage.getItem('temp_jobs'));
                        temp.push({
                            title: nameOfFile_1,
                            anexo: _this.camData,
                            // anexo: this.toUploadImage,
                            cliente: _this.clienteIndex,
                            rede: _this.pageParams.RED_ID,
                            promotor: _this.promotorFromRedeFilial,
                            filial: _this.pageParams.RFIL_ID,
                            status: 'Realizado',
                            data: data_1,
                            hora: hora_1,
                            obs: _this.obs,
                            foto: nameOfFile_1,
                            lat: _this.myLat,
                            lon: _this.myLong,
                            modelo: _this._device.model.toString().trim() || null,
                            plataforma: _this._device.platform.toString().trim() || null,
                            versao: _this._device.version.toString().trim() || null,
                            manufact: _this._device.manufacturer.toString().trim() || null
                        });
                        localStorage.setItem('temp_jobs', JSON.stringify(temp));
                    }
                });
            }
            catch (exp) {
                var a = this._alert.create({
                    title: 'Orientação incorreta!',
                    subTitle: 'Você deve tirar a foto com o celular virado, em orientação "Paisagem"   errorCode: ' + JSON.stringify(exp),
                    buttons: ['OK']
                });
                a.present();
                loading.dismiss();
            }
        }
    };
    NewJobModalPage.prototype.setCliente = function (c) {
        if (c.aux_semana) {
            var days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
            var dayName = days[new Date().getDay()];
            var elem = c.aux_semana.split(';');
            for (var i = 0; i < elem.length; i++) {
                if (elem[i].trim() == dayName) {
                    this.clienteOK = false;
                    this.cliente = c.Cli_Nome;
                    this.clienteIndex = c.Cli_ID;
                    break;
                }
                else if (i == elem.length - 1) {
                    var a = this._alert.create({
                        title: 'Fora de rotina',
                        subTitle: 'Sua rotina determina que não deve ter job deste cliente hoje!',
                        buttons: ['Ok']
                    });
                    a.present();
                }
            }
        }
        this.clienteOK = false;
    };
    NewJobModalPage.prototype.removePicture = function () {
        this.camData = [];
    };
    NewJobModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-new-job-modal',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\new-job-modal\new-job-modal.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content mdl-color--white" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <div class="mdl-card" style="width: 100vw;">\n\n        <div class="mdl-card__title">\n\n        </div>\n\n        <div class="mdl-card__media mdl-color--white">\n\n          <span style="display: table; margin: auto">{{pageParams.RED_Nome}}</span>\n\n          <span style="display: table; margin: auto">{{pageParams.RFIL_Nome}}</span>\n\n          <span style="display: table; margin: auto; font-size: 12px;">{{pageParams.RFIL_Cidade}}</span>\n\n\n\n          <div class="mdl-textfield mdl-js-textfield comboBox">\n\n            <input style="cursor: pointer" readonly [(ngModel)]="this.cliente" (click)="clienteOK = !clienteOK"  type="text" class="comboBox__input">\n\n\n\n            <button class="comboBox__button mdl-button mdl-js-button mdl-button--icon" (click)="clienteOK = !clienteOK">\n\n              <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>\n\n            </button>\n\n\n\n            <div class="comboBox__items" *ngIf="clienteOK" (blur)="clienteOK = false">\n\n              <button *ngFor="let c of clientes; let i = index" (click)="setCliente(c)" class="comboBox__item mdl-button mdl-js-button" style="position: relative;">\n\n                <span style="position: absolute; left: 2px; top: 2px;">{{c.Cli_Nome}}</span>\n\n              </button>\n\n            </div>\n\n          </div>\n\n\n\n          <div style="width: 90%; margin: auto; display: table;">\n\n            <span style="display: table; margin: auto;">Observações: </span>\n\n            <textarea id="txtJob" class="simpleInput" type="text" [(ngModel)]="this.obs" rows="5" cols="30"></textarea>\n\n          </div>\n\n\n\n          <div *ngIf="this.camData.length < 1; else camElse" style="position: relative; top: 20px; bottom: 20px; border: 1px solid #fcfcfc; border-radius: 4px; margin: 0 auto; display: table;">\n\n            <i class="material-icons mdl-color-text--grey-300" style="font-size: 180px; margin: 10px 10px auto; display: table; ">camera_alt</i>\n\n          </div>\n\n          <ng-template #camElse>\n\n            <div style="margin: auto;  position: relative; top: 15px; bottom: 20px; border: 1px solid #dcdcdc;">\n\n              <div *ngFor="let cd of camData; let i = index;" style="position: relative; width: 70%; margin: 10px auto; display: block;">\n\n                <button (click)="removePhoto(i)" style="position: absolute; right: 2px; top: -2px" class="mdl-button mdl-js-button mdl-button--icon mdl-color--red-500 mdl-color-text--white">\n\n                  <i class="material-icons">remove</i>\n\n                </button>\n\n                <img class="{{i > 0 ? \'extra-photo\':\'\'}} normal-photo"  [src]="\'data:image/png;base64,\'+cd"/>\n\n              </div>\n\n            </div>\n\n          </ng-template>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </main>\n\n  <div style="z-index: 5; position: relative; background: #8c1515; width: 100vw;" [ngStyle]="{\'bottom\': (this.platform.is(\'ios\') ? \'45px\':\'0px\')}">\n\n    <div style="display: table; margin: auto;">\n\n      <button (click)="dismiss()" class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-color--white">\n\n        <i class="material-icons">arrow_back</i>\n\n      </button>\n\n      <button (click)="takePicture()" [disabled]="this.camData.length == 3" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab mdl-color--white" style="position: relative;bottom: 25px;width: 60px;height: 60px;border: 1px solid black;">\n\n        <i class="material-icons">camera_alt</i>\n\n      </button>\n\n      <button (click)="doneJob()" class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-color--white">\n\n        <i class="material-icons">done</i>\n\n      </button>\n\n    </div>\n\n  </div>\n\n</div>\n\n'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\new-job-modal\new-job-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_7__providers_rota_rota__["a" /* RotaProvider */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_vibration__["a" /* Vibration */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_9__providers_connection_checker_connection_checker__["a" /* ConnectionCheckerProvider */]])
    ], NewJobModalPage);
    return NewJobModalPage;
}());

//# sourceMappingURL=new-job-modal.js.map

/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MarketShareChartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the MarketShareChartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MarketShareChartPage = /** @class */ (function () {
    function MarketShareChartPage(platform, _alert, navCtrl, navParams, _http) {
        this.platform = platform;
        this._alert = _alert;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this.data = [];
        this.doneChart = false;
        this.chartOpts = {
            type: "doughnut",
            data: {
                labels: ['', 'Outros produtos'],
                datasets: [
                    {
                        data: [],
                        backgroundColor: [
                            "#8c1515",
                            "rgba(27, 55, 44, 0.25)"
                        ],
                        borderWidth: 1
                    }
                ]
            },
            options: {
                circumference: 4 * Math.PI
            }
        };
        this.grafico_select = 0;
        this.resultData = [];
        this.empty = true;
    }
    MarketShareChartPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad MarketShareChartPage');
    };
    MarketShareChartPage.prototype.ngOnInit = function () {
        var _this = this;
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectMarketShare.php', {
            red_id: this.navParams.get('red_id'),
            rfil_id: this.navParams.get('rfil_id'),
            cliente: this.navParams.get('cli_id'),
            pro_id: this.navParams.get('pro_id'),
            pro_nome: this.navParams.get('pro_nome')
        }, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            if (response.data.status == '0x104' && response.data.result.length > 0) {
                _this.empty = false;
                _this.chartOpts.data.labels[0] = _this.navParams.get('pro_nome');
                _this.chartOpts.data.datasets[0].data[0] = parseInt(response.data.result[0].soma);
                _this.data[0] = response.data.result[0].porcentagem;
                _this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](_this.chartC.nativeElement, _this.chartOpts);
            }
        });
    };
    MarketShareChartPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    MarketShareChartPage.prototype.selectGrafico = function () {
        var _this = this;
        var a = this._alert.create();
        var ar = [
            {
                Nome: 'Em pizza',
                value: 'pie'
            },
            {
                Nome: 'Em rede',
                value: 'polarArea'
            },
            {
                Nome: 'Em circulo',
                value: 'doughnut'
            }
        ];
        ar.forEach(function (el, index) {
            a.addInput({ type: 'radio', label: el.Nome, value: el.value + ':' + index, id: 'graf-' + index });
        });
        a.addButton('Cancel');
        a.addButton({
            text: 'OK',
            handler: function (data) {
                _this.grafico_select = ar[data.split(':')[1]];
                // this.chartType = data.split(':')[0];
            }
        });
        a.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("chart"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], MarketShareChartPage.prototype, "chartC", void 0);
    MarketShareChartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-market-share-chart',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\market-share-chart\market-share-chart.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;"></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" style="background: #f5f7fa">\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;"\n\n      class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n      <i class="material-icons">arrow_back</i>\n\n    </button>\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n\n\n  <div style="display: table; margin: auto;" *ngIf="this.empty">\n\n    <i class="material-icons" style="display: table; margin: auto; font-size: 32px;">mood_bad</i>\n\n    <span style="display: table; margin: auto; font-size: 12px;">Nenhum resultado até agora!</span>\n\n  </div>\n\n\n\n  <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <span\n\n      style="display: table; margin: 10px auto; font-size: 12px;">{{this.navParams.get(\'cli_nome\')}}</span>\n\n      <span\n\n      style="display: table; margin: 10px auto; font-size: 12px;">{{this.navParams.get(\'red_nome\')}}</span>\n\n      <span\n\n      style="display: table; margin: 10px auto; font-size: 12px;">{{this.navParams.get(\'rfil_nome\')}}</span>\n\n\n\n    <canvas #chart>\n\n    </canvas>\n\n\n\n    <div style="display: table; margin: 20px auto">\n\n      <span style="display: table; margin: 5px auto" [ngStyle]="{\'font-size\': i == 0 ? \'14px\':\'10px\'}">{{this.chartOpts.data.labels[0]}}</span>\n\n      <small> ocupa: </small>\n\n      <span style="display: table; margin: 5px auto" [ngStyle]="{\'font-size\': i == 0 ? \'16px\':\'12px\'}"> {{ this.data[0] }}%</span>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\market-share-chart\market-share-chart.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__["a" /* HTTP */]])
    ], MarketShareChartPage);
    return MarketShareChartPage;
}());

//# sourceMappingURL=market-share-chart.js.map

/***/ }),

/***/ 405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EsqueceuSenhaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the EsqueceuSenhaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EsqueceuSenhaPage = /** @class */ (function () {
    function EsqueceuSenhaPage(platform, viewCtrl, navCtrl, navParams, _http, alertCtrl) {
        this.platform = platform;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this.alertCtrl = alertCtrl;
        this.email = '';
    }
    EsqueceuSenhaPage.prototype.recuperar = function () {
        var _this = this;
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/forgotSenha.php', {
            email: this.email
        }, {}).then(function (response) {
            if (response.status == 200) {
                var a = _this.alertCtrl.create({
                    title: 'Sucesso!',
                    subTitle: 'Um e-mail foi enviado a você verifique a caixa de entrada',
                    buttons: ['Ok']
                });
                a.present();
                _this.viewCtrl.dismiss();
            }
            else {
                var a = _this.alertCtrl.create({
                    title: 'Erro',
                    subTitle: 'Ocorreu um erro ao enviar o e-mail, por favor tente novamente!',
                    buttons: ['Ok']
                });
                a.present();
            }
        });
    };
    EsqueceuSenhaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-esqueceu-senha',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\esqueceu-senha\esqueceu-senha.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content mdl-color--white" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <div class="mdl-card mdl-shadow--2dp scaleIn" style="width: 80%; display: table; margin: 15% auto; margin-bottom: 20px;">\n\n          <div class="mdl-card__title">\n\n            <span class="descChar">Esqueceu a senha?</span>\n\n          </div>\n\n          <div class="mdl-card__media mdl-color--white">\n\n            <div style="width: 100%; margin-top: 20px;">\n\n              <input id="emailInput" class="simpleInput" type="email" placeholder="E-mail" [(ngModel)]="this.email"/>\n\n            </div>\n\n            <p class="descChar" style="position: relative; top: 20px; padding: 8px; font-size: 12px">\n\n              Será enviado suas credenciais no e-mail digitado acima.\n\n              Isso pode levar alguns minutos.\n\n            </p>\n\n            <!-- <span (click)="forgot()" class="char" style="position: relative; left: 10px; top: 15px; font-weight: bold;">Esqueceu a senha?</span> -->\n\n          </div>\n\n          <div class="mdl-card__actions">\n\n            <button (click)="recuperar()" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-color-text--white" style="background: #8c1515; z-index: 999; margin: 10px; float: right">\n\n              <span>Recuperar</span>\n\n            </button>\n\n          </div>\n\n        </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\esqueceu-senha\esqueceu-senha.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], EsqueceuSenhaPage);
    return EsqueceuSenhaPage;
}());

//# sourceMappingURL=esqueceu-senha.js.map

/***/ }),

/***/ 406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginOptsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the LoginOptsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginOptsPage = /** @class */ (function () {
    function LoginOptsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LoginOptsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginOptsPage');
    };
    LoginOptsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login-opts',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\login\opts\login-opts\login-opts.html"*/'<!--\n  Generated template for the LoginOptsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>LoginOpts</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\login\opts\login-opts\login-opts.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], LoginOptsPage);
    return LoginOptsPage;
}());

//# sourceMappingURL=login-opts.js.map

/***/ }),

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the UpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UpdatePage = /** @class */ (function () {
    function UpdatePage(platform, _platform, navCtrl, navParams) {
        this.platform = platform;
        this._platform = _platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.resumeToUpdate = false;
        this._platform.registerBackButtonAction(function () {
            //console.log('u cant return');
            return;
        }, 1);
    }
    UpdatePage.prototype.ngOnInit = function () {
        document.addEventListener('backbutton', function () {
            //console.log('u cant return');
            return;
        }, false);
    };
    UpdatePage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    UpdatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-update',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\update\update.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n    <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n      <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n        <i class="material-icons">arrow_back</i>\n\n      </button>\n\n      <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n      <div class="mdl-layout-spacer"></div>\n\n    </header>\n\n    <main class="mdl-layout__content" style="width: 100%; height: 100vh">\n\n      <div class="page-content" style="background: #f5f7fa">\n\n        <!-- <iframe src="http://eccotrade.com.br/sistema/aplicativo" style="width: 100%; height: 100vh"></iframe> -->\n\n      </div>\n\n    </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\update\update.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], UpdatePage);
    return UpdatePage;
}());

//# sourceMappingURL=update.js.map

/***/ }),

/***/ 408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(416);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 416:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser_animations__ = __webpack_require__(571);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(573);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_loading_loading__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_init_init__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_forms__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_new_job_modal_new_job_modal__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_view_photo_view_photo__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_vibration__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_update_update__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_browser_tab__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_geolocation__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_pesquisa_modal_pesquisa_modal__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_utils_utils__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__node_modules_ionic_native_network__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_date_picker__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_device__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_local_notifications__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_quiz_modal_quiz_modal__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_market_share_chart_market_share_chart__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_main_main__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_main_main_module__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_rota_rota__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__angular_common_http__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_connection_checker_connection_checker__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31_telefone_mask_ng2__ = __webpack_require__(574);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_view_new_complete_view_new_complete__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33_ionic_img_viewer__ = __webpack_require__(336);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_keyboard__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__ionic_native_admob_free__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























// import { ChartsModule } from '';












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_init_init__["a" /* InitPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_loading_loading__["a" /* LoadingPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_new_job_modal_new_job_modal__["a" /* NewJobModalPage */],
                // LoginPage,
                __WEBPACK_IMPORTED_MODULE_13__pages_view_photo_view_photo__["a" /* ViewPhotoPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_update_update__["a" /* UpdatePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_pesquisa_modal_pesquisa_modal__["a" /* PesquisaModalPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_quiz_modal_quiz_modal__["a" /* QuizModalPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_market_share_chart_market_share_chart__["a" /* MarketShareChartPage */],
                __WEBPACK_IMPORTED_MODULE_31_telefone_mask_ng2__["a" /* TelefoneMask */],
                __WEBPACK_IMPORTED_MODULE_32__pages_view_new_complete_view_new_complete__["a" /* ViewNewCompletePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], { preloadModules: true }, {
                    links: [
                        { loadChildren: '../pages/area-cliente/cliente-filiais/cliente-filiais.module#ClienteFiliaisPageModule', name: 'ClienteFiliaisPage', segment: 'cliente-filiais', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/area-cliente/cliente-lojas/cliente-lojas.module#ClienteLojasPageModule', name: 'clienteLojas', segment: 'cliente-lojas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/area-cliente/cliente-promotores/cliente-promotores.module#ClientePromotoresPageModule', name: 'clientePromotores', segment: 'cliente-promotores', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/area-cliente/job-infos/job-infos.module#JobInfosPageModule', name: 'JobInfosPage', segment: 'job-infos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/area-cliente/jobs-cliente-promotor/jobs-cliente-promotor.module#JobsClientePromotorPageModule', name: 'JobsClientePromotorPage', segment: 'jobs-cliente-promotor', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/area-cliente/jobs/jobs.module#JobsPageModule', name: 'JobsPage', segment: 'jobs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/area-representante/metas/metas.module#MetasPageModule', name: 'metas', segment: 'metas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/area-representante/pedidos/pedidos.module#PedidosPageModule', name: 'pedidos', segment: 'pedidos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/area-visitante/lojas-visitante/lojas-visitante.module#LojasVisitantePageModule', name: 'LojasVisitante', segment: 'lojas-visitante', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/confirm/confirm.module#ConfirmPageModule', name: 'ConfirmPage', segment: 'confirm', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/coord-job/coord-job.module#CoordJobPageModule', name: 'CoordJobPage', segment: 'coord-job', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/datas-criticas-modal/critic-modal.module#CriticModalPageModule', name: 'clientePesquisa', segment: 'critic-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/datas-criticas/datas-criticas.module#DatasCriticasPageModule', name: 'criticDate', segment: 'datas-criticas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/entrada-saida-job/entrada-saida-job.module#EntradaSaidaJobPageModule', name: 'EntradaSaidaJobPage', segment: 'entrada-saida-job', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/esqueceu-senha/esqueceu-senha.module#EsqueceuSenhaPageModule', name: 'EsqueceuSenhaPage', segment: 'esqueceu-senha', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/greetings/greetings.module#GreetingsPageModule', name: 'GreetingsPage', segment: 'greetings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/init/init.module#InitPageModule', name: 'InitPage', segment: 'init', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/jobs-do-dia/jobs-do-dia.module#JobsDoDiaPageModule', name: 'jobsDoDia', segment: 'jobs-do-dia', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/loading/loading.module#LoadingPageModule', name: 'LoadingPage', segment: 'loading', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/opts/login-opts/login-opts.module#LoginOptsPageModule', name: 'LoginOptsPage', segment: 'login-opts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'login', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/lojas/lojas.module#LojasPageModule', name: 'lojas', segment: 'lojas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/main.module#MainPageModule', name: 'main', segment: 'main', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/market-share-chart/market-share-chart.module#MarketShareChartPageModule', name: 'MarketShareChartPage', segment: 'market-share-chart', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/market-share/market-share.module#MarketSharePageModule', name: 'marketShare', segment: 'market-share', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/new-job-modal/new-job-modal.module#NewJobModalPageModule', name: 'NewJobModalPage', segment: 'new-job-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pesquisa-modal/pesquisa-modal.module#PesquisaModalPageModule', name: 'PesquisaModalPage', segment: 'pesquisa-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/quiz-modal/quiz-modal.module#QuizModalPageModule', name: 'QuizModalPage', segment: 'quiz-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/update/update.module#UpdatePageModule', name: 'UpdatePage', segment: 'update', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/view-new-complete/view-new-complete.module#ViewNewCompletePageModule', name: 'ViewNewCompletePage', segment: 'view-new-complete', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/view-photo/view-photo.module#ViewPhotoPageModule', name: 'ViewPhotoPage', segment: 'view-photo', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_forms__["a" /* FormsModule */],
                // ChartsModule,
                __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_27__pages_main_main_module__["MainPageModule"],
                __WEBPACK_IMPORTED_MODULE_29__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_33_ionic_img_viewer__["b" /* IonicImageViewerModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_init_init__["a" /* InitPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_loading_loading__["a" /* LoadingPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_new_job_modal_new_job_modal__["a" /* NewJobModalPage */],
                // LoginPage,
                __WEBPACK_IMPORTED_MODULE_13__pages_view_photo_view_photo__["a" /* ViewPhotoPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_update_update__["a" /* UpdatePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_pesquisa_modal_pesquisa_modal__["a" /* PesquisaModalPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_quiz_modal_quiz_modal__["a" /* QuizModalPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_market_share_chart_market_share_chart__["a" /* MarketShareChartPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_main_main__["a" /* MainPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_view_new_complete_view_new_complete__["a" /* ViewNewCompletePage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_vibration__["a" /* Vibration */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_browser_tab__["a" /* BrowserTab */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_geolocation__["a" /* Geolocation */],
                { provide: __WEBPACK_IMPORTED_MODULE_2__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_19__providers_utils_utils__["a" /* UtilsProvider */],
                __WEBPACK_IMPORTED_MODULE_20__node_modules_ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_date_picker__["a" /* DatePicker */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_device__["a" /* Device */],
                // BackgroundMode,
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_local_notifications__["a" /* LocalNotifications */],
                __WEBPACK_IMPORTED_MODULE_28__providers_rota_rota__["a" /* RotaProvider */],
                __WEBPACK_IMPORTED_MODULE_30__providers_connection_checker_connection_checker__["a" /* ConnectionCheckerProvider */],
                __WEBPACK_IMPORTED_MODULE_34__ionic_native_keyboard__["a" /* Keyboard */],
                __WEBPACK_IMPORTED_MODULE_35__ionic_native_admob_free__["a" /* AdMobFree */],
                __WEBPACK_IMPORTED_MODULE_36__ionic_native_http__["a" /* HTTP */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 446:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 202,
	"./af.js": 202,
	"./ar": 203,
	"./ar-dz": 204,
	"./ar-dz.js": 204,
	"./ar-kw": 205,
	"./ar-kw.js": 205,
	"./ar-ly": 206,
	"./ar-ly.js": 206,
	"./ar-ma": 207,
	"./ar-ma.js": 207,
	"./ar-sa": 208,
	"./ar-sa.js": 208,
	"./ar-tn": 209,
	"./ar-tn.js": 209,
	"./ar.js": 203,
	"./az": 210,
	"./az.js": 210,
	"./be": 211,
	"./be.js": 211,
	"./bg": 212,
	"./bg.js": 212,
	"./bm": 213,
	"./bm.js": 213,
	"./bn": 214,
	"./bn.js": 214,
	"./bo": 215,
	"./bo.js": 215,
	"./br": 216,
	"./br.js": 216,
	"./bs": 217,
	"./bs.js": 217,
	"./ca": 218,
	"./ca.js": 218,
	"./cs": 219,
	"./cs.js": 219,
	"./cv": 220,
	"./cv.js": 220,
	"./cy": 221,
	"./cy.js": 221,
	"./da": 222,
	"./da.js": 222,
	"./de": 223,
	"./de-at": 224,
	"./de-at.js": 224,
	"./de-ch": 225,
	"./de-ch.js": 225,
	"./de.js": 223,
	"./dv": 226,
	"./dv.js": 226,
	"./el": 227,
	"./el.js": 227,
	"./en-SG": 228,
	"./en-SG.js": 228,
	"./en-au": 229,
	"./en-au.js": 229,
	"./en-ca": 230,
	"./en-ca.js": 230,
	"./en-gb": 231,
	"./en-gb.js": 231,
	"./en-ie": 232,
	"./en-ie.js": 232,
	"./en-il": 233,
	"./en-il.js": 233,
	"./en-nz": 234,
	"./en-nz.js": 234,
	"./eo": 235,
	"./eo.js": 235,
	"./es": 236,
	"./es-do": 237,
	"./es-do.js": 237,
	"./es-us": 238,
	"./es-us.js": 238,
	"./es.js": 236,
	"./et": 239,
	"./et.js": 239,
	"./eu": 240,
	"./eu.js": 240,
	"./fa": 241,
	"./fa.js": 241,
	"./fi": 242,
	"./fi.js": 242,
	"./fo": 243,
	"./fo.js": 243,
	"./fr": 244,
	"./fr-ca": 245,
	"./fr-ca.js": 245,
	"./fr-ch": 246,
	"./fr-ch.js": 246,
	"./fr.js": 244,
	"./fy": 247,
	"./fy.js": 247,
	"./ga": 248,
	"./ga.js": 248,
	"./gd": 249,
	"./gd.js": 249,
	"./gl": 250,
	"./gl.js": 250,
	"./gom-latn": 251,
	"./gom-latn.js": 251,
	"./gu": 252,
	"./gu.js": 252,
	"./he": 253,
	"./he.js": 253,
	"./hi": 254,
	"./hi.js": 254,
	"./hr": 255,
	"./hr.js": 255,
	"./hu": 256,
	"./hu.js": 256,
	"./hy-am": 257,
	"./hy-am.js": 257,
	"./id": 258,
	"./id.js": 258,
	"./is": 259,
	"./is.js": 259,
	"./it": 260,
	"./it-ch": 261,
	"./it-ch.js": 261,
	"./it.js": 260,
	"./ja": 262,
	"./ja.js": 262,
	"./jv": 263,
	"./jv.js": 263,
	"./ka": 264,
	"./ka.js": 264,
	"./kk": 265,
	"./kk.js": 265,
	"./km": 266,
	"./km.js": 266,
	"./kn": 267,
	"./kn.js": 267,
	"./ko": 268,
	"./ko.js": 268,
	"./ku": 269,
	"./ku.js": 269,
	"./ky": 270,
	"./ky.js": 270,
	"./lb": 271,
	"./lb.js": 271,
	"./lo": 272,
	"./lo.js": 272,
	"./lt": 273,
	"./lt.js": 273,
	"./lv": 274,
	"./lv.js": 274,
	"./me": 275,
	"./me.js": 275,
	"./mi": 276,
	"./mi.js": 276,
	"./mk": 277,
	"./mk.js": 277,
	"./ml": 278,
	"./ml.js": 278,
	"./mn": 279,
	"./mn.js": 279,
	"./mr": 280,
	"./mr.js": 280,
	"./ms": 281,
	"./ms-my": 282,
	"./ms-my.js": 282,
	"./ms.js": 281,
	"./mt": 283,
	"./mt.js": 283,
	"./my": 284,
	"./my.js": 284,
	"./nb": 285,
	"./nb.js": 285,
	"./ne": 286,
	"./ne.js": 286,
	"./nl": 287,
	"./nl-be": 288,
	"./nl-be.js": 288,
	"./nl.js": 287,
	"./nn": 289,
	"./nn.js": 289,
	"./pa-in": 290,
	"./pa-in.js": 290,
	"./pl": 291,
	"./pl.js": 291,
	"./pt": 292,
	"./pt-br": 293,
	"./pt-br.js": 293,
	"./pt.js": 292,
	"./ro": 294,
	"./ro.js": 294,
	"./ru": 295,
	"./ru.js": 295,
	"./sd": 296,
	"./sd.js": 296,
	"./se": 297,
	"./se.js": 297,
	"./si": 298,
	"./si.js": 298,
	"./sk": 299,
	"./sk.js": 299,
	"./sl": 300,
	"./sl.js": 300,
	"./sq": 301,
	"./sq.js": 301,
	"./sr": 302,
	"./sr-cyrl": 303,
	"./sr-cyrl.js": 303,
	"./sr.js": 302,
	"./ss": 304,
	"./ss.js": 304,
	"./sv": 305,
	"./sv.js": 305,
	"./sw": 306,
	"./sw.js": 306,
	"./ta": 307,
	"./ta.js": 307,
	"./te": 308,
	"./te.js": 308,
	"./tet": 309,
	"./tet.js": 309,
	"./tg": 310,
	"./tg.js": 310,
	"./th": 311,
	"./th.js": 311,
	"./tl-ph": 312,
	"./tl-ph.js": 312,
	"./tlh": 313,
	"./tlh.js": 313,
	"./tr": 314,
	"./tr.js": 314,
	"./tzl": 315,
	"./tzl.js": 315,
	"./tzm": 316,
	"./tzm-latn": 317,
	"./tzm-latn.js": 317,
	"./tzm.js": 316,
	"./ug-cn": 318,
	"./ug-cn.js": 318,
	"./uk": 319,
	"./uk.js": 319,
	"./ur": 320,
	"./ur.js": 320,
	"./uz": 321,
	"./uz-latn": 322,
	"./uz-latn.js": 322,
	"./uz.js": 321,
	"./vi": 323,
	"./vi.js": 323,
	"./x-pseudo": 324,
	"./x-pseudo.js": 324,
	"./yo": 325,
	"./yo.js": 325,
	"./zh-cn": 326,
	"./zh-cn.js": 326,
	"./zh-hk": 327,
	"./zh-hk.js": 327,
	"./zh-tw": 328,
	"./zh-tw.js": 328
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 446;

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnectionCheckerProvider; });
/* unused harmony export Connection */
/* unused harmony export RequestResultPattern */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the ConnectionCheckerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ConnectionCheckerProvider = /** @class */ (function () {
    function ConnectionCheckerProvider(network) {
        var _this = this;
        this.network = network;
        this.connection = new Connection();
        this.mStatus = '';
        network.onConnect().subscribe(function () {
            _this.connection.status = 'connected';
            if (_this.network.type === 'wifi') {
                _this.connection.type = 'wifi';
            }
            _this.setMStatus('online');
        });
        network.onDisconnect().subscribe(function () {
            _this.connection.status = 'disconnected';
            _this.setMStatus('offline');
        });
    }
    ConnectionCheckerProvider.prototype.setMStatus = function (s) {
        this.mStatus = s;
    };
    ConnectionCheckerProvider.prototype.getMStatus = function () {
        return this.mStatus;
    };
    ConnectionCheckerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], ConnectionCheckerProvider);
    return ConnectionCheckerProvider;
}());

var Connection = /** @class */ (function () {
    function Connection() {
        this._status = 'disconnected';
        this._type = '3G';
    }
    Object.defineProperty(Connection.prototype, "status", {
        get: function () {
            return this._status;
        },
        set: function (value) {
            this._status = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Connection.prototype, "type", {
        get: function () {
            return this._type;
        },
        set: function (value) {
            this._type = value;
        },
        enumerable: true,
        configurable: true
    });
    return Connection;
}());

var RequestResultPattern = /** @class */ (function () {
    function RequestResultPattern() {
        this._status = '';
        this._result = [];
    }
    Object.defineProperty(RequestResultPattern.prototype, "status", {
        get: function () {
            return this._status;
        },
        set: function (value) {
            this._status = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestResultPattern.prototype, "result", {
        get: function () {
            return this._result;
        },
        set: function (value) {
            this._result = value;
        },
        enumerable: true,
        configurable: true
    });
    return RequestResultPattern;
}());

//# sourceMappingURL=connection-checker.js.map

/***/ }),

/***/ 573:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_init_init__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_network__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__node_modules_ionic_native_local_notifications__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_connection_checker_connection_checker__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MyApp = /** @class */ (function () {
    function MyApp(_checker, _localNotifications, _alert, platform, _network, statusBar, splashScreen, _http) {
        this._checker = _checker;
        this._localNotifications = _localNotifications;
        this._alert = _alert;
        this.platform = platform;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_init_init__["a" /* InitPage */];
        this.loginInterval = null;
        this.myIds = [];
        this.myMessages = [];
        splashScreen.hide();
        _http.setDataSerializer('json');
        platform.ready().then(function () {
            statusBar.styleDefault();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__providers_connection_checker_connection_checker__["a" /* ConnectionCheckerProvider */],
            __WEBPACK_IMPORTED_MODULE_6__node_modules_ionic_native_local_notifications__["a" /* LocalNotifications */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_http__["a" /* HTTP */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoadingPage = /** @class */ (function () {
    function LoadingPage(platform, navCtrl, navParams) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LoadingPage.prototype.ngOnInit = function () {
        var _this = this;
        if (this.navParams.get('page')) {
            setTimeout(function () {
                // this.navCtrl.pop();
                setTimeout(function () {
                    _this.navCtrl.setRoot(_this.navParams.get('page'));
                }, 100);
                // window.location.href = '/#/'+this.navParams.get('page');
            }, 1500);
        }
    };
    LoadingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-loading',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\loading\loading.html"*/'<div style="background: #f5f7fa; height: calc(100vh - 10px)">  \n\n  <img src="./assets/images/logoecco.png" style="position: relative; left: 15px; display: table; margin: auto; width: 200px; top: 5%">\n\n  <img src="./assets/images/loading.gif" style="display: table; margin: 25% auto; width: 64px;"/>\n\n  <span style="position: relative; margin: auto; display: table" class="char">Carregando...</span>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\loading\loading.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], LoadingPage);
    return LoadingPage;
}());

//# sourceMappingURL=loading.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewPhotoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_img_viewer__ = __webpack_require__(336);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ViewPhotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewPhotoPage = /** @class */ (function () {
    function ViewPhotoPage(platform, _alert, _http, _image, navCtrl, navParams, viewCtrl) {
        this.platform = platform;
        this._alert = _alert;
        this._http = _http;
        this._image = _image;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.errorOccured = false;
        this.fotos = [];
        this.comentarios = [];
    }
    ViewPhotoPage.prototype.ngOnInit = function () {
        this.pageParams = this.viewCtrl.getNavParams().data;
        this.fotos = this.viewCtrl.getNavParams().get('fotos');
        this.getComentarios();
    };
    ViewPhotoPage.prototype.getComentarios = function () {
        var _this = this;
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectComentarios.php', {
            new_id: this.pageParams.id
        }, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            if (response.data.status == '0x104') {
                _this.comentarios = response.data.result;
            }
        });
    };
    ViewPhotoPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ViewPhotoPage.prototype.callError = function (e, imgElem) {
        this.errorOccured = true;
        imgElem.style.display = 'none';
    };
    ViewPhotoPage.prototype.presentImage = function (image) {
        this._image.create(image).present();
    };
    ViewPhotoPage.prototype.abrirOptsComentarios = function () {
        var _this = this;
        var alert = this._alert.create();
        alert.setTitle('Escolha um comentario:');
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectComentariosDefinidos.php', {
            all: true
        }, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            if (response.data.status == '0x104') {
                response.data.result.forEach(function (e) {
                    alert.addInput({
                        type: 'radio',
                        label: e.comdef_title,
                        value: e.comdef_title
                    });
                });
                alert.addButton('Fechar');
                alert.addButton({
                    text: 'Comentar!',
                    handler: function (data) {
                        var o = {
                            new_id: _this.pageParams.id,
                            comentario_ref_nome: '',
                            comentario_desc: data
                        };
                        if (JSON.parse(localStorage.getItem('session')).visitante != null) {
                            o.comentario_ref_nome = JSON.parse(localStorage.getItem('session')).visitante.Usr_Nome;
                        }
                        else if (JSON.parse(localStorage.getItem('session'))[0] !== undefined &&
                            JSON.parse(localStorage.getItem('session'))[0].PRO_ID !== undefined) {
                            o.comentario_ref_nome = JSON.parse(localStorage.getItem('session'))[0].content[0].PRO_Nome;
                        }
                        else if (JSON.parse(localStorage.getItem('session')).user != null) {
                            o.comentario_ref_nome = JSON.parse(localStorage.getItem('session')).user.Usr_Nome;
                        }
                        _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertComentario.php', o, {}).then(function (response) {
                            response.data = JSON.parse(response.data);
                            if (response.data.status == '0x104') {
                                _this.getComentarios();
                            }
                        });
                    }
                });
                alert.present();
            }
        });
    };
    ViewPhotoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-view-photo',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\view-photo\view-photo.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #f5f7fa; width: 100%;"></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" style="overflow-Y: auto">\n\n  <main class="mdl-layout__content mdl-color--white"\n\n    style="height: calc(100vh - 56px); background-color: #f5f7fa !important; ">\n\n    <button (click)="dismiss()" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n      <i class="material-icons" style="color: red;">arrow_back</i>\n\n    </button>\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <div class="mdl-card" style="width: 100%; background-color: transparent">\n\n\n\n        <div class="mdl-card__media" style="margin-top: 55px; background-color: transparent">\n\n          <span style="display: table; margin: 5px auto; text-align: center"\n\n            *ngIf="pageParams.JOB_Title !== undefined">{{pageParams.JOB_Title}}</span>\n\n          <span style="font-weight: bold; display: table; margin: 5px auto; text-align: center"\n\n            *ngIf="pageParams.JOB_Obs !== undefined">{{pageParams.JOB_Obs}}</span>\n\n\n\n          <img *ngIf="!errorOccured && pageParams.JOB_Foto !== undefined" (error)="callError($event)"\n\n            (click)="presentImage(image)" #image [src]="\'https://kstrade.com.br/sistema/assets/kstrade_php/uploads/\' + pageParams.JOB_Foto"\n\n            style="max-width: 95vw; display: table; margin: auto;">\n\n\n\n          <ion-slides zoom="true" style="margin: auto; display: table; height: 50%"\n\n            *ngIf="fotos !== undefined && fotos.length > 0">\n\n            <ion-slide *ngFor="let p of fotos; let i = index" style="position: relative;">\n\n\n\n              <span *ngIf="fotos.length > 1"\n\n                style="border-radius: 80px; width: 40px; background: rgba(0,177,0,.2); padding: 10px; position: absolute; top: 15px; right: 15px; font-weight: bold">{{i + 1}}</span>\n\n\n\n              <div class="swiper-zoom-container">\n\n                <img (click)="presentImage(image)" #image (error)="callError($event, image)"\n\n                  style="width: 95%; margin: auto; display: table;" [src]="\'https://kstrade.com.br/sistema/assets/kstrade_php/uploads/\'+p" />\n\n\n\n                <div *ngIf="errorOccured"\n\n                  style="border-radius: 4px; background-color: #f5f7fa; text-align: center; display: table; margin: 15% auto; width: 80%">\n\n                  <i class="material-icons"\n\n                    style="font-size: 56px; display: table; margin: auto; color: #000">warning</i>\n\n                  <span style="color: #000">Ocorreu um erro ao carregar imagem, por favor tente novamente mais\n\n                    tarde!</span>\n\n                </div>\n\n              </div>\n\n            </ion-slide>\n\n          </ion-slides>\n\n\n\n          <span style="color: #000" style="display: table; margin: auto; font-size: 12px; margin-top: 20px">\n\n            Comente e demonstre o que achou desta notícia!\n\n          </span>\n\n\n\n          <div\n\n            style="overflow: auto; width: 100%; display: table; margin: 10px auto; border-top: 1px solid #dcdcdc; max-height: 150px;">\n\n            <span style="display: table; margin: auto; font-weight: bold">Comentarios</span>\n\n\n\n            <div *ngIf="comentarios.length < 1"\n\n              style="border-radius: 4px; background-color:  #f5f7fa; text-align: center; display: table; margin: 15% auto; width: 80%">\n\n              <i class="material-icons" style="font-size: 36px; display: table; margin: auto; color: #000">mood_bad</i>\n\n              <span style="color: #000">Nenhum comentário ainda</span>\n\n            </div>\n\n\n\n            <table *ngIf="comentarios.length > 0" class="ion-diagnostic-table"\n\n              style="font-size: 13px; margin-top: 20px; margin-bottom: 20px; width: 100%;">\n\n              <thead>\n\n                <tr>\n\n                  <td style="width: 42px"></td>\n\n                  <td style="width: 120px"></td>\n\n                  <td></td>\n\n                  <td></td>\n\n                </tr>\n\n              </thead>\n\n              <tbody>\n\n                <tr style="position: relative; cursor: pointer;" *ngFor="let c of comentarios; let i = index">\n\n                  <td>\n\n                    <div\n\n                      style="width: 32px; height: 32px; background-color: #940000; border-radius: 250px; position: relative;">\n\n                      <span\n\n                        style="font-weight: bold; color: #f5f7fa; display: table; margin: auto; position: absolute; top: 25%; left: 40%;">{{c.comentario_ref_nome.split(\'\')[0]}}</span>\n\n                    </div>\n\n                  </td>\n\n                  <td>{{c.comentario_ref_nome}}</td>\n\n                  <td>{{c.comentario_desc}}</td>\n\n                  <td style="position: relative">\n\n                    <span style="position: absolute; font-size: 10px;right: 5px;top: 10px;">\n\n                      {{c.comentario_data}}\n\n                    </span>\n\n                  </td>\n\n                </tr>\n\n              </tbody>\n\n            </table>\n\n          </div>\n\n\n\n          <div style="width: 100%; margin: 5px auto; position: relative; bottom: 35px">\n\n            <a (click)="abrirOptsComentarios()"\n\n              class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect mdl-color-text--red-700"\n\n              style="width: 100%; display: table; margin: auto">\n\n              Comentar\n\n            </a>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\view-photo\view-photo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_img_viewer__["a" /* ImageViewerController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */]])
    ], ViewPhotoPage);
    return ViewPhotoPage;
}());

//# sourceMappingURL=view-photo.js.map

/***/ })

},[408]);
//# sourceMappingURL=main.js.map
webpackJsonp([2],{

/***/ 582:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobsPageModule", function() { return JobsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__jobs__ = __webpack_require__(609);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var JobsPageModule = /** @class */ (function () {
    function JobsPageModule() {
    }
    JobsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__jobs__["a" /* JobsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__jobs__["a" /* JobsPage */]),
            ],
        })
    ], JobsPageModule);
    return JobsPageModule;
}());

//# sourceMappingURL=jobs.module.js.map

/***/ }),

/***/ 607:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobInfosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JobInfosPage = /** @class */ (function () {
    function JobInfosPage(platform, _datePicker, navCtrl, navParams, _http) {
        this.platform = platform;
        this._datePicker = _datePicker;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        // map: GoogleMap;
        this.photos = [];
    }
    JobInfosPage.prototype.ngOnInit = function () {
        var _this = this;
        if (this.navParams.get('type') == 'photo') {
            //console.log(this.navParams.get('obs'));
            this.photos.push(this.navParams.get('job_foto'));
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectFotosExtras.php', {
                job_id: this.navParams.get('job_id')
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    response.data.result.forEach(function (element) {
                        _this.photos.push(element.JFOT_Foto);
                    });
                }
            });
        }
        else if (this.navParams.get('type') == 'local') {
            setTimeout(function () {
                _this.loadMap(_this.navParams.get('lat'), _this.navParams.get('long'));
            }, 500);
        }
    };
    JobInfosPage.prototype.loadMap = function (lat, lon) {
        if (lat != 'nada' && lon != 'nada') {
            var myLatlng = new window.google.maps.LatLng(lat, lon);
            var map = new window.google.maps.Map(document.getElementById('map_canvas'), {
                center: myLatlng,
                zoom: 17
            });
            var marker = new window.google.maps.Marker({
                position: myLatlng,
                map: map
            });
        }
    };
    JobInfosPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    JobInfosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-job-infos',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\job-infos\job-infos.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #f5f7fa; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">\n\n  <!-- <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n      <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n        <i class="material-icons">arrow_back</i>\n\n      </button>\n\n      <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n      <div class="mdl-layout-spacer"></div>\n\n    </header> -->\n\n  <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: rgba(0,0,0,0.4);" (click)="this.navParams.get(\'type\') != \'local\' ? this.navCtrl.pop() : return;">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <div *ngIf="this.navParams.get(\'type\') == \'photo\'">\n\n        <div style="background: #f5f7fa; border-radius: 2px;width: 95%; display: table; margin: 10% auto">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 42px; color: #000;">info_outline</i>\n\n          <div style="width: 100%; height: 1px; border-top: 1px solid #000; display: table; margin: 5px auto;"></div>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Observações</span>\n\n          <span style="display: table; margin: auto; font-size: 16px; padding: 15px;">{{this.navParams.get(\'obs\')}}</span>\n\n        </div>\n\n        <ion-slides zoom="true" style="margin: auto; display: table; margin-top: 5%">\n\n          <ion-slide *ngFor="let p of photos">\n\n            <div class="swiper-zoom-container">\n\n              <img style="width: 95%; margin: auto; display: table;" [src]="\'https://kstrade.com.br/sistema/assets/kstrade_php/uploads/\'+p" />\n\n            </div>\n\n          </ion-slide>\n\n        </ion-slides>\n\n        <div style="display: table; margin: auto; width: 100%; border-bottom: 1px solid #dcdcdc">\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Arraste para os\n\n            lados para visualizar as demais fotos</span>\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Toque fora da\n\n            foto para fechar</span>\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngIf="this.navParams.get(\'type\') == \'local\'">\n\n        <div style="position: absolute; top: 0; width: 100%; height: 100%; z-index: 5" (click)="this.navCtrl.pop()">\n\n        </div>\n\n        <div id="map_canvas" style="margin: 15% auto; width: 300px; height: 360px;" *ngIf="this.navParams.get(\'lat\') != \'nada\' && this.navParams.get(\'lon\') != \'nada\'"></div>\n\n        <div style="background: #f5f7fa; border-radius: 2px;width: 95%; display: table; margin: 20% auto" *ngIf="this.navParams.get(\'lat\') == \'nada\' || this.navParams.get(\'lon\') == \'nada\'">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 42px; color: #000;">location_off</i>\n\n          <div style="width: 80%; height: 1px; border-top: 1px solid #000; display: table; margin: 5px auto;"></div>\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #000; font-weight: bold">Este promotor não\n\n            divulgou sua localização</span>\n\n        </div>\n\n        <div style="display: table; margin: 10px auto; width: 100%; border-bottom: 1px solid #dcdcdc">\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Toque fora do\n\n            mapa para fechar</span>\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngIf="this.navParams.get(\'type\') == \'infos\'">\n\n        <div style="background: #f5f7fa; border-radius: 2px;width: 95%; display: table; margin: 20% auto">\n\n          <i class="material-icons" style="display: table; margin: auto; font-size: 42px; color: #000;">info_outline</i>\n\n          <div style="width: 100%; height: 1px; border-top: 1px solid #000; display: table; margin: 5px auto;"></div>\n\n          <span style="display: table; margin: auto; margin-top: 30px; margin-bottom: 10px; font-size: 18px; font-weight: bold;">Informações\n\n            adquiridas do aparelho:</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 20px; font-size: 12px;">Para evitarmos fraudes,\n\n            adquirimos informações do aparelho.</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Modelo do\n\n            aparelho</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 15px; font-size: 16px;">{{this.navParams.get(\'modelo\')}}</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Sistema\n\n            operacional e versão</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 15px; font-size: 18px;">{{this.navParams.get(\'plataforma\')}}\n\n            - {{this.navParams.get(\'versao\')}}</span>\n\n          <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Fabricante</span>\n\n          <span style="display: table; margin: auto; font-size: 18px;">{{this.navParams.get(\'manufact\')}}</span>\n\n        </div>\n\n        <div style="display: table; margin: auto; width: 100%; border-bottom: 1px solid #dcdcdc">\n\n          <span style="display: table; margin: auto; font-size: 12px; color: #f5f7fa; font-weight: bold">Toque fora da\n\n            foto para fechar</span>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\job-infos\job-infos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__["a" /* DatePicker */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__["a" /* HTTP */]])
    ], JobInfosPage);
    return JobInfosPage;
}());

//# sourceMappingURL=job-infos.js.map

/***/ }),

/***/ 608:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoordJobPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_view_controller__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CoordJobPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CoordJobPage = /** @class */ (function () {
    function CoordJobPage(_geolocation, _alert, _viewCtrl, _http, navCtrl, navParams) {
        this._geolocation = _geolocation;
        this._alert = _alert;
        this._viewCtrl = _viewCtrl;
        this._http = _http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.obs = '';
    }
    CoordJobPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CoordJobPage');
    };
    CoordJobPage.prototype.ngOnInit = function () {
        var _this = this;
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        this._geolocation.getCurrentPosition().then(function (data) {
            _this.myLat = data.coords.latitude;
            _this.myLong = data.coords.longitude;
        }).catch(function (e) {
            //console.log(e);
            var a = _this._alert.create({
                title: 'Ative a localização',
                subTitle: 'Você deve ativar a localização!',
                buttons: ['Ok']
            });
            a.present();
            _this._viewCtrl.dismiss({});
        });
    };
    CoordJobPage.prototype.dismiss = function () {
        this._viewCtrl.dismiss();
    };
    CoordJobPage.prototype.doneJob = function () {
        var _this = this;
        var data = new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0, 10);
        var hora = (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes());
        this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertJobCoord.php', {
            usr_id: JSON.parse(localStorage.getItem('session')).user.Usr_ID,
            rede: this.navParams.get('rede_id'),
            filial: this.navParams.get('rfil_id'),
            status: 'Feedback',
            data: data,
            hora: hora,
            obs: this.obs,
            lat: this.myLat,
            lon: this.myLong
        }, {}).then(function (response) {
            response.data = JSON.parse(response.data);
            if (response.data.status == '0x104') {
                _this._alert.create({
                    subTitle: 'Feedback eviado com sucesso'
                }).present();
                _this._viewCtrl.dismiss();
            }
        });
    };
    CoordJobPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-coord-job',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\coord-job\coord-job.html"*/'<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n    <div class="mdl-layout-spacer"></div>\n  </header>\n  <main class="mdl-layout__content mdl-color--white" style="height: calc(100vh - 56px); background: #f5f7fa">\n    <div class="page-content" style="background: #f5f7fa">\n      <div class="mdl-card" style="width: 100vw;">\n        <div class="mdl-card__title">\n        </div>\n        <div class="mdl-card__media mdl-color--white">\n          <span style="display: table; margin: auto">{{this.navParams.get(\'rede_nome\')}}</span>\n          <span style="display: table; margin: auto">{{this.navParams.get(\'rfil_nome\')}}</span>\n          <small style="text-align: center; display: table; margin: auto;">Deixe seu feedback para a equipe, o mesmo é muito importante para novos horizontes.</small>\n\n          <div style="width: 90%; margin: 30px auto; display: table;">\n            <span style="display: table; margin: auto;">Observações: </span>\n            <textarea id="txtJob" class="simpleInput" type="text" [(ngModel)]="this.obs" rows="5" cols="30"></textarea>\n          </div>\n        </div>\n      </div>\n    </div>\n  </main>\n  <div style="z-index: 5; position: relative; background: #8c1515; width: 100vw;">\n    <div style="display: table; margin: auto;">\n      <button (click)="dismiss()" class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-color--white">\n        <i class="material-icons">arrow_back</i>\n      </button>\n\n      <button (click)="doneJob()" class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-color--white">\n        <i class="material-icons">done</i>\n      </button>\n    </div>\n  </div>\n</div>\n'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\coord-job\coord-job.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_view_controller__["a" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], CoordJobPage);
    return CoordJobPage;
}());

//# sourceMappingURL=coord-job.js.map

/***/ }),

/***/ 609:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__job_infos_job_infos__ = __webpack_require__(607);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_date_picker__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__coord_job_coord_job__ = __webpack_require__(608);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the JobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var JobsPage = /** @class */ (function () {
    function JobsPage(platform, _alert, _datePicker, navCtrl, navParams, _http, _modal) {
        this.platform = platform;
        this._alert = _alert;
        this._datePicker = _datePicker;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this._modal = _modal;
        this.jobs = [];
        this.data_inicio = {
            value: '2018-02-01',
            showValue: 'Data inicial',
            selected: false
        };
        this.data_fim = {
            showValue: 'Data final',
            value: '2018-10-01',
            selected: false
        };
    }
    JobsPage.prototype.ngOnInit = function () {
        var _this = this;
        var type = typeof this.navParams.get('cli_id');
        this.userData = JSON.parse(localStorage.getItem('session'));
        console.log(this.userData);
        if (type === 'object') {
            //console.log('is obj', this.navParams.get('cli_id'));
            this.navParams.get('cli_id').forEach(function (element) {
                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/cliente/selectJobs.php', {
                    id: JSON.parse(localStorage.getItem('session')).clientes !== undefined ?
                        element.aux_cliente_id :
                        element.Cli_ID,
                    rede_id: _this.navParams.get('red_id'),
                    rfil_id: _this.navParams.get('rfil_id'),
                    dataone: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 7).toISOString().substring(0, 10),
                    datatwo: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().substring(0, 10)
                }, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        response.data.result.forEach(function (e) {
                            _this.jobs.push(e);
                        });
                    }
                });
            });
        }
        else {
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/cliente/selectJobs.php', {
                id: this.navParams.get('cli_id'),
                rede_id: this.navParams.get('red_id'),
                rfil_id: this.navParams.get('rfil_id'),
                dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
                datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString().substring(0, 10)
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    response.data.result.forEach(function (e) {
                        _this.jobs.push(e);
                    });
                }
            });
        }
    };
    JobsPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    JobsPage.prototype.openInfos = function (type, j) {
        switch (type) {
            case 'photo':
                {
                    //console.log('job', j);
                    var a = this._modal.create(__WEBPACK_IMPORTED_MODULE_2__job_infos_job_infos__["a" /* JobInfosPage */], {
                        type: 'photo',
                        job_id: j.JOB_ID,
                        job_foto: j.JOB_Foto,
                        obs: j.JOB_Obs
                    });
                    a.present();
                }
                break;
            case 'local':
                {
                    var a = this._modal.create(__WEBPACK_IMPORTED_MODULE_2__job_infos_job_infos__["a" /* JobInfosPage */], {
                        type: 'local',
                        long: j.JOB_Longitude !== null ? j.JOB_Longitude : 'nada',
                        lat: j.JOB_Latitude !== null ? j.JOB_Latitude : 'nada'
                    });
                    a.present();
                }
                break;
            case 'infos':
                {
                    var a = this._modal.create(__WEBPACK_IMPORTED_MODULE_2__job_infos_job_infos__["a" /* JobInfosPage */], {
                        type: 'infos',
                        modelo: j.JOB_PRO_Modelo,
                        plataforma: j.JOB_PRO_Plataforma,
                        versao: j.JOB_PRO_Versao,
                        manufact: j.JOB_PRO_Manufact,
                        obs: j.JOB_Obs
                    });
                    a.present();
                }
                break;
        }
    };
    JobsPage.prototype.showDate = function (obj) {
        this._datePicker.show({
            allowFutureDates: true,
            date: new Date(),
            mode: 'date',
            androidTheme: this._datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
        }).then(function (date) {
            obj.selected = true;
            obj.value = date.toISOString().split('T')[0];
            obj.showValue = date.toISOString().split('T')[0].split('-')[2] + '/' +
                date.toISOString().split('T')[0].split('-')[1] + '/' +
                date.toISOString().split('T')[0].split('-')[0];
        }, function (err) {
            obj.selected = false;
        });
    };
    JobsPage.prototype.searchJobs = function (init, end) {
        var _this = this;
        this.jobs = [];
        if (init.length < 1 || end.length < 1) {
            var a = this._alert.create({
                title: 'Selecione uma data!',
                subTitle: 'Para utilizar o filtro, deve-se escolher a Data inicial e Data final'
            });
            a.present();
        }
        else {
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/cliente/selectJobs.php', {
                id: this.navParams.get('cli_id'),
                rede_id: this.navParams.get('red_id'),
                rfil_id: this.navParams.get('rfil_id'),
                dataone: init,
                datatwo: end
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    response.data.result.forEach(function (e) {
                        _this.jobs.push(e);
                    });
                }
            });
        }
    };
    JobsPage.prototype.insertFeedback = function () {
        this._modal.create(__WEBPACK_IMPORTED_MODULE_5__coord_job_coord_job__["a" /* CoordJobPage */], {
            Usr_ID: this.userData.user.Usr_ID,
            rede_id: this.navParams.get('red_id'),
            rfil_id: this.navParams.get('rfil_id'),
            rede_nome: this.navParams.get('red_nome'),
            rfil_nome: this.navParams.get('rfil_nome')
        }).present();
    };
    JobsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-jobs',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\jobs\jobs.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;"></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;"\n\n      class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n      <i class="material-icons">arrow_back</i>\n\n    </button>\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: #f5f7fa;">\n\n    <div class="page-content">\n\n      <!-- <div style="display: table; margin: auto; width: 100%;">\n\n          <span style="display: table; margin: 4px auto">Filtrar</span>\n\n          <ion-item style="width: 90%; display: table; margin: auto">\n\n            <button class="mdl-button mdl-js-button mdl-js-ripple-effect" (click)="showDate(data_inicio)" style="width: 120px; text-transform: none;">\n\n              <span>{{data_inicio.showValue}}</span>\n\n            </button>\n\n            <div style="display: inline-table; width: 35px; height: 30px; background: #8c1515; color: #f5f7fa; border-radius: 2px;" >\n\n              Data\n\n            </div>\n\n            <button class="mdl-button mdl-js-button mdl-js-ripple-effect" (click)="showDate(data_fim)" style="width: 120px; text-transform: none;">\n\n              <span>{{data_fim.showValue}}</span>\n\n            </button>\n\n          </ion-item>\n\n          <button class="mdl-button mdl-js-button mdl-color--red-500 mdl-color-text--white" (click)="searchJobs(data_inicio.value, data_fim.value)" style="margin: auto;display: table; width: 100%;">\n\n            <span>Filtrar</span>\n\n          </button>\n\n        </div> -->\n\n\n\n      <div style="width: 100%; margin: 10px auto; display: table;">\n\n        <span style="display: table; margin: auto; color: #8c1515">{{this.navParams.get(\'red_nome\')}}</span>\n\n        <span style="font-size: 12px; margin: auto; display: table;">{{this.navParams.get(\'rfil_nome\')}}</span>\n\n\n\n        <button (click)="insertFeedback()" *ngIf="this.userData.user && this.userData.user.Usr_Nivel == \'Externo\'" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-color-text--white"\n\n          style="background: #8c1515; z-index: 999; margin: 10px; width: 85%; margin: auto; display: table;">\n\n          <span>Enviar feedback</span>\n\n        </button>\n\n      </div>\n\n      <div *ngIf="jobs.length < 1" style="width: 100%; display: table; margin: 20px auto">\n\n        <i class="material-icons" style="display: table; margin: auto; font-size: 42px;">mood_bad</i>\n\n        <div style="width: 80%; height: 1px; border-top: 1px solid #dcdcdc; display: table; margin: 5px auto;"></div>\n\n        <span style="display: table; margin: auto; font-size: 12px;">Nenhum job registrado</span>\n\n      </div>\n\n      <table *ngIf="jobs.length > 0" class="ion-diagnostic-table"\n\n        style="font-size: 13px; margin-bottom: 20px; width: 100%; border-top: 1px solid #dcdcdc;">\n\n        <thead>\n\n          <tr>\n\n            <td>Cliente</td>\n\n            <td>Promotor</td>\n\n            <td>Data</td>\n\n            <td>Hora</td>\n\n            <td>Foto</td>\n\n            <!-- <td >Local.</td>-->\n\n            <td>Infos.</td>\n\n          </tr>\n\n        </thead>\n\n        <tbody>\n\n          <tr style="cursor: pointer" *ngFor="let j of jobs; let i = index">\n\n            <td>{{j.Cli_Nome}}</td>\n\n            <td>{{j.PRO_Nome}}</td>\n\n            <td>{{j.JOB_Data_Hora_Input2}}</td>\n\n            <td>{{j.JOB_Hora_Job}}</td>\n\n            <td>\n\n              <button (click)="openInfos(\'photo\', j)"\n\n                class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n                <i class="material-icons" style="color: #8c1515">photo_library</i>\n\n              </button>\n\n            </td>\n\n            <!-- <td>\n\n                <button (click)="openInfos(\'local\', j)" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n                  <i class="material-icons" style="color: #8c1515">location_on</i>\n\n                </button>\n\n              </td>\n\n              <td> -->\n\n            <td>\n\n              <button (click)="openInfos(\'infos\', j)"\n\n                class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n                <i class="material-icons" style="color: #8c1515">info_outline</i>\n\n              </button>\n\n            </td>\n\n          </tr>\n\n        </tbody>\n\n      </table>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\area-cliente\jobs\jobs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_date_picker__["a" /* DatePicker */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */]])
    ], JobsPage);
    return JobsPage;
}());

//# sourceMappingURL=jobs.js.map

/***/ })

});
//# sourceMappingURL=2.js.map
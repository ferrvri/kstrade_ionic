webpackJsonp([4],{

/***/ 598:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LojasPageModule", function() { return LojasPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lojas__ = __webpack_require__(622);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LojasPageModule = /** @class */ (function () {
    function LojasPageModule() {
    }
    LojasPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__lojas__["a" /* LojasPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__lojas__["a" /* LojasPage */]),
            ],
        })
    ], LojasPageModule);
    return LojasPageModule;
}());

//# sourceMappingURL=lojas.module.js.map

/***/ }),

/***/ 613:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntradaSaidaJobPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_sweetalert2__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_vibration__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_rota_rota__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the EntradaSaidaJobPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EntradaSaidaJobPage = /** @class */ (function () {
    function EntradaSaidaJobPage(platform, _rota, viewCtrl, loadingCtrl, _vibration, _alert, _geolocation, navCtrl, navParams, _http) {
        this.platform = platform;
        this._rota = _rota;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        this._vibration = _vibration;
        this._alert = _alert;
        this._geolocation = _geolocation;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this.mylat = 0;
        this.mylong = 0;
        this.makeIt = false;
    }
    EntradaSaidaJobPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad EntradaSaidaJobPage');
    };
    EntradaSaidaJobPage.prototype.ngOnInit = function () {
        var _this = this;
        this._geolocation.getCurrentPosition().then(function (data) {
            _this.mylat = data.coords.latitude;
            _this.mylong = data.coords.longitude;
        }).catch(function (e) {
            //console.log(e);
            var a = _this._alert.create({
                title: 'Ative a localização',
                subTitle: 'Você deve ativar a localização!',
                buttons: ['Ok']
            });
            a.present();
        });
    };
    EntradaSaidaJobPage.prototype.checkIn = function (kind) {
        var _this = this;
        this.makeIt = true;
        switch (kind) {
            case 'entrada': {
                var s_1 = setTimeout(function () {
                    loading_1.dismiss();
                    __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                        title: 'Conexão ruim!',
                        text: 'A resposta demorou muito a chegar!',
                        type: 'error',
                        showCloseButton: true,
                        confirmButtonText: 'OK'
                    });
                    _this._vibration.vibrate(250);
                    _this._vibration.vibrate(250);
                }, 30000);
                var loading_1 = this.loadingCtrl.create({
                    content: 'Enviando...'
                });
                loading_1.present();
                this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertEntrada.php', {
                    pro_id: this.navParams.get('content').PRO_ID,
                    rede_id: this.navParams.get('content').RED_ID,
                    rfil_id: this.navParams.get('content').RFIL_ID,
                    data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                    hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                    lat_entrada: this.mylat,
                    long_entrada: this.mylong
                }, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        clearTimeout(s_1);
                        loading_1.dismiss();
                        __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                            title: 'Entrada registrada!',
                            text: 'Sua entrada foi registrada com sucesso!',
                            type: 'success',
                            showCloseButton: true,
                            confirmButtonText: 'OK'
                        });
                        _this._vibration.vibrate(250);
                        _this._rota.disableRota({
                            pro_id: _this.navParams.get('content').PRO_ID,
                            rede_id: _this.navParams.get('content').RED_ID,
                            rfil_id: _this.navParams.get('content').RFIL_ID,
                            data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                            hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                            lat_entrada: _this.mylat,
                            long_entrada: _this.mylong
                        });
                        _this.viewCtrl.dismiss({ done: true });
                        _this.makeIt = false;
                    }
                }, function (err) {
                    clearTimeout(s_1);
                    loading_1.dismiss();
                    __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                        title: 'Entrada salva!',
                        text: 'Sua entrada foi salva com sucesso!',
                        type: 'success',
                        showCloseButton: true,
                        confirmButtonText: 'OK'
                    });
                    if (!localStorage.getItem('temp_entradas')) {
                        var temp = [];
                        temp.push({
                            pro_id: _this.navParams.get('content').PRO_ID,
                            rede_id: _this.navParams.get('content').RED_ID,
                            rfil_id: _this.navParams.get('content').RFIL_ID,
                            data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                            hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                            lat_entrada: _this.mylat,
                            long_entrada: _this.mylong
                        });
                        localStorage.setItem('temp_entradas', JSON.stringify(temp));
                    }
                    else {
                        var temp = JSON.parse(localStorage.getItem('temp_entradas'));
                        temp.push({
                            pro_id: _this.navParams.get('content').PRO_ID,
                            rede_id: _this.navParams.get('content').RED_ID,
                            rfil_id: _this.navParams.get('content').RFIL_ID,
                            data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                            hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                            lat_entrada: _this.mylat,
                            long_entrada: _this.mylong
                        });
                        localStorage.setItem('temp_entradas', JSON.stringify(temp));
                    }
                });
                this.viewCtrl.dismiss({ done: true });
                break;
            }
            case 'saida': {
                var s_2 = setTimeout(function () {
                    loading_2.dismiss();
                    __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                        title: 'Conexão ruim!',
                        text: 'A resposta demorou muito a chegar!',
                        type: 'error',
                        showCloseButton: true,
                        confirmButtonText: 'OK'
                    });
                    _this._vibration.vibrate(250);
                    _this._vibration.vibrate(250);
                }, 30000);
                var loading_2 = this.loadingCtrl.create({
                    content: 'Enviando...'
                });
                loading_2.present();
                this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/insertSaida.php', {
                    pro_id: this.navParams.get('content').PRO_ID,
                    rede_id: this.navParams.get('content').RED_ID,
                    rfil_id: this.navParams.get('content').RFIL_ID,
                    data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                    hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                    lat_saida: this.mylat,
                    long_saida: this.mylong
                }, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        clearTimeout(s_2);
                        loading_2.dismiss();
                        __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                            title: 'Saída registrada!',
                            text: 'Sua saída foi registrada com sucesso!',
                            type: 'success',
                            showCloseButton: true,
                            confirmButtonText: 'OK'
                        });
                        _this._vibration.vibrate(250);
                        _this._rota.enableRota({
                            pro_id: _this.navParams.get('content').PRO_ID,
                            rede_id: _this.navParams.get('content').RED_ID,
                            rfil_id: _this.navParams.get('content').RFIL_ID,
                            data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                            hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                            lat_saida: _this.mylat,
                            long_saida: _this.mylong
                        });
                        _this.viewCtrl.dismiss({ done: true });
                        _this.makeIt = false;
                    }
                }, function (err) {
                    clearTimeout(s_2);
                    loading_2.dismiss();
                    __WEBPACK_IMPORTED_MODULE_2_sweetalert2___default()({
                        title: 'Saída salva!',
                        text: 'Sua saída foi salva com sucesso!',
                        type: 'success',
                        showCloseButton: true,
                        confirmButtonText: 'OK'
                    });
                    if (!localStorage.getItem('temp_saidas')) {
                        var temp = [];
                        temp.push({
                            pro_id: _this.navParams.get('content').PRO_ID,
                            rede_id: _this.navParams.get('content').RED_ID,
                            rfil_id: _this.navParams.get('content').RFIL_ID,
                            data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                            hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                            lat_saida: _this.mylat,
                            long_saida: _this.mylong
                        });
                        localStorage.setItem('temp_saidas', JSON.stringify(temp));
                    }
                    else {
                        var temp = JSON.parse(localStorage.getItem('temp_saidas'));
                        temp.push({
                            pro_id: _this.navParams.get('content').PRO_ID,
                            rede_id: _this.navParams.get('content').RED_ID,
                            rfil_id: _this.navParams.get('content').RFIL_ID,
                            data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0],
                            hora: (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()),
                            lat_saida: _this.mylat,
                            long_saida: _this.mylong
                        });
                        localStorage.setItem('temp_saidas', JSON.stringify(temp));
                    }
                });
                this.viewCtrl.dismiss({ done: true });
                break;
            }
        }
    };
    EntradaSaidaJobPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-entrada-saida-job',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\entrada-saida-job\entrada-saida-job.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content mdl-color--white" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <span style="padding: 10px; display: table; margin: 15px auto; font-size: 32px; font-weight: bold;">Realizar {{this.navParams.get(\'kind\') == \'entrada\' ? \'Check-In\': \'Check-Out\'}}</span>\n\n      <span style="text-align: center; display: table; margin: 5px auto; font-size: 12px; width: 95%" > Olá <b>{{this.navParams.get(\'content\').PRO_Nome}}</b> realize o {{this.navParams.get(\'kind\') == \'entrada\' ? \'Check-In\': \'Check-Out\'}} na loja para garantirmos ainda mais a efetividade dos trabalhos!</span>\n\n      <div >\n\n        <div style="display: table; margin: auto; text-align: center; width: 85%;" >\n\n          <span style="display: table; margin: auto;text-align: center; font-size: 12px; font-weight: bold;"> Quais informações são adquiridas: </span>\n\n          <span style="display: table; margin: auto;text-align: center; font-size: 10px"> Localização GPS, Horario, Loja, Email </span>\n\n          <span style="display: table; margin: auto;text-align: center; font-size: 12px; font-weight: bold;"> É realmente necessario? </span>\n\n          <span style="display: table; margin: auto;text-align: center; font-size: 10px"> Essas informações são de extrema importância, elas garantem sua segurança e nossa funcionalidade, sempre adquirindo um feedback muito transparente do que realmente ocorre em nosso sistema. </span>\n\n        </div>\n\n        \n\n        <button [disabled]="this.makeIt" class=" mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab mdl-color--red-500" (click)="checkIn(this.navParams.get(\'kind\'))" style="display: table; margin: 20px auto; width: 120px; height: 120px;">\n\n          <i class="material-icons mdl-color-text--white">add_location</i>\n\n        </button>\n\n        <span style="display: table; margin: auto; font-size: 10px; position:relative; bottom: 15px; padding: 5px; width: 85px; border-radius: 4px; background: rgba(0,0,0,0.8); color: #f5f7fa">Relizar {{this.navParams.get(\'kind\') == \'entrada\' ? \'Check-In\': \'Check-Out\'}}</span>\n\n      </div>\n\n    </div>\n\n  </main>\n\n  <div style="z-index: 5; position: relative; background: #8c1515; width: 100vw;" [ngStyle]="{\'bottom\': (this.platform.is(\'ios\') ? \'35px\':\'0px\')}">\n\n    <div style="display: table; margin: auto;">\n\n      <button (click)="this.viewCtrl.dismiss({done: false})" class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-color--white">\n\n        <i class="material-icons">arrow_back</i>\n\n      </button>\n\n    </div>\n\n  </div>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\entrada-saida-job\entrada-saida-job.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__providers_rota_rota__["a" /* RotaProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_vibration__["a" /* Vibration */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_http__["a" /* HTTP */]])
    ], EntradaSaidaJobPage);
    return EntradaSaidaJobPage;
}());

//# sourceMappingURL=entrada-saida-job.js.map

/***/ }),

/***/ 622:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LojasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__new_job_modal_new_job_modal__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__entrada_saida_job_entrada_saida_job__ = __webpack_require__(613);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_connection_checker_connection_checker__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LojasPage = /** @class */ (function () {
    function LojasPage(platform, _alert, viewCtrl, app, navCtrl, navParams, _http, modalCtrl, _checker) {
        this.platform = platform;
        this._alert = _alert;
        this.viewCtrl = viewCtrl;
        this.app = app;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this.modalCtrl = modalCtrl;
        this._checker = _checker;
        this.menu = [
            { title: 'Inicio', icon: 'home', media: 'main' },
            { title: 'Lojas', icon: 'meeting_room', media: 'lojas' },
            { title: 'Tarefas', icon: 'ballot', media: 'home' },
            { title: 'Jobs realizados', icon: 'group_work', media: 'home' },
            { title: 'Sair', icon: 'exit_to_app', action: 'logout' }
        ];
        this.redes = [];
        this.totalJobs = 0;
        this.doneLoading = false;
    }
    LojasPage.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem('session')) {
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectRotina2_.php', {
                content: JSON.parse(localStorage.getItem('session'))[0].content,
                data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0]
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    _this.redes = response.data.result;
                }
            }, function () {
                var userData = JSON.parse(localStorage.getItem('session'))[0];
                userData.content.forEach(function (e, index) {
                    var o = {
                        RFIL_ID: e.RFIL_ID,
                        RFIL_Nome: e.RFIL_Nome,
                        RED_ID: e.RED_ID,
                        RED_Nome: e.RED_Nome,
                        RFIL_Bairro: e.RFIL_Bairro,
                        RFIL_CEP: e.RFIL_CEP,
                        RFIL_Cidade: e.RFIL_Cidade,
                        RFIL_Endereco: e.RFIL_Endereco,
                        RFIL_Estado: e.RFIL_Estado,
                        RFIL_Numero: e.RFIL_Numero,
                        RFIL_Telefone: e.RFIL_Telefone,
                        RFIL_Numero_Visitas: 0,
                        RFIL_Nova_Entrada: true,
                        RFIL_Nova_Saida: false,
                        RFIL_All_Done: false,
                        cliente: [],
                        visible: true,
                        PRO_Nome: JSON.parse(localStorage.getItem('session'))[0].content[0].PRO_Nome,
                        PRO_ID: e.Aux_Promotor_ID
                    };
                    userData.clientes.forEach(function (element) {
                        if (element.aux_rede_id == e.RED_ID && element.aux_rede_filial == e.RFIL_ID) {
                            var days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                            var dayName_1 = days[new Date().getDay()];
                            var elem = element.aux_semana.split(';');
                            elem.forEach(function (element2) {
                                if (element2 == dayName_1) {
                                    o.cliente.push(element);
                                    o.RFIL_Numero_Visitas++;
                                }
                            });
                        }
                    });
                    _this.redes.push(o);
                    if (index == (userData.content.length - 1)) {
                        _this.doneLoading = true;
                    }
                });
            });
        }
    };
    LojasPage.prototype.logOut = function () {
        if (localStorage.getItem('session')) {
            localStorage.removeItem('session');
            this.viewCtrl.dismiss();
            this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
        }
    };
    LojasPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    LojasPage.prototype.newJob = function (rede) {
        var _this = this;
        if (rede.RFIL_Nova_Entrada == true && rede.RFIL_Nova_Saida == false && rede.RFIL_Numero_Visitas > 0) {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__entrada_saida_job_entrada_saida_job__["a" /* EntradaSaidaJobPage */], { kind: 'entrada', content: rede });
            modal.present();
            modal.onDidDismiss(function (data) {
                if (data.done && data.done == true) {
                    _this.redes = [];
                    _this.doneLoading = false;
                    _this.ngOnInit();
                }
            });
        }
        else if (rede.RFIL_Nova_Saida == true && rede.RFIL_Nova_Entrada == false && rede.RFIL_Numero_Visitas < 1) {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__entrada_saida_job_entrada_saida_job__["a" /* EntradaSaidaJobPage */], { kind: 'saida', content: rede });
            modal.present();
            modal.onDidDismiss(function () {
                _this.redes = [];
                _this.doneLoading = false;
                _this.ngOnInit();
            });
        }
        else if (rede.RFIL_Numero_Visitas > 0 && rede.cliente.length > 0 && rede.RFIL_Nova_Entrada == false && rede.RFIL_Nova_Saida == false) {
            rede.menuRow = false;
            var modal_1 = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__new_job_modal_new_job_modal__["a" /* NewJobModalPage */], rede);
            setTimeout(function () {
                modal_1.present();
            }, 150);
            modal_1.onDidDismiss(function (data) {
                if (data.job) {
                    if (data.job == 'done') {
                        _this.doneLoading = false;
                        _this.redes = [];
                        _this.ngOnInit();
                    }
                }
            });
        }
        else {
            return;
        }
    };
    LojasPage.prototype.newJobOFF = function (rede) {
        rede.offline = true;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__new_job_modal_new_job_modal__["a" /* NewJobModalPage */], rede);
        setTimeout(function () {
            modal.present();
        }, 150);
    };
    LojasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-lojas',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\lojas\lojas.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;"></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;"\n\n      class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n      <i class="material-icons">arrow_back</i>\n\n    </button>\n\n    <img src="./assets/images/logoecco.png"\n\n      style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n\n\n  <div *ngIf="this._checker.getMStatus() == \'offline\'" style="text-align:center; width: 100%; background: #f44242">\n\n    <p style="font-size: 10px; margin: 2px; color: white">Sem internet</p>\n\n  </div>\n\n\n\n  <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" style="background: #f5f7fa">\n\n      <div *ngIf="redes.length < 1" style="display: table; margin: 2% auto;">\n\n        <div class="rotate"\n\n          style="border-radius: 200px; width: 40px; height: 40px; border: 2px solid #940000; border-style: dashed solid dashed solid;">\n\n        </div>\n\n      </div>\n\n\n\n      <div *ngIf="redes.length > 0">\n\n        <div *ngFor="let r of redes; let i = index">\n\n\n\n          <div id="loja-{{i}}" class="mdl-card mdl-shadow--2dp mdl-js-button mdl-js-ripple-effect"\n\n            style="border-radius: 4px; min-height: 120px; position: relative; display: table; margin: 10px auto;"\n\n            (click)="this._checker.getMStatus() == \'online\' ? newJob(r): newJobOFF(r)">\n\n            <div class="mdl-card__title" style="color: #8c1515; position: relative; min-height: 70px;">\n\n              <h2 class="mdl-card__title-text descChar"\n\n                style="max-width: 200px; font-size: 14px; position: relative; bottom: 7px">{{r.RED_Nome}} -\n\n                {{r.RFIL_Nome}}</h2>\n\n\n\n              <div style="float: right; position: absolute; right: 10px" *ngIf="this._checker.getMStatus() == \'online\'">\n\n                <span\n\n                  *ngIf="(r.cliente.length < 1 || r.RFIL_Numero_Visitas <= 0) && (r.RFIL_Nova_Entrada == false && r.RFIL_Nova_Saida == false) && r.RFIL_All_Done == true"\n\n                  style="float: right; margin: 10px; padding: 10px; background-color: rgba(122, 23, 23, .7); border-radius: 150px; color: #f5f7fa; font-weight: bold; font-size: 25px"\n\n                  class="material-icons">done</span>\n\n                <span\n\n                  *ngIf="(r.cliente.length > 0 || r.RFIL_Numero_Visitas < 0) && (r.RFIL_Nova_Entrada == true && r.RFIL_Nova_Saida == false) || (r.RFIL_Nova_Entrada == false && r.RFIL_Nova_Saida == true) "\n\n                  style="float: right; margin: 2px; padding: 10px; background-color: rgba(122, 23, 23, .7); border-radius: 150px; color: #f5f7fa; font-weight: bold; font-size: 17px;"\n\n                  class="material-icons">transfer_within_a_station</span>\n\n                <span *ngIf="(r.cliente.length > 0 && r.RFIL_Numero_Visitas > 0)"\n\n                  style="float: right; margin: 0px; padding: 10px; background-color: rgba(122, 23, 23, .7); border-radius: 150px; color: #f5f7fa; font-weight: bold; font-size: 14px;">{{r.RFIL_Numero_Visitas}}</span>\n\n              </div>\n\n            </div>\n\n          </div>\n\n\n\n\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\lojas\lojas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_connection_checker_connection_checker__["a" /* ConnectionCheckerProvider */]])
    ], LojasPage);
    return LojasPage;
}());

//# sourceMappingURL=lojas.js.map

/***/ })

});
//# sourceMappingURL=4.js.map
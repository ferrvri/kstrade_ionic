webpackJsonp([8],{

/***/ 600:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MarketSharePageModule", function() { return MarketSharePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__market_share__ = __webpack_require__(623);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MarketSharePageModule = /** @class */ (function () {
    function MarketSharePageModule() {
    }
    MarketSharePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__market_share__["a" /* MarketSharePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__market_share__["a" /* MarketSharePage */]),
            ],
        })
    ], MarketSharePageModule);
    return MarketSharePageModule;
}());

//# sourceMappingURL=market-share.module.js.map

/***/ }),

/***/ 623:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MarketSharePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__market_share_chart_market_share_chart__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MarketSharePage = /** @class */ (function () {
    function MarketSharePage(platform, _modal, _alert, _datePicker, _http, navCtrl, navParams) {
        this.platform = platform;
        this._modal = _modal;
        this._alert = _alert;
        this._datePicker = _datePicker;
        this._http = _http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.produtos = [];
        this.clientes = [];
        this.redes = [];
        this.filiais = [];
        this.concorrentes = [];
        this.data_inicio = {
            value: '2019-05-01',
            // value: '',
            showValue: 'Data inicial',
            // selected: false
            selected: true
        };
        this.data_fim = {
            value: '2019-06-01',
            // value: '',
            showValue: 'Data final',
            // selected: false
            selected: true
        };
        this.produto_select = 0;
        this.cliente_select = 0;
        this.rede_select = 0;
        this.filial_select = 0;
        this.concorrente_select = 0;
    }
    MarketSharePage.prototype.ionViewDidLoad = function () {
    };
    MarketSharePage.prototype.ngOnInit = function () {
        var _this = this;
        if (this.navParams.get('forPromotor') && this.navParams.get('forPromotor') == true) {
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectClientePromotor.php', {
                pro_id: JSON.parse(localStorage.getItem('session'))[0].PRO_ID
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    response.data.result.forEach(function (element) {
                        _this.clientes.push(element);
                    });
                    JSON.parse(localStorage.getItem('session')).forEach(function (element) {
                        element.content.forEach(function (element2) {
                            _this.redes.push({
                                RED_ID: element2.RED_ID,
                                RED_Nome: element2.RED_Nome
                            });
                        });
                    });
                }
            });
        }
        else if (this.navParams.get('forVendedor') && this.navParams.get('forVendedor') == true) {
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/selectClientes.php', {
                unique: true,
                id: JSON.parse(localStorage.getItem('session')).redes[0].aux_cliente_id
            }, {}).then(function (response) {
                if (response.data.status == '0x104') {
                    _this.redes = JSON.parse(localStorage.getItem('session')).redes;
                    response.data.result.forEach(function (element) {
                        _this.clientes.push(element);
                    });
                }
            });
        }
        else {
            this.clientes = JSON.parse(localStorage.getItem('session')).clientes;
            this.redes = JSON.parse(localStorage.getItem('session')).redes;
        }
    };
    MarketSharePage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    MarketSharePage.prototype.showDate = function (obj) {
        this._datePicker.show({
            allowFutureDates: true,
            date: new Date(),
            mode: 'date',
            androidTheme: this._datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
        }).then(function (date) {
            obj.selected = true;
            obj.value = date;
            obj.showValue = date.toISOString().split('T')[0].split('-')[2] + '/' +
                date.toISOString().split('T')[0].split('-')[1] + '/' +
                date.toISOString().split('T')[0].split('-')[0];
        }, function (err) {
            obj.selected = false;
        });
    };
    MarketSharePage.prototype.selectCliente = function () {
        var _this = this;
        var a = this._alert.create();
        this.clientes.forEach(function (el, index) {
            if (_this.navParams.get('forVendedor') == true) {
                a.addInput({ type: 'radio', label: el.Cli_Nome, value: el.Cli_ID, id: 'cli-' + index });
            }
            else {
                a.addInput({ type: 'radio', label: el.Cli_Nome, value: el.aux_cliente_id, id: 'cli-' + index });
            }
        });
        a.addButton('Cancel');
        a.addButton({
            text: 'OK',
            handler: function (data) {
                var o = { cliente: 0 };
                if (_this.navParams.get('forVendedor') == true) {
                    _this.cliente_select = _this.clientes.filter(function (e) {
                        return e.Cli_ID == data;
                    })[0];
                    o.cliente = _this.cliente_select.Cli_ID;
                }
                else {
                    _this.cliente_select = _this.clientes.filter(function (e) {
                        return e.aux_cliente_id == data;
                    })[0];
                    o.cliente = _this.cliente_select.aux_cliente_id;
                }
                _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/selectProdutos.php', o, {}).then(function (response) {
                    response.data = JSON.parse(response.data);
                    if (response.data.status == '0x104') {
                        _this.produtos = [];
                        response.data.result.forEach(function (element) {
                            _this.produtos.push(element);
                        });
                    }
                    else {
                        var a_1 = _this._alert.create({
                            title: 'Sem produtos!',
                            subTitle: 'Este cliente não possui produtos cadastrados.'
                        });
                        a_1.present();
                    }
                });
            }
        });
        a.present();
    };
    MarketSharePage.prototype.selectConcorrentes = function () {
        var _this = this;
        var a = this._alert.create();
        var reg = new RegExp((".*" + 'concorrente'.split("").join(".*") + ".*"), "i");
        this.produtos.forEach(function (el, index) {
            if (reg.test(el.pro_nome)) {
                a.addInput({ type: 'radio', label: el.pro_nome, value: el.pro_id, id: 'pro-' + index });
            }
            else {
                a.addInput({ type: 'radio', label: el.pro_nome, value: el.pro_id, id: 'pro-' + index });
            }
        });
        a.addButton('Cancel');
        a.addButton({
            text: 'OK',
            handler: function (data) {
                _this.produtos.forEach(function (el) {
                    if (el.pro_id === data) {
                        _this.concorrente_select = el;
                    }
                });
            }
        });
        a.present();
    };
    MarketSharePage.prototype.selectProdutos = function () {
        var _this = this;
        var a = this._alert.create();
        this.produtos.forEach(function (el, index) {
            a.addInput({ type: 'radio', label: el.pro_nome, value: el.pro_id, id: 'pro-' + index });
        });
        a.addButton('Cancel');
        a.addButton({
            text: 'OK',
            handler: function (data) {
                _this.produtos.forEach(function (el) {
                    if (el.pro_id === data) {
                        _this.produto_select = el;
                    }
                });
            }
        });
        a.present();
    };
    MarketSharePage.prototype.selectRede = function () {
        var _this = this;
        var a = this._alert.create();
        this.redes.forEach(function (el, index) {
            a.addInput({ type: 'radio', label: el.RED_Nome, value: el.RED_ID, id: 'RED-' + index });
        });
        a.addButton('Cancel');
        a.addButton({
            text: 'OK',
            handler: function (data) {
                _this.redes.forEach(function (el) {
                    if (el.RED_ID === data) {
                        _this.rede_select = el;
                        _this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/cliente/selectFilial.php', {
                            id: data
                        }, {}).then(function (response2) {
                            response2.data = JSON.parse(response2.data);
                            if (response2.data.status == '0x104') {
                                _this.filiais = [];
                                response2.data.result.forEach(function (element) {
                                    _this.filiais.push(element);
                                });
                            }
                        });
                    }
                });
            }
        });
        a.present();
    };
    MarketSharePage.prototype.selectFilial = function () {
        var _this = this;
        this.filial_select = [];
        var a = this._alert.create();
        this.filiais.forEach(function (el, index) {
            a.addInput({ type: 'radio', label: el.RFIL_Nome, value: el.RFIL_ID, id: 'RFIL-' + index });
        });
        a.addButton('Cancel');
        a.addButton({
            text: 'OK',
            handler: function (data) {
                _this.filiais.forEach(function (el) {
                    if (el.RFIL_ID === data) {
                        _this.filial_select = el;
                    }
                });
            }
        });
        a.present();
    };
    MarketSharePage.prototype.next = function () {
        //console.log(this.filial_select);
        var m = this._modal.create(__WEBPACK_IMPORTED_MODULE_3__market_share_chart_market_share_chart__["a" /* MarketShareChartPage */], {
            red_id: this.rede_select.RED_ID,
            red_nome: this.rede_select.RED_Nome,
            rfil_id: this.filial_select.RFIL_ID,
            rfil_nome: this.filial_select.RFIL_Nome,
            cli_id: this.navParams.get('forVendedor') == true ? this.cliente_select.Cli_ID : this.cliente_select.aux_cliente_id,
            cli_nome: this.cliente_select.Cli_Nome,
            pro_id: this.produto_select.pro_id,
            pro_nome: this.produto_select.pro_nome
        });
        m.present();
    };
    MarketSharePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-market-share',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\market-share\market-share.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n  <header class="mdl-layout__header" style="background: #8c1515; position: relative; box-shadow: none;">\n\n    <button (click)="this.goBack()" style="position: absolute; left: 10px; top: 12px;" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">\n\n      <i class="material-icons">arrow_back</i>\n\n    </button>\n\n    <img src="./assets/images/logoecco.png" style="width: 64px; display: table; margin: 5px auto;">\n\n    <div class="mdl-layout-spacer"></div>\n\n  </header>\n\n  <main class="mdl-layout__content" style="height: calc(100vh - 56px); background: #f5f7fa">\n\n    <div class="page-content" >\n\n      <div class="mdl-card mdl-shadow--2dp" style="display: table; margin: auto; width: 95%">\n\n        <div class="mdl-card__title">\n\n          <span style="display: table; margin: auto; color: #8c1515;">Visualizar Market Share</span>\n\n        </div>\n\n        <div class="mdl-card__media mdl-color--white" style="max-width: 95%;">\n\n        </div>\n\n        <span style="display: table; margin: 4px auto">Clientes <b style="color: #d50000">*</b></span>\n\n        <ion-item style="max-width: 95%;">\n\n          <button (click)="selectCliente()" class="mdl-button mdl-js-button mdl-js-ripple-effect" style="width: 100%; margin-left: 4px">\n\n            <span>{{cliente_select == 0 ? \'Selecione um cliente\':cliente_select.Cli_Nome }}</span>\n\n            <i class="material-icons" style="position: relative; left: 10px;">keyboard_arrow_down</i>\n\n          </button>\n\n        </ion-item>\n\n\n\n        <!-- <div *ngIf="cliente_select != 0">\n\n          <span style="display: table; margin: 4px auto">Produtos concorrentes <b style="color: #d50000">*</b></span>\n\n          <ion-item style="max-width: 95%;">\n\n            <button (click)="selectConcorrentes()" class="mdl-button mdl-js-button mdl-js-ripple-effect" style="width: 100%; margin-left: 4px">\n\n              <span>{{concorrente_select == 0 ? \'Selecione um concorrente\':concorrente_select.pro_nome }}</span>\n\n              <i class="material-icons" style="position: relative; left: 10px;">keyboard_arrow_down</i>\n\n            </button>\n\n          </ion-item>\n\n        </div> -->\n\n\n\n        <div *ngIf="cliente_select != 0 && this.produtos.length > 0">\n\n          <span style="display: table; margin: 4px auto">Rede e Filial <b style="color: #d50000">*</b></span>\n\n          <ion-item style="max-width: 95%;">\n\n            <button (click)="selectRede()" class="mdl-button mdl-js-button mdl-js-ripple-effect" style="width: 100%; margin-left: 4px">\n\n              <span>{{rede_select == 0 ? \'Selecione uma rede\':rede_select.RED_Nome }}</span>\n\n              <i class="material-icons" style="position: relative; left: 10px;">keyboard_arrow_down</i>\n\n            </button>\n\n          </ion-item>\n\n          <ion-item *ngIf="rede_select != 0" style="max-width: 95%;">\n\n            <button (click)="selectFilial()" class="mdl-button mdl-js-button mdl-js-ripple-effect" style="width: 100%; margin-left: 4px">\n\n              <span>{{filial_select == 0 ? \'Selecione uma filial\':filial_select.RFIL_Nome }}</span>\n\n              <i class="material-icons" style="position: relative; left: 10px;">keyboard_arrow_down</i>\n\n            </button>\n\n          </ion-item>\n\n        </div>\n\n\n\n        <div *ngIf="cliente_select != 0 && rede_select != 0 && filial_select != 0">\n\n          <span style="display: table; margin: 4px auto">Produtos <b style="color: #d50000">*</b></span>\n\n          <ion-item style="max-width: 95%;">\n\n            <button (click)="selectProdutos()" class="mdl-button mdl-js-button mdl-js-ripple-effect" style="width: 100%; margin-left: 4px">\n\n              <span>{{produto_select == 0 ? \'Selecione um produto\':produto_select.pro_nome }}</span>\n\n              <i class="material-icons" style="position: relative; left: 10px;">keyboard_arrow_down</i>\n\n            </button>\n\n          </ion-item>\n\n        </div>\n\n\n\n        <!-- <div *ngIf="rede_select && filial_select != 0">\n\n          <span style="display: table; margin: 4px auto">Filtrar</span>\n\n          <ion-item style="max-width: 95%; width: 95%; display: table; margin: auto">\n\n            <button class="mdl-button mdl-js-button mdl-js-ripple-effect" (click)="showDate(data_inicio)" style="text-transform: none; width: 35%; display: inline-table">\n\n              <span>{{data_inicio.showValue}}</span>\n\n            </button>\n\n            <div style="text-align: center; display: inline-table; width: 25%; height: 30px; background: #8c1515; color: #f5f7fa; border-radius: 2px;  display: inline-table" >\n\n              <span>Data</span>\n\n            </div>\n\n            <button class="mdl-button mdl-js-button mdl-js-ripple-effect" (click)="showDate(data_fim)" style="text-transform: none; width: 35%;  display: inline-table">\n\n              <span>{{data_fim.showValue}}</span>\n\n            </button>\n\n          </ion-item>\n\n        </div> -->\n\n        <div class="mdl-card__actions">\n\n          <button (click)="next()" class="mdl-button mdl-js-button mdl-color--red-500  mdl-color-text--white when_disabled" style="margin-top: 10px; text-transform: none; width: 100%;">\n\n            <span>Prosseguir</span>\n\n            <i class="material-icons">navigate_next</i>\n\n          </button>\n\n        </div>\n\n      </div>\n\n    </div>  \n\n  </main>\n\n</div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\market-share\market-share.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__["a" /* DatePicker */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], MarketSharePage);
    return MarketSharePage;
}());

//# sourceMappingURL=market-share.js.map

/***/ })

});
//# sourceMappingURL=8.js.map
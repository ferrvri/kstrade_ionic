webpackJsonp([12],{

/***/ 586:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmPageModule", function() { return ConfirmPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__confirm__ = __webpack_require__(619);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ConfirmPageModule = /** @class */ (function () {
    function ConfirmPageModule() {
    }
    ConfirmPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__confirm__["a" /* ConfirmPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__confirm__["a" /* ConfirmPage */]),
            ],
        })
    ], ConfirmPageModule);
    return ConfirmPageModule;
}());

//# sourceMappingURL=confirm.module.js.map

/***/ }),

/***/ 619:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfirmPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loading_loading__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ConfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ConfirmPage = /** @class */ (function () {
    function ConfirmPage(platform, navCtrl, navParams, _http, _alert) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._http = _http;
        this._alert = _alert;
        this.codigo = '';
    }
    ConfirmPage.prototype.ngOnInit = function () {
    };
    ConfirmPage.prototype.confirm = function () {
        var _this = this;
        if (this.codigo.length < 1 || this.codigo === undefined) {
            var a = this._alert.create({
                title: 'Preencha o campo!',
                subTitle: 'Preencha todos os campos para confimar',
                buttons: ['Ok']
            });
            a.present();
        }
        else {
            this._http.post('https://kstrade.com.br/sistema/assets/kstrade_php/app/confirm.php', {
                pro_nome: JSON.parse(localStorage.getItem('session'))[0].content[0].PRO_Nome
            }, {}).then(function (response) {
                response.data = JSON.parse(response.data);
                if (response.data.status == '0x104') {
                    localStorage.removeItem('wantconfirm');
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__loading_loading__["a" /* LoadingPage */], { page: 'main' });
                }
            });
        }
    };
    ConfirmPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-confirm',template:/*ion-inline-start:"G:\Projects\t\kstrade_ionic\src\pages\confirm\confirm.html"*/'<div *ngIf="this.platform.is(\'ios\')" style="height: 35px; background: #8c1515; width: 100%;" ></div>\n\n\n\n<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" >\n\n    <header class="mdl-layout__header" style="background: #8c1515">\n\n      <span class="mdl-layout-title char" style="padding: 25px">Confirme o seu acesso!</span>\n\n      <div class="mdl-layout-spacer"></div>\n\n    </header>\n\n    <main class="mdl-layout__content" style="height: calc(100vh - 70px);">\n\n      <div class="page-content" style="background: #f5f7fa">\n\n        <div class="mdl-card mdl-shadow--2dp scaleIn" style="width: 80%; display: table; margin: 15% auto; margin-bottom: 20px;">\n\n          <div class="mdl-card__title">\n\n            <span class="descChar">Digite o código</span>\n\n          </div>\n\n          <div class="mdl-card__media mdl-color--white">\n\n            <div style="width: 100%; margin-top: 20px;">\n\n              <input class="simpleInput" type="text" [(ngModel)]="this.codigo"/>\n\n            </div>\n\n            <p class="descChar" style="position: relative; top: 20px; padding: 8px; font-size: 12px">\n\n              Digite o código fornecido no seu e-mail para continuar. Caso o e-mail não chege, contate-nos\n\n            </p>\n\n          </div>\n\n          <div class="mdl-card__actions">\n\n            <button (click)="confirm()" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-color-text--white" style="background: #8c1515; z-index: 999; margin: 10px; float: right">\n\n              <span>Cadastrar!</span>\n\n            </button>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </main>\n\n  </div>'/*ion-inline-end:"G:\Projects\t\kstrade_ionic\src\pages\confirm\confirm.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ConfirmPage);
    return ConfirmPage;
}());

//# sourceMappingURL=confirm.js.map

/***/ })

});
//# sourceMappingURL=12.js.map